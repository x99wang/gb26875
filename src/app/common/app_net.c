/*
 * app_net.c
 *
 *  Created on: 2020年12月28日
 *      Author: WangXi
 */


#include <stdint.h>
#include <sys/socket.h>
#include "project_config.h"
#include "app_flash.h"
#include <shell.h>

#define STR_FMT_NET_INFO         "type=%d,ip_type=%d,ip_addr=%s,submask=%s,gateway=%s,dns1=%s,dns2=%s"

bool_t Net_InfoSave(const char *net_card_name, struct NetInfoSt *net_info_in, bool_t override)
{
    char str_net_file_name[64] = {0};
    char str_net_info[256] = {0};//最大长度137
    s32 net_info_len = 0;
    if(net_card_name == NULL || net_info_in == NULL)
        return false;
    snprintf(str_net_file_name, 64, CFG_NET_INFO_FILE_NAME, net_card_name);
    // 已存在配置文件且非覆盖
    if(!override && File_GetNameValueFs(str_net_file_name, str_net_info, 256) > 0)
        return false;
    net_info_len = snprintf(str_net_info, 256, STR_FMT_NET_INFO, net_info_in->type, net_info_in->ip_type,\
            net_info_in->ip_addr, net_info_in->subnet_mask, net_info_in->gateway,\
            net_info_in->dns1, net_info_in->dns2);
    if(net_info_len <= 0)
        return false;
    return File_SetNameValueFs(str_net_file_name, str_net_info, net_info_len);
}

bool_t Net_InfoLoad(const char *net_card_name, struct NetInfoSt *net_info_out)
{
    char str_net_file_name[64] = {0};
    char str_net_info[256] = {0};//最大长度137
    s32 net_info_len = 0;
    s32 ret = 0;
    if(net_card_name == NULL || net_info_out == NULL)
        return false;
    snprintf(str_net_file_name, 64, CFG_NET_INFO_FILE_NAME, net_card_name);
    net_info_len = File_GetNameValueFs(str_net_file_name, str_net_info, 256);
    if(net_info_len <= 0)
        return false;
    ret = sscanf(str_net_info, STR_FMT_NET_INFO, net_info_out->type, net_info_out->ip_type,\
            net_info_out->ip_addr, net_info_out->subnet_mask, net_info_out->gateway,\
            net_info_out->dns1, net_info_out->dns2);
    return ret == 7;
}

bool_t Net_InfoErase(const char *net_card_name)
{
    char str_net_file_name[64] = {0};
    if(net_card_name == NULL)
        return false;
    snprintf(str_net_file_name, 64, CFG_NET_INFO_FILE_NAME, net_card_name);
    return File_RmNameValueFs(str_net_file_name);
}


bool_t Net_InfoInit()
{
    u32  signature[3];

    struct NetInfoSt net_info;
    bool_t has_net_cfg = false;
    RouterRemoveByNetDev(CFG_SELECT_NETCARD); // 先删除路由
    has_net_cfg = Net_InfoLoad(CFG_SELECT_NETCARD, &net_info);
    if(has_net_cfg)
    {
        if(net_info.type == STATIC_IP)
        {
            u32 hop,subnet,ip,submask,dns,broad;
            tagHostAddrV4  ipv4addr;
            tagRouterPara para;

            //we use the static ip we like
        //  memset((void *)&ipv4addr,0,sizeof(ipv4addr));
            memset(&para,0,sizeof(para));
            ip      = inet_addr(net_info.ip_addr);
            submask = INADDR_ANY;
            hop  = inet_addr(net_info.gateway);
            dns     = inet_addr(net_info.dns1);
            broad   = inet_addr("192.168.0.255");
        //  hop = INADDR_ANY;
            subnet = ip & submask;
            para.ver = EN_IPV_4;
            para.host = &ip;
            para.mask = &submask;
            para.broad = &broad;
            para.hop = &hop;
            para.net = &subnet;
            para.prior = CN_ROUT_PRIOR_UNI;
            para.ifname = CFG_SELECT_NETCARD;

            RouterCreate(&para);

            memset(&para,0,sizeof(para));
            ip      = inet_addr(net_info.ip_addr);
            submask = inet_addr(net_info.subnet_mask);
        //  hop  = inet_addr(CFG_MY_GATWAY);
            dns     = inet_addr(net_info.dns1);
            broad   = inet_addr("192.168.0.255");
            DNS_Set(EN_IPV_4,&dns, 0);

            hop = INADDR_ANY;
            subnet = ip & submask;
            para.ver = EN_IPV_4;
            para.host = &ip;
            para.mask = &submask;
            para.broad = &broad;
            para.hop = &hop;
            para.net = &subnet;
            para.prior = CN_ROUT_PRIOR_UNI;
            para.ifname = CFG_SELECT_NETCARD;

            if(RouterCreate(&para))
            {
                printk("%s:CreateRout:%s:%s success\r\n",__FUNCTION__,CFG_SELECT_NETCARD,inet_ntoa(ip));
            }
            else
            {
                printk("%s:CreateRout:%s:%s failed\r\n",__FUNCTION__,CFG_SELECT_NETCARD,inet_ntoa(ip));
            }
        }
        else if(net_info.type == AUTO_IP)
        {
            if(DHCP_AddClientTask(CFG_SELECT_NETCARD,0))
            {
               printk("%s:Add %s success\r\n",__FUNCTION__,CFG_SELECT_NETCARD);
            }
            else
            {
                printk("%s:Add %s failed\r\n",__FUNCTION__,CFG_SELECT_NETCARD);
            }
        }
    }
    else if(CFG_STATIC_IP == 1)
    {

        u32 hop,subnet,ip,submask,dns,broad;
        tagHostAddrV4  ipv4addr;
        tagRouterPara para;

        //we use the static ip we like
    //  memset((void *)&ipv4addr,0,sizeof(ipv4addr));
        memset(&para,0,sizeof(para));
        ip      = inet_addr(CFG_MY_IPV4);
        submask = INADDR_ANY;
        hop  = inet_addr(CFG_MY_GATWAY);
        dns     = inet_addr(CFG_MY_DNS);
        broad   = inet_addr("192.168.0.255");
    //  hop = INADDR_ANY;
        subnet = ip & submask;
        para.ver = EN_IPV_4;
        para.host = &ip;
        para.mask = &submask;
        para.broad = &broad;
        para.hop = &hop;
        para.net = &subnet;
        para.prior = CN_ROUT_PRIOR_UNI;
        para.ifname = CFG_SELECT_NETCARD;

        RouterCreate(&para);

        memset(&para,0,sizeof(para));

        ip      = inet_addr(CFG_MY_IPV4);
        submask = inet_addr(CFG_MY_SUBMASK);
    //  hop  = inet_addr(CFG_MY_GATWAY);
        dns     = inet_addr(CFG_MY_DNS);
        broad   = inet_addr("192.168.0.255");

        DNS_Set(EN_IPV_4,&dns, 0);

        hop = INADDR_ANY;
        subnet = ip & submask;
        para.ver = EN_IPV_4;
        para.host = &ip;
        para.mask = &submask;
        para.broad = &broad;
        para.hop = &hop;
        para.net = &subnet;
        para.prior = CN_ROUT_PRIOR_UNI;
        para.ifname = CFG_SELECT_NETCARD;

        if(RouterCreate(&para))
        {
            printk("%s:CreateRout:%s:%s success\r\n",__FUNCTION__,CFG_SELECT_NETCARD,inet_ntoa(ip));
        }
        else
        {
            printk("%s:CreateRout:%s:%s failed\r\n",__FUNCTION__,CFG_SELECT_NETCARD,inet_ntoa(ip));
        }

        //下一个路由，用于生产测试用，利用CPU ID 随机生成主机地址，网络地址用 192.168.1
        //WE WILL ADD A ROUT DIFFERENT FOR EACH DEVICE USE THE CPU SIGNATURE
        //USE THE NET:192.168.1.xx
        u8 value8 = 0;
        GetCpuID(&signature[0],&signature[1],&signature[2]);
        value8 = +((u8)signature[0]>>0)+((u8)signature[0]>>8)+((u8)signature[0]>>16)+((u8)signature[0]>>24);
        if((value8==0)||(value8==1)||(value8==255))
        {
            value8=253;
        }
        u32 value32 = 0;
        memset((void *)&ipv4addr,0,sizeof(ipv4addr));
        value32 = inet_addr("192.168.1.0");
        value32 = ntohl(value32);
        value32 =(value32&0xffffff00) + value8;
        ip      = htonl(value32);
        submask = inet_addr("255.255.255.0");
        hop  = inet_addr("192.168.1.1");
        dns     = inet_addr("192.168.1.1");
        broad   = inet_addr("192.168.1.255");

        subnet = ip & submask;
        para.ver = EN_IPV_4;
        para.host = &ip;
        para.mask = &submask;
        para.broad = &broad;
        para.hop = &hop;
        para.net = &subnet;
        para.prior = CN_ROUT_PRIOR_UNI;
        para.ifname = CFG_SELECT_NETCARD;

        if(0/*RouterCreate(&para)*/)
        {
            printk("%s:CreateRout:%s:%s success\r\n",__FUNCTION__,CFG_SELECT_NETCARD,inet_ntoa(ipv4addr.ip));
        }
        else
        {
            printk("%s:CreateRout:%s:%s failed\r\n",__FUNCTION__,CFG_SELECT_NETCARD,inet_ntoa(ipv4addr.ip));
        }

    }
    else
    {
        if(DHCP_AddClientTask(CFG_SELECT_NETCARD,0))
        {
           printk("%s:Add %s success\r\n",__FUNCTION__,CFG_SELECT_NETCARD);
        }
        else
        {
            printk("%s:Add %s failed\r\n",__FUNCTION__,CFG_SELECT_NETCARD);
        }
    }
    return true;
}


bool_t Net_InfoCfg(enum NetIpType type, char *ipv4, char *gatway, char *submask, char *dns)
{
    struct NetInfoSt net_info;
    net_info.ip_type = type;
    if(type == STATIC_IP)
    {
        if(ipv4 != NULL && gatway != NULL && submask != NULL && dns != NULL)
        {
            strncpy(net_info.ip_addr, ipv4, CFG_NET_IP_LEN);
            strncpy(net_info.gateway, gatway, CFG_NET_IP_LEN);
            strncpy(net_info.subnet_mask, submask, CFG_NET_IP_LEN);
            strncpy(net_info.dns1, dns, CFG_NET_IP_LEN);
            Net_InfoSave(CFG_SELECT_NETCARD, &net_info, true);
        }
    }
    return Net_InfoInit();
}

bool_t __Shell_NetInfoCfg(char *param)
{
    char type[10] = {0};
    char ipv4[CFG_NET_IP_LEN];
    char gatway[CFG_NET_IP_LEN];
    char submask[CFG_NET_IP_LEN];
    char dns[CFG_NET_IP_LEN];
    enum NetIpType ip_type;
    sscanf(param, "%s %s %s %s %s", type, ipv4, gatway, submask, dns);
    if(atoi(type) == 1) ip_type = STATIC_IP;
    else ip_type = AUTO_IP;
    return Net_InfoCfg(ip_type, ipv4, gatway, submask, dns);
}

ADD_TO_ROUTINE_SHELL(net_config, __Shell_NetInfoCfg, "配置网络参数\r\n联网类型[0:动态IP 1:静态IP]\r\n[联网类型 ipv4地址 网关 子网掩码 DNS服务器]");
