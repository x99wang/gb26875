/*
 * net_time.c
 *
 *  Created on: 2020骞�11鏈�18鏃�
 *      Author: WangXi
 */
#include "mongoose.h"
extern void *psram_malloc (unsigned int size);
#ifndef free
#define free(x) M_Free(x)
#endif

#define hex_dump

struct tm *__gmtime(const time_t *time, int time_zone);


static unsigned int gRtcTickSecOffset = 0;
static unsigned int gRtcTickSecTimestamp = 0;

int ClearTimeStamp()
{
    gRtcTickSecOffset = 0;
    gRtcTickSecTimestamp = 0;
    return 0;
}

char *arr_ntp_server[] = {
        "ntp.ntsc.ac.cn",
        "cn.pool.ntp.org",
        "us.pool.ntp.org",
};
int get_ntp_tiemstamp(u32 *ptimestamp, char *ntp_domain, int timeout_ms);

int ntp_timestamp(u32 *ptimestamp, int timeout_ms)
{
    unsigned int timestamp = 0;
    static int index = 0;
    index++;
    index = index % (sizeof(arr_ntp_server)/sizeof(arr_ntp_server[0]));
    int status = get_ntp_tiemstamp(&timestamp, arr_ntp_server[index], 5000);
    printf("info: get_ntp_tiemstamp, status:%d, index:%d, ntp_server:%s, timestamp=%d!\r\n", status, index, arr_ntp_server[index], timestamp);
    if (status >= 0) {
        *ptimestamp = timestamp;
    }
    return status;
}

int  GetTimeStamp(unsigned int *timestamp_out, int timeout_ms)
{
    unsigned int cur_tick_sec  = 0;
    unsigned int timestamp = 0;
    int status = 0;
    time_t t = 0;
    if (gRtcTickSecOffset == 0 && gRtcTickSecTimestamp == 0) {
        //status = NetGetTimeStamp(&timestamp, timeout_ms);
        status = ntp_timestamp(&timestamp, timeout_ms);
        if (status >= 0 && timestamp) {
            *timestamp_out = timestamp;
            t = timestamp;
            Time_SetDateTime(__gmtime(&t, 8));
            gRtcTickSecOffset = DJY_GetSysTime()/1000000;
            gRtcTickSecTimestamp = timestamp;
        }
        printf("gRtcTickSecOffset = %d, gRtcTickSecTimestamp = %d!\r\n", gRtcTickSecOffset, gRtcTickSecTimestamp);
    }
    else {
        cur_tick_sec = DJY_GetSysTime()/1000000;
        *timestamp_out  = gRtcTickSecTimestamp + cur_tick_sec - gRtcTickSecOffset;
        //printf("Tick Offset: %d, TimeStamp: %d, RTC TIME: %d!\r\n", gRtcTickSecOffset, gRtcTickSecTimestamp, *timestamp_out);
    }
    return 0;
}

int GetTimeHourMinute(int *hour, int *min)
{
    unsigned int timestamp_out = 0;
    unsigned int cur_tick_sec  = 0;
    struct tm *ptm = 0;
    cur_tick_sec = DJY_GetSysTime()/1000000;
    timestamp_out  = gRtcTickSecTimestamp + cur_tick_sec - gRtcTickSecOffset;
    time_t t = timestamp_out;
    ptm = __gmtime(&t, 8);
    *hour = ptm->tm_hour;
    *min = ptm->tm_min;
    //printf("---->hour=%d, min=%d!\r\n", *hour, *min);
    //strftime(GMT, sizeof(GMT), "%a, %d %b %Y %H:%M:%S GMT", oss_gmtime(&t, 0));
    //printf("GetTimeHourMinute: %d, TimeStamp: %d, RTC TIME: %d!\r\n", gRtcTickSecOffset, gRtcTickSecTimestamp, timestamp_out);
    return 0;
}

struct tm *__gmtime(const time_t *time, int time_zone)
{
    static struct  tm result;
    s64 temp_time;
    if(time == NULL)
    {
        extern s64 __Rtc_Time(s64 *rtctime);
        temp_time = __Rtc_Time(NULL);
        temp_time += ((s64)8*3600);
    }
    else
    {
        temp_time = *time + ((s64)time_zone*3600);
    }
    return Time_LocalTime_r(&temp_time, &result);
}
char *mg_strftime_patch(char *base)
{
    //char base[100] = "Sun, 29 Sep 2019 7:5:33 GMT";
    int hour, mini, sec;
    char zone[20] = { 0 };
    char buf[50] = { 0 };
    char *p = strstr(base, ":");
    while (p > base && *p != ' ') {
        p--;
    }
    if (*p == ' ') p++;
    sscanf(p, "%d:%d:%d %s", &hour, &mini, &sec, zone);
    sprintf(buf, "%02d:%02d:%02d %s", hour, mini, sec, zone);
    strcpy(p, buf);
    printf("%s", base);
    return base;
}

char *GTM_TIME(unsigned int timestamp, char *buf, int len)
{
    char GMT[100] = {0};
    time_t t = timestamp;
    strftime(GMT, sizeof(GMT), "%a, %d %b %Y %H:%M:%S GMT", __gmtime(&t, 0));
    //mg_strftime_patch(GMT);
    int min = strlen(GMT)+1;
    min = min < len ? min : len;
    memcpy(buf, GMT, min);
    return buf;
}
//"%Y%m%d%H%M%S"
int FormatTime(char *outbuf, int len, char *fmt)
{
    int ret = 0;
    unsigned int timestamp = 0;
    if (outbuf==0) return -1;
    ret = GetTimeStamp(&timestamp, 5000);
    if (ret < 0) return -1;
    time_t t = timestamp;
    if (fmt==0) {
        strftime(outbuf, len, "%Y%m%d%H%M%S", __gmtime(&t, 8));
    }
    else {
        strftime(outbuf, len, fmt, __gmtime(&t, 8));
    }
    return 0;
}

