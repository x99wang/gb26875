/*
 * app_flash.c
 *
 *  Created on: 2020年11月20日
 *      Author: WangXi
 */

#include <stdint.h>
#include <stddef.h>
#include <string.h>
#include <project_config.h>
#include <dbug.h>
#include <netbsp.h>
#include <shell.h>
#include "app_flash.h"
#include "driver_info_to_fs.h"

char gSn_Str[15] = {0};

bool_t Sn_InfoSave(char *info ,u32 len)
{
    if((info == NULL)|| (len) > CFG_SN_LEN)
        return false;

    if(File_SetNameValueFs(CFG_SN_INFO_FILE_NAME, info, len))
    {
        strncpy(gSn_Str, info, sizeof(gSn_Str));
        return true;
    }
    else
        return false;
}

bool_t Sn_InfoLoad(char *buf ,u32 maxlen)
{
    s32 len;
    if((maxlen == 0) || (buf == NULL))
        return false;
    len = maxlen > FLASH_SECTOR_SIZE ? FLASH_SECTOR_SIZE :maxlen;
    File_GetNameValueFs(CFG_SN_INFO_FILE_NAME, buf, len);
    strncpy(gSn_Str, buf, sizeof(gSn_Str));
    if(strlen(buf) < len)
    {
        return true;
    }
    return false;
}

bool_t Sn_InfoErase( )
{
    if(File_RmNameValueFs(CFG_SN_INFO_FILE_NAME))
    {
        strncpy(gSn_Str, 0, sizeof(gSn_Str));
        return true;
    }
    else
        return false;
}

bool_t Sn_Init()
{
    static char SN_BUF[64];

    char *sn = "DJYOS-MAC-%x%x%x%x%x%x";

    u8 * macaddr = NetDevGetMac(NetDevGet("STM32F7_ETH"));
    if(false == Sn_InfoLoad(SN_BUF,sizeof(SN_BUF)))
    {
        //用%2x打印实际上只出来一位
        sprintf(SN_BUF,sn,  (macaddr[3]>>4)&0x0f,\
                            (macaddr[3]>>0)&0x0f,\
                            (macaddr[4]>>4)&0x0f,\
                            (macaddr[4]>>0)&0x0f,\
                            (macaddr[5]>>4)&0x0f,\
                            (macaddr[5]>>0)&0x0f);
        Sn_InfoSave(SN_BUF,sizeof(SN_BUF));
    }
    return true;
}

char *Sn_Get()
{
    // return "A787PG204800004";// 四监3208
    // return "A787PG204800003";// 四监压力传感
    return gSn_Str;
}
















bool_t sh_setsn(char *param)
{
    return  Sn_InfoSave(param,strlen(param));
}

ADD_TO_ROUTINE_SHELL(snset,sh_setsn,"Rewrite SN for example: snst djysn1");


