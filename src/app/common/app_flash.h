/*
 * app_flash.h
 *
 *  Created on: 2020年11月20日
 *      Author: WangXi
 */

#ifndef APP_APP_FLASH_H_
#define APP_APP_FLASH_H_


#include "cpu_peri.h"

#define FLASH_PAGE_SIZE 256
#define FLASH_SECTOR_SIZE 4096
//
//#define FLASH_START_ADDR 0x3E0000   //2M-100K
//
///* 从此地址开始用于数据的保存 */
//#define MACHINE_STRING_ADDR     (FLASH_START_ADDR)
//
#define CFG_SN_INFO_FILE_NAME           "/yaf2/sn_info.dat"
#define CFG_NET_INFO_FILE_NAME          "/yaf2/net_info_%s.dat"
#define CFG_WIFI_INFO_FILE_NAME         "/yaf2/wifi_info.dat"
#define CFG_OTA_INFO_FILE_NAME          "/yaf2/ota_info.dat"
#define CFG_TIME_INFO_FILE_NAME         "/yaf2/time_info.dat"

#define CFG_SN_LEN                      15
#define CFG_NET_IP_LEN                  15
#define CFG_WIFI_SSID_LEN               32
#define CFG_WIFI_PWD_LEN                16
#define CFG_SERVER_ADDR_LEN             128
#define CFG_OTA_VER_LEN                 12
#define CFG_TIME_INFO_LEN                128
#define CFG_OTA_BRANCH_LEN              32


bool_t Sn_InfoSave(char *info ,u32 len);
bool_t Sn_InfoLoad(char *buf ,u32 maxlen);
char * Sn_Get();
bool_t Sn_Init();
bool_t Sn_InfoErase();

/* 网络配置模块 */

//  网络连接类型
enum NetType
{
    //  有线网络
    LINE_NET,
    //  无线网络
    WIFI_NET
};

//  获取IP类型
enum NetIpType
{
    //  动态获取IP
    AUTO_IP,
    //  静态IP
    STATIC_IP
};

//  网络配置信息
struct NetInfoSt
{
    //  网卡名称
    const char *name;
    enum NetType type;
    enum NetIpType ip_type;
    //  IP地址
    char ip_addr[CFG_NET_IP_LEN];
    //  子网掩码
    char subnet_mask[CFG_NET_IP_LEN];
    //  网关
    char gateway[CFG_NET_IP_LEN];
    //  主DNS服务器
    char dns1[CFG_NET_IP_LEN];
    //  备DNS
    char dns2[CFG_NET_IP_LEN];
};

bool_t Net_InfoSave(const char *net_card_name, struct NetInfoSt *net_info_in, bool_t override);
bool_t Net_InfoLoad(const char *net_card_name, struct NetInfoSt *net_info_out);
bool_t Net_InfoErase();


/* WIFI配置模块 */

//  Wifi配置信息
struct WifiInfoSt
{
    char ssid[CFG_WIFI_SSID_LEN];
    char password[CFG_WIFI_PWD_LEN];
};

bool_t Wifi_InfoAdd(struct NetInfoSt *net_info_in, bool_t override);
bool_t Wifi_InfoFind(char *ssid, struct NetInfoSt *net_info_out);
u32 Wifi_InfoFindAll(struct NetInfoSt **net_info_out);
bool_t Wifi_InfoRemove(char *ssid);
bool_t Wifi_InfoClear();



/* 升级配置模块 */

//  升级时间频率
enum OtaType
{
    OTA_DAY,
    OTA_WEEK,
    OTA_MONTH,
    OTA_NEVER
};

//  升级配置
struct OtaInfoSt
{
    //  升级服务器地址
    char ota_addr[CFG_SERVER_ADDR_LEN];
    //  升级频率
    enum OtaType ota_plan;
    //  上次升级时间
    char last_timestamp[CFG_TIME_INFO_LEN];
    //  升级前版本
    char last_ver[CFG_OTA_VER_LEN];
    //  升级前分支
    char last_branch[CFG_OTA_BRANCH_LEN];
};

bool_t Ota_InfoSave(struct OtaInfoSt *ota_info_in);
bool_t Ota_InfoLoad(struct OtaInfoSt *ota_info_out);
bool_t Ota_InfoErase();


/* 时间配置模块 */
enum TimeSyncType
{
    //  从NTP服务器获取时间
    TIME_NTP,
    //  从服务器通过HTTP请求获取时间
    TIME_HTTP,
    //  不获取网络时间 采用本地时间
    TIME_LOCAL
};

//  时间配置信息
struct TimeInfoSt
{
    //  时间同步类型
    enum TimeSyncType time_type;
    //  服务器地址
    char server_time[CFG_SERVER_ADDR_LEN];
    //  上次手动设置时间
    char last_local_timestamp[CFG_TIME_INFO_LEN];
};

void Time_InfoInit();
bool_t Time_InfoSave(struct TimeInfoSt *time_info_in);
bool_t Time_InfoLoad(struct TimeInfoSt *time_info_out);
bool_t Time_InfoErase();

#endif /* APP_APP_FLASH_H_ */
