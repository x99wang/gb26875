#include "log.h"

#include <dbug.h>
#include <string.h>
#include <time.h>
#include "mongoose.h"
#include "../GuiWin/Info/WinSwitch.h"
#include "../trans_dev/usr_info_trans_dev.h"
const char log_format[] = "[%s],<%s>:{"
                         "\"date\":\"%s\","
                         "\"check\":\"%s\""
                         "}";

const char log_sock[] = "[%s],<%s>:%s";


void Trans_Hexadecimal(char *data,int len,char *result)
{
    char *p=data;
    int j=0;
    while(j<len)
    {
        j+=sprintf(result+j,"%02x ",(unsigned char)*p++);
    }
}

#include "lock.h"
struct MutexLCB *log_lock = NULL;
void LogLockInit(void)
{
     log_lock = Lock_MutexCreate("log_lock");
     if (NULL == log_lock)
     {
         debug_printf("xqnewlog","Create log_lock fail.\r\n");
         Lock_MutexDelete(log_lock);
     }
}

static void Log_dGetDataStr(char *date_str,u32 len)
{
    struct tm curr_datetime;
    get_datetime(&curr_datetime);
    snprintf(date_str,len, "%04d/%02d/%02d,%02d:%02d:%02d",
            curr_datetime.tm_year,
            curr_datetime.tm_mon,
            curr_datetime.tm_mday,
            curr_datetime.tm_hour,
            curr_datetime.tm_min,
            curr_datetime.tm_sec);
}

void Log_GetFileName(char *log_type, char *file_name)
{
    char date[16];
    struct tm curr_datetime;
    memset(date, 0, 16);
    get_datetime(&curr_datetime);
    snprintf(date,16, "%02d",curr_datetime.tm_mday);
    snprintf(file_name,56, "%s%s_%s.log", LOG_PATH, log_type, date);
    printf("file_name is %s\r\n",file_name);
}

void Log_GetUnit(char *data,u8 sys_addr, u32 unit_addr, u16 unit_sts)
{
    snprintf(data,126,"sys_addr is %d,unit_addr is %d,unit_sts is %d",sys_addr,unit_addr,unit_sts);
}
static void Log_GetData(char *msg,u32 len,char *log_type,char *data,char *check)
{
    char time[32]={0};
    Log_dGetDataStr(time,sizeof(time));
    snprintf(msg,len, log_format,time,log_type,data,check);
}

static void Log_GetSockData(char *msg, u32 len,char *log_type,char *data)
{
    char time[32]={0};
    Log_dGetDataStr(time,sizeof(time));
    snprintf(msg,len,log_sock,time,log_type,data);
}

void Log_WriteFile_A(char *file, char *msg)
{
    u32 i = 0;
    u32 write_len=0;
    u32 msg_len=strlen(msg);

    FILE *log_file = fopen(file, "a+");

    if(log_file == NULL)
    {
        error_printf("LOG", "fopen <%s> failure!", file);
        return;
    }

    fseek(log_file, 0L, SEEK_END);

    while(i < msg_len)
    {
        write_len = fwrite(msg, 1, msg_len - i, log_file);
        i += write_len;
    }

    fclose(log_file);

}
const char log_serial_name[] = "%sserial_inf_%d.dat";
const char log_ip_name[] = "%sip_inf_%d.dat";
void Log_WriteFile_W(char *msg,int addr,int type)
{
    Lock_MutexPend(log_lock,CN_TIMEOUT_FOREVER);

    u32 i = 0;
    u32 write_len=0;
    u32 msg_len=strlen(msg);

    char serial_name[56]={0};
    if(type == Log_WriteFile_Serial)
    {
        snprintf(serial_name,sizeof(serial_name),log_serial_name,LOG_PATH,addr);
    }
    else if(type == Log_WriteFile_Ip)
    {
        snprintf(serial_name,sizeof(serial_name),log_ip_name,LOG_PATH,addr);
    }

//    printf("write serial_name is %s\r\n",serial_name);
//    printf("write serial_msg is %s\r\n",msg);

    FILE *log_file = fopen(serial_name, "w");

    if(log_file == NULL)
    {
        error_printf("LOG", "fopen <%s> failure!", serial_name);
        Lock_MutexPost(log_lock);
        return;
    }

    fseek(log_file, 0L, SEEK_END);

    while(i < msg_len)
    {
        write_len = fwrite(msg, 1, msg_len - i, log_file);
        i += write_len;
    }
    fclose(log_file);

    Lock_MutexPost(log_lock);
}
s32 Log_ReadFile_R(char *file, char *msg,int len)
{
    s32 ret=0;
    FILE *log_file = fopen(file, "r");

    if(log_file == NULL)
    {
        error_printf("LOG", "fopen <%s> failure!", file);
        return ret;
    }

//  fseek(log_file, 0L, SEEK_END);
//  fseek(log_file, 0, SEEK_SET);
    ret=fread(msg, 1, len, log_file);
    if(ret<=0)
    {
        printf("���ļ�ʧ��\r\n");
    }
    fclose(log_file);
    return ret;
}
void Log_TransSocketLog(char *data,char *log_type)
{
    printf("data is %s\r\n",data);
    Lock_MutexPend(log_lock,CN_TIMEOUT_FOREVER);

    char file_name[64]={0};
    char msg[512]={0};

    memset(file_name, 0, 64);
    memset(msg, 0, 512);

    Log_GetFileName(log_type, file_name);

    Log_GetSockData(msg,512,log_type,data);

    Log_WriteFile_A(file_name,msg);

    printf("socket_msg is %s\r\n",msg);

    Lock_MutexPost(log_lock);
}

void Log_TransHexLog(char *data,u32 len,char *check,char *log_type)
{
    Lock_MutexPend(log_lock,CN_TIMEOUT_FOREVER);

    char *file_name=malloc(64);
    char *hex_date=malloc(len*3);
    char *msg=malloc(len*3+256);

    memset(file_name, 0, 64);
    memset(hex_date, 0, len*3);
    memset(msg, 0, len*3+256);

    Trans_Hexadecimal(data,len,hex_date);

    Log_GetFileName(log_type, file_name);

    Log_GetData(msg,len*3+256,log_type,hex_date,check);

    Log_WriteFile_A(file_name,msg);

    printf("hex_msg is %s\r\n",msg);

    free(file_name);
    free(hex_date);
    free(msg);

    Lock_MutexPost(log_lock);
}

void Log_TransLog(char *data,u32 len,char *check,char *log_type)
{
    Lock_MutexPend(log_lock,CN_TIMEOUT_FOREVER);

    char file_name[64]={0};
    char *msg=malloc(len+256);

    memset(file_name, 0, 64);
    memset(msg, 0, len+256);

    Log_GetFileName(log_type, file_name);

    Log_GetData(msg,len+256,log_type,data,check);

    Log_WriteFile_A(file_name,msg);

    printf("msg is %s\r\n",msg);

    free(msg);

    Lock_MutexPost(log_lock);
}

void log_copy(char *path,u32 count)
{
    char read_name[64] = {0};
    char write_name[64] = {0};
    snprintf(read_name,sizeof(read_name), "%s%s_%d.log",LOG_PATH, path, count);
    snprintf(write_name,sizeof(write_name), "/u_disk/%s_%d.log", path, count);
//SN��Ŀ¼
    FILE *r=NULL,*c=NULL;
    r=fopen(read_name, "rb");
    if(r == NULL)
    {
        printf("file open %s error\r\n",read_name);
        return;
    }
    c=fopen(write_name, "wb");
    if(c == NULL)
    {
        fclose(r);
        printf("file open %s error\r\n",write_name);
        return;
    }
    else
    {
        unsigned char *tmpBuffer = (unsigned char*)malloc(2048);
        fseek(r, 0L, SEEK_END);

        int file_size = ftell(r);

        fseek(r, 0L, SEEK_SET);

        int ret=0;
        int offset=0;

        while(1)
        {
            if(offset>=file_size) break;
            ret=fread(tmpBuffer, 1, 2048, r);
            fwrite(tmpBuffer, 1, ret, c);
            offset+=ret;
        }

        fclose(r);
        fclose(c);
        free(tmpBuffer);
        printf("%s copy is sucess\r\n",read_name);
    }



    DJY_EventDelay(1000*1000);


}

void usb_log_copy(void)
{
    Lock_MutexPend(log_lock,CN_TIMEOUT_FOREVER);

    for(int i=1;i<=30;i++)
    {
        log_copy(DEVICE_READ_LOG,i);
//        log_copy(STATE_PUSH_LOG,i);
//        log_copy(STATE_POP_LOG,i);
        log_copy(DEVICE_READ_LOG,i);
        log_copy(UNIT_STATE_LOG,i);
        log_copy(UPDATE_MESSAGE_LOG,i);
        log_copy(DOWN_MESSAGE_LOG,i);
        log_copy(SOCKET_STATE_LOG,i);
        log_copy(APP_STATE_LOG,i);
    }

    Lock_MutexPost(log_lock);

    DJY_EventDelay(1000*1000);
}

void log_remove(char *path,u32 count)
{
    char file_name[64] = {0};
    snprintf(file_name,sizeof(file_name), "%s%s_%d.log",LOG_PATH, path, count);
    if(0 == remove(file_name))
    {
        printf("remove %s is sucess\r\n",file_name);
    }
    else
    {
        printf("remove %s is fail\r\n",file_name);
    }

    DJY_EventDelay(1000*1000);
}
void remove_all(u32 begin,u32 end)
{
    Lock_MutexPend(log_lock,CN_TIMEOUT_FOREVER);

    for(u32 i = begin;i <= end;i++)
    {
        log_remove(DEVICE_READ_LOG,i);
//        log_copy(STATE_PUSH_LOG,i);
//        log_copy(STATE_POP_LOG,i);
        log_remove(DEVICE_READ_LOG,i);
        log_remove(UNIT_STATE_LOG,i);
        log_remove(UPDATE_MESSAGE_LOG,i);
        log_remove(DOWN_MESSAGE_LOG,i);
        log_remove(SOCKET_STATE_LOG,i);
        log_remove(APP_STATE_LOG,i);
    }


    Lock_MutexPost(log_lock);

    DJY_EventDelay(1000*1000);
}

const char log_serial_inf[] = "@@%d.%d.%d.%d.%d.%d.%d.%d.%d.%d.%d.%s##";
void Write_SerialInf(struct GuiSerial serial,u32 addr)
{
    char serial_msg[128]={0};
    snprintf(serial_msg,sizeof(serial_msg),log_serial_inf,serial.gdata_nub,serial.gparity_nub,serial.gstop_nub,serial.gbitrate_nub,
            serial.gdata_bit,serial.gparity_bit,serial.gstop_bit,serial.gbitrate,
            serial.gequipment_address,serial.gsys_type_nub,serial.gsys_type_flag,serial.gsys_type);

#if INF_LOG
    log_queue_push(serial_msg,addr,LOG_SERIAL_INFO,NULL);
#endif
}

void Read_SerialInf(struct GuiSerial *serial,u32 addr)
{
    Lock_MutexPend(log_lock,CN_TIMEOUT_FOREVER);
    char serial_name[56]={0};
    char serial_msg[128]={0};
    u32 s[11]={0};
    char dev_name[48]={0};
    s32 len=0;
    s32 ret=0;

    snprintf(serial_name,sizeof(serial_name),log_serial_name,LOG_PATH,addr);
//    printf("read serial_name is %s\r\n",serial_name);

    len=Log_ReadFile_R(serial_name,serial_msg,sizeof(serial_msg));
//    printf("read serial_msg is %s\r\n",serial_msg);
    if(len <= 0)
    {
        serial->gbitrate = 0;
        Lock_MutexPost(log_lock);
        return;
    }
    if(serial_msg[0] == '@' && serial_msg[1] == '@' && serial_msg[len-1] == '#' && serial_msg[len-2] == '#')
    {
        ret=sscanf(serial_msg, "@@%d.%d.%d.%d.%d.%d.%d.%d.%d.%d.%d.%48[^#]", &s[0], &s[1],&s[2],&s[3],
                &s[4],&s[5],&s[6],&s[7],&s[8],&s[9], &s[10],dev_name);
//        printf("s[0] is %d\r\n",s[0]);
//        printf("s[1] is %d\r\n",s[1]);
//        printf("s[2] is %d\r\n",s[2]);
//        printf("s[3] is %d\r\n",s[3]);
//        printf("s[4] is %d\r\n",s[4]);
//        printf("s[5] is %d\r\n",s[5]);
//        printf("s[6] is %d\r\n",s[6]);
//        printf("s[7] is %d\r\n",s[7]);
//        printf("s[8] is %d\r\n",s[8]);
//        printf("s[9] is %d\r\n",s[9]);
//        printf("s[10] is %d\r\n",s[10]);
//        printf("dev_name is %s\r\n",dev_name);

        if(ret == 11 || ret == 12)
        {
            serial->gdata_nub=(u8)s[0];
            serial->gparity_nub=(u8)s[1];
            serial->gstop_nub=(u8)s[2];
            serial->gbitrate_nub=(u8)s[3];
            serial->gdata_bit=(u8)s[4];
            serial->gparity_bit=(u8)s[5];
            serial->gstop_bit=(u8)s[6];
            serial->gbitrate=s[7];
            serial->gequipment_address=(u8)s[8];
            serial->gsys_type_nub=(u8)s[9];
            serial->gsys_type_flag=s[10];
            strncpy(serial->gsys_type,dev_name,sizeof(serial->gsys_type));
        }
    }
    Lock_MutexPost(log_lock);
}

const char log_ip_inf[] = "@@%s_%s##";
void Write_IpInf(struct GuiIp ip,u32 addr)
{
    char ip_msg[128]={0};
    snprintf(ip_msg,sizeof(ip_msg),log_ip_inf,ip.gip_address,ip.gport);

#if INF_LOG
    log_queue_push(ip_msg,addr,LOG_IP_INFO,NULL);
#endif
}

void Read_IpInf(struct GuiIp *ip,u32 addr)
{
    Lock_MutexPend(log_lock,CN_TIMEOUT_FOREVER);
    char ip_name[56]={0};
    char ip_msg[56]={0};
    char address[32]={0};
    char port[16]={0};
    int len=0;

    snprintf(ip_name,sizeof(ip_name),log_ip_name,LOG_PATH,addr);
//    printf("read ip_name is %s\r\n",ip_name);

    len=Log_ReadFile_R(ip_name,ip_msg,sizeof(ip_msg));
//    printf("read ip_msg is %s\r\n",ip_msg);

    if(ip_msg[0] == '@' && ip_msg[1] == '@' && ip_msg[len-1] == '#' && ip_msg[len-2] == '#')
    {
        sscanf(ip_msg, "@@%[^_]_%[^#]",address,port);

//        printf("address is %s\r\n",address);
//        printf("port is %s\r\n",port);

        strcpy(ip->gip_address,address);
        strcpy(ip->gport,port);

    }
    else if(addr==0)
    {
        strcpy(ip->gip_address,"120.55.103.0");
        strcpy(ip->gport,"12345");
    }
    Lock_MutexPost(log_lock);
}

#include <shell.h>

void shell_remove_log(char *param)
{
    u32 s[2]={0};
    sscanf(param,"%d %d",&s[0],&s[1]);
    remove_all(s[0],s[1]);
}
ADD_TO_ROUTINE_SHELL(remove, shell_remove_log, "ɾ���ļ�����");
