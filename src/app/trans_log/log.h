#ifndef LOG_H
#define LOG_H

#include <stdio.h>
#include "../GuiWin/Info/WinSwitch.h"
#include "../trans_log/log_init.h"
#define LOG_PATH           "/yaf2/"   //todo宏命名
#define INF_LOG 1
#define DEVICE_READ_LOG     "device"               //设备数据
//#define STATE_PUSH_LOG      "state_push"                //设备状态改变进栈
//#define STATE_POP_LOG       "state_pop"                 //设备状态改变出栈
#define UNIT_STATE_LOG      "unit"                //设备状态改变

#define UPDATE_MESSAGE_LOG  "update"              //上传报文日志
#define DOWN_MESSAGE_LOG    "down"                //下载报文日志

#define SOCKET_STATE_LOG         "socket"         //socket状态
#define APP_STATE_LOG            "app"            //app状态

enum {
    Log_WriteFile_Serial = 0,
    Log_WriteFile_Ip,
};

/**
 *
 * 把字符串转为十六进制的数据输出
 *
 * @param data    要处理的数据
 * @param len     要处理的数据长度
 * @param result  得到的结果
 */
void Trans_Hexadecimal(char *data,int len,char *result);

/**
 *
 * 日志锁初始化
 *
 */
void LogLockInit(void);

/**
 *
 * 获得日志时间
 *
 * @param date_str    得到的日志时间
 */
static void Log_dGetDataStr(char *date_str,u32 len);

/**
 *
 * 获得日志文件名
 *
 * @param log_type    日志类型
 * @param file_name   得到的日志名
 */
void Log_GetFileName(char *log_type, char *file_name);

/**
 *
 * 获得要存的数据
 *
 * @param msg      得到数据
 * @param log_type 到处理的数据类型
 * @param date     要处理的数据(内容)
 * @param check    要处理的数据(标志位)
 */
static void Log_GetData(char *msg,u32 len,char *log_type,char *data,char *check);

/**
 *
 * 获得要存的数据
 *
 * @param msg      得到数据
 * @param log_type 到处理的数据类型
 * @param date     要处理的数据(内容)
 */
static void Log_GetSockData(char *msg, u32 len,char *log_type,char *data);

/**
 *
 * 写文件
 *
 * @param file    文件名称
 * @param msg     文件内容
 */
void Log_WriteFile_A(char *file, char *msg);
void Log_WriteFile_W(char *msg,int addr,int type);
/**
 *
 * 存socket日志接口
 *
 * @param date       要存的数据
 * @param log_type   日志类型
 */
void Log_TransSocketLog(char *data,char *log_type);

/**
 *
 * 存日志接口
 *
 * @param date       要存的数据
 * @param len        数据长度
 * @param check      数据标志位
 * @param log_type   日志类型
 */
void Log_TransLog(char *data,u32 len,char *check,char *log_type);

/**
 *
 * 存报文（16机制格式）日志接口
 *
 * @param date       要存的报文
 * @param len        报文长度
 * @param check      报文标志位
 * @param log_type   日志类型
 */
void Log_TransHexLog(char *data,u32 len,char *check,char *log_type);

/**
 *
 * 文件系统格式化接口
 *
 */
void log_remove(char *path,u32 count);
void remove_all(u32 begin,u32 end);
void erasechip(void);
void Log_SerialInf(struct GuiSerial serial,u32 addr);
void Read_IpInf(struct GuiIp *ip,u32 addr);
#endif /* LOG_H */
