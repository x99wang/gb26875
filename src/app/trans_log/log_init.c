#include "log_init.h"

#include <dbug.h>
#include <string.h>
#include <time.h>
#include "../trans_dev/queue.h"
#include "log.h"

struct QueueInfo *log_queue=NULL;


void log_queue_init(void)
{
    if(log_queue == NULL)
    {
        log_queue = Queue_CreateQueue();
        printf("Log_Queue Create Success:%x\r\n", log_queue);
    }
    if(log_queue == NULL)
    {
        printf("Log_Queue Create Failure.\r\n");
    }
}

void log_queue_push(char *data, int len,int type,char *check)
{
    struct LogString *str;
    str = (struct LogString *)malloc(sizeof(struct LogString));

    char *buf =NULL;

    if(LOG_UNIT_STATE == type || LOG_DEVICE_READ == type || LOG_SOCKET_STATE == type)
    {
        buf=malloc(len);
        memset(buf,0,len);
        strcpy(buf,data);
    }
    else if(type == LOG_SERIAL_INFO || type == LOG_IP_INFO)
    {
        buf=malloc(strlen(data));
        memset(buf,0,strlen(data));
        strcpy(buf,data);
    }
    else
    {
        len=len*3;
        buf=malloc(len);
        memset(buf,0,len);
        Trans_Hexadecimal(data,len,buf);
    }

    str->buff = buf;
    str->len = len;
    str->type = type;
    str->check = check;

//    printf("str->buff is %s\r\n",str->buff);
//    printf("str->len is %d\r\n",str->len);
//    printf("str->type is %d\r\n",str->type);
//    printf("str->check is %s\r\n",str->check);
    if(!Queue_Push(log_queue, (ElementType)str))
    {
        free((void *)str);
        free(buf);
    }
}

static bool_t log_begin=true;

void log_queue_pop(void)
{
    struct LogString *str;
    if(Queue_Pop(log_queue, (ElementType *)&str) == 0)
    {
//        printf("Log_Queue_Pop Error\r\n");
        return;
    }
    if(str == NULL)
    {
//        printf("Log_Queue_Pop is NULL\r\n");
        return;
    }
//    printf("str->buff is %s\r\n",str->buff);
//    printf("str->len is %d\r\n",str->len);
//    printf("str->type is %d\r\n",str->type);
//    printf("str->check is %s\r\n",str->check);

    if(log_begin)
    {
        if(str->type == LOG_DEVICE_READ)
        {
            Log_TransLog(str->buff,str->len,str->check,DEVICE_READ_LOG);
        }
//        else if(str->type == LOG_STATE_PUSH)
//        {
//            Log_TransLog(str->buff,str->len,str->check,STATE_PUSH_LOG);
//        }
//        else if(str->type == LOG_STATE_POP)
//        {
//            Log_TransLog(str->buff,str->len,str->check,STATE_POP_LOG);
//        }
        else if(str->type == LOG_UNIT_STATE)
        {
            Log_TransLog(str->buff,str->len,str->check,UNIT_STATE_LOG);
        }
        else if(str->type == LOG_UPDATE_MESSAGE)
        {
            Log_TransLog(str->buff,str->len,str->check,UPDATE_MESSAGE_LOG);
        }
        else if(str->type == LOG_DOWN_MESSAGE)
        {
            Log_TransLog(str->buff,str->len,str->check,DOWN_MESSAGE_LOG);
        }
        else if(str->type == LOG_SOCKET_STATE)
        {
            Log_TransSocketLog(str->buff,SOCKET_STATE_LOG);
        }
        if(str->type == LOG_SERIAL_INFO)
        {
            Log_WriteFile_W(str->buff,str->len,Log_WriteFile_Serial);
        }
        else if(str->type == LOG_IP_INFO)
        {
            Log_WriteFile_W(str->buff,str->len,Log_WriteFile_Ip);
        }
    }

    free(str->buff);
    free(str->check);
    free(str);
}

ptu32_t log_trans(void)
{
    log_queue_init();

    while(1)
    {
        log_queue_pop();

        DJY_EventDelay(1000*1000);
    }
    return 0;
}

u16 evtt_log = CN_EVTT_ID_INVALID;

void trans_log_init()
{
    evtt_log = DJY_EvttRegist(EN_CORRELATIVE, CN_PRIO_RRS, 0, 0,
            log_trans, NULL, 0x1000, "log trans");

    if (CN_EVTT_ID_INVALID != evtt_log)
    {
        DJY_EventPop(evtt_log, NULL, 0, 0, 0, 0);
    }
    else
    {
        printf("[DEBUG] Djy_EvttRegist-evtt_log error\r\n");
    }
}

#include <shell.h>

void begin_log(void)
{
    if(log_begin)
    {
        log_begin=false;
    }
    else
    {
        log_begin=true;
    }

}
ADD_TO_ROUTINE_SHELL(logbegin, begin_log, "控制日志线程");
