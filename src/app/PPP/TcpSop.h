/*================================================================================
 * 文件描述：103通讯用网络Tcp封装
 * 文件版本: V1.00
 * 开发人员: 姜超
 * 定版时间:
 * 版本修订:
 * 修订人员:
 *================================================================================*/

#ifndef _TCP_H
#define _TCP_H

//#include "Resource.h"
//#include "NetCfg.h"
#include "lock.h"
#include "sunri_types.h"
#include "timer_b.h"
/*================================================================================*/

#define CN_INCLUDE_TCP      ( 1 )           // 包含TCP
#define CN_INCLUDE_UDP      ( 0 )           // 包含UDP


#ifdef __cplusplus
extern "C" {
#endif
#if( CN_INCLUDE_TCP )        // 包含TCP

#define CN_TCP_NUM_SVR              ( 2 )   // TCP 连接最大服务器数目
#define CN_TCP_NUM_SOCK             ( 12 )   // TCP 连接最大个数
#define CN_TCP_NUM_CONNECT          ( 4 )  // TCP 每个服务器允许的最大连接数目

// 收取一定长度的数据

struct MTcpConn
{
    //---------this if for tcp portnum user----------
    char    ServerIP[20];   // IP地址字符串
    char    ClientIP[20];   // IP地址字符串
    short   nServerPort;
    int     SockType;       // 0=Server Type; 1=Client Type
    int     fd;

    enum
    {
        unUse=1,
        Connecting,
        Using,
        Closing
    }nState;

    int     (*FrameGet)();
//FrameGet is called at form of FrameGet(int nPortNum,char* buff,int (*BasicGet)())
//the BasicGet is Called int BasicGet(int nPortNum,char*buff,int nLen)
    char*   pReadPara;

    int     (*CheckEnable)(void*);
//检查连接是否启用,由调用者编写该函数,该函数返回TRUE表示连接启用,否则停用
    void*   pCheckEPara;    // CheckEnable的参数指针

//----------the following is for running use-------------------
    int     iReadTaskID;
    int     iSendTaskID;

    enum
    {
        ReadFree=-1,
        Reading,
        ReadFail,
        ReadExit
    }nReadState;

    enum
    {
        SendFree=-1,
        Sending,
        SendFail,
        SendExit
    }nSendState;
//--------------------the buffer area------------------------------
    char            *pReadBuff;
    char            *pSendBuff;
    int             iReadBuffLen;
    int             iSendBuffLen;
    int             iReadLen;
    int             iSendLen;
    short           nTimeCount;
    short           nTimeLimit;
    short           nConnTimeout;                   // 连接超时时间(秒)
    tagTimerB       tTimerTcpRecv;                  // TCP收发任务超时计时器
    tagTimerB       tTimerReConnect;                // TCP重连定时器
    BYTE            byIfIndex;                      // 使用的网口序号
    BYTE            byDstSlot;                      // 目标板件插槽号
// tianrz add @20170427 for sibida
    int             iFailNum;                       // TCP连接不成功次数
    int             iFailRstFlag;                   // 无线通讯模块重拨标志
    int             iFailRstFlag2;                  // 无线通讯模块重拨标志
    tagTimerB       tTimerEstablish;                // Tcp链接建立超时

//    struct SemaphoreLCB *pReadSemp;                      // 读取任务信号量
//    struct SemaphoreLCB *pSendSemp;                      // 读取任务信号量

};

struct MTcpServerPort
{
    char    ServerIP[20];   // IP地址字符串
    short   nPort;
    int     Fd;
    enum
    {
        unListen=-1,
        Listening
    }nState;
};

struct MTcpCtrlStruc
{
    short   nTcpNo;         // 链接数目
    short   nServerNo;      // 服务器数目
    struct  MTcpServerPort  m_ServerArray[CN_TCP_NUM_SVR];
    struct  MTcpConn        m_ConnArray[CN_TCP_NUM_SOCK];
    struct  MutexLCB        *ptSemIDServer;  // 增加服务器互斥标识
    struct  MutexLCB        *ptSemIDConn;    // 增加连接互斥标识
//--add for debug @ 20170413
    short   nModerm;        //  拨号状态 0-成功 非0-失败
    short   nModerm_bak;    //  拨号状态备份 0-成功 非0-失败
};

extern UINT16   Hard_Tcp_TxStartup(SINT16 nTcpSockNo, UINT8 *pbyMsgBuf, UINT16 wMsgLen, UINT8 byPortNo, UINT8 byDbgBit);
extern int   tcp_basic_read( short nTcpNo, char *buf, int nLen );
extern short TcpAddConnection( BYTE* ServerIP, BYTE* ClientIP, short nPort, int nType, int ( *ReadFunc )( ), BYTE byDstSlot );
extern void  TcpSetConnPara( short nTcpNo, int iReadLen, int iSendLen, char* ReadPara, short nTimeLimit, short nConnTimeout );
extern int   TcpRead( short nTcpNo, char* pBuff );
extern int   TcpGetSockState( short nTcpNo );

    
extern void TcpSetModermState( short nModerm );
extern void Tcp_Initial(void);
extern void TcpReadTask(void);
extern void TcpSendTask(void);
extern int  TcpClientStart(short nTcpNo);
extern void StartNewConnect(short nTcpNo,int newFd);
extern void CloseOldConnect(short nTcpNo);
extern int  TcpServerStart(void);
extern void TcpMonTask( void );
extern void TcpCountTask(void);
extern void TcpStartNet( void );
extern void ModuleInstall_TcpDebug(void);
extern void Dbg_ShowTcp( void );
extern unsigned int TCP_Get_Modem_RstFlag(void);
extern unsigned int TCP_Get_Modem_RstFlag2(void);
extern short TcpGetModermState( void );
extern void Dbg_Set_ResetFlag(void);
extern void Dbg_Set_g_uiLogTcp(void);
extern void Dbg_Reset_g_uiLogTcp(void);
extern void Dbg_Set_nSendState_SendFree(void);
extern void Dbg_Set_nSendState_Sending(void);
extern u32 Tcp_Get_Socket_Status(u16 uSocket);
extern void Tcp_Set_iFailRstFlag2(u16 uSocket);
extern void Tcp_Reset_iFailRstFlag2(u16 uSocket);
extern u32 Tcp_Reset_iFailRstFlag2_All(void);



#endif // #if( CN_INCLUDE_TCP )

#ifdef __cplusplus
}
#endif

/*================================================================================*/
#endif // _TCP_H_
