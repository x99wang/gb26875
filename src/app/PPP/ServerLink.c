#include "TcpSop.h"
#include "ServerLink.h"
#include "protocol.h"

#define  CN_SG104_TASK_STACK_LEN    0x2000
static UINT8  gs_SG104_TaskStack[CN_SG104_TASK_STACK_LEN];
#define         CN_SG104_SEND_BUFF             ( 512 )
static UINT8    g_bySend[CN_SG104_SEND_BUFF];
static tagSG104RunCtl g_tSG104RunCtl;
static      UINT16      g_wCommAddr1=0;
BOOL bProtocal_Start = 0;
sNum_tagTimeBCD tOffLine2[30];


//===========================================================
// 函数功能: 报文接收函数
// 输入参数: 通讯口编号
// 输出参数: 无
// 返回值  :
//===========================================================
int SG104_NetMsgRecv( UINT32 uiTcpSockNo,  UINT8 *pbyMsgBuf,  UINT8 *pbyParamTag )
{
    UINT16    wMsgLen, wCounter;

    for(wCounter=0; wCounter<CN_VOL_BUF_RECV; wCounter++)       // 避免死循环
    {
        if(-1 == tcp_basic_read(uiTcpSockNo, (char *)&pbyMsgBuf[0], 1))
        {
            return -1;                                          // ERROR
        }
        if(pbyMsgBuf[0] != CN_SG104_START_CODE)
        {
            continue;
        }

        if(-1 == tcp_basic_read(uiTcpSockNo, (char *)&pbyMsgBuf[1], 2))
        {
            return -1;                                          // ERROR
        }
//--modify for NW @20160729
        wMsgLen = M_MakeWord(pbyMsgBuf[1],pbyMsgBuf[2]);
        if((wMsgLen > CN_SG104_MSGLEN_MAX)||(wMsgLen < CN_SG104_MSGLEN_MIN))
        {
            continue;
        }

        if(-1 == tcp_basic_read(uiTcpSockNo, (char *)&pbyMsgBuf[3], wMsgLen))
        {
            return -1;                                          // ERROR
        }

        return (wMsgLen+3);
    }

    return 0;
}

//===========================================================
// 函数功能: 初始化函数
// 输入参数: 通讯口编号
// 输出参数: 无
// 返回值  :
//===========================================================
void SG104_Init(  UINT8 byPortNo  )
{
    tagPortVal          *ptPortVal;
    tagPortSet          *ptPortSet;
    tagMsgCtrl          *ptMsgCtrl;
    tagPParamNet        ptParamNet;
    tagSG104RunCtl      *ptSG104RunCtl;
    tagSockCtrl         *ptSockCtrl;
    tagNRSCtrl          *ptNRSCtrl;
    short               iTcpSockNo;
    tagSendCtrl         *ptSendCtrl;
    UINT32              dwLoop;
    UINT8               byTcpIP[4] = {0};
    UINT8               *pbyMonIP;


    if(FALSE == Fun_GetPortVariable(byPortNo, &ptPortVal))
    {
        return;
    }
    ptPortSet     = ptPortVal->ptPortSet;
    ptSG104RunCtl = (tagSG104RunCtl *)(&g_tSG104RunCtl);

    if((ptPortSet->wPortType  != CN_PORT_NET)||(byPortNo < CN_PORT_BASE_NET))
    {
        return;
    }

    // 端口号
    ptParamNet = (tagPParamNet)(ptPortSet->dwParam);
    ptParamNet->wNetPort = 9999;    //服务器端口号

    // 监控IP地址
    ptParamNet->byMonIP[0] = 192;
    ptParamNet->byMonIP[1] = 168;
    ptParamNet->byMonIP[2] = 0;
    ptParamNet->byMonIP[3] = 157;

    // 装置IP地址
    byTcpIP[0] = 192;
    byTcpIP[1] = 168;
    byTcpIP[2] = 253;
    byTcpIP[3] = 003;


    // 获取装置编号参数定值
    ptSG104RunCtl->byTerminalNo[7] = 0;
    ptSG104RunCtl->byTerminalNo[6] = 0;
    ptSG104RunCtl->byTerminalNo[5] = 0;
    ptSG104RunCtl->byTerminalNo[4] = 0;
    ptSG104RunCtl->byTerminalNo[3] = 0;
    ptSG104RunCtl->byTerminalNo[2] = 0;
    ptSG104RunCtl->byTerminalNo[1] = 0;
    ptSG104RunCtl->byTerminalNo[0] = 0;

    g_wCommAddr1 = ((ptSG104RunCtl->byTerminalNo[1])<<8) + (ptSG104RunCtl->byTerminalNo[0]);
printf("g_wCommAddr1:%x %x %x\r\n",g_wCommAddr1,ptSG104RunCtl->byTerminalNo[1],ptSG104RunCtl->byTerminalNo[0]);

    for(dwLoop=0; dwLoop<CN_NUM_MSG_PORT; dwLoop++)
    {
        ptMsgCtrl = &(ptPortVal->tMsgCtrl[dwLoop]);
        ptMsgCtrl->wLenAll     = 0x00;
        ptMsgCtrl->wLenCur     = 0x00;
        ptMsgCtrl->byMsgType   = 0xFF;
        ptMsgCtrl->byMsgStatus = CN_STATUS_RECEIVE_IDLE;
        ptMsgCtrl->byMsgInfo[0]= 0x00;
        ptMsgCtrl->byMsgInfo[1]= 0x00;
    }
    ptSG104RunCtl->tSockCtrl.iTcpSockNo  = -1;
    ptSG104RunCtl->byRunStat             = 0;
    ptSG104RunCtl->byProcessState        = EN_SG104_PROC_STATE_IDLE;
    ptSG104RunCtl->wSendBase             = 0;
    ptSG104RunCtl->byRemoteUpdate = 0xFF;

    M_GrSave_bGlobalRam(FALSE, EN_BOOL_SN_EVC_AuthenFlag_01);
    M_GrSave_bGlobalRam(FALSE, EN_BOOL_SN_EVC_AuthenFlag_02);
    M_GrSave_bGlobalRam(FALSE, EN_BOOL_SN_EVC_REMOTE_UPDATE);
    M_GrSave_bGlobalRam(TRUE,  EN_BOOL_SN_EVC_Network_Err);

    Time_TimerBStart(&ptSG104RunCtl->tFrameUSend,    CN_SG104_TIMEOUT_FRAMEU     );
    Time_TimerBStart(&ptSG104RunCtl->tLinkIdle,      CN_SG104_TIMEOUT_IDLE       );
    Time_TimerBStart(&ptSG104RunCtl->tProtocolStart, CN_SG104_TIMEOUT_PROSTART   );
    Time_TimerBStart(&ptSG104RunCtl->tLinkTimeout,   CN_SG104_TIMEOUT_FRAME_U    );
    Time_TimerBStart(&ptSG104RunCtl->tRecTimeout,    3000                        );
    Time_TimerBStart(&ptSG104RunCtl->tPolling,        CN_SG104_TIMEOUT_CHARGEDATA);
    Time_TimerBSetExpired(&ptSG104RunCtl->tProtocolStart);
    Time_TimerBSetExpired(&ptSG104RunCtl->tRecTimeout);

    // 创建连接，只考虑单机单网
    iTcpSockNo = TcpAddConnection(ptParamNet->byMonIP, byTcpIP, ptParamNet->wNetPort, EN_NET_MODE_CLIENT, SG104_NetMsgRecv, 0);

    pbyMonIP = &ptParamNet->byMonIP[0];

    if(iTcpSockNo < 0)
    {
        return;                                                 // ERROR
    }

    TcpSetConnPara(iTcpSockNo, 261, 261, (char *)0,  20*5, 0);    // fix me shenxb 标准要求为20秒

    ptSockCtrl = &ptSG104RunCtl->tSockCtrl;
    ptSockCtrl->iTcpSockNo = iTcpSockNo;
    memcpy(ptSockCtrl->byServerIP,pbyMonIP, 4);
    memcpy(ptSockCtrl->byClientIP,byTcpIP, 4);

    ptNRSCtrl = &ptSG104RunCtl->tNRSCtrl;
    ptNRSCtrl->iTcpSockNo = iTcpSockNo;
    ptNRSCtrl->wNR        = 0;
    ptNRSCtrl->wNS        = 0;
    ptNRSCtrl->wNS_ACK    = 0;

    // 发送缓冲区指针赋值
    ptPortVal->tSendCtrl.pbySendBuf = &g_bySend[0];
    ptSendCtrl = &ptPortVal->tSendCtrl;
    ptSendCtrl->wSendLen  = 0;
    ptSendCtrl->wSendRead = 0;
    ptSendCtrl->wSendFlag = CN_STATUS_SEND_IDLE;
    tChargeRecCtrl.byAckNak = 0x00;
    tChargeRecCtrl.byCnt_RecUp = 0;
    return;
}

//===========================================================
// 函数功能: 报文解析
// 输入参数: 通讯口编号
// 输出参数: 无
// 返回值  :
//===========================================================
void SG104_ProcParse( UINT8 byPortNo, UINT8 *pbyMsg )
{
    tagPortVal          *ptPortVal;
    tagSG104RunCtl      *ptSG104RunCtl;
    UINT16              wMsgLen;
    UINT8               byASDUType;

    if(FALSE == Fun_GetPortVariable(byPortNo, &ptPortVal))
    {
        return;
    }
    ptSG104RunCtl = (tagSG104RunCtl *)(&g_tSG104RunCtl);
////printf("SG104_ProcParse1\n");
//    SG104_LinkStaUpdate(byPortNo, pbyMsg);                      // 处理链路控制信息
////printf("SG104_ProcParse2\n");
//
//    //思必达公司对此未做要求 fix me shenxb
////    if( (ptSG104RunCtl->tNRSCtrl.wNS-ptSG104RunCtl->tNRSCtrl.wNS_ACK) > CN_SG104_NUM_ACKFRAME )
////    {
////        return;                                                 // 上位机未确认
////    }
//
//    if( 0x04!=ptSG104RunCtl->byRunStat )  return;                 // 未下发启动传输帧
//
//    if(M_GrGet_bGlobalRam(EN_BOOL_SN_EVC_RemoteUpdate_StartTask)) // 正在执行远程升级
//    {
//        printf("SG104:正在执行远程升级!\r\n");
//        return;
//    }
//
//    wMsgLen = pbyMsg[1];
//    if(wMsgLen <= CN_SG104_LEN_APCI)                              // 短帧不处理
//    {
//        return;
//    }
//
//    pbyMsg    += CN_SG104_LEN_APCI;
//    byASDUType = pbyMsg[0];
//    switch(byASDUType)
//    {
//        case ENUM_SG104_C_CS_NA:                                // 校时命令
//            SG104_CheckTimeProcess(byPortNo, pbyMsg);
//            break;
//        case ENUM_SG104_C_IC_NA:                                // 总召唤命令
//            SG104_IgiProcess(byPortNo, pbyMsg);
//            break;
//        case ENUM_SG104_C_CI_NA:                                // 计数量总召命令
//            SG104_YMProcess(byPortNo, pbyMsg);
//            break;
//        case ENUM_SG104_C_SD_NA:                                // 下发数据项
//            SG104_CtrlProcess(byPortNo, pbyMsg);
//            break;
//        case ENUM_SG104_M_RE_NA:                                //充电记录回复报文
//            SG104_ChargeProcess(byPortNo, pbyMsg);
////printf("充电记录确认结果111=%x %x %x %x %x %x %x %x %x %x\n",pbyMsg[0],pbyMsg[1],pbyMsg[2],pbyMsg[3],pbyMsg[4],pbyMsg[5],pbyMsg[6],pbyMsg[7],pbyMsg[8],pbyMsg[9]);
//            break;
//        default:
//            SG104_SendFrameS( byPortNo );
//            break;
//    }

    return;
}



//===========================================================
// 函数功能: 报文预处理
// 输入参数: 通讯口编号
// 输出参数: 无
// 返回值  :
//===========================================================
void SG104_Proc( UINT8 byPortNo )
{
    tagMsgCtrl          *ptMsgCtrl;
    tagPortVal          *ptPortVal;
    tagSG104RunCtl      *ptSG104RunCtl;
    tagSendCtrl         *ptSendCtrl;
    UINT8               *pbyMsg;
    int                 iMsgLen/*,iLoop*/;
    short               iTcpSockNo;

    if( FALSE==Fun_GetPortVariable( byPortNo, &ptPortVal) ) return;
    ptSG104RunCtl = (tagSG104RunCtl *)(&g_tSG104RunCtl);
    ptSendCtrl = &ptPortVal->tSendCtrl;

    if( CN_STATUS_SEND_BUSY == ptSendCtrl->wSendFlag )
        return;

    iTcpSockNo= ptSG104RunCtl->tSockCtrl.iTcpSockNo;

    ptMsgCtrl = &ptPortVal->tMsgCtrl[0];
    pbyMsg = ptMsgCtrl->byMsgBuf;                   // 使用第一个报文接受缓冲区

    if(iTcpSockNo<CN_TCP_NUM_SOCK)
        iMsgLen = TcpRead(iTcpSockNo, (char *)pbyMsg);
    else
        iMsgLen = 0;

    if( iMsgLen <=0 )
    {
        Port_CheckCommERR( byPortNo );
        return;
    }

    Port_CheckCommOK( byPortNo );

    SG104_ProcParse(byPortNo, (UINT8*)pbyMsg);

    ptMsgCtrl->wLenCur      = 0;
    ptMsgCtrl->wLenAll      = 0;
    ptMsgCtrl->byMsgStatus  = CN_STATUS_RECEIVE_IDLE;

    return;
}

// ============================================================================
// 函数功能：获取当前充电枪编号
// 输入参数：无
// 输出参数：无
// 返回值：  充电枪索引
// ============================================================================
UINT32 EVC_HMI_GetCurrentPlug( void )
{
    return 0;
}

//===========================================================
// 函数功能: 实时上送时间控制
// 输入参数: 通讯口编号
// 输出参数: 无
// 返回值  :
//===========================================================
void SG104_Send_PollingTimeCtrl( UINT8 byPortNo )
{
    tagSG104RunCtl      *ptSG104RunCtl;
    tagPortVal          *ptPortVal;
    UINT32              dwCharge;
    static  UINT32      dwSwitch = 0x55;


    if(FALSE == Fun_GetPortVariable(byPortNo, &ptPortVal))
    {
        return;
    }
    ptSG104RunCtl = (tagSG104RunCtl *)(&g_tSG104RunCtl);

    if(EVC_HMI_GetCurrentPlug()==0)
    {
        dwCharge = EN_BOOL_SN_EVC_Charge_01;            // 开始充电标志虚端子
    }
    else
    {
        dwCharge = EN_BOOL_SN_EVC_Charge_02;            // 开始充电标志虚端子
    }

    if(M_GrGet_bGlobalRam( dwCharge )==FALSE)
//    if(g_dwCharge==0)
    {
        if(dwSwitch==0xAA)
        {
            dwSwitch = 0x55;
            Time_TimerBStart(&ptSG104RunCtl->tPolling, CN_SG104_TIMEOUT_CHARGEDATA);

        }
    }
    else
    {
        if(dwSwitch==0x55)
        {
            dwSwitch = 0xAA;
            Time_TimerBStart(&ptSG104RunCtl->tPolling, CN_SG104_TIMEOUT_REALDATA);
        }
    }

    return;
}

// ============================================================================
// 函数功能：卡状态、区域状态复位
// ============================================================================
void EVC_MT_Reader_Reset(UINT8 byIndex)
{
//    tagRF_CardInfo      *ptCardInfo;
//    tagEVC_MT_RederCtrl *ptCellRam;
//    tagRF_DataDef       *ptRFDataDef;
//
//     // 指针初始化
//     ptCellRam    = &g_tMT_RederCtrl;
//    ptCellRam->pdwHookUser = byIndex;
//    ptCardInfo   = &ptCellRam->ptHookInfo[byIndex];
//    ptRFDataDef = (tagRF_DataDef*)&(g_dwRF_DataDef[byIndex]);
//
//
//     // 更新卡信息区域标志位
//     ptCardInfo->dwEreaStatus = EN_RF_EREA_STATUS_NULL;
//     ptCardInfo->dwCardStatus = EN_EVC_RF_CARD_NULL;
//     // 更新读卡器状态
//     ptCellRam->nCpuCardCmd   = EN_EVC_CPU_CARD_CMD_SeekCard; // Cpu卡控制指令
//     ptCellRam->nM1CardCmd    = EN_EVC_M1_CARD_CMD_SeekCard;  // M1 卡控制指令
//     ptCellRam->dwMsgSendFlg  = 0;                             // 发送处理标志
//     ptCellRam->dwMsgRecvFlg  = 0;                             // 接受处理标志
//
//     //清除卡号、余额
//     memset(ptCardInfo->tRdInfo.byCardNo, 0, CN_NUM_RF_IC_BYTE);    // 卡号(08位方式存储)
//     ptCardInfo->tRdInfo.dwBalance = 0;
//
//     M_GrSave_bGlobalRam(FALSE, ptRFDataDef->dwPinNO_Valid);
//     M_GrSave_bGlobalRam(FALSE, ptRFDataDef->dwPinNO_Enough);
//     M_GrSave_bGlobalRam(FALSE, ptRFDataDef->dwCardAuthenFlag);

    return;
}

//===========================================================
// 函数功能: 鉴权时效定时器控制
// 输入参数: 通讯口编号
// 输出参数: 无
// 返回值  :
//===========================================================
void SG104_Send_AuthenTimeCtrl( UINT8 byPortNo )
{
    tagSG104RunCtl      *ptSG104RunCtl;
    tagPortVal          *ptPortVal;
    UINT32              dwCardInfNo;
    UINT32              dwCardAuthenFlag;                       // 卡鉴权标志位

    dwCardInfNo = EVC_HMI_GetCurrentPlug();

    if(FALSE == Fun_GetPortVariable(byPortNo, &ptPortVal))
    {
        return;
    }
    ptSG104RunCtl = (tagSG104RunCtl *)(&g_tSG104RunCtl);

    if(dwCardInfNo==0)
    {
        dwCardAuthenFlag = EN_BOOL_SN_EVC_AuthenticationFlag_01;
    if(TRUE == Time_TimerBCheck( &ptSG104RunCtl->tAuthentication))
    {
        Time_TimerBStop( &ptSG104RunCtl->tAuthentication  );

        M_GrSave_bGlobalRam(FALSE, dwCardAuthenFlag);
        EVC_MT_Reader_Reset(dwCardInfNo);

        printf("EN_BOOL_SN_EVC_AuthenticationFlag  %d !!!\n",dwCardInfNo);
    }
    }
    else
    {
        dwCardAuthenFlag = EN_BOOL_SN_EVC_AuthenticationFlag_02;
        if(TRUE == Time_TimerBCheck( &ptSG104RunCtl->tAuthentication2))
        {
            Time_TimerBStop( &ptSG104RunCtl->tAuthentication2  );

            M_GrSave_bGlobalRam(FALSE, dwCardAuthenFlag);
            EVC_MT_Reader_Reset(dwCardInfNo);

            printf("EN_BOOL_SN_EVC_AuthenticationFlag2  %d !!!\n",dwCardInfNo);
        }
    }
    return;
}

//===========================================================
// 函数功能: 发送协议标识帧
// 输入参数: 通讯口编号
// 输出参数: 无
// 返回值  :
//===========================================================
UINT16 SG104_SendFrameP( UINT8 byPortNo  )
{
    tagSG104RunCtl      *ptSG104RunCtl;
    tagPortVal          *ptPortVal;
    tagSendCtrl         *ptSendCtrl;
    UINT8               *pbySendBuf;
    short               iTcpSockNo;

    if(FALSE == Fun_GetPortVariable(byPortNo, &ptPortVal))
    {
        return 0;
    }
    ptSG104RunCtl = (tagSG104RunCtl *)(&g_tSG104RunCtl);

    ptSendCtrl  = &ptPortVal->tSendCtrl;
    pbySendBuf  = ptSendCtrl->pbySendBuf;

    pbySendBuf[0 ]  = 0x68;                                     // 起始符
    pbySendBuf[1 ]  = 0x02;                                     // 连接类型:01-监控或虚拟站;02-离散充电桩;03-智能车载终端
    pbySendBuf[2 ]  = ptSG104RunCtl->byTerminalNo[0];                                     // 8字节设备编号:连接类型为2时标识设备编号,否则为0
    pbySendBuf[3 ]  = ptSG104RunCtl->byTerminalNo[1];
    pbySendBuf[4 ]  = ptSG104RunCtl->byTerminalNo[2];
    pbySendBuf[5 ]  = ptSG104RunCtl->byTerminalNo[3];
    pbySendBuf[6 ]  = ptSG104RunCtl->byTerminalNo[4];
    pbySendBuf[7 ]  = ptSG104RunCtl->byTerminalNo[5];
    pbySendBuf[8 ]  = ptSG104RunCtl->byTerminalNo[6];
    pbySendBuf[9 ]  = ptSG104RunCtl->byTerminalNo[7];
    //pbySendBuf[11]  = M_HexToBcd( g_tSysSet.wCommAddr/100 );
    //pbySendBuf[12]  = M_HexToBcd( g_tSysSet.wCommAddr%100 );
    pbySendBuf[10]  = M_GetByteLo(g_wCommAddr1);       // 两字节站地址(BCD)
    pbySendBuf[11]  = M_GetByteHi(g_wCommAddr1);       // 两字节站地址(BCD)

    iTcpSockNo  = ptSG104RunCtl->tSockCtrl.iTcpSockNo;
    bProtocal_Start = 1;
    Hard_Tcp_TxStartup(iTcpSockNo, pbySendBuf, 12, byPortNo, EN_DBG_MSG_SEND);
    bProtocal_Start = 0;
    Time_TimerBStop ( &ptSendCtrl->tTimerAbandon  );
    Time_TimerBReset( &ptSendCtrl->tTimerLineIdle );
    ptSendCtrl->wSendLen  = 0;
    ptSendCtrl->wSendRead = 0;
    ptSendCtrl->wSendFlag = CN_STATUS_SEND_IDLE;
//printf("SG104:iTcpSockNo=%d\n",iTcpSockNo);
    Time_TimerBReset( &ptSG104RunCtl->tLinkIdle   );
    Time_TimerBReset( &ptSG104RunCtl->tFrameUSend );
    Time_TimerBReset( &ptSG104RunCtl->tLinkTimeout);
    return 0;
}


//===========================================================
// 函数功能: 主动发送测试帧
// 输入参数: 通讯口编号
// 输出参数: 无
// 返回值  :
//===========================================================
BOOL SG104_Send_ProtocolStart( UINT8 byPortNo )
{
    tagSG104RunCtl      *ptSG104RunCtl;
    tagPortVal          *ptPortVal;

    if(FALSE == Fun_GetPortVariable(byPortNo, &ptPortVal))
    {
        return FALSE;
    }
    ptSG104RunCtl = (tagSG104RunCtl *)(&g_tSG104RunCtl);

    if(FALSE == Time_TimerBCheck( &ptSG104RunCtl->tProtocolStart ))
    {
        return FALSE;
    }

    SG104_SendFrameP(byPortNo);
    //printf("SG104:tProtocolStart\r\n");
    Time_TimerBReset( &ptSG104RunCtl->tProtocolStart );

    return TRUE;
}

//===========================================================
// 函数功能: 发送报文
// 输入参数: 通讯口编号
// 输出参数: 无
// 返回值  :
//===========================================================
void SG104_Send( UINT8 byPortNo )
{
    tagSG104RunCtl      *ptSG104RunCtl;
    tagPortVal          *ptPortVal;
    tagSendCtrl         *ptSendCtrl;
    short               iTcpSockNo;
    UINT16              wMsgLen;
    BOOL                bSend;

    if(FALSE == Fun_GetPortVariable(byPortNo, &ptPortVal))
    {
        return;
    }
    ptSG104RunCtl = (tagSG104RunCtl *)(&g_tSG104RunCtl);
    if(ptSG104RunCtl->byRunStat!=0x04)                          // 未下发启动传输命令
    {
        SG104_Send_ProtocolStart( byPortNo );
        return;
    }

    ptSendCtrl  = &ptPortVal->tSendCtrl;
    if(FALSE == Time_TimerBCheck( &ptSendCtrl->tTimerLineIdle ))
    {
        return;                                                 // 空闲时间没到
    }

    iTcpSockNo  = ptSG104RunCtl->tSockCtrl.iTcpSockNo;
    if(FALSE == TcpGetSockState( iTcpSockNo ))                  // 当前连接中断
    {
        ptSG104RunCtl->byRunStat = 0;
printf("SG104:TCP链接中断，尝试重新连接!**%d**\r\n",iTcpSockNo);
        M_GrSave_bGlobalRam(TRUE, EN_BOOL_SN_EVC_Network_Err);

        Time_TimerBSetExpired(&ptSG104RunCtl->tProtocolStart);
        return;
    }

//--wangrq
// 若60s未收到后台下发的报文，则主动关闭TCP链接
    static sNum_tagTimeBCD *ptOffLine2 = &tOffLine2[0];
    static UINT32 sdwNum_Log_Offline2 = 1;
    if(Time_TimerBCheck( &ptSG104RunCtl->tLinkTimeout ))
    {
        //printf("接收后台报文超时,主动关闭TCP链接.\r\n");
        CloseOldConnect( iTcpSockNo );

        ptSG104RunCtl->byRunStat = 0;
printf("tFrameSReceive=========01\n");
        Time_DbgSysTime( );
        Time_CopySystemTimeBCD(&ptOffLine2->tTime_Log);
        ptOffLine2->dwNum_Log = sdwNum_Log_Offline2;
        ptOffLine2++;
        sdwNum_Log_Offline2++;
        if(ptOffLine2 - &tOffLine2[0] > 29)
        {
            ptOffLine2 = &tOffLine2[0];
        }
        M_GrSave_bGlobalRam(TRUE, EN_BOOL_SN_EVC_Network_Err);
        Time_TimerBSetExpired(&ptSG104RunCtl->tProtocolStart);
        return;
    }

// tianrz add 20170503 for sibida umodem
// 如果无线模块自己掉线了，TCP将主动断开链接
#if(CN_EVC_MODEM)
    if(TcpGetModermState())
    {
        //printf("无线模块掉线,主动关闭TCP链接.\r\n");
        CloseOldConnect( iTcpSockNo );

        ptSG104RunCtl->byRunStat = 0;
printf("tFrameSReceive=========02\n");
        Time_DbgSysTime( );
        M_GrSave_bGlobalRam(TRUE, EN_BOOL_SN_EVC_Network_Err);
        Time_TimerBSetExpired(&ptSG104RunCtl->tProtocolStart);
        return;
    }
#endif

//--wangrq
    SG104_Send_PollingTimeCtrl( byPortNo );
    SG104_Send_AuthenTimeCtrl( byPortNo );
//--

//    // message-watching
//    #if(CN_SG104_HAVE_RMON)
//        if(ptSendCtrl->wSendLen != 0)
//        {
//            if( RMon_MsgDataSend(byPortNo) )
//            {
//                RMon_SavePortMsg(byPortNo, EN_DBG_MSG_SEND, ptSendCtrl->wSendLen, ptSendCtrl->pbySendBuf);
//            }
//        }
//    #endif

    if(CN_STATUS_SEND_BUSY == ptSendCtrl->wSendFlag)
    {
        Time_TimerBStart(&(ptSendCtrl->tTimerLineIdle), CN_INTERVAL_NET_IDLE);   // 链路空闲时间
        Time_TimerBStart(&(ptSendCtrl->tTimerAbandon),  CN_TIMEOUT_NET_SEND);   // 发送取消时间
        wMsgLen = ptSendCtrl->wSendLen;
        if(wMsgLen == Hard_Tcp_TxStartup(iTcpSockNo, ptSendCtrl->pbySendBuf, ptSendCtrl->wSendLen, byPortNo, EN_DBG_MSG_SEND))
        {
            // 发送成功
            Time_TimerBStop ( &ptSendCtrl->tTimerAbandon  );
            Time_TimerBReset( &ptSendCtrl->tTimerLineIdle );
            ptSendCtrl->wSendLen  = 0;
            ptSendCtrl->wSendRead = 0;
            ptSendCtrl->wSendFlag = CN_STATUS_SEND_IDLE;
            return;
        }
    }
    else
    {
//        bSend   = FALSE;
//        if(FALSE == bSend)
//        {
//            bSend = SG104_SendIgi( byPortNo );
//        }
//        if(FALSE == bSend)
//        {
//            bSend = SG104_Send_Authentication( byPortNo );
//        }
//        if(FALSE == bSend)
//        {
//            bSend = SG104_Send_ChargeRec( byPortNo );
//        }
//        if(FALSE == bSend)
//        {
//            bSend = SG104_Send_YX( byPortNo );
//        }
//        if(FALSE == bSend)
//        {
//            bSend = SG104_Send_UpDate( byPortNo );
//        }
//        if(FALSE == bSend)
//        {
//            bSend = SG104_Polling( byPortNo );
//        }
//        if(FALSE == bSend)
//        {
//            bSend = SG104_Send_LinkTest( byPortNo );
//        }
//        if(FALSE == bSend)
//        {
//            bSend = SG104_Send_AckFrame( byPortNo );
//        }
    }

    return;
}



//===========================================================
// 函数功能: 执行主函数
// 输入参数: 通讯口编号
// 输出参数: 无
// 返回值  :
//===========================================================
void SG104_Main( UINT8 byPortNo )
{
    u32      param;

    DJY_GetEventPara((ptu32_t *)&param,NULL);

    byPortNo = (u8)param;
    SG104_Init( byPortNo );

    while( 1 )
    {
        DJY_EventDelay( 100*mS );

        SG104_Proc( byPortNo );
        SG104_Send( byPortNo );
    }

    return;
}


//===========================================================
// 函数功能: 任务启动函数
// 输入参数: 通讯口编号
// 输出参数: 无
// 返回值  :
//===========================================================
int SG104_TaskStart( UINT8 byPortNo )
{
    UINT16          wEvttId;
    char            strTaskName[20]={"tMxx_SG104" };

    //if(byPortNo >= CN_NUM_PORT)
    //{
    //    return -1;
    //}

    strTaskName[2] = '0' + ( byPortNo / 10 );                   // 向上通讯口显示端口序号：十位
    strTaskName[3] = '0' + ( byPortNo % 10 );                   // 个位
    wEvttId = DJY_GetEvttId( strTaskName );
    if(CN_EVTT_ID_INVALID == wEvttId)
    {
        wEvttId = DJY_EvttRegist(EN_CORRELATIVE, CN_PRIO_RRS, 0, 0, (ptu32_t(*)(void))SG104_Main,
                                gs_SG104_TaskStack, CN_SG104_TASK_STACK_LEN, strTaskName);
        DJY_EventPop(wEvttId, NULL, 0, byPortNo, 0, 0);
    }
    else
    {
        ;
        ;// NOP
    }

    return wEvttId;
}
