/*================================================================================
 * 文件名称：EFS_File_Manager.c
 * 文件描述：在EFS简易文件系统中创建文件、删除文件、写文件
 * 文件版本: V1.00
 * 开发人员: 田润泽
 * 定版时间: 
 * 版本修订:
 * 修订人员: 
 *================================================================================*/

// include 关系
//#include "InstCell.h"
//#include "DebugShell.h"
#include "stdint.h"
#include "stddef.h"
#include "stdio.h"
#include "stdlib.h"
#include "endian.h"
#include "string.h"
#include "os.h"
#include "string.h"
#include <endian.h>
#include "pppdata.h"
#include <dirent.h>

#ifndef EFS_FILE_MODE
#define EFS_FILE_MODE (O_RDWR|O_CREAT)
#endif
#define EFS_FILE_MODE_READ      ("r")
#define EFS_FILE_MODE_UPDATE    ("a+")
#define EFS_FILE_SUFFIXES       (".txt")
#define EFS_CUR_DIR             ("/efs/")
#define EFS_NUMLEN      (10)
#define EFS_DATELEN     (30)
#define EFS_DIRLEN      (256)
#define EFS_SIZELIMIT   (0x8000)
#define EFS_AMOUNTLIMIT (32)

#define EFS_DIR_CONFIG  ("/efs/config.txt")

const tag_EFS_Private g_tag_EFS_Private[] = 
{
     // pDir_Content                 pDir_Prefix                pDir_Suffix         dwType                   dwAmount  dwMode                    bNoContinuous  bWithDate
    {"/efs/NetLog_Content.txt",      "/efs/Net_Log_",           ".txt",             EN_EFS_TYPE_NETLOG,      10,       EN_EFS_MODE_CONTINUOUS,   1,             0},
    //{"/efs/ChgRec_Content.txt",      "/efs/Charge_Record_",     ".txt",     EN_EFS_TYPE_CHGREC,      10,       EN_EFS_MODE_CONTINUOUS,   0,             1},
};
const UINT32 g_u32RecordFileNum = (sizeof(g_tag_EFS_Private)/sizeof(tag_EFS_Private));

//=================================================
// 函数功能：安装记录文件模块
//=================================================
BOOL EFS_Log_Init(fnEFSEventHook fHook)
{
    tag_EFS_Private* pt_EFS_Private;
    char* pDir_Content;
    char* pDir_Prefix;
    char* pDir_Suffix;
    BOOL  bWithDate;
    BOOL  bNoContinuous;

    char cTemp[EFS_DIRLEN] = {0};
    UINT32 dwLoop;
    UINT32 dwTemp;
    
    for(dwLoop = 0; dwLoop < g_u32RecordFileNum; dwLoop++)
    {
        pt_EFS_Private = &g_tag_EFS_Private[dwLoop];
        pDir_Content = pt_EFS_Private->pDir_Content;
        pDir_Prefix  = pt_EFS_Private->pDir_Prefix;
        pDir_Suffix  = pt_EFS_Private->pDir_Suffix;
        bWithDate    = pt_EFS_Private->bWithDate;
        dwTemp = EFS_SeekFileWithDate(cTemp, pDir_Content, pDir_Suffix, bWithDate);
        if(1 == dwTemp)
        {
            printf("%sx%s文件已初始化过,无需再次初始化.\r\n", pDir_Prefix, pDir_Suffix);
            continue;
        }
        else if(2 == dwTemp)
        {
            printf("%sx%s文件未初始化过,直接初始化.\r\n", pDir_Prefix, pDir_Suffix);
            EFS_FormatFile(dwLoop);
            if(fHook)
            {
                fHook((enEFSEvent)dwLoop);
            }
        }
        else if(0 == dwTemp)
        {
            printf("打开%sx%s文件路径失败.\r\n", pDir_Prefix, pDir_Suffix);
            EFS_FormatFile(dwLoop);
            if(fHook)
            {
                fHook((enEFSEvent)dwLoop);
            }
        }
    }
}

//=================================================
// 函数功能：写Net_Log.txt文件,定义Net_Log包含10个文件,保存当前目录的文件1个(NetLog_Content.txt)
//=================================================
UINT32 EFS_WriteFile(char* pContext, UINT32 dwLen, UINT32 dwType)
// 先open一个名字为"Net_Log_0.txt的文件"
// 如果写入的长度 > 32KB - 已写入长度 - 创建新文件"Net_Log_1.txt",一直可以创建到"Net_Log_9.txt"
// 如果Net_Log_9.txt写满了，则将Net_Log_0.txt删除，重新建立，继续写
// 初次上电调试的时候，要格式化一下盘，将当前目录设置为"Net_Log_0.txt"(写入文件)，并将目录中所有的Net_Log删除
{
    tag_EFS_Private* pt_EFS_Private;
    char* pDir_Content;
    char* pDir_Prefix;
    char* pDir_Suffix;
    UINT32 dwAmount, dwMode;
    BOOL  bWithDate;
    BOOL  bNoContinuous;

    char cCurFName[EFS_DIRLEN]  = {0};          // 当前的文件名称
    char cTemp[EFS_DIRLEN]      = {0};          // 临时空间
    char cTemp1[EFS_DIRLEN]     = {0};          // 临时空间1
    char cFileNum[EFS_NUMLEN]   = {0};          // 文件编号
    char cDate[EFS_DATELEN]     = {0};          // 系统时间
    INT32S iFile;                               // 文件系统返回值
    FILE* fd;                                   // 文件指针
    struct stat stNetLog;                       // 文件信息
    UINT32 dwCurSize;                           // 当前文件大小                      
    UINT32 dwRetLen;                            // 返回长度
    UINT32 dwTemp;                              // 临时变量

    if(!((dwType >= 0) && (dwType < g_u32RecordFileNum)))  {printf("文件类型不对.\r\n");return 0;}

    pt_EFS_Private = &g_tag_EFS_Private[dwType];
    pDir_Content = pt_EFS_Private->pDir_Content;
    pDir_Prefix  = pt_EFS_Private->pDir_Prefix;
    pDir_Suffix  = pt_EFS_Private->pDir_Suffix;
    dwAmount = pt_EFS_Private->dwAmount;
    dwMode   = pt_EFS_Private->dwMode;
    bWithDate = pt_EFS_Private->bWithDate;
    bNoContinuous = pt_EFS_Private->bNoContinuous;

    if(0 == dwLen)                                          {printf("写入%sx%s文件的长度必须大于0.\r\n", pDir_Prefix, pDir_Suffix);return 0;}
    if(dwLen > EFS_SIZELIMIT)                               {printf("写入%sx%s文件的长度不能超过%dKB.\r\n", pDir_Prefix, pDir_Suffix, EFS_SIZELIMIT/0x400);return 0;}
    
    iFile = open(pDir_Content, EFS_FILE_MODE, 0);
    if(!iFile)                                              {printf("打开%s文件失败.\r\n", pDir_Content);return 0;}
    read(iFile, cTemp, EFS_DIRLEN);
    close(iFile);   

    dwTemp = strlen(cTemp) - strlen(pDir_Prefix);

    EFS_SubStr(cFileNum, cTemp, strlen(pDir_Prefix), dwTemp);

    if(!EFS_IsDigitString(cFileNum))                        {printf("文件%s, 的编号获取错误, 不是数字.\r\n", cCurFName);return 0;}

    switch(dwMode)
    {
        case EN_EFS_MODE_CONTINUOUS:
            EFS_SeekFileWithoutDate(cCurFName, cTemp, pDir_Suffix);
            iFile = open(cCurFName, EFS_FILE_MODE, 0);
            if(!iFile)                                      {printf("打开%s文件失败.\r\n", cCurFName);return 0;}
            if((-1) == fstat(iFile, &stNetLog))             {printf("读取%s文件状态信息失败.\r\n", cCurFName);return 0;}
            dwCurSize = (UINT32)stNetLog.st_size;    
            close(iFile);
            
            if((dwLen + dwCurSize) > EFS_SIZELIMIT)
            {
                if((dwTemp = (UINT32)EFS_atoi(cFileNum)) >= dwAmount)
                {
                    if(bNoContinuous)
                    {
                        itoa(++dwTemp, cFileNum, 10);
                        sprintf(cTemp1, "%s%d%s", pDir_Prefix, (dwTemp - dwAmount), pDir_Suffix);
                    }
                    else
                    {
                        sprintf(cFileNum, "1");
                    } 
                }
                else
                {
                    itoa(++dwTemp, cFileNum, 10);
                    sprintf(cTemp1, "%s%d%s", pDir_Prefix, dwTemp, pDir_Suffix);
                }
                memset(cTemp, 0, sizeof(char)*EFS_DIRLEN);
                if(bWithDate)
                {
                    EFS_GetSysTime(cDate);
                    sprintf(cTemp, "%s%s_%s%s", pDir_Prefix, cFileNum, cDate, pDir_Suffix);
                }
                else
                {
                    sprintf(cTemp, "%s%s%s", pDir_Prefix, cFileNum, pDir_Suffix);
                }
                remove(pDir_Content);
                if(bNoContinuous)
                {
                    EFS_DelFileWithoutDate(cTemp1);
                }
                else
                {
                    EFS_DelFileWithoutDate(cTemp);
                }

                iFile = open(pDir_Content, EFS_FILE_MODE, 0);
                if(!iFile)                                  {printf("创建%s文件失败.\r\n", pDir_Content);return 0;}
                write(iFile, cTemp, strlen(pDir_Prefix) + strlen(cFileNum));
                close(iFile);
            }
            else
            {
                strcpy(cTemp, cCurFName);
            }
            break;
        case EN_EFS_MODE_SEPARATE:  
            EFS_SeekFileWithDate(cCurFName, cTemp, pDir_Suffix, bWithDate);
            
            remove(pDir_Content);

            if(((dwTemp = (UINT32)EFS_atoi(cFileNum)) > dwAmount) && bNoContinuous)
            {
                sprintf(cTemp, "%s%d", pDir_Prefix, (dwTemp - dwAmount));
            }
            else
            {
                sprintf(cTemp, "%s%s", pDir_Prefix, cFileNum);
            }
                
            EFS_DelFileWithDate(cTemp, strlen(cTemp));

            memset(cTemp, 0, sizeof(char)*EFS_DIRLEN);
            if(bWithDate)
            {
                EFS_GetSysTime(cDate);
                sprintf(cTemp, "%s%s_%s%s", pDir_Prefix, cFileNum, cDate, pDir_Suffix);
            }
            else
            {
                sprintf(cTemp, "%s%s%s", pDir_Prefix, cFileNum, pDir_Suffix);
            }
            
            if((dwTemp = (UINT32)EFS_atoi(cFileNum)) >= dwAmount)
            {
                if(bNoContinuous)
                {
                    itoa(++dwTemp, cFileNum, 10);
                }
                else
                {
                    sprintf(cFileNum, "1");
                }
            }
            else
            {
                itoa(++dwTemp, cFileNum, 10);
            }
            
            sprintf(cTemp1, "%s%s", pDir_Prefix, cFileNum);
            iFile = open(pDir_Content, EFS_FILE_MODE, 0);
            if(!iFile)                                  {printf("创建%s文件失败.\r\n", pDir_Content);return 0;}
            write(iFile, cTemp1, strlen(pDir_Prefix) + strlen(cFileNum));
            close(iFile);

            break;
        default:
            break;
    }
    
    fd = fopen(cTemp, EFS_FILE_MODE_UPDATE);
    if(!fd)                                                     {printf("打开%s文件失败.\r\n", cTemp);return 0;}
    dwRetLen = fwrite(pContext, sizeof(char), dwLen, fd);
    fclose(fd);

    return((dwRetLen == dwLen)? dwRetLen: 0);
    
}

//=================================================
// 函数功能：Charge_Record.txt文件格式化
//=================================================
BOOL EFS_FormatFile(UINT32 dwType)
{
    tag_EFS_Private* pt_EFS_Private;
    char* pDir_Content;
    char* pDir_Prefix;
    char* pDir_Suffix;
    UINT32 dwAmount, dwMode;
    BOOL  bWithDate;
    BOOL  bNoContinuous;

    char cTempFName[EFS_DIRLEN] = {0};
    //char cbuff[10] = {0};
    FILE* fd;
    UINT32 dwLoop;


    if(!((dwType >= 0) && (dwType < g_u32RecordFileNum)))  {printf("文件类型不对.\r\n");return FALSE;}
    
    pt_EFS_Private = &g_tag_EFS_Private[dwType];
    pDir_Content = pt_EFS_Private->pDir_Content;
    pDir_Prefix  = pt_EFS_Private->pDir_Prefix;
    pDir_Suffix  = pt_EFS_Private->pDir_Suffix;
    dwAmount = pt_EFS_Private->dwAmount;
    dwMode   = pt_EFS_Private->dwMode;
    bWithDate = pt_EFS_Private->bWithDate;
    bNoContinuous = pt_EFS_Private->bNoContinuous;
    
    if(dwAmount > EFS_AMOUNTLIMIT)                          {printf("%sx%s文件的总个数超过%d个.\r\n", pDir_Prefix, pDir_Suffix, EFS_AMOUNTLIMIT);return FALSE;}

    memset(cTempFName, 0, sizeof(char)*EFS_DIRLEN);
    sprintf(cTempFName, "%s", pDir_Prefix);
    while((UINT32)1 == EFS_DelFileWithDate(cTempFName, strlen(cTempFName)));
    remove(pDir_Content);
    sprintf(cTempFName, "%s%s", pDir_Prefix, "1");    

    fd = fopen(pDir_Content, EFS_FILE_MODE_UPDATE);
    fwrite(cTempFName, sizeof(char), strlen(cTempFName), fd);
    fclose(fd);

    return TRUE;
    
}

UINT32 EFS_DelFileWithDate(char* pcFileName, UINT32 dwLen)
{
    DIR *ptDir;
    struct dirent *ptDirentInfo;
    char cTempFName[EFS_DIRLEN] = {0};

    ptDir = opendir(EFS_CUR_DIR);
    if(ptDir)
    {
        while((ptDirentInfo = readdir(ptDir)) != NULL)
        {
        	memset(cTempFName, 0, EFS_DIRLEN);
            strcpy(cTempFName, EFS_CUR_DIR);
            strcpy(cTempFName + strlen(EFS_CUR_DIR), ptDirentInfo->d_name);
            if(0 == (strncmp(pcFileName, cTempFName, dwLen)))
            {
                remove(cTempFName);
                closedir(ptDir);
                return 1;
            }
        }
        printf("没有找到文件名于%s相似的文件.\r\n", pcFileName);
        closedir(ptDir);
        return 2;
    }
    closedir(ptDir);
    printf("打开路径%s失败.\r\n", EFS_CUR_DIR);
    return 0;
}

UINT32 EFS_DelFileWithoutDate(char* pcFileName)
{
    remove(pcFileName);

    return 0;
}


UINT32 EFS_SeekFileWithDate(char* pcName, char* pcFileName, char* pDir_Suffix, BOOL bWithDate)
{
    DIR *ptDir;
    struct dirent *ptDirentInfo;
    char cTempFName[EFS_DIRLEN] = {0};
    char cDate[EFS_DATELEN]     = {0};          // 系统时间

    ptDir = opendir(EFS_CUR_DIR);
    if(ptDir)
    {
        while((ptDirentInfo = readdir(ptDir)) != NULL)
        {
            memset(cTempFName, 0, EFS_DIRLEN);
            strcpy(cTempFName, EFS_CUR_DIR);
            strcpy(cTempFName + strlen(EFS_CUR_DIR), ptDirentInfo->d_name);
            if(0 == strncmp(pcFileName, cTempFName, strlen(pcFileName)))
            {
                strcpy(pcName, cTempFName);
                closedir(ptDir);
                return 1;
            }
        }
        printf("没有找到文件名于%s相似的文件.\r\n", pcFileName);
        if(bWithDate)
        {
            EFS_GetSysTime(cDate);
            sprintf(pcName, "%s_%s%s", pcFileName, cDate, pDir_Suffix);
        }
        else
        {
            sprintf(pcName, "%s%s", pcFileName, pDir_Suffix);
        }
        closedir(ptDir);
        return 2;
    }
    closedir(ptDir);
    printf("打开路径%s失败.\r\n", EFS_CUR_DIR);
    return 0;
}

UINT32 EFS_SeekFileWithoutDate(char* pcName, char* pcFileName, char* pDir_Suffix)
{
    char cTempFName[EFS_DIRLEN] = {0};

    sprintf(pcName, "%s%s", pcFileName, pDir_Suffix);

    return 0;
}


//=================================================
// 函数功能：config.txt文件删除
//=================================================
BOOL EFS_DelConfig(void)
{
// 每次后台下发了config.txt的内容，删除原有的config.txt，创建新的config.txt
// 如果config.txt存在，则按照config.txt里面的内容设置，若不存在，则读取EEP里面的
// 触摸屏上做一个按钮，删除config.txt文件，手动设置参数
    return (EFS_DelFileWithDate(EFS_DIR_CONFIG, strlen(EFS_DIR_CONFIG)));
}


//=================================================
// 函数功能：读config文件，读取的长度由功能组件定义
//=================================================
BOOL EFS_ReadConfig(UINT32* pdwNum, UINT8* pbyHostIP, UINT8* pbyNetGate)
// 读取config.txt文件的方法由应用定义
{
    return TRUE;
}

//=================================================
// 函数功能：从一个字符串中截取从第m个字符开始，长度为n个字符的字符串
//=================================================
BOOL EFS_SubStr(char* pcDst, char* pcSrc, UINT32 dwStart, UINT32 dwLen)
{
    UINT32 dwLen_Temp;
    char*  pcTemp;
    UINT32 dwOffset = 0;
    
    pcTemp = pcSrc + dwStart;//定义指针变量指向需要提取的字符的地址
    dwLen_Temp = strlen(pcTemp);//求字符串长度

    dwLen = (dwLen > dwLen_Temp) ? dwLen_Temp : dwLen;
    while(dwLen != 0)
    {
        pcDst[dwOffset] = pcSrc[dwOffset + dwStart];
        dwLen--;
        dwOffset++;
    }//复制字符串到dst中
    pcDst[dwOffset]='\0';
    return 0;
}

//=================================================
// 函数功能：判断一个字符串是否为纯数字的字符串
//=================================================
BOOL Standard_IsDigitString(char* pInput)
{
    UINT32 dwLoop;
    
    for(dwLoop = 0; isspace(pInput[dwLoop]); dwLoop++);//跳过空白符;

    if(pInput[dwLoop] == '+' || pInput[dwLoop]== '-')//跳过符号
    {
        dwLoop++;
    }
    
    for( ; *(pInput + dwLoop) != '\0'; dwLoop++)
    {
        if(!isdigit(*(pInput + dwLoop)))
        {
            return FALSE;
        }
    }
    return TRUE;
}

//=================================================
// 函数功能：将字符串转成有符号整形
//=================================================
INT32S Standard_atoi (char* pInput)
{
    UINT32 dwLoop = 0;
    INT32S dwSign, dwRet;

    for(dwLoop = 0; isspace(pInput[dwLoop]); dwLoop++);//跳过空白符;
    
    dwSign = (pInput[dwLoop] == '-')? -1 : 1;
    
    if(pInput[dwLoop] == '+' || pInput[dwLoop]== '-')//跳过符号
    {
        dwLoop++;
    }
    
    for(dwRet = 0; isdigit(pInput[dwLoop]); dwLoop++)
    {
        dwRet = 10*dwRet + (pInput[dwLoop] - '0');//将数字字符转换成整形数字
    }
    return (dwSign * dwRet);
}

//=================================================
// 函数功能：判断一个字符串是否为纯数字的字符串
//=================================================
BOOL EFS_IsDigitString(char* pInput)
{
    UINT32 dwLoop;
    
    for(dwLoop = 0; *(pInput + dwLoop) != '\0'; dwLoop++)
    {
        if(!isdigit(*(pInput + dwLoop)))
        {
            return FALSE;
        }
    }
    return TRUE;
}

//=================================================
// 函数功能：将字符串转成有符号整形
//=================================================
UINT32 EFS_atoi (char* pInput)
{
    UINT32 dwLoop = 0;
    UINT32 dwSign, dwRet;
    
    for(dwRet = 0; isdigit(pInput[dwLoop]); dwLoop++)
    {
        dwRet = 10*dwRet + (pInput[dwLoop] - '0');//将数字字符转换成整形数字
    }
    return dwRet;
}

// ============================================================================
// 函数功能：打印系统时间
// 输入参数：无
// 输出参数：无
// 返回值：  无
// ============================================================================
void EFS_GetSysTime( char *cTime )
{
    tagTimeBCD  tTimeBCD;

    // 获取系统时间(包括SPI年月日和FPGA时分秒)
    Time_CopySystemTimeBCD( &tTimeBCD );

    // 输出调试信息
    sprintf(cTime, "%2.2X-%2.2X-%2.2X_%2.2X:%2.2X:%2.2X_%X%2.2X",
            tTimeBCD.byYear_L, tTimeBCD.byMonth, tTimeBCD.byDay, tTimeBCD.byHour,
            tTimeBCD.byMinute, tTimeBCD.bySecond,tTimeBCD.byMS_H,tTimeBCD.byMS_L );
}


