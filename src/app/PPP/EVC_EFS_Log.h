/*================================================================================
 * 文件名称：EFS_File_Manager.h
 * 文件描述：在EFS简易文件系统中创建文件、删除文件、写文件
 * 文件版本: V1.00
 * 开发人员: 田润泽
 * 定版时间: 
 * 版本修订:
 * 修订人员: 
 *================================================================================*/
 
#ifndef _EVC_EFS_LOG_H
#define _EVC_EFS_LOG_H

// include 包含关系
//#include "Resource.h"
#include "sunri_types.h"
#include "pppdata.h"

#define EVCEFS_LOGNUMLIMIT      (64)

enum _EN_NETLOG_TYPE
{
    EN_NETLOG_TYPE_START = 0,
// 拨号状态
    EN_NETLOG_TYPE_IPGET = EN_NETLOG_TYPE_START,// 成功获得公网IP
    EN_NETLOG_TYPE_IPRELEASE,               // 公网IP丢失
    EN_NETLOG_TYPE_LINKUP,                  // 和基站建立通讯
    EN_NETLOG_TYPE_LINKDOWN,                // 和基站通讯丢失
// PPP功能模块和上电重启相关逻辑
    EN_NETLOG_TYPE_POWEROFF,                // 10分钟未建立通讯,重启装置
    EN_NETLOG_TYPE_POWERON,                 // 装置初次上电
    EN_NETLOG_TYPE_READCONFIG,              // 初次读取枚举信息成功
// 4G模块Hard Reset的原因
    EN_NETLOG_TYPE_TCP50TIMES,              // Tcp50次握手都没有成功
    EN_NETLOG_TYPE_TCPOVERTIME,             // Tcp5分钟没有连接成功
    EN_NETLOG_TYPE_RECALL3TIMES,            // 三次拨号不成功，重启4G模块
    EN_NETLOG_TYPE_P15TIMES,                // 协议启动帧发送了15次,重启4G模块
    EN_NETLOG_TYPE_HARDRESET,               // 4G模块硬件重启
// Tcp相关
    EN_NETLOG_TYPE_TCPUP,                   // Tcp连接成功
    EN_NETLOG_TYPE_TCPREADDOWN,             // Tcp由于读取报文不成功而主动断开
    EN_NETLOG_TYPE_TCPSENDDOWN,             // Tcp由于发送报文不成功而主动断开
// SG104网络状态
    EN_NETLOG_TYPE_SG104TRANSTART,          // 业务层启动传输
    EN_NETLOG_TYPE_SG104DOWNFORTCP,         // 由于TCP断开了，SG104网络异常
    EN_NETLOG_TYPE_SG104DOWNFOR4G,          // 由于4G模块掉线,SG104网络异常并断开Tcp
    EN_NETLOG_TYPE_SG104DOWNFORS,           // 由于接收心跳报文的间隔超时，SG104网络异常  
// 4G模块从在线到掉线
    EN_NETLOG_TYPE_TRAILEDGE,               // 4G无线模块由在线状态变成掉线状态
    
    EN_NETLOG_TYPE_END,                     // 结束
};

typedef struct
{
    char* pLog;
    UINT32 dwType;
}tag_EVC_EFSLogTable;

typedef struct
{
    tag_EVC_EFSLogTable* pt_EFSLogTable;
    UINT32 dwFramAddr;
    UINT32 dwLogNum;
    BOOL bWithTimes;
}tag_EVC_EFSLogCfg;

typedef struct
{
    const char* pcCellName;
    tag_EVC_EFSLogCfg* EVC_EFSLogCfg;
    UINT32 dwLogTypeNum;
}tag_EVC_EFSLog;


typedef struct
{
    UINT32  dwLogTimes[EVCEFS_LOGNUMLIMIT];
    UINT32  dwTypeFlag[EVCEFS_LOGNUMLIMIT];
}tag_EVC_EFSLog_Ram;

typedef struct
{
    UINT8                   byRamScan1;
	UINT32				    MyKO_Tag; 				        // KO访问标志。
    tag_EVC_EFSLog_Ram      tCellRam[EN_EFS_TYPE_END];      // 组件私有变量
    const tag_EVC_EFSLog    *ptCellCfg;
    UINT8                   byRamScan2;
}tagEVC_EFSLog_P;
typedef struct GlobalCellObj tagGlobalCellObj;
extern  tagGlobalCellObj        *s_ptEVC_EFSlog_C01;                // 组件私有变量
extern  const   tag_EVC_EFSLog   CNST_tag_EFSLog;            // 组件入口参数表实例化


// 外部函数声明
extern BOOL EFS_Init_Event(enEFSEvent event);
extern tagGlobalCellObj *ModuleInstall_EVC_EFSLog( const tag_EVC_EFSLog *ptApp );
extern void EVC_EFSLog_TaskStart(void);
extern void EVC_EFSLog_Init( tagGlobalCellObj *ptThis );
extern BOOL EVC_EFSLog_TimesInit(tagEVC_EFSLog_P *ptMyCellTag, UINT32 dwType);
extern BOOL EVC_EFSLog_ReadTimesFRAM(tagEVC_EFSLog_P *ptMyCellTag);
extern BOOL EVC_EFSLog_TimesRefresh(tagEVC_EFSLog_P *ptMyCellTag, UINT32 dwType, UINT32 dwNum);
extern void EVC_EFSLog_TimesResetWeekly(tagEVC_EFSLog_P *ptMyCellTag, UINT32 dwType);
extern BOOL EVC_EFSLog_FlagInit(tagEVC_EFSLog_P *ptMyCellTag, UINT32 dwType);
extern UINT32 EVC_EFSLog_FlagScan(tagEVC_EFSLog_P *ptMyCellTag, UINT32 dwType);
extern void EVC_EFSLog_FlagSet_Ext(UINT32 dwType, UINT32 dwNum);
extern UINT32 EVC_EFSLog_WriteOneRecord(tagEVC_EFSLog_P *ptMyCellTag, UINT32 dwType, UINT32 dwNum);
extern void EVC_EFSLog( void );

/*================================================================================*/
#endif // _EVC_EFS_LOG_H
