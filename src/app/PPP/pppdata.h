#ifndef _PPPDATE_H
#define _PPPDATE_H

#include <stdint.h>
#include <object.h>
#include "EFS_File_Manager.h"
#include "EVC_EFS_Log.h"
#include "PPP_Scan.h"
#include "sunri_types.h"
#include "SunriTime.h"
#include "TcpSop.h"
#include "timer_b.h"

// ms时间定义(单位:ms)
//#define CN_MS_T_1MS                 (1)
//#define CN_MS_T_2MS                 (2)
//#define CN_MS_T_3MS                 (3)
//#define CN_MS_T_4MS                 (4)
//#define CN_MS_T_5MS                 (5)
//#define CN_MS_T_6MS                 (6)
//#define CN_MS_T_7MS                 (7)
//#define CN_MS_T_8MS                 (8)
//#define CN_MS_T_9MS                 (9)
#define CN_MS_T_10MS                (10)
//#define CN_MS_T_20MS                (CN_MS_T_10MS*2)
//#define CN_MS_T_25MS                (CN_MS_T_10MS*2.5)
//#define CN_MS_T_30MS                (CN_MS_T_10MS*3)
//#define CN_MS_T_40MS                (CN_MS_T_10MS*4)
//#define CN_MS_T_50MS                (CN_MS_T_10MS*5)
//#define CN_MS_T_60MS                (CN_MS_T_10MS*6)
//#define CN_MS_T_70MS                (CN_MS_T_10MS*7)
//#define CN_MS_T_80MS                (CN_MS_T_10MS*8)
//#define CN_MS_T_90MS                (CN_MS_T_10MS*9)
//#define CN_MS_T_100MS               (CN_MS_T_10MS*10)
//#define CN_MS_T_110MS               (CN_MS_T_10MS*11)
//#define CN_MS_T_140MS               (CN_MS_T_10MS*14)
//#define CN_MS_T_160MS               (CN_MS_T_10MS*16)
//#define CN_MS_T_200MS               (CN_MS_T_10MS*20)
//#define CN_MS_T_250MS               (CN_MS_T_10MS*25)
//#define CN_MS_T_400MS               (CN_MS_T_10MS*40)
//#define CN_MS_T_500MS               (CN_MS_T_10MS*50)
//#define CN_MS_T_600MS               (CN_MS_T_10MS*60)
//#define CN_MS_T_1300MS              (CN_MS_T_10MS*130)
//#define CN_MS_T_1500MS              (CN_MS_T_10MS*150)
//#define CN_MS_T_2500MS              (CN_MS_T_10MS*250)
#define CN_MS_T_1S                  (CN_MS_T_10MS*100)
//#define CN_MS_T_2S                  (CN_MS_T_1S*2)
//#define CN_MS_T_3S                  (CN_MS_T_1S*3)
//#define CN_MS_T_4S                  (CN_MS_T_1S*4)
//#define CN_MS_T_5S                  (CN_MS_T_1S*5)
//#define CN_MS_T_6S                  (CN_MS_T_1S*6)
//#define CN_MS_T_7S                  (CN_MS_T_1S*7)
//#define CN_MS_T_8S                  (CN_MS_T_1S*8)
//#define CN_MS_T_9S                  (CN_MS_T_1S*9)
//#define CN_MS_T_10S                 (CN_MS_T_1S*10)
//#define CN_MS_T_11S                 (CN_MS_T_1S*11)
//#define CN_MS_T_12S                 (CN_MS_T_1S*12)
//#define CN_MS_T_13S                 (CN_MS_T_1S*13)
//#define CN_MS_T_14S                 (CN_MS_T_1S*14)
//#define CN_MS_T_15S                 (CN_MS_T_1S*15)
//#define CN_MS_T_20S                 (CN_MS_T_1S*20)
//#define CN_MS_T_30S                 (CN_MS_T_1S*30)
//#define CN_MS_T_40S                 (CN_MS_T_1S*40)
//#define CN_MS_T_50S                 (CN_MS_T_1S*50)
//#define CN_MS_T_60S                 (CN_MS_T_1S*60)
//#define CN_MS_T_70S                 (CN_MS_T_1S*70)
//#define CN_MS_T_80S                 (CN_MS_T_1S*80)
//#define CN_MS_T_90S                 (CN_MS_T_1S*90)
//#define CN_MS_T_100S                (CN_MS_T_1S*100)
//#define CN_MS_T_120S                (CN_MS_T_1S*120)
//#define CN_MS_T_300S                (CN_MS_T_1S*300)
#define CN_MS_T_600S                (CN_MS_T_1S*600)
//#define CN_MS_T_900S                (CN_MS_T_1S*900)
#define CN_RAMSCAN_LEN_NAME     ( 128 )                         // 扫描内存片名称长度
//typedef struct
//{
//    u8            byYear_L;                       // 年(低位)
//    u8            byYear_H;                       // 年(高位) 默认为0x20
//    u8            byMonth;                        // 月
//    u8            byDay;                          // 日
//    u8            byHour;                         // 时
//    u8            byMinute;                       // 分
//    u8            bySecond;                       // 秒
//    u8            byMS_L;                         // 毫秒(低位)
//    u8            byMS_H;                         // 毫秒(高位)
//    u8            byWeek;                         // 星期
//    u8            byRes[2];                       // 备用字节(配齐4的整数倍)
//}tagTimeBCD;
//typedef struct
//{
//    u32      dwNum_Log;
//    tagTimeBCD  tTime_Log;
//}sNum_tagTimeBCD;


enum _DBG_PARAM_
{
    EN_DBG_MSG_RECV = 0,                                        // 0:规约的接收报文
    EN_DBG_MSG_SEND,                                            // 1:规约的发送报文
    EN_DBG_DATA_RECV,                                           // 2:通道的接收数据
    EN_DBG_DATA_SEND,                                           // 3:通道的发送数据

    EN_DBG_INSIDE_RECV = 32,                                    // 0+32:内部通讯口的接收报文
    EN_DBG_INSIDE_SEND,                                         // 1+32:内部通讯口的发送报文
};

// 全局变量标志配置
enum _BOOLRAM_USAGE_NUMBER
{
    EN_BOOLRAM_SN_START=0,                  // 定义开始，本条不能修改
//----------------------------------------------
// 平台专用标记 begin
//----------------------------------------------
    EN_BOOL_SN_PINNO_INVALID,               // 无效虚端子
    EN_BOOL_SN_RLY_START_WAVE,              // 录波启动开出
    EN_BOOL_SN_SYS_LOCK_SAM,                // 本板系统闭锁
    EN_BOOL_SN_SYS_LOCK_USE,                // 合成系统闭锁
    EN_BOOL_SN_SYS_FLG_TEST,                // 装置检修标志
//----------------------------------------------
// 平台专用标记 end

//----------------------------------------------
    // 启动汇总
    EN_BOOL_STR_ALL_OC,                     // 过流相关保护总启动标志
    EN_BOOL_STR_ALL_RLY,                    // 总保护启动标志
    EN_BOOL_STR_ALL_RLY_EX,                 // 总保护启动标志(延时返回，保护启动录波使用)
    // 动作汇总
    EN_BOOL_OP_ALL_OC,                      // 过流相关保护总动作标志
    EN_BOOL_OP_ALL_TRIP,                    // 保护跳闸总标志(跳闸信号继电器LED使用，汇总所有跳闸bool)
    EN_BOOL_OP_ALL_CLOSE,                   // 保护合闸总标志(合闸信号继电器LED使用，汇总所有合闸bool)
    EN_BOOL_OP_ALL_RLY,                     // 总保护动作标志(来自于跳闸合闸汇总，经过延时返回给启动继电器)
    EN_BOOL_OP_ALL_RLY_EX,                  // 总保护动作标志(延时返回，保护动作启动继电器)
    // 告警动作汇总
    EN_BOOL_OP_ALL_ALM,                     // 总告警动作汇总
    // 其他汇总
    EN_BOOL_FLG_BS_PTBrk,                   // 闭锁PT断线汇总
    EN_BOOL_FLG_BHSTA_Recls,                // 保护启动重合闸标志汇总
    EN_BOOL_FLG_BHSTA_Recls_EX,             // 保护启动重合闸标志汇总展宽
    EN_BOOL_FLG_BHBS_Recls,                 // 保护闭锁重合闸标志汇总
    // 已汇总到永跳中
    EN_BOOL_FLG_YTBHBS_Recls,               // 永跳GOOSE开入闭锁重合闸合成标志标志

//----------------------------------------------
    // 事故总组件
    EN_BOOL_OP_FAULSIG,                     // 事故总动作标志
    // 有流检测
    EN_BOOL_FLG_HACURA,                     // A相电流有流标志
    EN_BOOL_FLG_HACURB,                     // B相电流有流标志
    EN_BOOL_FLG_HACURC,                     // C相电流有流标志

    // 方向相关
    EN_BOOL_FLG_NORJY,                      // 记忆电压正常标志
    EN_BOOL_FLG_HACURABC,                   // ABC任一相有流标志
    EN_BOOL_FLG_DIRPA,                      // A相正方向标志
    EN_BOOL_FLG_DIRPB,                      // B相正方向标志
    EN_BOOL_FLG_DIRPC,                      // C相正方向标志

    // 复压相关
    EN_BOOL_FLG_VOLENA,                     // 复压开放标志

    // 过流1~3段
    EN_BOOL_SW_OC1,                         // 过流Ⅰ段投退标志
    EN_BOOL_SW_OC2,                         // 过流Ⅱ段投退标志
    EN_BOOL_SW_OC3,                         // 过流Ⅲ段投退标志
    EN_BOOL_SW_OCIP,                        // 过流Ⅲ段反时限投退标志
    EN_BOOL_STR_OC1,                        // 过流Ⅰ段启动标志
    EN_BOOL_STR_OC2,                        // 过流Ⅱ段启动标志
    EN_BOOL_STR_OC3,                        // 过流Ⅲ段启动标志
    EN_BOOL_STR_OCIP,                       // 过流Ⅲ段反时限启动标志
    EN_BOOL_OP_OC1,                         // 过流Ⅰ段动作标志
    EN_BOOL_OP_OC2,                         // 过流Ⅱ段动作标志
    EN_BOOL_OP_OC3,                         // 过流Ⅲ段动作标志
    EN_BOOL_OP_OCIP,                        // 过流Ⅲ段反时限动作标志

    // 过电压
    EN_BOOL_SW_OV,                          // 过电压投退标志
    EN_BOOL_STR_OV,                         // 过电压启动标志
    EN_BOOL_OP_OV,                          // 过电压动作标志
    EN_BOOL_ALM_OV,                         // 过电压告警标志
    // 低电压
    EN_BOOL_SW_LV,                          // 低电压投退标志
    EN_BOOL_STR_LV,                         // 低电压启动标志
    EN_BOOL_OP_LV,                          // 低电压动作标志
    EN_BOOL_ALM_LV,                         // 低电压告警标志

    // EVC-保护组件
    EN_BOOL_SN_EVC_OCP_ACT,                 // 过流保护动作全局标志
    EN_BOOL_SN_EVC_OVP_ACT,                 // 过压保护动作全局标志
    EN_BOOL_SN_EVC_UVP_ACT,                 // 欠压保护动作全局标志

    // EVC-充电控制组件
    EN_BOOL_SN_EVC_CH_01,                   // 充电开始命令
    EN_BOOL_SN_EVC_CH_02,                   // 充电开始命令
    EN_BOOL_SN_EVC_CH_03,                   // 充电开始命令
    EN_BOOL_SN_EVC_CH_04,                   // 充电开始命令
    EN_BOOL_SN_EVC_Stop_01,                 // 手动停止充电命令
    EN_BOOL_SN_EVC_Stop_02,                 // 手动停止充电命令
    EN_BOOL_SN_EVC_Stop_03,                 // 手动停止充电命令
    EN_BOOL_SN_EVC_Stop_04,                 // 手动停止充电命令

    EN_BOOL_SN_EVC_Charge_01,               // 开始充电标志
    EN_BOOL_SN_EVC_Charge_02,               // 开始充电标志
    EN_BOOL_SN_EVC_Charge_03,               // 开始充电标志
    EN_BOOL_SN_EVC_Charge_04,               // 开始充电标志
    EN_BOOL_SN_EVC_Over_01,                 // 充电结束标志
    EN_BOOL_SN_EVC_Over_02,                 // 充电结束标志
    EN_BOOL_SN_EVC_Over_03,                 // 充电结束标志
    EN_BOOL_SN_EVC_Over_04,                 // 充电结束标志
    EN_BOOL_SN_EVC_Suspend_01,              // 充电中止标志
    EN_BOOL_SN_EVC_Suspend_02,              // 充电中止标志
    EN_BOOL_SN_EVC_Suspend_03,              // 充电中止标志
    EN_BOOL_SN_EVC_Suspend_04,              // 充电中止标志
    EN_BOOL_SN_EVC_Suspend_LED1,            // 充电中止标志
    EN_BOOL_SN_EVC_Start_Err_01,            // 充电启动失败标志
    EN_BOOL_SN_EVC_Start_Err_02,            // 充电启动失败标志
    EN_BOOL_SN_EVC_Start_Err_03,            // 充电启动失败标志
    EN_BOOL_SN_EVC_Start_Err_04,            // 充电启动失败标志
    EN_BOOL_SN_EVC_Close_Err_01,            // 合接触器失败标志
    EN_BOOL_SN_EVC_Close_Err_02,            // 合接触器失败标志
    EN_BOOL_SN_EVC_Open_Err_01,             // 分接触器失败标志
    EN_BOOL_SN_EVC_Open_Err_02,             // 分接触器失败标志

    EN_BOOL_SN_EVC_Valid_IC_01,             // 充电卡有效
    EN_BOOL_SN_EVC_Cash_IC_01,              // 充电卡余额充足

    EN_BOOL_SN_EVC_AppStartCmd_01,          // App遥控启动
    EN_BOOL_SN_EVC_AppStopCmd_01,           // App遥控停止
    EN_BOOL_SN_EVC_CardStartCmd_01,         // Card启动
    EN_BOOL_SN_EVC_CardStopCmd_01,          // Card停止
    EN_BOOL_SN_EVC_FULL,                 // 充满标志

    // 通信组件
    EN_BOOL_SN_EVC_RF_CommOK,               // 电表通信正常
    EN_BOOL_SN_EVC_CardAuthenUp,            // 有人刷卡
    EN_BOOL_SN_EVC_REMOTE_UPDATE,           // 远程升级命令解析
    EN_BOOL_SN_EVC_RemoteUpdate_StartTask,  // 远程升级任务启动
    EN_BOOL_SN_EVC_SelfChk_Err,             // 自检
    EN_BOOL_SN_EVC_AuthenFlag,              // 鉴权标志位
    EN_BOOL_SN_EVC_Account_Unused,          // 该卡不在使用中
    EN_BOOL_SN_EVC_Network_Err,             // 网络通信异常
    EN_BOOL_SN_EVC_Network_OK,              // 网络通信异常

    EN_BOOL_SN_EVC_Whm_Comm_01,             // 电表通信正常
    EN_BOOL_SN_EVC_Whm_CommErr_01,          // 电表通信异常

    EN_BOOL_SN_EVC_Whm_Comm_02,             // 电表通信正常
    EN_BOOL_SN_EVC_Whm_CommErr_02,          // 电表通信异常

    EN_BOOL_SN_EVC_TempErr,                         // 过温故障标志字
    EN_BOOL_SN_EVC_LockErr,                             // 电子锁故障标志字
    // 防抖后开入量
    EN_BOOL_SN_EMERGENCY,           // 急停防抖后
    EN_BOOL_SN_THND,                //避雷器异常防抖后
    EN_BOOL_SN_DOOR,                // 门禁开入防抖后

    EN_BOOL_SN_DLQ,                 // 断路器正常合位

    EN_BOOL_SN_CON_HW_01,                   // 输出接触器合位开入01
    EN_BOOL_SN_CON_HW_02,                   // 输出接触器合位开入02
    // 急停防抖后

    EN_BOOL_SN_EVC_SaveEVCData_01,          // dwSaveEVCData;   // 充电记录标志

// 组件输出
    EN_BOOL_SN_EVC_Measure_Over_01,         // dwMeasure_Over;  // 计费结束标志位

    EN_BOOL_SN_EVC_OptOutTime_01,

// tianrz add for sibida
    EN_BOOL_SN_EVC_CARDAUTHENTICATIONDOWN,
    EN_BOOL_SN_EVC_APPAUTHENTICATIONDOWN,
// --
// tianrz add for sibida
    EN_BOOL_SN_EVC_AuthenticationFlag,                  // 鉴权标志位

    EN_BOOL_SN_EVC_CARD_CASH_ENOUGH_01,                 // 充电卡费用充足
    EN_BOOL_SN_EVC_CARD_PARA_ENOUGH_01,                 // 设置参数满足

//zhoushuo
    EN_BOOL_SN_PRICEMODEL,                   //FALSE有计费模型   TRUE  //无计费模型  //桩内是否有计费模型
    EN_BOOL_SN_EVC_Differ_Card_01,                      //01与启动卡不一致
    EN_BOOL_SN_EVC_Differ_Card_02,                      //02与启动卡不一致
    EN_BOOL_SN_EVC_Admin_Card_01,                       //01管理卡
    EN_BOOL_SN_EVC_Admin_Card_02,                       //02管理卡
    EN_BOOL_SN_EVC_Mainten_Card_01,                     //01运维卡
    EN_BOOL_SN_EVC_Mainten_Card_02,                     //02运维卡

    EN_BOOL_SN_EVC_Unbalanced_01,                       // 余额不足标志位
    EN_BOOL_SN_CHARGE_START_TIMEOUT,            //启动超时

// tianrz add
    EN_BOOL_SN_EVC_U_BELOW,                             // 进线交流电压低

// 给更新费率模型留的全局变量
    EN_BOOL_SN_EVC_HOST_UPDATE_RATE_MODEL,              // 后台已经完成计费模型的下发

// tianrz add for test
    EN_BOOL_SN_EVC_HMI_CHARGE,                          // HMI处于充电相关的界面
    EN_BOOL_SN_EVC_IDLE,                                // 整机处于空闲状态
//    EN_BOOL_SN_EVC_UPDT_DOWNLOAD,                       // 程序已经下载成功

// tianrz add for realtimedata
    EN_BOOL_SN_EVC_FST_TURNON,                          // 首次上电
    EN_BOOL_SN_EVC_NOT_BALANCED,                        // 充电启动后，未上送充电记录
//fwp add for SR5261
    EN_BOOL_SN_DC_PROT_ACT_01,
    EN_BOOL_SN_DC_PROT_ACT_02,
    EN_BOOL_SN_INSLMON_Err_01,
    EN_BOOL_SN_INSLMON_Err_02,
    EN_BOOL_SN_FZ_Err_01,
    EN_BOOL_SN_FZ_Err_02,
    EN_BOOL_SN_TEMP1_ALARM,
    EN_BOOL_SN_TEMP2_ALARM,
    EN_BOOL_SN_JDL_01,
    EN_BOOL_SN_JDL_02,
    EN_BOOL_SN_LOCK_01,
    EN_BOOL_SN_LOCK_02,
    EN_BOOL_SN_DC_INVERSE_01,
    EN_BOOL_SN_DC_INVERSE_02,
    EN_BOOL_SN_EVC_PROT_ACT,

    EN_BOOL_SN_SUPPLY_12V_01,
    EN_BOOL_SN_SUPPLY_24V_01,
    EN_BOOL_SN_CON_CUR_01,
    EN_BOOL_SN_HZ_Err_01,
    EN_BOOL_SN_TEMP1_ERROR,
    EN_BOOL_SN_CCU_COMM_Err_01,
    EN_BOOL_SN_EVC_EXCHANGE_PLUG_01,

    EN_BOOL_SN_EVC_Cash_IC_02,
    EN_BOOL_SN_SUPPLY_12V_02,
    EN_BOOL_SN_SUPPLY_24V_02,
    EN_BOOL_SN_CON_CUR_02,
    EN_BOOL_SN_HZ_Err_02,
    EN_BOOL_SN_TEMP2_ERROR,
    EN_BOOL_SN_RECT_ERR_01,
    EN_BOOL_SN_RECT_ERR_02,
    EN_BOOL_SN_CCU_COMM_Err_02,
    EN_BOOL_SN_EVC_EXCHANGE_PLUG_02,

    EN_BOOL_SN_EVC_PCU_ERR,
    EN_BOOL_SN_EVC_NoNeedAuthen_01,
    EN_BOOL_SN_EVC_Account_Unused_01,
    EN_BOOL_SN_EVC_Account_Unused_02,  // dwCardUnused;         // 该卡不在使用中
    EN_BOOL_SN_EVC_Unbalanced_02,   // dwUnbalanced;         // 卡余额不足
    EN_BOOL_SN_EVC_CardStartCmd_02, // dwCardStartCmd;       // Card结束充电指令
    EN_BOOL_SN_EVC_CardStopCmd_02,  // dwCardStopCmd;        // Card开始充电指令
    EN_BOOL_SN_EVC_AppStartCmd_02,  // dwAppStartCmd;        // App开始充电指令
    EN_BOOL_SN_EVC_AppStopCmd_02,   // dwAppStopCmd;         // App结束充电指令
    EN_BOOL_SN_EVC_NoNeedAuthen_02,  // dwNoNeedAuthen;      //无鉴权Card启动
    EN_BOOL_SN_EVC_OptOutTime_02,   // dwOptOutTime;         // 操作超时
    EN_BOOL_SN_EVC_Measure_Over_02, // dwMeasure_Over;       // 计费结束标志位
    EN_EVC_TIME_02,
    EN_BOOL_SN_EVC_SaveEVCData_02,          // dwRecordOK;      // 充电记录保存完毕
    EN_BOOL_SN_EVC_CARD_CASH_ENOUGH_02,     //dwCashEnough  // 充电卡金额充足
    EN_BOOL_SN_EVC_CARD_PARA_ENOUGH_02,     //dwParaEnough  // 设置参数满足
    EN_EVC_CURRENT_SERV_COST_02,           //dwServCost;//当前服务费用
    EN_BOOL_SN_EVC_AuthenFlag_01,
    EN_BOOL_SN_EVC_CardAuthenUp_01,
    EN_BOOL_SN_EVC_Valid_IC_02,    // dwPinNO_Valid;        // 充电卡有效
    EN_BOOL_SN_EVC_CardAuthenUp_02,   // dwCardAuthenUp;    // 卡鉴权数据更新(上行)
    EN_BOOL_SN_EVC_AuthenFlag_02,     // dwCardAuthenFlag;  // 卡鉴权标志位

    EN_BOOL_SN_EVC_AuthenticationFlag_01,                  // 鉴权标志位
    EN_BOOL_SN_EVC_AuthenticationFlag_02,                  // 鉴权标志位

    EN_BOOL_SN_ERR_MEA_01,                       //计费组件整型、浮点型计算出错
    EN_BOOL_SN_ERR_MEA_02,                       //计费组件整型、浮点型计算出错
    EN_BOOL_SN_EVC_CHECK_VAL_ERR,

    // 测试预留
    EN_BOOL_FLG_TEST1,                      // TEST1标志
    EN_BOOL_FLG_TEST2,                      // TEST2标志
    EN_BOOL_FLG_TEST3,                      // TEST3标志

//================================================================================
    EN_BOOLRAM_SN_END,                      // 定义结束,本条不能修改
};

// 与触摸屏寄存器保持一致，两个字节为基本单位
enum _EN_LEN_SET_PARA
{
    EN_LEN_WHM1_ADDR        = 4,
    EN_LEN_WHM2_ADDR        = 4,
    EN_LEN_WHM_NUM          = 2,             //电表数量

    EN_LEN_DEVICE_ADDR00    = 4,          //桩编号
    EN_LEN_DEVICE_ADDR01    = 4,          //桩编号
    EN_LEN_PORT_NUM         = 2,            //端口号
    EN_LEN_SERVER_IP00      = 2,         //服务器IP
    EN_LEN_SERVER_IP01      = 2,         //服务器IP
    EN_LEN_SERVER_IP02      = 2,         //服务器IP
    EN_LEN_SERVER_IP03      = 2,         //服务器IP

    EN_LEN_CLIENT_IP00      = 2,         //服务器IP
    EN_LEN_CLIENT_IP01      = 2,         //服务器IP
    EN_LEN_CLIENT_IP02      = 2,         //服务器IP
    EN_LEN_CLIENT_IP03      = 2,         //服务器IP

    EN_LEN_GATE_WAY00       = 2,           //网关
    EN_LEN_GATE_WAY01       = 2,           //网关
    EN_LEN_GATE_WAY02       = 2,           //网关
    EN_LEN_GATE_WAY03       = 2,           //网关

    EN_LEN_RATE_POINTED     = 4,         //尖费率
    EN_LEN_RATE_PEAK        = 4,            //峰费率
    EN_LEN_RATE_FLAT        = 4,            //平费率
    EN_LEN_RATE_VALLEY      = 4,          //谷费率
    EN_LEN_RATE_SERV        = 4,           //服务费

    EN_LEN_INTERV_NUM       = 2,

    EN_LEN_INTERV_TIME00_01 = 4,
    EN_LEN_INTERV_TIME02_03 = 4,
    EN_LEN_INTERV_TIME04_05 = 4,
    EN_LEN_INTERV_TIME06_07 = 4,
    EN_LEN_INTERV_TIME08_09 = 4,
    EN_LEN_INTERV_TIME10_11 = 4,
    EN_LEN_INTERV_TIME12_13 = 4,
    EN_LEN_INTERV_TIME14_15 = 4,
    EN_LEN_INTERV_TIME16_17 = 4,
    EN_LEN_INTERV_TIME18_19 = 4,
    EN_LEN_INTERV_TIME20_21 = 4,
    EN_LEN_INTERV_TIME22_23 = 4,

    EN_LEN_INTERV00_05_RATE = 4,
    EN_LEN_INTERV06_11_RATE = 4,
    EN_LEN_INTERV12_17_RATE = 4,
    EN_LEN_INTERV18_23_RATE = 4,

    EN_LEN_EVC_CARD_TYPE    = 2,
    EN_LEN_RECT_VOL_MAX     = 2,
    EN_LEN_RECT_VOL_MIN     = 2,
    EN_LEN_RECT_CUR_MAX     = 2,
    EN_LEN_RECT_CUR_RATED   = 2,
    EN_LEN_EVC01_RECT_NUM   = 2,
    EN_LEN_EVC02_RECT_NUM   = 2,

    EN_LEN_QR_CODE_00       = 4,
    EN_LEN_QR_CODE_04       = 4,
    EN_LEN_QR_CODE_08       = 4,
    EN_LEN_QR_CODE_12       = 4,
    EN_LEN_QR_CODE_16       = 4,
    EN_LEN_QR_CODE_20       = 4,
    EN_LEN_QR_CODE_24       = 4,
    EN_LEN_QR_CODE_28       = 4,
};

enum _PORT_TYPE_
{
    CN_PORT_NET = 0,                                            // define the port 网络口
    CN_PORT_CAN,                                                // define the port CAN 口
    CN_PORT_COM,                                                // define the port 标准串口
    CN_PORT_RS232,                                              // define the port 简易232口
    CN_PORT_RS422,                                              // define the port 简易422口
    CN_PORT_RS485,                                              // define the port 简易485口
    CN_PORT_VNET,                                               // define the port 虚拟网络口
    CN_PORT_MAXTYPE,                                            // 最大端口类型
};

enum _PORT_USAGE_
{                                                               // 定义端口用途常量
    EN_PORT_USAGE_FOR_MON = 0,                                  // 用于与上级管理机连接
    EN_PORT_USAGE_FOR_UNIT                                      // 用于与保护测控装置连接
};

// 组件自检标志类型
enum _CHK_TYPE_INDEX_TYPE_
{
    EN_CHK_TYPE_HARDWARE,                       // 硬件异常
    EN_CHK_TYPE_DESIGN,                         // 设计异常
    EN_CHK_TYPE_COMMBREAK,                      // 通信中断
    EN_CHK_TYPE_COMMERR,                        // 通信异常
    EN_CHK_TYPE_CRC,                            // CRC校验异常
//==============================================//
    EN_CHK_TYPE_END,                            // 组件自检标志类型END，本条不能修改
};
// TLV参数类型枚举表
enum _TLV_PARA_TYPE_
{
    EN_TLV_PARA_01,                                             // TLV参数类型01(遥信防抖时间)
    EN_TLV_PARA_02,                                             // TLV参数类型02(遥信类型)
    EN_TLV_PARA_03,                                             // TLV参数类型03(定值组别)
    EN_TLV_PARA_04,                                             // TLV参数类型04(相位基准)
    EN_TLV_PARA_05,                                             // TLV参数类型05(装置地址编号)
    EN_TLV_PARA_06,                                             // TLV参数类型06(通讯端口设置)
    EN_TLV_PARA_07,                                             // TLV参数类型07(网口设置1)
    EN_TLV_PARA_08,                                             // TLV参数类型08(网口设置2)
    EN_TLV_PARA_09,                                             // TLV参数类型09(通讯设置)
    EN_TLV_PARA_10,                                             // TLV参数类型10
    EN_TLV_PARA_11,                                             // TLV参数类型11
    EN_TLV_PARA_12,                                             // TLV参数类型12
    EN_TLV_PARA_13,                                             // TLV参数类型13
    EN_TLV_PARA_14,                                             // TLV参数类型14
    EN_TLV_PARA_15,                                             // TLV参数类型15
    EN_TLV_PARA_16,                                             // TLV参数类型16
//-----------------------------------------------//
    EN_TLV_PARA_SAVE_END,                                       // TLV参数类型保存END，前面的保存，并初始化读取

    EN_TLV_PARA_UNSAVE_01,                                      // 非掉电保持TLV参数类型01(虚遥信调试)
    EN_TLV_PARA_UNSAVE_02,                                      // 非掉电保持TLV参数类型02(虚事件调试)
    EN_TLV_PARA_UNSAVE_03,                                      // 非掉电保持TLV参数类型03(开出调试)
    EN_TLV_PARA_UNSAVE_04,                                      // 非掉电保持TLV参数类型04
//-----------------------------------------------//
    EN_TLV_PARA_END,                                            // TLV参数类型END
};

#define EN_TLV_PARA_YX_TIME         (EN_TLV_PARA_01)            // TLV参数类型(遥信防抖时间)
#define EN_TLV_PARA_YX_TYPE         (EN_TLV_PARA_02)            // TLV参数类型(遥信类型)
#define EN_TLV_PARA_SET_ZONE        (EN_TLV_PARA_03)            // TLV参数类型(定值组别)
#define EN_TLV_PARA_PASE_BASE       (EN_TLV_PARA_04)            // TLV参数类型(相位基准)
#define EN_TLV_PARA_EQUIP_NO        (EN_TLV_PARA_05)            // TLV参数类型(装置地址编号)
#define EN_TLV_PARA_SIO_PORTSET     (EN_TLV_PARA_06)            // TLV参数类型(串口设置)
#define EN_TLV_PARA_NET_PORTSET1    (EN_TLV_PARA_07)            // TLV参数类型(网口设置1)
#define EN_TLV_PARA_NET_PORTSET2    (EN_TLV_PARA_08)            // TLV参数类型(网口设置2)
#define EN_TLV_PARA_COMM_PORTSET    (EN_TLV_PARA_09)            // TLV参数类型(通讯参数设置)
#define EN_TLV_PARA_YX_VIRTUAL      (EN_TLV_PARA_UNSAVE_01)     // TLV参数类型(虚遥信调试)
#define EN_TLV_PARA_EVENT_VIRTUAL   (EN_TLV_PARA_UNSAVE_02)     // TLV参数类型(虚事件调试)
#define EN_TLV_PARA_KO_DEBUG        (EN_TLV_PARA_UNSAVE_03)     // TLV参数类型(开出调试)


#define     CN_EVC_HMI_WHMADDR_ADDR00      (0)
#define     CN_EVC_HMI_LEN_WHMADDR00       (EN_LEN_WHM1_ADDR)

#define     CN_EVC_HMI_WHMADDR_ADDR01      (CN_EVC_HMI_WHMADDR_ADDR00+CN_EVC_HMI_LEN_WHMADDR00)
#define     CN_EVC_HMI_LEN_WHMADDR01       (EN_LEN_WHM2_ADDR)

#define     CN_EVC_HMI_WHM_NUM             (CN_EVC_HMI_WHMADDR_ADDR01+CN_EVC_HMI_LEN_WHMADDR01)
#define     CN_EVC_HMI_LEN_WHM_NUM         (EN_LEN_WHM_NUM)

#define     CN_EVC_HMI_DEVICE_ADDR00       (CN_EVC_HMI_WHM_NUM+CN_EVC_HMI_LEN_WHM_NUM)
#define     CN_EVC_HMI_LEN_DEV_ADDR00      (EN_LEN_DEVICE_ADDR00)

#define     CN_EVC_HMI_DEVICE_ADDR01       (CN_EVC_HMI_DEVICE_ADDR00+CN_EVC_HMI_LEN_DEV_ADDR00)
#define     CN_EVC_HMI_LEN_DEV_ADDR01      (EN_LEN_DEVICE_ADDR01)

#define     CN_EVC_HMI_PORT_ADDR           (CN_EVC_HMI_DEVICE_ADDR01+CN_EVC_HMI_LEN_DEV_ADDR01)
#define     CN_EVC_HMI_LEN_PORT            (EN_LEN_PORT_NUM)

#define     ADDR_OF_FRAM_NETLOG_START       (0x00001000)
#define CN_NUM_SET_ZONE         ( 2 )                           // 保护定值最大区
#define CN_NUM_PORT_SIO         ( 4 )                           // 外部通讯口数目2个485+2个232
#define CN_NUM_PORT_NET         ( 2 )                           // 外通讯物理网口数
#define CN_NUM_PORT_CAN         ( 0 )                           // CAN通讯口数目
#define CN_NUM_NET_MON          ( 2 )                           // 外通讯网口监控数

#define M_GrSave_bGlobalRam( LineNO, PinNO )    ( g_bBoolRam[(PinNO)]=(LineNO) )    // 保存全局布尔变量标志值

// 是否使用无线模块
#define CN_EVC_MODEM                    ( TRUE )

#define M_BcdToHex(  x )                    ( (((x) & 0xF0) >>4) * 10 + ((x) & 0x0F) )

typedef struct
{
    UINT32      dwNum_Log;
    tagTimeBCD  tTime_Log;
}sNum_tagTimeBCD;

u32            g_bBoolRam[EN_BOOLRAM_SN_END];                  // 全局布尔变量标志定义
#define M_GrGet_bGlobalRam( PinNO )             ( g_bBoolRam[(PinNO)] )             // 获取全局布尔变量标志值

typedef struct DelayLock
{
    struct DelayLock *next,*previous;
    UINT32 u32LockStatus;      //闭锁状态,参看CN_DELAYLOCK_xxxx系列常量
    UINT32 u32LockActTime;     //闭锁保护动作延时(us)
    UINT32 u32LockRetTime;     //闭锁保护返回延时(us或cn_limit_uint32)
    UINT32 u32StartTime;       //当前状态开始时间，us数
    UINT32 u32EndTime;         //当前状态结束时间，us数
    UINT32 u32RestTime;        //剩余的动作时间
    UINT32 u32ScanError;       //赋值为CN_ERROR_MAGIC,用于自检数据误改
}tagDelayLock;

//GlobalCellObj本来定义在CellManager.h中，但因为部分平台程序也要用，又不想开放给
//用户看，故定义在这里，应用程序不可引用本文件，工程中也没有把本路径加到的-I路径
//中，应用开发不要擅自修改工程设置。
typedef struct Object TagObject;
struct GlobalCellObj
{
    TagObject CellRsc;                  //资源结点
    UINT32  u32CellSN;                  //组件号，由系统分配
    UINT32  u32CellLock;                // 组件瞬时闭锁标志
    UINT32  u32RunInterval;             // 组件高优先级任务调用间隔，取值1、2、4、8
    void (*HighPrioTask)(tagGlobalCellObj *ThisCell);   //组件高优先级任务（原中断调用）
    void (*LowPrioTask)(tagGlobalCellObj *ThisCell);    //组件低优先级任务（原主循环）
    tagDelayLock *ptCellFlg[EN_CHK_TYPE_END];// 组件自检状态(在自检组件中配置闭锁保护)
    ptu32_t tagPrivate;                    // 组件私有数据，含义由组件自行定义
    //以下4个成员，考虑使用MMU提供保护，节省大量计算时间。高压保护必须带MMU，
    //没有MMU的不提供校验功能。
    UINT32 *KeyStart;                   //组件关键数据区首地址
    UINT32 KeySize;                     //关键数据区长度
    UINT32  dwTagSum;                  // 组件关键数据区校验和(正码)
    UINT32  dwTagSumN;                 // 组件关键数据区校验和(反码)
    UINT32  u32ScanError;               //赋值为CN_ERROR_MAGIC,用于自检数据误改
};
#endif
