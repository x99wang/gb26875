#ifndef _SG104_H_
#define _SG104_H_

#include "pppdata.h"

#ifdef __cplusplus
extern "C" {
#endif

#define CN_SG104_MSGLEN_MAX          ( 253 )                    // 报文最大长度255-2
#define CN_SG104_MSGLEN_MIN          ( 4 )                      // 报文最小长度,控制域
#define CN_SG104_START_CODE          ( 0x68 )                   // 起始符
#define CN_SG104_TIME_SECOND         ( 1000 )                   // 定义秒
#define CN_SG104_TIMEOUT_FRAMEU      ( 60 * CN_SG104_TIME_SECOND)                     // 秒,10S后无报文交互则主动发U帧
#define CN_SG104_TIMEOUT_IDLE        ( 10 * CN_SG104_TIME_SECOND)                     // 秒，长期无报文确认的超市时间
#define CN_SG104_TIMEOUT_PROSTART    ( 10 * CN_SG104_TIME_SECOND)                     // 秒，协议标识帧上送间隔
#define CN_SG104_TIMEOUT_FRAME_U     ( 30* 2 * CN_SG104_TIME_SECOND)                     // 秒，协议标识帧上送间隔
#define CN_SG104_TIMEOUT_CHARGEDATA  ( 20 * CN_SG104_TIME_SECOND)                     // 秒，协议标识帧上送间隔
#define CN_SG104_TIMEOUT_REALDATA    ( 5 * CN_SG104_TIME_SECOND)                     // 秒，四遥信号周期上送间隔
#define CN_SG104_LEN_DEVICECODE      ( 8 )                      // 充电桩编号长度
#define CN_NUM_QUE_REC_EVC      (512)                       // 充电记录个数

#define M_MakeWord(l, h)                    ( (((INT16U)(h)) << 8 ) | ((INT16U)(l) & 0xFF) )
#define M_GetByteLo( x )                    (UINT8)( (x) & 0xFF )
#define M_GetByteHi( x )                    (UINT8)( ( (x) >> 8 ) & 0xFF )

//当前进程处理状态
enum _SG104_PROC_STATE_
{
    EN_SG104_PROC_STATE_IDLE,                               // 总查询传送保护动作状态
/*
    EN_SG104_PROC_STATE_ALLACT,                             // 总查询传送保护动作状态
    EN_SG104_PROC_STATE_ALLALM,                             // 总查询传送告警遥信状态
    EN_SG104_PROC_STATE_ALLCHK,                             // 总查询传送自检遥信状态
    EN_SG104_PROC_STATE_ALLINPUT,                           // 总查询传送保护开入状态
*/
    EN_SG104_PROC_STATE_ALLYX,                              // 总查询传送全遥信
    EN_SG104_PROC_STATE_ALLYC,                              // 总查询传送全遥测
    EN_SG104_PROC_STATE_ALLYM,                              // 总查询传送全遥脉
    EN_SG104_PROC_STATE_END,                                // 总查询结束
//--wangrq add for NW
    EN_SG104_PROC_STATE_CALC,                               // 总查询传送计数量
    EN_SG104_PROC_STATE_CALC_END,                           // 总查询结束
//--
    EN_SG104_PROC_STATE_POLLINGYX,                          // 循环上送遥信
    EN_SG104_PROC_STATE_POLLINGYC,                          // 循环上送遥测
    EN_SG104_PROC_STATE_POLLINGYM,                          // 循环上送遥测
/*
    EN_SG104_PROC_STATE_SENDBCS,                            // 循环上送BCS
    EN_SG104_PROC_STATE_SENDCCS,                            // 循环上送CCS
    EN_SG104_PROC_STATE_SENDBSM,                            // 循环上送BSM
*/
};

enum _NET_MODE_
{                                                               // 定义NET 运行模式常量
    EN_NET_MODE_SERVER = 0,                                     // Server 模式
    EN_NET_MODE_CLIENT                                          // Client 模式

};

// 网络控制变量
typedef struct
{
    tagTimerB       tNetTimer;                                  // 连接状态检查计时器
    tagTimerB       tTestDelay;                                 // 测试应答发送延时计时器
    SINT16          iSockNo;                                    // Sock 链接编号
    BOOL            bOk;                                        // 连接状态
//--wangrq for sibida
    SINT16          iTcpSockNo;                                 // Sock 链接编号
    UINT8           byServerIP[4];                              // 服务器IP地址
    UINT8           byClientIP[4];                              // 客户端IP地址
//--
}tagSockCtrl;

// 收发计数器数据结构
typedef struct {
    SINT16          iTcpSockNo;                                 // Sock 链接编号
    SINT16          wNR;                                        // 接收计数器
    SINT16          wNS;                                        // 发送计数器
    SINT16          wNS_ACK;                                    // 发送接收确认
}tagNRSCtrl;

//IEC104运行控制结构，占用tagPortVal的dwRes（2K空间）
typedef struct
{
    tagSockCtrl             tSockCtrl;                                  // TCP网络链接的编号
    tagNRSCtrl              tNRSCtrl;                                   // 收发计数器数据结构
    UINT8                   byRunStat;                                  // 启动传输状态
    tagTimerB               tFrameUSend;                                // U帧超时计时器
    tagTimerB               tLinkIdle;                                  // S帧超时计时器
    tagTimerB               tProtocolStart;                             // 协议标识帧发送计时器
    tagTimerB               tPolling;                                   // 实时数据遥测循环上送计时器
    tagTimerB               tAuthentication;                           // 鉴权时效计时器_枪1
    tagTimerB               tAuthentication2;                           // 鉴权时效计时器_枪2
    tagTimerB               tLinkTimeout;                               // 链路连接超时计时器
    tagTimerB               tRecTimeout;                               // 充电记录确认超时计时器
    UINT8                   byProcessState;                             // 保护管理板状态处理过程变量
    UINT16                  wSendBase;                                  // 分帧发送时，本帧的偏移量
    UINT8                   byTerminalNo[CN_SG104_LEN_DEVICECODE];      // 充电桩编号
    UINT8                   bySendBSDFlg;                               // 通信组件发送BSD报文标志
    UINT8                   bySendCSDFlg;                               // 通信组件发送CSD报文标志
    UINT8                   byUpdateFailFlg;                            // 升级失败标志 0xFF-默认 0xAA-发送标志
    UINT8                   byRemoteUpdate;                             // 远程升级状态 0xFF-默认 0-启动下载 1-编号错误 2-升级失败 3-下载超时
    UINT8                   byRemoteBalance;                            // 远程修改余额 0xFF-默认 0-修改成功 1-编号错误 2-卡号错误
}tagSG104RunCtl, *tagPSG104RunCtl;
//---shenjn 借鉴南网
typedef struct
{
    UINT8           byAckNak;                       // 充电记录确认, 0x07-已确认 0x00-未确认
    UINT8           byUpFlag;
    UINT8           byCnt_RecUp;                    // 单条记录重复上送计数器
    UINT16          wUnRecNum;                      // 未确认记录数
    UINT8           byUnRecUpFlag;                  // 未确认记录启动重传标志位
    UINT8           byUnRecCtrl[1+CN_NUM_QUE_REC_EVC/8];                // 未确认记录条目控制
}tagChargeRecCtrl;
tagChargeRecCtrl    tChargeRecCtrl;
#ifdef __cplusplus
}
#endif

#endif
