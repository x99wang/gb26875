/*================================================================================
 * 文件描述：规约全局定义
 * 文件版本: V1.00
 * 开发人员: 沈习波
 * 定版时间:
 * 版本修订:
 * 修订人员:
 *================================================================================*/
#ifndef _PROTOCOL_H_
#define _PROTOCOL_H_
#include "pppdata.h"
#include <uartctrl.h>
/*================================================================================*/

/*================================================================================*/
//外部通讯相关常量
#define CN_LEN_RES_PORT         (3)                             // 端口保留参数数据长度(UINT32)
#define CN_LEN_RES_SYS          (9)                             // 系统保留参数数据长度(UINT32)

#define CN_NUM_MSG_PORT         (1)                             // 通讯口接收报文缓冲区个数

#define CN_VOL_BUF_RECV         (2048)                          // 接收数据缓冲区容量
//#define CN_VOL_BUF_SEND         (256)                           // 发送报文缓冲区容量
#define CN_VOL_BUF_MSG          (512)                          // 接收报文缓冲区容量

#define CN_PORT_BASE_SIO        (0)                                 // SIO口起始编号
#define CN_PORT_BASE_CAN        (CN_NUM_PORT_SIO)                   // CAN口起始编号
#define CN_PORT_BASE_NET        (CN_PORT_BASE_CAN+CN_NUM_PORT_CAN)  // NET口起始编号

// 通讯端口最大数目（每个网口虚拟一个）
#define CN_NUM_PORT             ((CN_NUM_PORT_SIO)+(CN_NUM_PORT_NET)+(CN_NUM_PORT_CAN))
// 通讯参数长度(INT16U数)
//#define CN_NUM_PORT_PARA_LONG   (4+2*CN_LEN_RES_PORT)

#define CN_STATUS_SEND_IDLE             (0x00)                  // 发送空闲
#define CN_STATUS_SEND_BUSY             (0x01)                  // 发送忙
#define CN_STATUS_SEND_WAIT             (0x02)                  // 发送等待(等待确认)
#define CN_TIMEOUT_COMM_ERROR_PORT      (300)                   // 端口通讯故障超时时间(s)
#define CN_CMD_IDLE                     (0xFFFF)                // 命令空闲
#define CN_STATUS_RECEIVE_IDLE          (0x00)                  // 接收空闲
#define CN_STATUS_RECEIVE_SYNCH         (0x00)                  // 接收同步信息
#define CN_STATUS_RECEIVE_CONTROL       (0x01)                  // 接收控制信息
#define CN_STATUS_RECEIVE_MESSAGE       (0x02)                  // 接收数据
#define CN_STATUS_RECEIVE_OK            (0x03)                  // 接收完成


// 临时缓冲区报文相关常量
#define CN_FORMAT_CAN                   (0)                     // CAN报文格式的临时缓冲区报文
#define CN_FORMAT_103                   (1)                     // 103报文格式的临时缓冲区报文
#define CN_FORMAT_101                   (2)                     // 101报文格式的临时缓冲区报文
#define CN_FORMAT_301C                  (3)                     // 自定义 格式的临时缓冲区报文

// 数据标志相关参数常量
#define CN_QDS_OV                       (0x01)                  // 数据溢出(YC/YM)
#define CN_QDS_BL                       (0x10)                  // 数据被封锁(YC/YX/YM)
#define CN_QDS_SB                       (0x20)                  // 数据被取代(YC/YX/YM)
#define CN_QDS_NT                       (0x40)                  // 数据为非当前值(YC/YX/YM)
#define CN_QDS_IV                       (0x80)                  // 数据无效(YC/YX/YM)
#define CN_QDS_NEW                      (0xFF00)                // 数据更新(每调度一个)
#define CN_QDS_NEW_BIT                  (0x02)                  // 数据更新(从板向主板传输时使用,代替CONST_QDS_NEW)(YC/YX/YM)

#define CN_STACK_WF                     (0x3000)                // 五防逻辑判断任务堆栈大小
#define CN_STACK_CANLS                  (0x3000)                // CAN网联锁通讯任务堆栈大小
#define CN_STACK_NETLS                  (0x3000)                // 网络联锁通讯任务堆栈大小
#define CN_STACK_PORT                   (0x2000)                // 端口任务堆栈大小
#define CN_STACK_RMON                   (0x1000)                // 远程监视任务堆栈大小
#define CN_STACK_DBGTOOL                (0x2000)                // 调试工具任务堆栈大小
#define CN_STACK_NETPRN                 (0x2000)                // 网络打印任务堆栈大小
#define CN_DELAY_PORT                   (3)                     // 端口任务每个循环间休眠时间(tick)
#define CN_DELAY_RMON                   (20)                    // 远程监视任务每个循环间休眠时间(tick)
#define CN_DELAY_NETLS                  (3)                     // 网络联锁通讯任务每个循环间休眠时间(tick)
#define CN_DELAY_CANLS                  (3)                     // CAN网联锁通讯任务每个循环间休眠时间(tick)
#define CN_DELAY_WF                     (3)                     // 五防逻辑处理任务每个循环间休眠时间(tick)
#define CN_DELAY_DBGTOOL                (3)                     // 调试工具任务每个循环间休眠时间(tick)
#define CN_DELAY_NETPRN                 (3)                     // 网络打印任务每个循环间休眠时间(tick)

#define CN_TIMEOUT_EVENT_ACK            (3)                     // 事件报文响应超时周期(s)

#define CN_TIMEOUT_MON_WAVE_READ_BUFF   (20)                    // 上行端口命令录波读缓冲使用超时时间(m)
#define CN_TIMEOUT_MON_CMD              (60)                    // 上行端口命令响应超时时间(s)
#define CN_TIMEOUT_YK_CMD               (20)                    // 遥控命令响应超时时间(s)
#define CN_TIMEOUT_YK_LS                (6)                     // 五防连锁通讯超时时间(s)
#define CN_TIMEOUT_SET_CMD              (20)                    // 改定值命令响应超时时间(s)
#define CN_TIMEOUT_WAVE_CMD             (80)                    // 录波数据命令响应超时时间(s)
#define CN_INTERVAL_NET_IDLE            (20)                    // NET 总线空闲时间间隔(ms)
#define CN_INTERVAL_BUS_IDLE            (20)                    // CAN 总线空闲时间间隔(ms)
#define CN_INTERVAL_SIO_IDLE            (50)                    // SIO 线路发送空闲周期(ms)
#define CN_TIMEOUT_SIO_SEND             (5000)                  // SIO口报文发送超时周期(ms)
#define CN_TIMEOUT_CAN_SEND             (200)                   // CAN网报文发送超时周期(ms)
#define CN_TIMEOUT_NET_SEND             (200)                   // NET网报文发送超时周期(ms)
#define CN_TIMEROUT_RCV_CHAR            (500)                   // 报文接受字节最大间隔时间(ms)

#define CN_TIMEOUT_WFCMD                (5)                     // 五防逻辑判断命令超时(s)//debug cui 8
#define CN_TIMEOUT_WFBACK               (10)                    // 五防逻辑判断返回超时(s)//debug cui 10
#define CN_TIMEOUT_LSCOMM               (5)                     // 五防逻辑判断联锁通讯超时(s)//debug cui 2
#define CN_TIMEOUT_JBSEND               (40)                    // 脚本传输超时(s)

#define CN_MAX_VOL_JBSEND               (1024)                  // 一次脚本上传最大字节数

/*================================================================================*/
// 外部通讯相关数据结构xupf-2010-5-7
//typedef struct
//{                                                     // 数据上送控制数据结构
//    tagTimerB        tTimerYC;                       // 遥测量上送定时器(A类)
//    tagTimerB        tTimerYX;                       // 遥信量上送定时器
//    tagTimerB        tTimerYM;                       // 电能量上送定时器
//    tagTimerB        tTimerYC_B;                     // 遥测量上送定时器(B类)
//    tagTimerB        tTimerYC_C;                     // 遥测量上送定时器(C类)
//    UINT16           wSendKind;                      // 下次要发送的信号的类型
//    UINT16           wOffsetYC;                      // 下次要发送的遥测的起始点号偏移
//    UINT16           wOffsetYX;                      // 下次要发送的遥信的起始点号偏移
//    UINT16           wOffsetYM;                      // 下次要发送的遥脉的起始点号偏移
//    UINT16           wOffsetYC_B;                    // 下次要发送的遥测的起始点号偏移(B类)
//    UINT16           wOffsetYC_C;                    // 下次要发送的遥测的起始点号偏移(C类)
//}tagDataCtrl,*tagPDataCtrl;

// 事件转发的控制数据结构
//typedef struct
//{
//    tagTimerB        tEventTimer;                    // 事件报文等待确认计时器
//    UINT16           wEventKind;                     // 正在等待确认的事件类别代码
//    UINT16           wEventQueueID;                  // 正在等待确认的事件队列ID
//}tagEventCtrl,*tagPEventCtrl;

// 规约设定数据结构
//typedef struct
//{
//    UINT16           wProtocolID;                    // 规约编号
//    char             cProtocolName[20];              // 规约名称
//    UINT16           wProtocolAttr;                  // 规约属性
//    PFUNC            pFuncTaskStart;                 // 规约报文收发处理任务起动函数
//    PFUNC            pFuncInitUnit;                  // 规约对装置变量的初始化函数
//}tagProtocolSet,*tagPProtocolSet;

// 通用命令控制数据结构
//typedef struct
//{
//    tagTimerB        tCmdTimer;                      // 命令超时计时器
//    UINT16           wCmdCode;                       // 命令代码
//    UINT16           wCmdLast;                       // 前一个命令代码
//    UINT16           wCmdSource;                     // 命令来源(0：当地监控,1：远方调度)
//    UINT16           wCmdObject;                     // 遥控命令对象号(监控端使用)
//    UINT16           wCmdUnitID;                     // 命令目标(单元ID)
//    UINT16           wCmdChannel;                    // 命令目标(通道)
//    UINT16           wSrcStat;                       // 源态
//    UINT16           wDstStat;                       // 目标态
//    UINT16           wStep;                          // 目标态
//    UINT8            byPriority;                     // 命令优先级
//    UINT8            byResendTimes;                  // 重发次数
//    UINT8            byCOT1;                         // 传送原因的第一字节[源发站地址]
//    UINT8            byCOT2;                         // 传送原因的第二字节[源发站地址]
//    UINT8            byOBJ3;                         // 信息对象地址的第3字节[结构化地址]
//    UINT8            byCmdParam[7];                  // 命令附加参数 (104要求>=5!)
//}tagCmdCtrl,*tagPCmdCtrl;

// 命令重发数据结构
//typedef struct
//{
//    tagTimerB        wDelayTimer;                    // 重发延时计时器
//    UINT16           wCmdLen;                        // 命令报文长度
//    BOOL             bEnabled;                       // 是否启动重发计时
//    UINT8            byResendTimes;                  // 已经重发的次数
////--单片机节约内存
////    UINT8            byCmdBuf[CN_VOL_BUF_SEND];      // 命令报文缓冲区
////--
//}tagCmdResend,*tagPCmdResend;

// 端口接收缓冲区控制信息结构
typedef struct
{
    tagTimerB        tTimerRecv;                     // 通道接收中断时间计时器(s)
    tagTimerB        tTimerMsg;                      // 报文拼装间隔计时器(ms)
    UINT16           wRecvRead;                      // 接收队列读指针
    UINT16           wRecvWrite;                     // 接收队列写指针
//--单片机节约内存
    UINT8            *pbyRecvQue;                    // 接收队列指针(由应用组件根据需要赋值)
//    UINT8            byRecvQue[CN_VOL_BUF_RECV];     // 接收队列
//--
}tagRecvCtrl,*tagPRecvCtrl;

// 端口发送缓冲区控制信息结构
typedef struct
{
    tagTimerB        tTimerAbandon;                  // 报文发送延迟时间计时器(ms)(超过一段时间没发送完则取消)
    tagTimerB        tTimerLineIdle;                 // 通道发送空闲时间计时器(ms)(发送状态为IDLE时启动计时)
    UINT16           wSendLen;                       // 发送报文长度
    UINT16           wSendRead;                      // 发送读指针
    UINT16           wSendFlag;                      // 发送状态标志
    UINT16           wUnitAddr;                      // 目标发送地址
//--单片机节约内存
    UINT8            *pbySendBuf;                    // 发送缓冲区指针(由应用组件根据需要赋值)
//    UINT8            bySendBuf[CN_VOL_BUF_SEND];     // 发送缓冲区
//--
}tagSendCtrl,*tagPSendCtrl;

// 规约报文缓冲区控制信息结构
typedef struct
{
    tagTimerB        tTimeoutRecv;                   // 拼装超时计时器(ms)(暂时仅CAN规约使用)
    UINT16           wLenAll;                        // 报文的总长度
    UINT16           wLenCur;                        // 报文的当前长度(已接收)
    UINT8            byMsgStatus;                    // 报文缓冲区接收状态
    UINT8            byMsgType;                      // 报文类型信息
    UINT8            byMsgInfo[8];                   // 报文其他信息(规约相关)
    UINT8            byMsgBuf[CN_VOL_BUF_MSG];       // 接收报文缓冲区
}tagMsgCtrl,*tagPMsgCtrl;

// 物理端口通讯设置参数数据结构
typedef struct
{
    UINT16           wProtocolID;                    // 规约代号
    UINT16           wUsed;                          // 是否使用<0-关, 1-开>
    UINT16           wPortType;                      // 端口类型
    UINT16           wPortUsage;                     // 端口用途<向上/向下> <主/从>
    UINT16           wRunMode;                       // 运行模式(BasicCAN/PeliCAN, Synch/Asyn, Server/Client)
    UINT16           wBaudRate;                      // 波特率
    UINT32           dwAddBase;                      // 端口硬件地址基准
    UINT32           dwParam[8];                     // 通讯口其他参数(校验位、停止位等)
//--单片机节约内存
  //  UINT32           dwRes[CN_LEN_RES_PORT];         // 通讯口保留参数
//--
}tagPortSet,*tagPPortSet;

// 通讯端口运行值
typedef struct
{
    tagPPortSet      ptPortSet;                      // 通讯端口参数变量指针
    tagRecvCtrl      tRecvCtrl;                      // 接收缓冲区控制变量
    tagSendCtrl      tSendCtrl;                      // 发送缓冲区控制变量
    tagMsgCtrl       tMsgCtrl[CN_NUM_MSG_PORT];      // 接收报文控制变量
//    tagCmdCtrl       tCmdCtrl;                       // 命令控制变量
//    tagCmdCtrl       tLastCmdCtrl;                   // 前次命令控制变量（多进程命令处理中使用）
//    tagCmdResend     tCmdResend;                     // 命令重发控制变量
//    tagDataCtrl      tDataCtrl;                      // 数据发送控制变量
//    tagEventCtrl     tEventCtrl;                     // 事件发送控制变量
    tagTimerB        tTimerClock;                    // 校时间隔计时器(ms)
    tagTimerB        tTimerPolling;                  // 查询间隔计时器(ms)
    tagTimerB        tTimerCommOK;                   // 通讯口通讯状态计时器(s)
//    UINT32           RecordIndex;                    //记录的读者编号
    UINT16           wPollingUnitIdx;

    struct DjyDevice *DevFp;                         // 设备文件指针
    struct COMParam   ComPara;                       // 串口传输参数
    UINT16           wRdDataLen;                     // 每次读取数据的长度

//    UINT16           wIndexProtocol;                 // 规约序号 (数组下标值)
//    UINT16           wReportCtrlRead;                // 中转报文队列读出指针
//    UINT16           wReportIDRead;                  // 中转报文队列读出ID(当前准备读报文的ID)
//    UINT16           wResetTimes;                    // 通讯口复位次数
//    UINT16           wPollingTimes;                  // 循环查询的次数
//    UINT16           wCmdSta[CN_NUM_PORT];           // 新命令状态
//    UINT8            byCmdNum;                       // 等待处理的命令个数
    UINT8            byCurRecvMsgCtrl;               // 当前接收报文变量指针（配合接收报文控制变量使用）
    BOOL             bCommOK;                        // 通讯口通讯状态
//    BOOL             bNeedSwitch;                    // 是否需要收发切换(TRUE为需要)
//    BOOL             bIsVirtual;                     // 是否为虚拟端口（0为实际端口，1为虚拟端口）
//    UINT8            byRealPortNum;                  // 虚拟端口所对应实际端口号
//    UINT32           dwRes[512];                     // 通用变量内存空间(备用)

}tagPortVal, *tagPPortVal;

// CAN 口通讯参数数据结构
typedef struct
{
    UINT8            byCanIP;                        // CAN网节点通讯地址
}tagParamCan, *tagPParamCan;

// 串行口通讯参数数据结构
typedef struct
{
    UINT8            byDataBit;                      // 数据位<6,7,8>
    UINT8            byParity;                       // 奇偶校验 <0-无校验，1-奇校验，2-偶校验；其它: 无效>
    UINT8            byStopBit;                      // 停止位   <0:1位; 1: 1.5位; 2: 2位; 其它: 无效>
}tagParamSio, *tagPParamSio;

// 网络口通讯参数数据结构
typedef struct
{
    UINT16           wNetPort;                       // 端口号
    UINT8            byIP[4];                        // IP地址(配置文件中无效,从FLASH中读取)
    UINT8            byMonIP[4];                     // 上级主管理机的IP地址
    UINT8            bySlaMonIP[4];                     // 上级从管理机的IP地址
//    UINT8            byMask[4];                      // 子网掩码
//    UINT8            byGateway[4];                   // 子网网关
//    UINT8            byMac[6];                       // MAC地址(不保存到配置文件中,从FLASH中读取)
    BOOL             bDblMac;                        // 双机投退
    BOOL             bDblNet;                        // 双网投退
    UINT8            byMasPort;                      // 主机网口序号
    UINT8            bySlaPort;                      // 从机网口序号
    UINT8            byMonIP2[4];                    // 上级主管理机的IP2地址
    UINT8            bySlaMonIP2[4];                 // 上级从管理机的IP2地址}tagParamNet, *tagPParamNet;
}tagParamNet, *tagPParamNet;

// 系统设定参数数据结构
typedef struct
{
    UINT16           wCommAddr;                      // 通讯地址   <端口内唯一>
    UINT16           wNumPCPU;                       // PCPU数目
    UINT8            byTimeBase;                     // 校时时钟基准<0:不限制,1:GPS,2-7:向上通讯口1-6>
    UINT8            bySetYB;                        // 本管理板压板定值组号（103使用）
    UINT8            byFunc;                         // 本管理板功能类型号（103使用）
    UINT8            byMeasure;                      // 本管理板测量值组号（103使用）
    UINT8            bySetGroupNum;                  // 本管理板定值组别数（103使用）
    UINT8            bySet[CN_NUM_SET_ZONE];         // 本管理板定值组号（103使用）
    UINT8            byNumPort;                      // 通讯口个数
//    tagPortSet      tPortSet[CN_NUM_PORT];          // 端口参数
    BOOL             bHavePRN;                       // 存在打印机
    UINT8            cTypeName[30];                  // 装置型号名称 <"ISA-341F2">
    UINT32           dwParam[CN_LEN_RES_SYS];        // 其它辅助参数
}tagSysSet, *tagPSysSet;


tagPortSet      g_ptPortSet[CN_NUM_PORT];                       // 本管理板通讯端口设定参数指针
tagPortVal      g_tPortVal[CN_NUM_PORT];                        // 本管理板通讯端口运行数值
/*================================================================================*/
#endif /* _PROTOCOL_H_ */
