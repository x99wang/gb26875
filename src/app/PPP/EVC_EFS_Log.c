/*================================================================================
 * 文件名称：EFS_File_Manager.c
 * 文件描述：在EFS简易文件系统中创建文件、删除文件、写文件
 * 文件版本: V1.00
 * 开发人员: 田润泽
 * 定版时间: 
 * 版本修订:
 * 修订人员: 
 *================================================================================*/

// include 关系
//#include "InstCell.h"
//#include "DebugShell.h"
#include "stdint.h"
#include "stddef.h"
#include "stdio.h"
#include "stdlib.h"
#include "endian.h"
#include "string.h"
#include "os.h"
#include "string.h"
#include <endian.h>
#include "pppdata.h"

#define EVCEFS_WITHTIMES        (TRUE)

#define EVCEFS_DATELEN          (30)
#define EVCEFS_LEN_NETLOG       (512)
#define EVCEFS_LINE             ("-----------------------------------------------------------\r\n")

const tag_EVC_EFSLogTable CNST_tag_NetLogTable[] = 
{
     // pLog                                                           // dwType
// 拨号状态
     {"(1)4G模块:成功获得公网IP.",                                     EN_NETLOG_TYPE_IPGET},
     {"(2)4G模块:公网IP丢失.",                                         EN_NETLOG_TYPE_IPRELEASE},
     {"(3)4G模块:和基站建立通讯.",                                     EN_NETLOG_TYPE_LINKUP},
     {"(4)4G模块:和基站通讯丢失.",                                     EN_NETLOG_TYPE_LINKDOWN},
// PPP功能模块和上电重启相关逻辑     
     {"(5)重启装置:4G模块10分钟未获取到公网IP.",                       EN_NETLOG_TYPE_POWEROFF},
     {"(6)装置初次上电:4G模块初次上电.",                               EN_NETLOG_TYPE_POWERON},
     {"(7)初次读取4G模块枚举信息:成功.",                               EN_NETLOG_TYPE_READCONFIG},
// 4G模块Hard Reset的原因
     {"(8)4G模块硬复位:Tcp连接50次握手都没有成功.",                    EN_NETLOG_TYPE_TCP50TIMES},     
     {"(9)4G模块硬复位:Tcp连接5分钟没有连接成功.",                     EN_NETLOG_TYPE_TCPOVERTIME},
     {"(10)4G模块硬复位:连续重拨三次不成功.",                          EN_NETLOG_TYPE_RECALL3TIMES},
     {"(11)4G模块硬复位:协议启动帧在Tcp已经建立好后发送了15次.",       EN_NETLOG_TYPE_P15TIMES},
     {"(12)4G模块硬复位:进入硬复位状态.",                              EN_NETLOG_TYPE_HARDRESET},
// Tcp相关
     {"(13)Tcp连接:建立成功.",                                         EN_NETLOG_TYPE_TCPUP},
     {"(14)Tcp连接:由于读取报文不成功而主动断开.",                     EN_NETLOG_TYPE_TCPREADDOWN},
     {"(15)Tcp连接:由于发送报文不成功而主动断开.",                     EN_NETLOG_TYPE_TCPSENDDOWN},
// SG104网络状态
     {"(16)SG104:业务层启动传输.",                                     EN_NETLOG_TYPE_SG104TRANSTART},
     {"(17)SG104:TCP断开导致SG104网络异常.",                           EN_NETLOG_TYPE_SG104DOWNFORTCP},
     {"(18)SG104:4G模块掉线导致SG104网络异常并断开Tcp.",               EN_NETLOG_TYPE_SG104DOWNFOR4G},
     {"(19)SG104:接收心跳报文超时导致SG104网络异常.",                  EN_NETLOG_TYPE_SG104DOWNFORS},
// 4G模块从在线到掉线
     {"(20)4G模块由在线状态到掉线状态的下降沿.",                       EN_NETLOG_TYPE_TRAILEDGE},
};

const tag_EVC_EFSLogCfg CNST_tag_EFSCfg[] =
{
    //pt_EFSLog              dwFramAddr                     dwLogNum                                                       bWithTimes
    {CNST_tag_NetLogTable,   ADDR_OF_FRAM_NETLOG_START,      (sizeof(CNST_tag_NetLogTable)/sizeof(tag_EVC_EFSLogTable)),      1},
};

const tag_EVC_EFSLog CNST_tag_EFSLog =
{
    "EFS文件记录组件",
    CNST_tag_EFSCfg,
    sizeof(CNST_tag_EFSCfg)/sizeof(tag_EVC_EFSLogCfg),
};

// 组件私有变量
tagGlobalCellObj    *s_ptEVC_EFSlog_C01;


// ============================================================================
// 函数功能：获取组件的私有私有标签
// 输入参数：Cell,待获取的组件私有标签的组件资源
// 输出参数：无
// 返回值：  CN_LIMIT_UINT32 =出错，否则组件私有标签
// ============================================================================
ptu32_t CM_GetCellTag(tagGlobalCellObj *Cell)
{
    if(NULL != Cell)
    {
        return Cell->tagPrivate;
    }
    else
    {
        return CN_LIMIT_UINT32;
        //是没有这么多组件号的，要达到这么多，必须不断的UNINSTALL 和INSTALL
    }
}


//=================================================
// 函数功能：EFS文件系统初始化钩子函数
//=================================================
BOOL EFS_Init_Event(enEFSEvent event)
{
    tagEVC_EFSLog_P          *ptMyCellTag;                       // 组件私有变量指针s
    ptMyCellTag = (tagEVC_EFSLog_P *)CM_GetCellTag( s_ptEVC_EFSlog_C01 );

	switch(event)
	{
        // 模块和基站建立通讯
    	case EN_EFS_TYPE_NETLOG:
            //printf("钩子:NetLog初始化,所有的计数器清零.\r\n");
            EVC_EFSLog_TimesInit(ptMyCellTag, EN_EFS_TYPE_NETLOG);
    		break; 
    	//case EN_EFS_TYPE_CHGREC:
    	//	break;       
    	default:
            
    		break;
	}

	return true;
}

tagGlobalCellObj *ModuleInstall_EVC_EFSLog( const tag_EVC_EFSLog *ptApp )
{
    tagEVC_EFSLog_P     *ptMyCellTag;
    tag_EVC_EFSLog_Ram   *ptCellRam;
    tagGlobalCellObj    *ptResult;
    char                cNote[CN_RAMSCAN_LEN_NAME];

    if(ptApp == NULL)
    {
        return NULL;
    }
    ptMyCellTag = malloc(sizeof(tagEVC_EFSLog_P));
    if(ptMyCellTag == NULL)
    {
        return NULL;
    }
    ptMyCellTag->ptCellCfg = ptApp;

    // 组件私有数据初始化
    // 初始化时间定值和定时器
    ptCellRam = &ptMyCellTag->tCellRam;
    //memset(ptCellRam, 0, sizeof(ptMyCellTag->tCellRam));
    

    // 注册扫描点
    sprintf(cNote, "%s.byRamScan1", ptApp->pcCellName);
    RamScan_AddPoint((UINT8 *)&ptMyCellTag->byRamScan1, cNote, 1, sizeof(tagEVC_EFSLog_P));
    sprintf(cNote, "%s.byRamScan2",ptApp->pcCellName);
    RamScan_AddPoint((UINT8 *)&ptMyCellTag->byRamScan2, cNote, 1, sizeof(tagEVC_EFSLog_P));

    // 注册组件
    ptResult = CM_InstallCell(ptApp->pcCellName, NULL, 0, NULL, (UINT32)ptMyCellTag);
    if(ptResult == NULL)
    {
        //初始化阶段出错，调试能发现，无须释放以分配的资源
        printk("Install %s module error", ptApp->pcCellName);
        return NULL;
    }
    
    return ptResult;
}

void EVC_EFSLog_TaskStart(void)
{
    u16 EFSLogAppEvtt;
    EFSLogAppEvtt = DJY_EvttRegist(EN_CORRELATIVE, CN_PRIO_RRS, 0, 1, (ptu32_t(*)(void))EVC_EFSLog,
                           NULL, 0x1000, "EVC_EFSLOG");
    
    if(EFSLogAppEvtt==CN_EVTT_ID_INVALID)
    {
        printf("EFSLogApp event register failed.\r\n");
    }
    else
    {
        DJY_EventPop(EFSLogAppEvtt,NULL,0,(ptu32_t)NULL,0,0);
        
    }
}


//=================================================
// 函数功能：APP初始化
//=================================================
void EVC_EFSLog_Init( tagGlobalCellObj *ptThis )
{
    tagEVC_EFSLog_P          *ptMyCellTag;                       // 组件私有变量指针
    tag_EVC_EFSLog           *ptApp;                             // 组件私有变量指针
    tag_EVC_EFSLog_Ram       *ptCellRam;
    UINT32                   dwTypeNum;
    UINT8                    byLoop;

    // 指针初始化
    ptMyCellTag = (tagEVC_EFSLog_P *)CM_GetCellTag( ptThis );
    ptApp       = (tag_EVC_EFSLog *)ptMyCellTag->ptCellCfg;
    ptCellRam   = &ptMyCellTag->tCellRam;  
    dwTypeNum   = ptApp->dwLogTypeNum;

    EFS_Log_Init((fnEFSEventHook)EFS_Init_Event);
    
    memset(ptCellRam, 0, sizeof(ptMyCellTag->tCellRam));
    EVC_EFSLog_ReadTimesFRAM(ptMyCellTag);
    
    for(byLoop = 0; byLoop < dwTypeNum; byLoop++)
    {
        EVC_EFSLog_FlagInit(ptMyCellTag, byLoop);
    }
    
    return;
}

//=================================================
// 函数功能：初始化次数
//=================================================
BOOL EVC_EFSLog_TimesInit(tagEVC_EFSLog_P *ptMyCellTag, UINT32 dwType)
{
    tag_EVC_EFSLog           *ptApp;                             // 组件私有变量指针
    tag_EVC_EFSLog_Ram       *ptCellRam;
    tag_EVC_EFSLogCfg        *ptCfg;

    UINT32 dwRet;
    UINT8  byLoop;
    
    ptApp       = (tag_EVC_EFSLog *)ptMyCellTag->ptCellCfg;
    ptCellRam   = &ptMyCellTag->tCellRam[dwType];  
    ptCfg       = (tag_EVC_EFSLogCfg *)&ptApp->EVC_EFSLogCfg[dwType];
    
    memset(ptCellRam->dwLogTimes, 0, sizeof(UINT32) * EVCEFS_LOGNUMLIMIT);

//printf("将NetLog中的计数器清零操作.\r\n");
    if(ptCfg->bWithTimes)
    {
        dwRet = Fram_Write_BYTE(ptCfg->dwFramAddr, (UINT8*)ptCellRam->dwLogTimes, (sizeof(UINT32) * ptCfg->dwLogNum));
        if(!dwRet)
        {
            printf("Read g_dwNetLogTimes failed!\r\n");
            return FALSE;
        }    
    }

    return TRUE;
}



//=================================================
// 函数功能：初始化次数
//=================================================
BOOL EVC_EFSLog_ReadTimesFRAM(tagEVC_EFSLog_P *ptMyCellTag)
{
    tag_EVC_EFSLog           *ptApp;                             // 组件私有变量指针
    tag_EVC_EFSLog_Ram       *ptCellRam;
    tag_EVC_EFSLogCfg        *ptCfg;

    UINT32 dwRet;
    UINT8  byLoop;

    ptApp       = NULL;
    ptCellRam   = NULL;  
    ptCfg       = NULL;

//printf("将NetLog中的计数器从铁电中读出.\r\n");

    for(byLoop = 0; byLoop < ptApp->dwLogTypeNum; byLoop++)
    {
        ptApp       = (tag_EVC_EFSLog *)ptMyCellTag->ptCellCfg;
        ptCellRam   = &ptMyCellTag->tCellRam[byLoop];  
        ptCfg       = (tag_EVC_EFSLogCfg *)&ptApp->EVC_EFSLogCfg[byLoop];
        if(ptCfg->bWithTimes)
        {
            dwRet = Fram_Read_BYTE(ptCfg->dwFramAddr, (UINT8*)ptCellRam->dwLogTimes, (sizeof(UINT32) * ptCfg->dwLogNum));
            if(!dwRet)
            {
                printf("Read g_dwNetLogTimes failed!\r\n");
                return FALSE;
            }            
        }
    }
    
    return TRUE;
}

//=================================================
// 函数功能：初始化次数
//=================================================
BOOL EVC_EFSLog_TimesRefresh(tagEVC_EFSLog_P *ptMyCellTag, UINT32 dwType, UINT32 dwNum)
{
    tag_EVC_EFSLog           *ptApp;                             // 组件私有变量指针
    tag_EVC_EFSLog_Ram       *ptCellRam;
    tag_EVC_EFSLogCfg        *ptCfg;

    UINT8  byLoop;
    UINT32 dwRet  = 0;
    UINT32 dwAddr = 0;

    ptApp       = (tag_EVC_EFSLog *)ptMyCellTag->ptCellCfg;
    ptCellRam   = &ptMyCellTag->tCellRam[dwType];  
    ptCfg       = (tag_EVC_EFSLogCfg *)&ptApp->EVC_EFSLogCfg[dwType];

    if(ptCfg->bWithTimes)
    {
//printf("将No.%d EFSLog中的计数器向铁电中更新，第 %d 条.\r\n", (dwType + 1), (dwNum + 1));
        dwAddr = ptCfg->dwFramAddr + sizeof(UINT32)*dwNum;
        dwRet = Fram_Write_BYTE(dwAddr, (UINT8*)&ptCellRam->dwLogTimes[dwNum], sizeof(UINT32));
        if(!dwRet)
        {
            printf("Write No.%d LogTimes failed!\r\n", (dwType + 1));
            return FALSE;
        }            
    }

    return TRUE;
}

//=================================================
// 函数功能：初始化次数
//=================================================
void EVC_EFSLog_TimesResetWeekly(tagEVC_EFSLog_P *ptMyCellTag, UINT32 dwType)
{
    tag_EVC_EFSLog           *ptApp;                             // 组件私有变量指针
    tag_EVC_EFSLog_Ram       *ptCellRam;
    tag_EVC_EFSLogCfg        *ptCfg;
    
    ptApp       = (tag_EVC_EFSLog *)ptMyCellTag->ptCellCfg;
    ptCellRam   = &ptMyCellTag->tCellRam[dwType];  
    ptCfg       = (tag_EVC_EFSLogCfg *)&ptApp->EVC_EFSLogCfg[dwType];
    
    tagTimeBCD  tTimeBCD;
    static BOOL bHoldFlag_Weekly[EN_EFS_TYPE_END] = {0};

// 获取系统时间(包括SPI年月日和FPGA时分秒)
    Time_CopySystemTimeBCD( &tTimeBCD );
    if(1 != tTimeBCD.byWeek)
    {
        bHoldFlag_Weekly[dwType] = 0;
        return;
    }
    if(0 != tTimeBCD.byHour)
    {
        bHoldFlag_Weekly[dwType] = 0;
        return;
    }     
    if(0 != tTimeBCD.byMinute)
    {
        bHoldFlag_Weekly[dwType] = 0;
        return;
    }     
    if(tTimeBCD.bySecond > 5)
    {
        bHoldFlag_Weekly[dwType] = 0;
        return;
    }
    if(0 != bHoldFlag_Weekly[dwType])
    {
        return;
    }
//    printf("Type %d , 刚好到了周一的00:00:00.\r\n", (dwType + 1));
    EVC_EFSLog_TimesInit(ptMyCellTag, dwType);
    bHoldFlag_Weekly[dwType] = 1;    
}

//=================================================
// 函数功能：标志位初始化
//=================================================
BOOL EVC_EFSLog_FlagInit(tagEVC_EFSLog_P *ptMyCellTag, UINT32 dwType)
{
    tag_EVC_EFSLog           *ptApp;                             // 组件私有变量指针
    tag_EVC_EFSLog_Ram       *ptCellRam;
    tag_EVC_EFSLogCfg        *ptCfg;

    ptApp       = (tag_EVC_EFSLog *)ptMyCellTag->ptCellCfg;
    ptCellRam   = &ptMyCellTag->tCellRam[dwType];  
    ptCfg       = (tag_EVC_EFSLogCfg *)&ptApp->EVC_EFSLogCfg[dwType];

    memset(ptCellRam->dwTypeFlag, 0, sizeof(UINT32) * EVCEFS_LOGNUMLIMIT);
     
    return TRUE;
}

//=================================================
// 函数功能：标志位初始化
//=================================================
UINT32 EVC_EFSLog_FlagScan(tagEVC_EFSLog_P *ptMyCellTag, UINT32 dwType)
{
    tag_EVC_EFSLog           *ptApp;                             // 组件私有变量指针
    tag_EVC_EFSLog_Ram       *ptCellRam;
    tag_EVC_EFSLogCfg        *ptCfg;
    UINT32 dwNum;
    UINT32 dwLoop;

    ptApp       = (tag_EVC_EFSLog *)ptMyCellTag->ptCellCfg;
    ptCellRam   = &ptMyCellTag->tCellRam[dwType];  
    ptCfg       = (tag_EVC_EFSLogCfg *)&ptApp->EVC_EFSLogCfg[dwType];
    dwNum       = ptCfg->dwLogNum;

    for(dwLoop = 0; dwLoop < dwNum; dwLoop++)
    {
        if(ptCellRam->dwTypeFlag[dwLoop] > 0)
        {
            ptCellRam->dwTypeFlag[dwLoop]--;
            return dwLoop;
        }
    }
     
    return 0xFFFFFFFF;
}

//=================================================
// 函数功能：标志位初始化
//=================================================
void EVC_EFSLog_FlagSet_Ext(UINT32 dwType, UINT32 dwNum)
{
    tagEVC_EFSLog_P          *ptMyCellTag;                       // 组件私有变量指针                           // 组件私有变量指针
    tag_EVC_EFSLog_Ram       *ptCellRam;
    
    ptMyCellTag = (tagEVC_EFSLog_P *)CM_GetCellTag( s_ptEVC_EFSlog_C01 );
    ptCellRam   = &ptMyCellTag->tCellRam[dwType];  

    ptCellRam->dwTypeFlag[dwNum]++;

    return;
}


//=================================================
// 函数功能：记录一条NetLog
//=================================================
UINT32 EVC_EFSLog_WriteOneRecord(tagEVC_EFSLog_P *ptMyCellTag, UINT32 dwType, UINT32 dwNum)
{
    tag_EVC_EFSLog           *ptApp;                             // 组件私有变量指针
    tag_EVC_EFSLog_Ram       *ptCellRam;
    tag_EVC_EFSLogCfg        *ptCfg;
    tag_EVC_EFSLogTable*     pt_EVC_EFSLogTable;
    char* pLog;
    char cTemp[EVCEFS_LEN_NETLOG] = {0};
    char cDate[EVCEFS_DATELEN] = {0};
    UINT32 dwLen = 0;
    UINT32 dwTypeNum = 0;

    ptApp       = (tag_EVC_EFSLog *)ptMyCellTag->ptCellCfg;
    ptCellRam   = &ptMyCellTag->tCellRam[dwType];  
    ptCfg       = (tag_EVC_EFSLogCfg *)&ptApp->EVC_EFSLogCfg[dwType];
    dwTypeNum   = ptApp->dwLogTypeNum;

    if(0xFFFFFFFF == dwNum)                             {return 0;}
    if(!((dwType >= 0) && (dwType < dwTypeNum)))        {printf("记录条目号错误.\r\n");return 3;}

//printf("将No.%d EFSLog中的写记录,第 %d 条.\r\n", (dwType + 1), (dwNum + 1));
    
    pt_EVC_EFSLogTable = &ptCfg->pt_EFSLogTable[dwNum];
    pLog = pt_EVC_EFSLogTable->pLog;
    
    EFS_GetSysTime(cDate);
    if(ptCfg->bWithTimes)
    {
        sprintf(cTemp, "%s, %s 本周第%d次.\r\n%s", cDate, pLog, ++(ptCellRam->dwLogTimes[dwNum]), EVCEFS_LINE);
        EVC_EFSLog_TimesRefresh(ptMyCellTag, dwType, dwNum);    
    }
    else
    {
        sprintf(cTemp, "%s, %s\r\n%s", cDate, pLog, EVCEFS_LINE);
    }
    dwLen = strlen(cTemp);
    return ((dwLen == EFS_WriteFile(cTemp, dwLen, dwType)) ? 1 : 2);
}


//=================================================
// 函数功能：规律性地每周初始化次数
//=================================================
void EVC_EFSLog( void )
{
    tagGlobalCellObj         *ptThis;
    tagEVC_EFSLog_P          *ptMyCellTag;                       // 组件私有变量指针s
    tag_EVC_EFSLog           *ptApp;                             // 组件私有变量指针
    tag_EVC_EFSLog_Ram       *ptCellRam;
    UINT32 dwTypeNum;
    UINT8 byLoop;
    UINT32 dwTemp;

    ptThis      = s_ptEVC_EFSlog_C01;
    ptMyCellTag = (tagEVC_EFSLog_P *)CM_GetCellTag( ptThis );
    ptApp       = (tag_EVC_EFSLog *)ptMyCellTag->ptCellCfg;
    dwTypeNum   = ptApp->dwLogTypeNum;

    EVC_EFSLog_Init(ptThis);

    while(1)
    {
        for(byLoop = 0; byLoop < dwTypeNum; byLoop++)
        {
            dwTypeNum   = ptApp->dwLogTypeNum;
            EVC_EFSLog_TimesResetWeekly(ptMyCellTag, byLoop);
            dwTemp = EVC_EFSLog_FlagScan(ptMyCellTag, byLoop);
            if(2 == EVC_EFSLog_WriteOneRecord(ptMyCellTag, byLoop, dwTemp))
            {
                printf("写%d类型文件失败.\r\n", byLoop);
            }
        }
        DJY_EventDelay(100*mS);
    }
    return;
}



