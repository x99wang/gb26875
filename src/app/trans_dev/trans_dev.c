#include "../trans_dev/trans_dev.h"

#include "project_config.h"
#include "netbsp.h"
#include "stdint.h"
#include "string.h"
#include "protocol_GB26875.h"
#include "usr_info_trans_dev.h"
#include "../GuiWin/Info/WinSwitch.h"
#include "../common/app_flash.h"

#define TRA_DEVS_COUNT 5

#define UITD_PRO 1 // 正式版本
#define UITD_DEQING 0 // 德清装置
#define THREAD1 0
#define THREAD2 0

const char *TRA_DEVS[TRA_DEVS_COUNT] = {
        "/dev/UART6",
        "/dev/UART7",
        "/dev/UART5",
        "/dev/UART4",
        "/dev/UART3"
};


extern struct ProtocolVTable PROTOCOL_PSM4L_DEV;

extern struct ProtocolVTable PROTOCOL_PSM4L;

extern struct ProtocolVTable PROTOCOL_P300;

extern struct ProtocolVTable PROTOCOL_J7600A;

extern struct ProtocolVTable PROTOCOL_J5000;

extern struct ProtocolVTable PROTOCOL_JXBS_3001;

extern struct ProtocolVTable PROTOCOL_J8100;

extern struct ProtocolVTable PROTOCOL_J4100;

extern struct ProtocolVTable PROTOCOL_XUBOAFD_E90;

extern struct ProtocolVTable PROTOCOL_TS_804;

extern struct ProtocolVTable PROTOCOL_U6125;

extern struct ProtocolVTable PROTOCOL_JB3208;
static u32 protocol0=0;
static u32 protocol1=0;
static u32 protocol2=0;
static u32 protocol3=0;
static u32 protocol4=0;

#if UITD_DEQING
protocol0=1;
protocol1=1;
protocol2=1;
protocol3=1;
protocol4=0;
#elif THREAD1
protocol0=1;
protocol1=0;
protocol2=1;
protocol3=1;
protocol4=0;
#elif THREAD2
protocol0=1;
protocol1=1;
protocol2=1;
protocol3=1;
protocol4=0;
#endif

void dev_protocol_init(u32 prl0,u32 prl1,u32 prl2,u32 prl3,u32 prl4)
{
    protocol0=prl0;
    protocol1=prl1;
    protocol2=prl2;
    protocol3=prl3;
    protocol4=prl4;
}
static bool_t *TransDevTest[UITD_SERIAL_NUM] = \
{\
    &protocol0,\
    &protocol1,\
    &protocol2,\
    &protocol3,\
    &protocol4\
};
int ThreadIndex[5] = {0,1,2,3,4};

struct ProtocolVTable *TransProtocals[] = \
{\
    &PROTOCOL_JB3208,\
    &PROTOCOL_J4100,\
    &PROTOCOL_J7600A,\
    &PROTOCOL_J5000,\
    &PROTOCOL_PSM4L,\
    &PROTOCOL_JXBS_3001,\
    &PROTOCOL_XUBOAFD_E90,\
    &PROTOCOL_TS_804,\
    &PROTOCOL_U6125,\
    &PROTOCOL_P300,\
    &PROTOCOL_J8100\
};


#include "uartctrl.h"
struct COMParam com={0};
struct SerialConfig trans_config={0};
void *UITD_DeviceOpen(u8 addr,struct SerialConfig *cfg)
{
    s32 fd=0;

    com.BaudRate=cfg->bitrate;
    com.DataBits=cfg->data_bit;
    com.Parity=cfg->parity_bit;
//    com.StopBits=trans_config.stop_bit;

    switch(addr)
    {
        case 0:
            fd=open(TRA_DEVS[addr], O_RDWR);
            fcntl(fd, CN_UART_COM_SET, &com);
            if(fd == 0)
            {
                return NULL;
            }
            else
            {
                printf("UART6 is sucess %d\r\n",fd);
                SetscStatues(0,1);
                return (void *)fd;
            }

        case 1:
            fd=open(TRA_DEVS[addr], O_RDWR);
            fcntl(fd, CN_UART_COM_SET, &com);
            if(fd == 0)
            {
                return NULL;
            }
            else
            {
                printf("UART7 is sucess %d\r\n",fd);
                SetscStatues(1,1);
                return (void *)fd;
            }

        case 2:
            fd=open(TRA_DEVS[addr], O_RDWR);
            fcntl(fd, CN_UART_COM_SET, &com);
            if(fd == 0)
            {
                return NULL;
            }
            else
            {
                printf("UART5 is sucess %d\r\n",fd);
                SetscStatues(2,1);
                return (void *)fd;
            }

        case 3:
            fd=open(TRA_DEVS[addr], O_RDWR);
            fcntl(fd, CN_UART_COM_SET, &com);
            if(fd == 0)
            {
                return NULL;
            }
            else
            {
                printf("UART4 is sucess %d\r\n",fd);
                SetscStatues(3,1);
                return (void *)fd;
            }

        case 4:
            fd=open(TRA_DEVS[addr], O_RDWR);
            fcntl(fd, CN_UART_COM_SET, &com);
            if(fd == 0)
            {
                return NULL;
            }
            else
            {
                printf("UART3 is sucess %d\r\n",fd);
                SetscStatues(4,1);
                return (void *)fd;
            }

        default:
            return NULL;
    }
}

static bool_t device_reads=false;
static bool_t device_writes=false;
int UITD_DeviceRead(void *dev, unsigned char *data, int size,u32 addr, u32 timeout)
{
    s64 read_timeout=DJY_GetSysTime() / 1000;
    s32 ret=0;
    int temp=0;
    if(dev == NULL)
    {
        SetscStatues(addr,0);
        return 0;
    }

    while(1)
    {
        ret= Device_Read((s32)dev, &data[temp], size, 0, 2000000);
        if (ret!=0)
        {
            temp+=ret;
        }
        else
        {
            if(temp!=0)
            {
//                printf("Device Read Data is %s\r\n",data);
                if(device_reads)
                {
                    printf("Device Read is ");
                    for(int i=0;i<temp;i++)
                    {
                        printf("%02x ",data[i]);
                    }
                    printf("\r\n");
                }
                SetscStatues(addr,1);
                return temp;
            }
            if (DJY_GetSysTime() / 1000 - read_timeout > timeout)
            {
                SetscStatues(addr,0);
                printf("Device Read is timeout\r\n");
                return 0;
            }
        }
    }
    SetscStatues(addr,1);
    return temp;
}

int UITD_DeviceWrite(void *dev, u8 *data, int len ,u32 addr)
{
    u32 ret=0;
    s32 temp=0;
    s64 write_timeout=DJY_GetSysTime() / 1000;
    if(dev == NULL)
    {
        SetscStatues(addr,0);
        return 0;
    }

    while(1)
    {
        ret= Device_Write((s32)dev, &data[temp], len, 0, 500000);

        if (ret!=0)
        {
            temp+=ret;
            if(temp>=len)
            {
//                printf("Device write Data is %s\r\n",data);
                if(device_writes)
                {
                    printf("Device write is ");
                    for(int i =0;i<temp;i++)
                    {
                        printf("%02x ",data[i]);
                    }
                    printf("\r\n");
                }
                return temp;
            }
        }
        else
        {
            if (DJY_GetSysTime() / 1000 - write_timeout > 2000)
            {
                SetscStatues(addr,0);
                return 0;
            }
        }
    }
    return temp;
}

void UITD_DeviceClose(u8 addr)
{
    void *dev = UITD_GetDevice(addr);
    close((int)dev);
}

extern struct GuiSerial GuiSerialConfig[5];
extern struct GuiIp GuiIpConfig[5];
void UITD_InitSerial()
{
    int i;
#if UITD_PRO
    for(i=0;i<UITD_SERIAL_NUM;i++)
    {
        trans_config.addr=i;
        trans_config.data_bit=GuiSerialConfig[i].gdata_bit;
        trans_config.stop_bit=GuiSerialConfig[i].gstop_bit;
        trans_config.parity_bit=GuiSerialConfig[i].gparity_bit;
        trans_config.bitrate=GuiSerialConfig[i].gbitrate;
        trans_config.dev_addr =GuiSerialConfig[i].gequipment_address;
        UITD_SetSerialConfig(i,&trans_config);
        UITD_SetSerialProtocol(i, TransProtocals[GuiSerialConfig[i].gsys_type_nub]);
    }
#else
    trans_config.addr=0;
    trans_config.data_bit=UITD_PROTOCOL_DATA_BIT_8;
    trans_config.stop_bit=UITD_PROTOCOL_STOP_1;
    trans_config.parity_bit=UITD_PROTOCOL_PARITY_TYPE_0;
    trans_config.bitrate=UITD_PROTOCOL_BITRATE_9600;
#endif

#if UITD_DEQING

    trans_config.dev_addr = 1;
    UITD_SetSerialConfig(0,&trans_config);
    UITD_SetSerialProtocol(0, &PROTOCOL_TS_804);

    trans_config.dev_addr = 2;
    UITD_SetSerialConfig(1,&trans_config);
    UITD_SetSerialProtocol(1, &PROTOCOL_TS_804);

    trans_config.dev_addr = 1;
    UITD_SetSerialConfig(2,&trans_config);
    UITD_SetSerialProtocol(2, &PROTOCOL_P300);

    trans_config.dev_addr = 0;
    UITD_SetSerialConfig(3,&trans_config);
    UITD_SetSerialProtocol(3, &PROTOCOL_U6125);

#elif THREAD
    trans_config.dev_addr = 4;
    UITD_SetSerialConfig(0,&trans_config);
    UITD_SetSerialProtocol(0, &PROTOCOL_PSM4L);

    trans_config.dev_addr = 1;
    UITD_SetSerialConfig(2,&trans_config);
    UITD_SetSerialProtocol(2, &PROTOCOL_J8100);

    trans_config.dev_addr = 2;
    UITD_SetSerialConfig(3,&trans_config);
    UITD_SetSerialProtocol(3, &PROTOCOL_XUBOAFD_E90);
#elif THREAD
    trans_config.dev_addr = 2;
    UITD_SetSerialConfig(0,&trans_config);
    UITD_SetSerialProtocol(0, &PROTOCOL_J4100);

//    trans_config.dev_addr = 1;
//    UITD_SetSerialConfig(3,&trans_config);
//    UITD_SetSerialProtocol(3, &PROTOCOL_TS_804);

    trans_config.dev_addr = 1;
    UITD_SetSerialConfig(1,&trans_config);
    UITD_SetSerialProtocol(1, &PROTOCOL_JXBS_3001);

    trans_config.dev_addr = 1;
    UITD_SetSerialConfig(2,&trans_config);
    UITD_SetSerialProtocol(2, &PROTOCOL_J7600A);

    trans_config.dev_addr = 1;
    UITD_SetSerialConfig(3,&trans_config);
    UITD_SetSerialProtocol(3, &PROTOCOL_J5000);

#endif
}

ptu32_t uitd_protocol_trans(void)
{
//    Trans_UITD_Init();
    int *thread_dbg;
    int *thread_index;
    struct ProtocolVTable *p;
    DJY_GetEventPara((ptu32_t*)&thread_index, (ptu32_t*)&thread_dbg);
    if(*thread_index<0 || *thread_index>4)
    {
        return 0;
    }
    while(1)
    {
        if(*thread_dbg)
        {
            UITD_ProtocolThread((u8)*thread_index);
        }
        p = UITD_GetSerialProtocol((u8)*thread_index);
        if(p != NULL)
        {
            DJY_EventDelay(p->delay * 1000);
        }
        else
        {
            DJY_EventDelay(1000 * 1000);
        }
    }
	return 0;

}
u16 evtt_uitd = CN_EVTT_ID_INVALID;

void trans_uitd_protocol_init()
{
    evtt_uitd = DJY_EvttRegist(EN_INDEPENDENCE, CN_PRIO_RRS, 0, 0,
            uitd_protocol_trans, NULL, 0xDD00, "uitd protocol trans");

    UITD_Init();

    if (CN_EVTT_ID_INVALID != evtt_uitd)
    {
        DJY_EventPop(evtt_uitd, NULL, 0, (ptu32_t)&ThreadIndex[0], (ptu32_t)&protocol0, 0);
#if !UITD_VERSION_MODEL
        DJY_EventPop(evtt_uitd, NULL, 0, (ptu32_t)&ThreadIndex[1], (ptu32_t)&protocol1, 0);
        DJY_EventPop(evtt_uitd, NULL, 0, (ptu32_t)&ThreadIndex[2], (ptu32_t)&protocol2, 0);
        DJY_EventPop(evtt_uitd, NULL, 0, (ptu32_t)&ThreadIndex[3], (ptu32_t)&protocol3, 0);
        DJY_EventPop(evtt_uitd, NULL, 0, (ptu32_t)&ThreadIndex[4], (ptu32_t)&protocol4, 0);
#endif
    }
    else
    {
        printf("[DEBUG] Djy_EvttRegist-palys_event_main error\r\n");
    }
}

#include <shell.h>

void SetProtocol(u32 serial,u8 protocol,struct SerialConfig trans_config)
{
    u8 serial_no, protocol_no;
    u8 protocal_table_size = 0;
    serial_no = serial;
    if(serial_no >= UITD_SERIAL_NUM)
    {
        return;
    }

    protocol_no = protocol;
    protocal_table_size = (sizeof(TransProtocals)/sizeof(struct ProtocolVTable *));
    if(protocol_no >= protocal_table_size)
    {
        return;
    }

    *TransDevTest[serial_no] = 0;
    printf("设置协议信息: 串口[%d] 协议[%d] 从机地址[%d]\r\n", serial_no, protocol_no, trans_config.dev_addr);
    UITD_SetSerialConfig(serial_no, &trans_config);
    UITD_SetSerialProtocol(serial_no, TransProtocals[protocol_no]);

    *TransDevTest[serial_no] = 1;
}

void set_uart(char *param)
{
    if(param == NULL)
    {
        printf("串口地址 比特率 校验位 数据位 停止位\r\n");
        return ;
    }
    u32 s[5] = {0};
    sscanf(param, "%d %d %d %d %d", &s[0], &s[1], &s[2], &s[3], &s[4]);
    printf("当前串口地址为%d,比特率为%d,校验位为%d,数据位为%d,停止位为%d\r\n",s[0],s[1],s[2],s[3],s[4]);
    trans_config.addr=(u8)s[0];
    trans_config.bitrate=s[1];
    trans_config.parity_bit=s[2];
    trans_config.data_bit=s[3];
    trans_config.stop_bit=s[4];

    GuiSerialConfig[s[0]].gdata_bit=trans_config.data_bit;
    GuiSerialConfig[s[0]].gparity_bit=trans_config.parity_bit;
    GuiSerialConfig[s[0]].gstop_bit=trans_config.stop_bit;
    GuiSerialConfig[s[0]].gbitrate=trans_config.bitrate;

    Write_SerialInf(GuiSerialConfig[s[0]],s[0]);

    *TransDevTest[s[0]] = 0;

    UITD_SetSerialConfig(trans_config.addr,&trans_config);

    *TransDevTest[s[0]] = 1;
}

void set_ip(char *param)
{
    if(param == NULL)
    {
        printf("指定哪个服务器 Ip地址 端口号\r\n");
        return ;
    }
    u32 s=0;
    char ip[32]={0};
    char port[16]={0};
    sscanf(param, "%d %s %s",&s, ip, port);

    printf("当前为%d服务器,IP地址为%s,端口号为%s\r\n",s,ip,port);
    strcpy(GuiIpConfig[s].gip_address,ip);
    strcpy(GuiIpConfig[s].gport,port);

    Write_IpInf(GuiIpConfig[s],s);

    SetTcpUrl(s);
    tra_breakout();
    SetConnectNumb(s);
}

bool_t __Trans_SetProtocol(char *param)
{
    if(param == NULL)
    {
        printf("串口地址 设备地址 从机地址\r\n");
        printf("[0] JB3208\r\n"\
               "[1] J4100\r\n"\
               "[2] J7600A 电气火灾监控\r\n"\
               "[3] J5000 防火门监控\r\n"\
               "[4] PSM4L 压力监控\r\n"\
               "[5] JXBS_3001 液位传感器\r\n"\
               "[6] XUBOAFD_E90 故障电弧\r\n"\
               "[7] TS_804 液位数显\r\n"\
               "[8] U6125 依爱火报通讯设备\r\n"\
               "[9] P300 压力传感器\r\n"\
               "[10] J8100\r\n");
        return false;
    }
    u32 s[4] = {0};
    u8 serial_no, protocol_no, dev_no;
    u8 protocal_table_size = 0;
    // TODO 判断shell输入合法性
    sscanf(param, "%d %d %d %d", &s[0], &s[1], &s[2], &s[3]);
    printf("当前串口地址为%d,设备地址为%d,从机地址为%d,开启状态为%d\r\n",s[0],s[1],s[2],s[3]);
    serial_no = s[0];
    if(serial_no >= UITD_SERIAL_NUM)
    {
        return false;
    }

    protocol_no = s[1];
    protocal_table_size = (sizeof(TransProtocals)/sizeof(struct ProtocolVTable *));
    printf("设置协议信息: 协议表长度[%d] \r\n", protocal_table_size);
    if(protocol_no >= protocal_table_size)
    {
        return false;
    }

    dev_no = s[2];

    GuiSerialConfig[serial_no].gequipment_address=dev_no;
    GuiSerialConfig[serial_no].gsys_type_nub=protocol_no;
    GuiSerialConfig[serial_no].gsys_type_flag=s[3];

    Write_SerialInf(GuiSerialConfig[serial_no],serial_no);

    *TransDevTest[serial_no] = 0;
    printf("设置协议信息: 串口[%d] 协议[%d] 从机地址[%d]\r\n", serial_no, protocol_no, dev_no);
    trans_config.dev_addr = dev_no;

    UITD_SetSerialConfig(serial_no, &trans_config);
    UITD_SetSerialProtocol(serial_no, TransProtocals[protocol_no]);

    *TransDevTest[serial_no] = 1;

    return true;
}

void set_unit(char *param)
{
    u32 number=atoi(param);
    UITD_AddUnit(1,1,number);
}

void set_thread(char *param)
{
    u32 s[2] = {0};
    sscanf(param, "%d %d", &s[0], &s[1]);
    switch(s[0])
    {
        case 0:
            protocol0=s[1];
            break;
        case 1:
            protocol1=s[1];
            break;
        case 2:
            protocol2=s[1];
            break;
        case 3:
            protocol3=s[1];
            break;
        case 4:
            protocol4=s[1];
            break;
        default:
            break;
    }
}
void read_device(void)
{
    if(device_reads)
    {
        device_reads=false;
    }
    else
    {
        device_reads=true;
    }
}
void write_device(void)
{
    if(device_writes)
    {
        device_writes=false;
    }
    else
    {
        device_writes=true;
    }
}
ADD_TO_ROUTINE_SHELL(setuart, set_uart, "设置端口");
ADD_TO_ROUTINE_SHELL(setip, set_ip, "设置ip");
ADD_TO_ROUTINE_SHELL(setunit, set_unit, "添加部件");
ADD_TO_ROUTINE_SHELL(thread, set_thread, "开关线程");
ADD_TO_ROUTINE_SHELL(readdevice, read_device, "打印读串口");
ADD_TO_ROUTINE_SHELL(writedevice, write_device, "打印写串口");
ADD_TO_ROUTINE_SHELL(setprotocol, __Trans_SetProtocol, "设置下游设备协议与从机地址");


static struct Est3GeneralInfo gEst3GeneralInfo={0};
static struct Est3OperationInfo gEst3OperationInfo={0};

static u32 Wrap1=0;
static u32 Wrap2=0;

void Get_Wrap(u8 *data)
{
    u32 count=1;
    for(u32 i=0;i<strlen(data);i++)
    {
        if(data[i] == 0x0a) //换行
        {
            if(1 == count)
            {
                Wrap1=i+1;
            }
            else if(2 == count)
            {
                Wrap2=i+1;
            }
            count++;
        }
    }
}

u32 Get_Spaces(u8 *data)
{
    for(int i=0;i<strlen(data);i++)
    {
        if(data[i] == 0x0d && data[i+1] == 0x0a && data[i+2] == 0x0d
        && data[i+3] == 0x0a && data[i+4] == 0x0d && data[i+5] == 0x0a)
        {
            return i+6;
        }
    }
    return 0;
}

void Get_Colon(u8 *data,u8 *result)
{
    char device[8+1]={0};
    u32 count=0;
    for(u32 i=0;i<strlen(data);i++)
    {
        if(data[i] == 0x3a)  //:冒号
        {
            if(count >= 3)
            {
                break;
            }
            else if(count >= 2)
            {
                memcpy(device+count*2, data+i+1, 4);
            }
            else
            {
                memcpy(device+count*2, data+i+1, 2);
            }
            count++;
        }
    }
    device[8]='\0';
    strcpy(result,device);
}

/********************对一般性信息的处理********************/
void Get_General_Info(u8 *data)
{
    Get_Wrap(data);
    char str[12]={0};

    memset(str, 0, strlen(str));
    memcpy(str, data, 4);
    str[4]='\0';
    strcpy(gEst3GeneralInfo.info_content,str);

    memset(str, 0, strlen(str));
    memcpy(str, data+10, 8);
    str[8]='\0';
    strcpy(gEst3GeneralInfo.time,str);

    memset(str, 0, strlen(str));
    memcpy(str, data+19, 10);
    str[10]='\0';
    strcpy(gEst3GeneralInfo.date,str);

    memset(str, 0, strlen(str));
    memcpy(str, data+30, 9);
    str[9]='\0';
    strcpy(gEst3GeneralInfo.plate_number,str);

    memset(str, 0, strlen(str));
    memcpy(str, data+41, 5);
    str[5]='\0';
    strcpy(gEst3GeneralInfo.card_number,str);

    memset(str, 0, strlen(str));
    memcpy(str, data+48, 9);
    str[9]='\0';
    strcpy(gEst3GeneralInfo.device_number,str);

    memset(str, 0, strlen(str));
    memcpy(str, data+Wrap1, 8);
    str[8]='\0';
    strcpy(gEst3GeneralInfo.device,str);

//    printf("info_content:%s\r\n",gEst3GeneralInfo.info_content);
//    printf("time:%s\r\n",gEst3GeneralInfo.time);
//    printf("date:%s\r\n",gEst3GeneralInfo.date);
//    printf("plate_number:%s\r\n",gEst3GeneralInfo.plate_number);
//    printf("card_number:%s\r\n",gEst3GeneralInfo.card_number);
//    printf("device_number:%s\r\n",gEst3GeneralInfo.device_number);
//    printf("device:%s\r\n",gEst3GeneralInfo.device);
}

u8 hand_report[8]={0xCA,0xD6,0xB1,0xA8};   //手报
u8 fire_alarm[8]={0xBB,0xF0,0xBE,0xAF};    //火警
u8 fault[8]={0xB9,0xCA,0xD5,0xCF};         //故障
u8 linkage[8]={0xC1,0xAA,0xB6,0xAF};       //联动
u8 state[8]={0xD7,0xB4,0xCC,0xAC};         //状态
u8 recovery[8]={0xBB,0xD6,0xB8,0xB4};      //恢复
u8 block[8]={0xC6,0xC1,0xB1,0xCE};         //屏蔽

u16 Get_Unit_Sts(void)
{
    if (strcmp(hand_report,gEst3GeneralInfo.info_content)==0 || strcmp(fire_alarm,gEst3GeneralInfo.info_content)==0)
    {
        printf("火警\r\n");
        return GB26875_UNIT_STS_FIRE_ALARM;
    }
    else if (strcmp(fault,gEst3GeneralInfo.info_content)==0)
    {
        printf("故障\r\n");
        return GB26875_UNIT_STS_ERROR;
    }
    else if (strcmp(recovery,gEst3GeneralInfo.info_content)==0)
    {
        printf("恢复\r\n");
        return GB26875_UNIT_STS_NORMAL;
    }
    else if (strcmp(linkage,gEst3GeneralInfo.info_content)==0)
    {
        printf("联动\r\n");
        return GB26875_UNIT_STS_WTC;
    }
    else if (strcmp(state,gEst3GeneralInfo.info_content)==0 || strcmp(block,gEst3GeneralInfo.info_content)==0)
    {
        printf("屏蔽\r\n");
        return GB26875_UNIT_STS_SHIELD;
    }
    else
    {
        printf("未知\r\n");
        return 0;
    }
}

u32 Get_Unit_Numb(void)
{
    return atoi(gEst3GeneralInfo.device);
}
/**************************************************/


/********************对控制面板操作信息的处理********************/
void Get_Operation_Info (u8 *data)
{
    Get_Wrap(data);
    u32 bufsz=strlen(data)-Wrap2;
    if(bufsz<(Wrap2-Wrap1-2))
    {
        bufsz=Wrap2-Wrap1-2;
    }
    char *str=malloc(bufsz+1);

    memset(str, 0, strlen(str));
    memcpy(str, data, 10);
    str[10]='\0';
    strcpy(gEst3OperationInfo.operation_info,str);

    memset(str, 0, strlen(str));
    memcpy(str, data+12, 8);
    str[8]='\0';
    strcpy(gEst3OperationInfo.time,str);

    memset(str, 0, strlen(str));
    memcpy(str, data+22, 10);
    str[10]='\0';
    strcpy(gEst3OperationInfo.date,str);

    memset(str, 0, strlen(str));
    memcpy(str, data+Wrap1, Wrap2-Wrap1-2);
    str[Wrap2-Wrap1-2]='\0';
    strcpy(gEst3OperationInfo.operation_content,str);

    memset(str, 0, strlen(str));
    memcpy(str, data+Wrap2, strlen(data)-Wrap2);
    str[strlen(data)-Wrap2]='\0';
    strcpy(gEst3OperationInfo.all_number,str);
    Get_Colon(gEst3OperationInfo.all_number,gEst3OperationInfo.device);


    free(str);
//    printf("operation_info:%s\r\n",gEst3OperationInfo.operation_info);
//    printf("time:%s\r\n",gEst3OperationInfo.time);
//    printf("date:%s\r\n",gEst3OperationInfo.date);
//    printf("operation_content:%s\r\n",gEst3OperationInfo.operation_content);
//    printf("all_number:%s\r\n",gEst3OperationInfo.all_number);
//    printf("device:%s\r\n",gEst3OperationInfo.device);
}

u8 recovery_control_module[16]={0xBB,0xD6,0xB8,0xB4,0xBF,0xD8,0xD6,0xC6,0xC4,0xA3,0xBF,0xE9};                           //恢复控制模块
u8 restore_alarm_silencing[16]={0xBB,0xD6,0xB8,0xB4,0xB1,0xA8,0xBE,0xAF,0xCF,0xFB,0xD2,0xF4};                           //恢复报警消音
u8 activate_reset[16]={0xBC,0xA4,0xBB,0xEE,0xB8,0xB4,0xCE,0xBB};                                                        //激活复位
u8 activate_control_module[16]={0xBC,0xA4,0xBB,0xEE,0xBF,0xD8,0xD6,0xC6,0xC4,0xA3,0xBF,0xE9};                           //激活控制模块
u8 activate_lamp_test[16]={0xBC,0xA4,0xBB,0xEE,0xB5,0xC6,0xB2,0xE2,0xCA,0xD4};                                          //激活灯测试
u8 activate_one_test[16]={0xBC,0xA4,0xBB,0xEE,0x20,0x31,0x20,0xB2,0xE2,0xCA,0xD4};                                      //激活 1 测试
u8 activate_control_panel_silencing[16]={0xBC,0xA4,0xBB,0xEE,0xBF,0xD8,0xD6,0xC6,0xC5,0xCC,0xCF,0xFB,0xD2,0xF4};        //激活控制盘消音
u8 activate_alarm_silencing[16]={0xBC,0xA4,0xBB,0xEE,0xB1,0xA8,0xBE,0xAF,0xCF,0xFB,0xD2,0xF4};                          //激活报警消音
u8 shielding_device[16]={0xC6,0xC1,0xB1,0xCE,0xC6,0xF7,0xBC,0xFE};                                                      //屏蔽器件
u8 device_enable[16]={0xC6,0xF7,0xBC,0xFE,0xCA,0xB9,0xC4,0xDC};                                                         //器件使能

u8 Get_Opt_Sts(void)
{

    for(int i=0;i<strlen(gEst3OperationInfo.operation_content);i++)
    {
        if(gEst3OperationInfo.operation_content[i] == 0xbB && gEst3OperationInfo.operation_content[i+1] == 0xd6
        && gEst3OperationInfo.operation_content[i+2] == 0xb8 && gEst3OperationInfo.operation_content[i+3] == 0xb4)
        {
            printf("复位\r\n");
            return GB26875_SYS_OPT_RESET;
        }
        else if(gEst3OperationInfo.operation_content[i] == 0xb8 && gEst3OperationInfo.operation_content[i+1] == 0xb4
        && gEst3OperationInfo.operation_content[i+2] == 0xce && gEst3OperationInfo.operation_content[i+3] == 0xbb)
        {
            printf("复位\r\n");
            return GB26875_SYS_OPT_RESET;
        }
        else if(gEst3OperationInfo.operation_content[i] == 0xcf && gEst3OperationInfo.operation_content[i+1] == 0xfb
        && gEst3OperationInfo.operation_content[i+2] == 0xd2 && gEst3OperationInfo.operation_content[i+3] == 0xf4)
        {
            printf("消音\r\n");
            return GB26875_SYS_OPT_SILIENCE;
        }
        else if(gEst3OperationInfo.operation_content[i] == 0xb2 && gEst3OperationInfo.operation_content[i+1] == 0xe2
        && gEst3OperationInfo.operation_content[i+2] == 0xca && gEst3OperationInfo.operation_content[i+3] == 0xd4)
        {
            printf("测试\r\n");
            return GB26875_SYS_OPT_TEST;
        }
        else if (strcmp(shielding_device,gEst3OperationInfo.operation_content)==0)
        {
            printf("报警消除\r\n");
            return GB26875_SYS_OPT_NO_ALARM;
        }
        else if (strcmp(device_enable,gEst3OperationInfo.operation_content)==0)
        {
            printf("复位\r\n");
            return GB26875_SYS_OPT_RESET;
        }
    }
    printf("其他\r\n");
    return 0;
}

u32 Get_Opt_Numb(void)
{
    return atoi(gEst3OperationInfo.device);
}
/********************************************************/
void Trans_UITD_Init(void)
{
    UITD_Init();
    UITD_AddSys(SysType_1_FireAlarm, 1);
//    UITD_AddUnit(1,1,0);
//    UITD_AddUnit(1,1,1020001);
//    UITD_AddUnit(1,1,1020002);
//    UITD_AddUnit(1,1,1020126);
//    UITD_AddUnit(1,1,1020127);
//    UITD_AddUnit(1,1,1020134);
//    UITD_AddUnit(1,1,1020135);
//    UITD_AddUnit(1,1,1020142);

    for(int i=0;i<=20;i++)
    {
        UITD_AddUnit(1,1,i);
//        UITD_UpdateUnitSts(1, i, GB26875_UNIT_STS_START);
    }

    Fake_GanwenInit();

    Fake_DqhzInit();

    Fake_DyjkInit();

    Fake_ZhjkInit();
}
u32 Trans_Recv_Info(u8 serial_addr, u8 dev_addr, u8 *recv_data, int len, struct FireCtrlInfo *info)
{
    info->unit_size=1;

    if(recv_data[len-2] != 0x0d && recv_data[len-1] != 0x0a)
    {
        return UITD_DATA_TYPE_NULL;
    }

    recv_data = &recv_data[2];

    if(recv_data[0] == 0x2d)  //-符号 操作信息标志
    {
        Get_Operation_Info(recv_data);
//        printf("控件状态  is %d\r\n",Get_Opt_Sts());
        printf("控件编号  is %d\r\n",Get_Opt_Numb());

        if(Get_Opt_Sts() == 0 )
        {
            return UITD_DATA_TYPE_NULL;
        }
        info->sys->addr=1;
        info->sys->opt=Get_Opt_Sts();
        info->sys->opt_id=0;

        u32 ret =Get_Spaces(recv_data);

        if(ret>0)
        {
            recv_data = &recv_data[ret];
            if(recv_data[0] != 0x2d)
            {
                Get_General_Info(recv_data);

                if(Get_Unit_Sts() == 0 )
                {
                    return UITD_DATA_TYPE_SYS_OPT;
                }

//                printf("控件状态  is %d\r\n",Get_Unit_Sts());
                printf("控件编号  is %d\r\n",Get_Unit_Numb());
                info->unit[0]->sys_addr = 1;
                info->unit[0]->addr=Get_Unit_Numb();
                info->unit[0]->status=Get_Unit_Sts();

                return UITD_DATA_TYPE_SYS_OPT | UITD_DATA_TYPE_UNIT_STS;
            }
        }

        return UITD_DATA_TYPE_SYS_OPT;
    }
    else  //控件状态信息
    {
        Get_General_Info(recv_data);

        if(Get_Unit_Sts() == 0 )
        {
            return UITD_DATA_TYPE_NULL;
        }

//        printf("控件状态  is %d\r\n",Get_Unit_Sts());
        printf("控件编号  is %d\r\n",Get_Unit_Numb());
        info->unit[0]->sys_addr = 1;
        info->unit[0]->addr=Get_Unit_Numb();
        info->unit[0]->status=Get_Unit_Sts();

        u32 ret =Get_Spaces(recv_data);
        if(ret>0)
        {
            recv_data = &recv_data[ret];
            if(recv_data[0] == 0x2d)
            {
                Get_Operation_Info(recv_data);
//                printf("控件状态  is %d\r\n",Get_Opt_Sts());
                printf("控件编号  is %d\r\n",Get_Opt_Numb());

                if(Get_Opt_Sts() == 0 )
                {
                    return UITD_DATA_TYPE_UNIT_STS;
                }
                info->sys->addr=1;
                info->sys->opt=Get_Opt_Sts();
                info->sys->opt_id=0;

                return UITD_DATA_TYPE_UNIT_STS | UITD_DATA_TYPE_SYS_OPT;
            }
        }

        return UITD_DATA_TYPE_UNIT_STS;
    }
}

struct ProtocolVTable ptc_1 =
{
    UITD_PROTOCOL_TYPE_3,
    0,
    5000,
    1024,
    0,
    0,
    0,
    Trans_Recv_Info,
};
/*************************TEST*******************************/

void Trans_UITD_AddSys(u8 sys_type, u8 sys_addr)
{
    UITD_AddSys(sys_type,sys_addr);
}

void Trans_UITD_AddUnit(u8 sys_addr, u8 unit_type)
{
    UITD_AddUnit(sys_addr,unit_type,Get_Unit_Numb());
}

void Trans_UITD_UpdateUnitSts(u8 sys_addr)
{
    UITD_UpdateUnitSts(sys_addr,Get_Unit_Numb(),Get_Unit_Sts());
}

struct FireCtrlUnit *Trans_UITD_FindUnit(u8 sys_addr)
{
    return UITD_FindUnit(sys_addr,Get_Unit_Numb());
}

void Trans_UITD_UpdateSysOpt(u8 sys_addr, u8 opt_id)
{
    UITD_UpdateSysOpt(sys_addr,Get_Opt_Sts(),opt_id);
}

struct FireCtrlSystem * Trans_UITD_FindSys(u8 sys_addr)
{
    return UITD_FindSys(sys_addr);
}

#define TRANS_RECV_MAX 512
void trans_test(void)
{
//    u8 *GeneralInfo="故障  ::  15:54:40 08/18/1998  控制盘:01  卡:02  器件:0001\r\n01020001项目部烟感";
//    Get_General_Info(GeneralInfo);
//    printf("控件状态  is %d\r\n",Get_Unit_Sts());
//    printf("控件编号  is %d\r\n",Get_Unit_Numb());

    u8 *OperationInfo="-操作命令-  14:06:13 2020/07/17  控制盘:01  卡:00  器件:00  LCD命令级别:4\r\n恢复控制模块\r\n盘:FFFFFFFFFFFFFFFF 卡:02 器件:0134\r\n      01020001项目部烟感1\r\n\r\n\r\n故障  ::  15:54:40 08/18/1998  控制盘:01  卡:02  器件:0001\r\n01020001项目部烟感";
    Get_Operation_Info(OperationInfo);
    printf("控件状态  is %d\r\n",Get_Opt_Sts());
    printf("控件编号  is %d\r\n",Get_Opt_Numb());
    for(int i=0;i<strlen(OperationInfo);i++)
    {
        if(OperationInfo[i] == 0x0d && OperationInfo[i+1] == 0x0a && OperationInfo[i+2] == 0x0d
           && OperationInfo[i+3] == 0x0a && OperationInfo[i+4] == 0x0d && OperationInfo[i+5] == 0x0a)
        {
            OperationInfo = &OperationInfo[i+6];
            printf("OperationInfo is %s\r\n",OperationInfo);
            Get_General_Info(OperationInfo);
            printf("控件状态  is %d\r\n",Get_Unit_Sts());
            printf("控件编号  is %d\r\n",Get_Unit_Numb());
        }
    }
}
////
//    struct FireCtrlUnit *unit=NULL;
//    struct FireCtrlSystem *system=NULL;
//
//    Trans_UITD_AddSys(1,1);
//    Trans_UITD_AddUnit(1,0);
//    Trans_UITD_UpdateUnitSts(1);
//    unit = UITD_FindUnit(1,Get_Unit_Numb());
//    printf("X :%x\r\n",unit->status);
//    Trans_UITD_UpdateSysOpt(1,0);
//    system = UITD_FindSys(1);
//    printf("X :%x\r\n",system->opt);

//    printf("1 is %s\r\n",hand_report);
//    printf("1 is %s\r\n",fire_alarm);
//    printf("1 is %s\r\n",fault);
//    printf("1 is %s\r\n",linkage);
//    printf("1 is %s\r\n",state);
//    printf("1 is %s\r\n",recovery);
//    printf("1 is %s\r\n",recovery_control_module);
//    printf("1 is %s\r\n",restore_alarm_silencing);
//    printf("1 is %s\r\n",activate_reset);
//    printf("1 is %s\r\n",activate_control_module);
//    printf("1 is %s\r\n",activate_lamp_test);
//    printf("1 is %s\r\n",activate_one_test);
//    printf("1 is %s\r\n",activate_control_panel_silencing);
//    printf("1 is %s\r\n",activate_alarm_silencing);
//    printf("1 is %s\r\n",block);
//    printf("1 is %s\r\n",shielding_device);
//    printf("1 is %s\r\n",device_enable);
//      }

/**
 * 设备源地址 类似于sn号 唯一识别
 */
u8 UITD_GetClientIp(u8 ip[6])
{
    char *sn;
    char sn_year[2] = {0};
    char sn_week[2] = {0};
    sn = Sn_Get();


    sn_year[0] = sn[6];
    sn_year[1] = sn[7];

    sn_week[0] = sn[8];
    sn_week[1] = sn[9];

    ip[0] = (u8)atoi(sn_year);

    ip[1] = (u8)atoi(sn_week);


    ip[2] = sn[11];
    ip[3] = sn[12];
    ip[4] = sn[13];
    ip[5] = sn[14];

    return 1;
}
