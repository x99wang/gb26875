#ifndef QUEUE_H_
#define QUEUE_H_

/*
 * queue.h
 *
 *  Created on: 2020年7月23日
 *      Author: WangXi
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#ifndef true
#define true 1
#endif

#ifndef false
#define false 0
#endif

#define QUEUE_SIZE 10

#define SUCCESS 1
#define FAILURE 0

typedef int32_t ElementType; /*队列元素类型*/

/*定义队列结构*/
 struct QueueInfo
{
     //todo：改为指针
    ElementType value; /*队列存储的数据*/
    struct QueueInfo *next; /*指向队列的下一个元素*/
};

typedef struct QueueInfo QueueInfo_st;

/**
 * 创建一个队列
 *
 * @author WangXi (2020/7/31)
 *
 * @param void
 *
 * @return QueueInfo_st* 队列指针
 */
QueueInfo_st *Queue_CreateQueue(void);

/**
 * 入队
 *
 * @author WangXi (2020/7/31)
 *
 * @param s 队列指针
 * @param value 入队元素
 *
 * @return int 结果
 */
int Queue_Push(QueueInfo_st *s,ElementType value);

/**
 * 出队
 *
 * @author WangXi (2020/7/31)
 *
 * @param s 队列指针
 * @param value 输出元素
 *
 * @return int 结果
 */
int Queue_Pop(QueueInfo_st *s,ElementType *value);

/**
 * 队列顶部元素
 *
 * @author WangXi (2020/7/31)
 *
 * @param s 队列指针
 * @param value 输出袁术
 *
 * @return int 结果
 */
int Queue_Top(QueueInfo_st *s,ElementType *value);

/**
 * 队列判空
 *
 * @author WangXi (2020/7/31)
 *
 * @param s 队列指针
 *
 * @return int 是否为空
 */
int Queue_IsEmpty(QueueInfo_st *s);

void Queue_Print(QueueInfo_st *s);

#endif /* QUEUE_H_ */
