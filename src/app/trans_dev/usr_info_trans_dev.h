/*
 * usr_info_trans_dev.h
 * 用户信息传输装置接口定义
 *
 *  Created on: 2020年7月22日
 *      Author: WangXi
 */
#ifndef GB26875_USR_INFO_TRANS_DEV_H_
#define GB26875_USR_INFO_TRANS_DEV_H_

#include "stdint.h"
#include "protocol_GB26875.h"

#if NANXIAO
#include "nanxiao.h"
#include "lincon.h"
#include "wenjian.h"
#define    printf(format, ...)        debug_printf(format, ##__VA_ARGS__)

#define true  1
#define false 0

typedef int s32;
typedef char s8;
typedef long long s64;


void SetPSM4LScan(u8 addr);
#endif

#define UITD_VERSION_MODEL 0    //  协议模块形态

#define UITD_AUTO_UPLOAD 1  // FIXME 信息更新自动添加至上传队列开关
#define UITD_AUTO_RELOAD 1  // 重发机制
#define UITD_AUTO_HEARTBEAT 1   // 心跳

#define UITD_DATA_TYPE_NULL             0
#define UITD_DATA_TYPE_SYS_STS          1<<1
#define UITD_DATA_TYPE_SYS_OPT          1<<2
#define UITD_DATA_TYPE_SYS_VER          1<<3
#define UITD_DATA_TYPE_SYS_CFG          1<<4
#define UITD_DATA_TYPE_UNIT_STS         1<<5
#define UITD_DATA_TYPE_UNIT_CFG         1<<6
#define UITD_DATA_TYPE_UNIT_AD          1<<7
#define UITD_DATA_TYPE_SUCC             1<<29
#define UITD_DATA_TYPE_FAIL             1<<30


#define UITD_PROTOCOL_FUNC  1

#if UITD_PROTOCOL_FUNC
/* 下游通讯定义 */

#if UITD_VERSION_MODEL
#define UITD_SERIAL_NUM         1   //  最大串口数量
#define UITD_UNIT_SIZE_MAX      1024 //  每个系统所接的部件上限
#else
#define UITD_SERIAL_NUM         5   //  最大串口数量
#define UITD_UNIT_SIZE_MAX      512 //  每个系统所接的部件上限
#endif
#define UITD_PROTOCOL_TYPE_1            (u8)1   //  主(FAS)-从(信息装置)
#define UITD_PROTOCOL_TYPE_2            (u8)2   //  从(FAS)-主(信息装置)
#define UITD_PROTOCOL_TYPE_3            (u8)3   //  打印口监听
#define UITD_PROTOCOL_TYPE_4            (u8)4   //  双向都有
#define UITD_PROTOCOL_TYPE_5            (u8)5   //  主(FAS)-从(信息装置)-主(FAS)

#define UITD_PROTOCOL_BITRATE_4800      4800    // 波特率
#define UITD_PROTOCOL_BITRATE_9600      9600    // 波特率
#define UITD_PROTOCOL_BITRATE_14400     14400   // 波特率
#define UITD_PROTOCOL_BITRATE_19200     19200   // 波特率
#define UITD_PROTOCOL_BITRATE_38400     38400   // 波特率
#define UITD_PROTOCOL_BITRATE_115200    115200  // 波特率

#define UITD_PROTOCOL_DATA_BIT_6        6   //  数据位6
#define UITD_PROTOCOL_DATA_BIT_7        7   //  数据位7
#define UITD_PROTOCOL_DATA_BIT_8        8   //  数据位8

#define UITD_PROTOCOL_PARITY_TYPE_0     0   //  无校验
#define UITD_PROTOCOL_PARITY_TYPE_1     1   //  奇校验
#define UITD_PROTOCOL_PARITY_TYPE_2     2   //  偶校验
#define UITD_PROTOCOL_PARITY_TYPE_3     3   //  Mark校验
#define UITD_PROTOCOL_PARITY_TYPE_4     4   //  空格校验

#define UITD_PROTOCOL_STOP_1            1   //  1
#define UITD_PROTOCOL_STOP_2            2   //  2

#define UITD_RELOAD_TIMEOUT             10000   //  10秒 重新上传机制超时时间
#define UITD_RELOAD_COUNT               3       //  3次 重新上传机制重传次数

struct FireCtrlInfo
{
    struct FireCtrlSystem *sys;
    struct FireCtrlUnit **unit;
    u16 unit_size;
};

struct ProtocolVTable {
    u8 type;                //  协议类型
    u8 sys_type;            //  系统类型
    u32 timeout;            //  读下游设备超时时间
    u32 data_size_max;      //  最大数据长度
    u32 data_size_min;      //  最小数据长度
    int (*build_request)(u8 serial_addr, u8 dev_addr, u8 *send_data);
    int (*build_response)(u8 serial_addr, u8 dev_addr, u32 request_ret, u8 *send_data);
    int (*parse_response)(u8 serial_addr, u8 dev_addr, u8 *recv_data, int len, struct FireCtrlInfo *info);
    int delay;
};

struct SerialConfig
{
    u8 addr;                //  端口地址
    u8 data_bit;            //  数据位
    u8 stop_bit;            //  停止位
    u8 parity_bit;          //  校验位
    u8 dev_addr;            //  从机地址 下位机自配置地址
    u32 bitrate;            //  比特率
    struct ProtocolVTable *protocol; // 下游协议

};

struct ReloadInfo
{
    u8 count;
    u16 no;
    u8 *buff;
    u32 buff_size;
    s64 timestamp;
};

int UITD_DeviceWrite(void *dev, u8 *data, int len ,u32 addr);

int UITD_DeviceRead(void *dev, unsigned char *data, int size,u32 addr, u32 timeout);

void *UITD_DeviceOpen(u8 addr, struct SerialConfig *cfg);

void UITD_DeviceClose(u8 addr);

void *UITD_GetDevice(u8 addr);


void UITD_ProtocolThread(u8 serial_addr);

u8 __UITD_ProtocolWork(struct SerialConfig *serial_config, u8 *data_buff, u32 data_size);

void __UITD_ProtocolHandler(u8 type, struct FireCtrlInfo *info);

void UITD_InitSerial();

void UITD_SetSerialConfig(u8 addr, struct SerialConfig *cfg);

struct SerialConfig * UITD_GetSerialConfig(u8 addr);

void UITD_SetSerialProtocol(u8 addr, struct ProtocolVTable *protocol);

struct ProtocolVTable * UITD_GetSerialProtocol(u8 addr);

#endif

/**
 * 初始化用户信息传输装置
 *
 * @author WangXi (2020/7/31)
 *
 * @param void
 */
void UITD_Init(void);

/**
 * 添加消防建筑设施系统
 *
 * @author WangXi (2020/7/31)
 *
 * @param sys_type 系统类型
 * @param sys_addr 系统地址
 *
 * @return struct FireCtrlSystem* 消防建筑设施系统据结构数指针
 */
struct FireCtrlSystem * UITD_AddSys(u8 sys_type, u8 sys_addr);

/**
 * 添加消防建筑设施部件
 *
 * @author WangXi (2020/7/31)
 *
 * @param sys_addr 系统地址 如直接接入装置则填0
 * @param unit_type 部件类型
 * @param unit_addr 部件地址
 *
 * @return struct FireCtrlUnit* 消防建筑设施部件数据结构指针
 */
struct FireCtrlUnit *UITD_AddUnit(u8 sys_addr, u8 unit_type, u32 unit_addr);

/**
 * 移除系统及系统下的所有部件
 *
 * @author WangXi (2020/7/31)
 *
 * @param sys_addr 系统地址
 */
void UITD_DeleteSys(u8 sys_addr);

/**
 * 移除指定消防建筑设施部件
 *
 * @author WangXi (2020/7/31)
 *
 * @param sys_addr 系统地址
 * @param unit_addr 部件地址
 */
void UITD_DeleteUnit(u8 sys_addr, u32 unit_addr);

/**
 * 获取指定消防建筑设施系统数据结构
 *
 * @author WangXi (2020/7/31)
 *
 * @param sys_addr 系统地址
 *
 * @return struct FireCtrlSystem* 消防建筑设施系统数据结构指针
 */
struct FireCtrlSystem * UITD_FindSys(u8 sys_addr);

/**
 * 获取指定消防建筑设施部件数据结构
 *
 * @author WangXi (2020/7/31)
 *
 * @param sys_addr 系统地址
 * @param unit_addr 部件地址
 *
 * @return struct FireCtrlUnit* 消防建筑设施部件数据结构指针
 */
struct FireCtrlUnit *UITD_FindUnit(u8 sys_addr, u32 unit_addr);

/**
 * 获取本用户信息传输装置数据结构
 *
 * @author WangXi (2020/7/31)
 *
 * @param void
 *
 * @return struct UserInfoTransDev* 本机数据结构指针
 */
struct UserInfoTransDev *UITD_FindDev(void);

/**
 * 更换消防建筑设施系统在装置上的地址
 *
 * @author WangXi (2020/7/31)
 *
 * @param sys_addr_old 旧地址
 * @param sys_addr_new 新地址
 *
 * @return int 置换结果
 */
int UITD_UpdateSysAddr(u8 sys_addr_old, u8 sys_addr_new);

/**
 * 更换消防建筑设施部件在系统中的地址
 *
 * @author WangXi (2020/7/31)
 *
 * @param sys_addr 所在系统地址
 * @param unit_addr_old 旧地址
 * @param unit_addr_new 新地址
 *
 * @return int 置换结果
 */
int UITD_UpdateUnitAddr(u8 sys_addr, u32 unit_addr_old, u32 unit_addr_new);

/**
 * 更新/设置消防建筑设施系统状态
 *
 * @author WangXi (2020/7/31)
 *
 * @param sys_addr 系统地址
 * @param sys_sts 系统状态
 */
void UITD_UpdateSysSts(u8 sys_addr, u16 sys_sts);

/**
 * 更新置消防建筑设施系统操作
 *
 * @author WangXi (2020/7/31)
 *
 * @param sys_addr 系统地址
 * @param opt_flag 操作类型标志
 * @param opt_id 操作员
 */
void UITD_UpdateSysOpt(u8 sys_addr, u8 opt_flag, u8 opt_id);

/**
 * 更新/设置消防建筑设施系统版本号
 *
 * @author WangXi (2020/7/31)
 *
 * @param sys_addr 系统地址
 * @param ver_main 主版本号
 * @param ver_user 用户版本号
 */
void UITD_UpdateSysVer(u8 sys_addr, u8 ver_main, u8 ver_user);

/**
 * 更新/设置消防建筑设施系统配置情况
 *
 * @author WangXi (2020/7/31)
 *
 * @param sys_addr 系统地址
 * @param cfg 配置情况
 * @param cfg_len 配置况数据情长度
 */
void UITD_UpdateSysCfg(u8 sys_addr, u8 *cfg, u8 cfg_len);

/**
 * 更新/设置消防建筑设施部件运行状态
 *
 * @author WangXi (2020/7/31)
 *
 * @param sys_addr 部件所在系统地址
 * @param unit_addr 部件地址
 * @param unit_sts 部件运行状态
 */
void UITD_UpdateUnitSts(u8 sys_addr, u32 unit_addr, u16 unit_sts);

/**
 * 更新/设置消防建筑设施部件模拟量值
 *
 * @author WangXi (2020/7/31)
 *
 * @param sys_addr 部件所在系统地址
 * @param unit_addr 部件地址
 * @param ad_type 模拟量类型
 * @param ad_val 模拟量值
 */
void UITD_UpdateUnitAd(u8 sys_addr, u32 unit_addr, u8 ad_type, u16 ad_val);

/**
 * 更新/设置消防建筑设施部件说明
 *
 * @author WangXi (2020/7/31)
 *
 * @param sys_addr 部件所在系统地址
 * @param unit_addr 部件地址
 * @param cfg 部件说明 最长31字节
 */
void UITD_UpdateUnitCfg(u8 sys_addr, u32 unit_addr, u8 *cfg);

/**
 * 更新/设置用户信息传输装置状态
 *
 * @author WangXi (2020/7/31)
 *
 * @param status 运行状态
 */
void UITD_UpdateDevSts(u8 status);

/**
 * 更新/设置用户信息传输装置操作
 *
 * @author WangXi (2020/7/31)
 *
 * @param opt 操作类型标志
 * @param opt_id 操作员 默认0
 */
void UITD_UpdateDevOpt(u8 opt, u8 opt_id);

/**
 * 更新/设置用户信息传输装置版本号
 *
 * @author WangXi (2020/7/31)
 *
 * @param ver_main 主版本号
 * @param ver_user 次版本号
 */
void UITD_UpdateDevVer(u8 ver_main, u8 ver_user);

/**
 * 更新/设置用户信息传输装置配置情况
 *
 * @author WangXi (2020/7/31)
 *
 * @param cfg 配置情况
 * @param cfg_len 数据长度
 */
void UITD_UpdateDevCfg(u8 *cfg, u8 cfg_len);

/**
 * 查询防建筑设施消系统状态并格式化为单元对象数据格式
 *
 * @author WangXi (2020/7/31)
 *
 * @param sys 消防建筑设施系统数组
 * @param obj_cnt 单元对象数目
 * @param info_data 输出单元对象数据
 *
 * @return int 数据长度
 */
int UITD_QuerySysSts(struct FireCtrlSystem sys[], int obj_cnt, u8 *info_data);
int UITD_QuerySysOpt(struct FireCtrlSystem *sys, int opt_cnt, u8 start[6], u8 *info_data);
int UITD_QuerySysVer(struct FireCtrlSystem *sys, u8 *info_data);
int UITD_QuerySysCfg(struct FireCtrlSystem sys[], int obj_cnt, u8 *info_data);
int UITD_QuerySysTime(struct FireCtrlSystem *sys, u8 *info_data);

/**
 * 查询防建筑设施消部件状态并格式化为单元对象数据格式
 *
 * @author WangXi (2020/7/31)
 *
 * @param unit 消防建筑设施部件数组
 * @param obj_cnt 单元对象数目
 * @param info_data 输出单元对象数据
 *
 * @return int 数据长度
 */
int UITD_QueryUnitSts(struct FireCtrlUnit unit[], int obj_cnt, u8 *info_data);
int UITD_QueryUnitCfg(struct FireCtrlUnit unit[], int obj_cnt, u8 *info_data);
int UITD_QueryUnitAd(struct FireCtrlUnit unit[], int obj_cnt, u8 *info_data);

/**
 * 查询用户信息传输装置状态并格式化为单元对象数据格式
 *
 * @author WangXi (2020/7/31)
 *
 * @param info_data 输出单元对象数据
 *
 * @return int 数据长度
 */
int UITD_QueryDevSts(u8 *info_data);
int UITD_QueryDevOpt(int opt_cnt, u8 start[6], u8 *info_data);
int UITD_QueryDevVer(u8 *info_data);
int UITD_QueryDevCfg(u8 *info_data);
int UITD_QueryDevTime(u8 *info_data);

/**
 * 查询并输出相应信息单元数据
 *
 * @author WangXi (2020/7/31)
 *
 * @param info_body 包含查询信息的信息单元数据
 * @param len 数据长度
 * @param result 输出包含查询结果的信息单元数据
 *
 * @return int 输出数据的长度
 */
int UITD_QueryInfo(u8 *info_body, int len, u8 *result);

/**
 * 添加数据到上传队列
 *
 * @author WangXi (2020/7/31)
 *
 * @param data 数据
 * @param len 数据长度
 */
void UITD_AddToUploadBuff(u8 *data, int len);


/**
 * 重置业务流水号
 */
void UITD_ResetInfoNo();

/**
 * 上传数据线程函数
 *
 * @author WangXi (2020/7/31)
 *
 * @param void
 */
void UITD_UploadThread(void);

void UITD_AddToReloadBuff(u8 *data, u32 len, u16 info_no);

/**
 * 上传数据
 *
 * @author WangXi (2020/7/31)
 *
 * @param data 数据
 * @param len 长度
 * @param recv 上传回应 如果有
 *
 * @return int 操作结果
 */
int UITD_Upload(u8 *data, int len, u8 *recv);

/**
 * 下行数据读取线程函数
 *
 * @author WangXi (2020/7/31)
 *
 * @param void
 */
void UITD_DownloadThread(void);

/**
 * 下行数据读取
 *
 * @author WangXi (2020/7/31)
 *
 * @param data 读到的数据
 * @param timepout 读数据超时时间 单位毫秒
 *
 * @return int 读到的数据长度
 */
int UITD_Download(u8 *data, s32 timeout);

/**
 * 获取本机时间
 *
 * @author WangXi (2020/7/31)
 *
 * @param time 本机时间
 */
void UITD_GetCurrentTime(u8 time[6]);

s64 UITD_GetTimestamp();

void UITD_Sleep(s32 timeout);

u8 UITD_GetClientIp(u8 ip[6]);

u8 UITD_GetServerIp(u8 ip[6]);

#if UITD_AUTO_HEARTBEAT
int UITD_HeartBeat();
#endif

/**
 * 同步本机时间
 *
 * @author WangXi (2020/7/31)
 *
 * @param time 目标同步时间
 */
void UITD_SyncTime(u8 time[6]);

u32 UITD_CRC_U16_MODBUS(u8 *buff, u32 len);

u8 UITD_U16CheckCRC(u8 *read_buf, u32 len);
void UITD_ReloadThread();
#endif /* GB26875_USR_INFO_TRANS_DEV_H_ */
