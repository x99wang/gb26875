/*
 * queue.c
 *
 *  Created on: 2020年7月23日
 *      Author: WangXi
 */

#if DJYOS
#include "../trans_dev/queue.h"
#else
#include "queue.h"
#endif

/*创建队列，外部释放内存*/
QueueInfo_st *Queue_CreateQueue(void)
{
    QueueInfo_st *queue = (QueueInfo_st *)malloc(sizeof(QueueInfo_st));
    if(queue == NULL)
    {
        printf("malloc failed\n");
        return NULL;
    }
    queue->next = NULL;
    return queue;
}

/*入队列，0表示成，非0表示出错*/
int Queue_Push(QueueInfo_st *s,ElementType value)
{
    /*用来保存尾部指针*/
    QueueInfo_st *temp;
    if(s == NULL)
    {
        return FAILURE;
    }
    temp = (QueueInfo_st *)malloc(sizeof(QueueInfo_st));
//  printf("Queue_Push: %x, %x\r\n", s, value);
    if(NULL == temp)
    {
        printf("push malloc failed\n");
        return FAILURE;
    }
//  printf("find linked list\r\n");
    /*找到链表的尾部*/
    while(s->next != NULL)
    {
        s = s->next;
    }
//  printf("find linked list end\r\n");
    temp->value = value;
    temp->next = s->next;
    s->next = temp;

    return SUCCESS;
}

/*出队列*/
int Queue_Pop(QueueInfo_st *s,ElementType *value)
{
    QueueInfo_st *temp;
    /*首先判断队列是否为空*/
    if(Queue_IsEmpty(s))
        return FAILURE;
    /*找出队列顶元素*/
    *value = s->next->value;
    /*保存等下需要free的指针*/
    temp = s->next;
    /*更换队列顶的位置*/
    s->next = s->next->next;

    /*释放队列顶节点内存*/
    if(temp != NULL)/*先判断指针是否为空*/
        free(temp);
    temp = NULL;

    return SUCCESS;
}

/*访问队列顶元素*/
int Queue_Top(QueueInfo_st *s,ElementType *value)
{
    /*首先判断队列是否为空*/
    if(Queue_IsEmpty(s))
    {
        return FAILURE;
    }
    *value = s->next->value;
    return SUCCESS;
}

/*判断队列是否为空，空返回1，未空返回0*/
int Queue_IsEmpty(QueueInfo_st *s)
{
    if(s == NULL)
    {
        return true;
    }
    /*队列顶指针为空，则队列为空*/
    if(s->next == NULL)
    {
//        printf("队列为空\n\r");
        return true;
    }

    return false;
}

void Queue_Print(QueueInfo_st *s)
{
    u32 i = 1;
    if(Queue_IsEmpty(s))
    {
        printf("队列[0x%x]为空\r\n", s);
        return;
    }
    while(s->next != NULL)
    {
        printf("%02d):[0x%x]{0x%x}\r\n", i, s->next, s->next->value);
        s = s->next;
        i++;
    }
}

