/*
 * protocol_GB26875.c
 *
 *  Created on: 2020年7月22日
 *      Author: WangXi
 */
#if DJYOS
#include "../trans_dev/protocol_GB26875.h"
#include "../trans_dev/usr_info_trans_dev.h"
#else
#include "protocol_GB26875.h"
#include "usr_info_trans_dev.h"
#endif
#include "stdlib.h"


#if NANXIAO_MAC
typedef struct
{
    unsigned char data4 :8;
    unsigned char data3 :8;
    unsigned char data2 :8;
    unsigned char data1 :8;
}Long_Data;

typedef union
{
    unsigned long u32Data;
    Long_Data u8data;
}U32L;
#endif

#define UITD 1  //  用户信息传输装置

static struct GB26875Info recv_info;            //  国标信息数据单元缓存
static struct GB26875InfoBody recv_info_body;   //  国标信息单元对象缓存

void GB26875_FormatTimeFlag(u8 time[6])
{	//	低位在前
	u8 t[6];
	if(time == NULL) return;
#if UITD
    //  国标格式中的时间标签通过用户信息装置的时间方法获取
    UITD_GetCurrentTime(t);

	time[5] = t[0];
	time[4] = t[1];
	time[3] = t[2];
	time[2] = t[3];
	time[1] = t[4];
	time[0] = t[5];

#endif
}

u8 GB26875_CheckSum(u8 *buf, int firstbyte, int end)
{
    char add_val=0x00;
    int i;
    for(i=firstbyte;i<=end;i++)
    {
       add_val=(add_val+buf[i])&0xff;
    }
    return add_val;
}

#if NANXIAO_MAC

/****************************************************************************
*   函数名称 : GetMac
*
*   返回类型 : void
*
*   参数类型 : void
*----------------------------------------------------------------------------
*   功能概述 : 获取系统MAC地址
*
*   设计时间 : 2019-06-18
****************************************************************************/
static char GetMac(char str1,char str2)
{
    char rt_val=0;
    if(str1>=0x30 && str1<=0x39)str1=str1-0x30;
    else if(str1>=0x41 && str2<=0x46)str1=str1-55;
    else str1=0;

    if(str2>=0x30 && str2<=0x39)str2=str2-0x30;
    else if(str2>=0x41 && str2<=0x46)str2=str2-55;
    else str2=0;

    rt_val = (str1<<4)+str2;
    return rt_val;
}

static void GB_IPMACToSM4(char *str2)
{
    char ip_addr1=0;
    char ip_addr2=0;
    char ip_addr3=0;
    char ip_addr4=0;
    char mac_addr1=0;
    char mac_addr2=0;
    char mac_addr3=0;
    char mac_addr4=0;
    char mac_addr5=0;
    char mac_addr6=0;
    char mac_addr[32];

    unsigned long yuan_ma[4] = {0x01234567, 0x89abcdef, 0xfedcba98, 0x76543210};
    unsigned long jia_ma[4] = {0};
    unsigned long yuan_count =0;
    U32L a1,a2,a3,a4;

    ip_addr1 = GetCFGIP(0, 0);
    ip_addr2 = GetCFGIP(0, 1);
    ip_addr3 = GetCFGIP(0, 2);
    ip_addr4 = GetCFGIP(0, 3);
    rtdb_get_data_value_by_name("hmi_system_set","eth0_mac",&mac_addr);

    mac_addr1 = GetMac(mac_addr[0],mac_addr[1]);
    mac_addr2 = GetMac(mac_addr[3],mac_addr[4]);
    mac_addr3 = GetMac(mac_addr[6],mac_addr[7]);
    mac_addr4 = GetMac(mac_addr[9],mac_addr[10]);
    mac_addr5 = GetMac(mac_addr[12],mac_addr[13]);
    mac_addr6 = GetMac(mac_addr[15],mac_addr[16]);

    a1.u8data.data1 = ip_addr1;
    a1.u8data.data2 = ip_addr2;
    a1.u8data.data3 = ip_addr3;
    a1.u8data.data4 = ip_addr4;

    a2.u8data.data1 = mac_addr1;
    a2.u8data.data2 = mac_addr2;
    a2.u8data.data3 = mac_addr3;
    a2.u8data.data4 = mac_addr4;

    a3.u8data.data1 = mac_addr5;
    a3.u8data.data2 = mac_addr6;
    a3.u8data.data3 = 0;
    a3.u8data.data4 = 0;

    a4.u8data.data1 = 0;
    a4.u8data.data2 = 0;
    a4.u8data.data3 = 0;
    a4.u8data.data4 = 0;


    yuan_ma[0] = a1.u32Data;
    yuan_ma[1] = a2.u32Data;
    yuan_ma[2] = a3.u32Data;
    yuan_ma[3] = a4.u32Data;


    sm4_one_enc(mk,yuan_ma,jia_ma);
    a1.u32Data = jia_ma[0];
    a2.u32Data = jia_ma[1];
    a3.u32Data = jia_ma[2];
    a4.u32Data = jia_ma[3];


    str2[0] = a1.u8data.data1;
    str2[1] = a1.u8data.data2;
    str2[2] = a1.u8data.data3;
    str2[3] = a1.u8data.data4;

    str2[4] = a2.u8data.data1;
    str2[5] = a2.u8data.data2;
    str2[6] = a2.u8data.data3;
    str2[7] = a2.u8data.data4;

    str2[8] = a3.u8data.data1;
    str2[9] = a3.u8data.data2;
    str2[10] = a3.u8data.data3;
    str2[11] = a3.u8data.data4;

    str2[12] = a4.u8data.data1;
    str2[13] = a4.u8data.data2;
    str2[14] = a4.u8data.data3;
    str2[15] = a4.u8data.data4;
}
#endif

/**
 * 构建完整数据包
 *
 * @author WangXi (2020/7/10)
 *
 * @param no   业务流水号
 * @param data 数据单元
 * @param data_len 数据单元长度
 * @param opt_byte 命令字节
 */
int GB26875_BuildData(struct GB26875Info *info, u8 *send_buf)
{
#if NANXIAO_MAC
    u8 str2[16];
    GB_IPMACToSM4((char *)str2);
#endif
    if(info == NULL || send_buf == 0)
    {
        return 0;
    }
    send_buf[0] = 64;                         /*00    启动符    固定值 @ 0x64         */
    send_buf[1] = 64;                         /*01    启动符    固定值 @ 0x64         */
    // FIXME 业务流水号
    send_buf[2] = info->no & 0xFF;            /*02    业务流水号 低字节在前                 */
    send_buf[3] = (info->no>>8) & 0xFF;       /*03    业务流水号 低字节在前                 */
    send_buf[4] = info->version & 0xFF;       /*04    协议版本:主版本号,固定值1     */
    send_buf[5] = (info->version>>8) & 0xFF;  /*05    协议版本:用户版本号                    */

    memcpy(&send_buf[6], info->time, 6);       /*06    时间标签                                     */
    memcpy(&send_buf[12], info->src, 6);       /*12    源地址 低字节在前                      */
    memcpy(&send_buf[18], info->dct, 6);       /*18    目的地址 低字节在前                   */

    if(info->data_len > 1024) info->data_len = 1024;

    send_buf[24] = info->data_len&0xff;        /*24    应用数据单元长度    低字节在前         */
    send_buf[25] = (info->data_len>>8)&0xff;   /*25    应用数据单元长度    低字节在前         */
    send_buf[26] = info->opt;                  /*26    命令字节     见表2 对应ControlComm定义*/

    if(info->data != NULL) memcpy(&send_buf[27], info->data, info->data_len);

#if NANXIAO_MAC
    send_buf[4] = str2[0];                     /*04 协议版本：主版本号，固定值1         */
    send_buf[5] = str2[1];                     /*05 协议版本：用户版本号                */
    send_buf[6] = str2[2];                     /*06 时间标签：秒                        */
    send_buf[11] = str2[3];                    /*11 时间标签：年                        */
    send_buf[12] = str2[4];//IP_check;                  /*12    源地址 低字节在前                   */
    send_buf[13] = str2[5];//mac_addr1;                 /*13    源地址 低字节在前                   */
    send_buf[14] = str2[6];//ip_addr4;                  /*14    源地址 低字节在前                   */
    send_buf[15] = str2[7];//mac_addr2;                 /*15    源地址 低字节在前                   */
    send_buf[16] = str2[8];//ip_addr3;                  /*16    源地址 低字节在前                   */
    send_buf[17] = str2[9];//mac_addr3;                 /*17    源地址 低字节在前                   */
    send_buf[18] = str2[10];//ip_addr2;                 /*18    目的地址 低字节在前                 */
    send_buf[19] = str2[11];//mac_addr4;                /*19    目的地址 低字节在前                 */
    send_buf[20] = str2[12];//ip_addr1;                 /*20    目的地址 低字节在前                 */
    send_buf[21] = str2[13];//mac_addr5;                /*21    目的地址 低字节在前                 */
    send_buf[22] = str2[14];//mac_addr6;                /*22    目的地址 低字节在前                 */
    send_buf[23] = str2[15];//mac_check;                /*23    目的地址 低字节在前                 */
#endif
    info->check_sum = GB26875_CheckSum(send_buf, 2, info->data_len + 27);
    send_buf[info->data_len + 27] = info->check_sum;

    send_buf[info->data_len + 28] = 35;
    send_buf[info->data_len + 29] = 35;
    return info->data_len + 30;
}

int GB26875_BuildSysSts(struct FireCtrlSystem *sys, u8 *str)
{
    if(sys == NULL || str == 0)
        return 0;

    str[0] = sys->type;
    str[1] = sys->addr;
    str[2] = sys->status & 0XFF;
    str[3] = (sys->status >> 8) & 0xFF;
    GB26875_FormatTimeFlag(&str[GB26875_OBJLEN_SYS_STS]);
    return GB26875_OBJLEN_SYS_STS + 6;
}

int GB26875_BuildSysOpt(struct FireCtrlSystem *sys, u8 *str)
{
    if(sys == NULL || str == 0)
        return 0;

    str[0] = sys->type;
    str[1] = sys->addr;
    str[2] = sys->opt;
    str[3] = sys->opt_id;
    GB26875_FormatTimeFlag(&str[GB26875_OBJLEN_SYS_OPT]);
    return GB26875_OBJLEN_SYS_OPT + 6;
}

int GB26875_BuildSysVer(struct FireCtrlSystem *sys, u8 *str)
{
    if(sys == NULL || str == 0)
        return 0;

    str[0] = sys->type;
    str[1] = sys->addr;
    str[2] = sys->version;
    str[3] = sys->version_user;
    GB26875_FormatTimeFlag(&str[GB26875_OBJLEN_SYS_VER]);
    return GB26875_OBJLEN_SYS_VER + 6;
}

int GB26875_BuildSysCfg(struct FireCtrlSystem *sys, u8 *str)
{
    if(sys == NULL || str == 0)
        return 0;

    str[0] = sys->type;
    str[1] = sys->addr;
    str[2] = sys->cfg_len;
    memcpy(&str[3], sys->cfg, sys->cfg_len);
    GB26875_FormatTimeFlag(&str[sys->cfg_len + 3]);
    return sys->cfg_len + 3 + 6;
}

int GB26875_BuildSysTime(struct FireCtrlSystem *sys, u8 *str)
{
    if(sys == NULL || str == 0)
        return 0;

    str[0] = sys->type;
    str[1] = sys->addr;
//    memcpy(&str[2], sys->time, 6);
    GB26875_FormatTimeFlag(&str[2]);
    return 8;
}

int GB26875_BuildUnitSts(struct FireCtrlSystem *sys, struct FireCtrlUnit *unit, u8 *str)
{
    if(sys == NULL || unit == NULL || str == 0)
    {
        printf("GB26875_BuildUnitSts sys or unit is NULL\r\n");
        return 0;
    }
    str[0] = sys->type;
    str[1] = sys->addr;
    str[2] = unit->type;
    str[3] = unit->addr & 0XFF;
    str[4] = (unit->addr >> 8) & 0XFF;
    str[5] = (unit->addr >> 16) & 0XFF;
    str[6] = (unit->addr >> 24) & 0XFF;
    str[7] = unit->status & 0XFF;
    str[8] = (unit->status >> 8) & 0xFF;
    memcpy(&str[9], unit->dsc, 31);
    GB26875_FormatTimeFlag(&str[GB26875_OBJLEN_UNIT_STS]);
    return GB26875_OBJLEN_UNIT_STS + 6;
}

int GB26875_BuildUnitAd(struct FireCtrlSystem *sys, struct FireCtrlUnit *unit, u8 *str)
{
    if(sys == NULL || unit == NULL || str == 0)
        return 0;

    str[0] = sys->type;
    str[1] = sys->addr;
    str[2] = unit->type;
    str[3] = unit->addr & 0XFF;
    str[4] = (unit->addr >> 8) & 0XFF;
    str[5] = (unit->addr >> 16) & 0XFF;
    str[6] = (unit->addr >> 24) & 0XFF;
    str[7] = unit->ad_type;
    str[8] = unit->ad_val & 0XFF;
    str[9] = (unit->ad_val >> 8) & 0xFF;
    GB26875_FormatTimeFlag(&str[GB26875_OBJLEN_UNIT_AD]);
    return GB26875_OBJLEN_UNIT_AD + 6;
}

int GB26875_BuildUnitCfg(struct FireCtrlSystem *sys, struct FireCtrlUnit *unit, u8 *str)
{
    if(sys == NULL || unit == NULL || str == 0)
        return 0;

    str[0] = sys->type;
    str[1] = sys->addr;
    str[2] = unit->type;
    str[3] = unit->addr & 0XFF;
    str[4] = (unit->addr >> 8) & 0XFF;
    str[5] = (unit->addr >> 16) & 0XFF;
    str[6] = (unit->addr >> 24) & 0XFF;
    memcpy(&str[7], unit->dsc, 31);
    GB26875_FormatTimeFlag(&str[GB26875_OBJLEN_UNIT_CFG]);
    return GB26875_OBJLEN_UNIT_CFG + 6;
}

int GB26875_BuildDevSts(struct UserInfoTransDev *dev, u8 *str)
{
    if(dev == NULL || str == 0)
        return 0;

    str[0] = dev->status;
    GB26875_FormatTimeFlag(&str[GB26875_OBJLEN_DEV_STS]);
    return GB26875_OBJLEN_DEV_STS + 6;
}

int GB26875_BuildDevOpt(struct UserInfoTransDev *dev, u8 *str)
{
    if(dev == NULL || str == 0)
        return 0;

    str[0] = dev->opt;
    str[1] = dev->opt_id;
    GB26875_FormatTimeFlag(&str[GB26875_OBJLEN_DEV_OPT]);
    return GB26875_OBJLEN_DEV_OPT + 6;
}

int GB26875_BuildDevVer(struct UserInfoTransDev *dev, u8 *str)
{
    if(dev == NULL || str == 0)
        return 0;

    str[0] = dev->version;
    str[1] = dev->version_user;
    GB26875_FormatTimeFlag(&str[GB26875_OBJLEN_DEV_VER]);
    return GB26875_OBJLEN_DEV_VER + 6;
}

int GB26875_BuildDevCfg(struct UserInfoTransDev *dev, u8 *str)
{
    if(dev == NULL || str == 0)
        return 0;
    str[0] = dev->cfg_len;
    memcpy(&str[1], dev->cfg, dev->cfg_len);
    GB26875_FormatTimeFlag(&str[dev->cfg_len + 1]);
    return dev->cfg_len + 1 + 6;
}

int GB26875_BuildDevTime(u8 *str)
{
    if(str == 0)
        return  0;
    GB26875_FormatTimeFlag(str);
    return 6;
}

int GB26875_FormatData(u8 *data, int len, struct GB26875Info *recv_info)
{
    u8 check_sum = 0;
    if(len < 30)
    {
        printf("数据包长度小于下限.(实际长度=%d)\r\n", len);
        return false;
    }
    if(data[0] != 64 || data[1] != 64)
    {
        printf("数据包起始符错误.([%x][%x])\r\n",data[0], data[1]);
        return false;
    }
    memset(recv_info, 0, sizeof(struct GB26875Info));

    recv_info->no = data[2] | (u16)(data[3] << 8);

    recv_info->version = data[4] | (u16)(data[5] << 8);

    memcpy(recv_info->time, &data[6], 6);

    memcpy(recv_info->src, &data[12], 6);

    memcpy(recv_info->dct, &data[18], 6);

    recv_info->data_len = data[24] | (u16)(data[25] << 8);
    if(len < recv_info->data_len + 30)
    {
        printf("数据包不完整.(解析应有长度=%d, 实际长度=%d)\r\n", recv_info->data_len + 30, len);
        return false;
    }
    if(recv_info->data_len + 27 > 1024)
    {
        printf("数据包长度大于上限.(实际长度=%d)\r\n", recv_info->data_len + 27);
        return false;
    }

    recv_info->opt = data[26];

    memset(recv_info->data, 0, 1024);
    memcpy(recv_info->data, &data[27], recv_info->data_len);

    recv_info->check_sum = data[recv_info->data_len + 27];

    if(data[recv_info->data_len + 28] != 35 || data[recv_info->data_len + 29] != 35)
    {
        printf("数据包结束符错误.([%x][%x])\r\n",data[recv_info->data_len + 28], data[recv_info->data_len + 29]);
        return false;
    }
    check_sum = GB26875_CheckSum(data, 2, recv_info->data_len + 26);
    if(check_sum != recv_info->check_sum)
    {
        printf("数据包校验和错误.(应为=%x, 收到=%x)\r\n", check_sum, recv_info->check_sum);
        return false;
    }

    return true;
}

int GB26875_FormatInfoBody(u8 *data, int len, struct GB26875InfoBody *recv_info_body)
{
    if(len < 2)
    {
        return false;
    }
    memset(recv_info_body, 0, sizeof(struct GB26875InfoBody));
    recv_info_body->type = data[0];
    recv_info_body->obj_cnt = data[1];
    memcpy(recv_info_body->data, &data[2], len - 2);
    recv_info_body->data_len = len - 2;
    return true;
}

void GB26875_FormatSysSts(u8 *data, int data_len, int obj_cnt, struct FireCtrlSystem *sys)
{
    int i;
    int step = 2;
    for(i=0; i<obj_cnt && step*(i+1)<=data_len; i++)
    {
        sys[i].type = data[step * i];
        sys[i].addr = data[step * i + 1];
    }
}

u8 GB26875_FormatSysOpt(u8 *data, int data_len, struct FireCtrlSystem *sys, u8 start_time[6])
{

    if(data_len < 9)
    {
        return 0;
    }
    sys->type = data[0];
    sys->addr = data[1];
    memcpy(start_time, &data[3], 6);
    return data[2];
}

void GB26875_FormatSysVer(u8 *data, int data_len, struct FireCtrlSystem *sys)
{
    if(data_len >= 2)
    {
        sys->type = data[0];
        sys->addr = data[1];
    }
}

void GB26875_FormatSysCfg(u8 *data, int data_len, int obj_cnt, struct FireCtrlSystem *sys)
{
    int i;
    int step = 2;
    for(i=0; i<obj_cnt && step*(i+1)<=data_len; i++)
    {
        sys[i].type = data[step * i];
        sys[i].addr = data[step * i + 1];
    }
}

void GB26875_FormatSysTime(u8 *data, int data_len, struct FireCtrlSystem *sys)
{
    if(data_len >= 2)
    {
        sys->type = data[0];
        sys->addr = data[1];
    }
}

void GB26875_FormatUnitSts(u8 *data, int data_len, int obj_cnt, struct FireCtrlSystem *sys, struct FireCtrlUnit *unit)
{
    int i;
    int step = 6;
    for(i=0; i<obj_cnt && step*(i+1)<=data_len; i++)
    {
        sys[i].type = data[step * i];
        sys[i].addr = data[step * i + 1];
        unit[i].sys_addr = data[step * i + 1];
        unit[i].addr = data[step * i + 3] |
                      (data[step * i + 4] << 8) |
                      (data[step * i + 5] << 16) |
                      (data[step * i + 6] << 24);
    }
}

void GB26875_FormatUnitAd(u8 *data, int data_len, int obj_cnt, struct FireCtrlSystem *sys, struct FireCtrlUnit *unit)
{
    int i;
    int step = 6;
    for(i=0; i<obj_cnt && step*(i+1)<=data_len; i++)
    {
        sys[i].type = data[step * i];
        sys[i].addr = data[step * i + 1];
        unit[i].sys_addr = data[step * i + 1];
        unit[i].addr = data[step * i + 3] |
                      (data[step * i + 4] << 8) |
                      (data[step * i + 5] << 16) |
                      (data[step * i + 6] << 24);
    }
}

void GB26875_FormatUnitCfg(u8 *data, int data_len, int obj_cnt, struct FireCtrlSystem *sys, struct FireCtrlUnit *unit)
{
    int i;
    int step = 6;
    for(i=0; i<obj_cnt && step*(i+1)<=data_len; i++)
    {
        sys[i].type = data[step * i];
        sys[i].addr = data[step * i + 1];
        unit[i].sys_addr = data[step * i + 1];
        unit[i].addr = data[step * i + 3] |
                      (data[step * i + 4] << 8) |
                      (data[step * i + 5] << 16) |
                      (data[step * i + 6] << 24);
    }
}

u8 GB26875_FormatDevSts(u8 *data, int data_len)
{
    return data[0];
}

u8 GB26875_FormatDevOpt(u8 *data, int data_len, u8 start_time[6])
{
    memcpy(start_time, &data[1], 6);
    return data[0];
}

u8 GB26875_FormatDevVer(u8 *data, int data_len)
{
    return data[0];
}

u8 GB26875_FormatDevCfg(u8 *data, int data_len)
{
    return data[0];
}

u8 GB26875_FormatDevTime(u8 *data, int data_len)
{
    return data[0];
}

u8 GB26875_FormatCard(u8 *data, int data_len)
{
    return data[0];
}
