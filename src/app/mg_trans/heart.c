#include "djyos.h"
#include <stdlib.h>
#include <stdio.h>
#include "host_send.h"
#include "../trans_dev/usr_info_trans_dev.h"
#include "../GuiWin/Info/WinSwitch.h"


#include "lock.h"
struct MutexLCB *ping_lock = NULL;
void PingLockInit(void)
{
    ping_lock = Lock_MutexCreate("ping_lock");
     if (NULL == ping_lock)
     {
         printf("Create ping_lock fail.\r\n");
         Lock_MutexDelete(ping_lock);
     }
}

static bool_t ping_flag=false;
bool_t GetPingFlag(void)
{
    return ping_flag;
}
s32 xq_ping(const char *url)
{
#include <netdb.h>
#define CN_PING_WAIT_TIME         (3*1000*mS)

    Lock_MutexPend(ping_lock,CN_TIMEOUT_FOREVER);

    struct in_addr ipaddr;
    struct hostent *host;
    s32 len = 60;
    u8 *buf = net_malloc(len);
    if(0 == inet_aton(url,&ipaddr))
    {
        //use the dns to get the ip
        host = gethostbyname(url);
        if(NULL != host)
        {
            memcpy((void *)&ipaddr,host->h_addr_list[0],sizeof(ipaddr));
        }
        else
        {
            printf("Unknown host:%s\n\r",url);
            Lock_MutexPost(ping_lock);
            return -1;
        }
    }

    u32 timestart = (u32)DJY_GetSysTime();
    bool_t ping_r = Icmp_EchoRequest(ipaddr.s_addr,buf,len,CN_PING_WAIT_TIME);
    u32 timeend = (u32)DJY_GetSysTime();
    u32 timeused = (u32)(timeend - timestart);

    net_free(buf);

    if(ping_r)
    {
        Lock_MutexPost(ping_lock);
        return timeused > CN_PING_WAIT_TIME ? -1 : timeused/mS;
    }
    else
    {
        Lock_MutexPost(ping_lock);
        return -1;
    }
#undef CN_PING_WAIT_TIME
}

extern struct GuiIp GuiIpConfig[5];
extern s32 FIRST_HB_FLAG;
//����
ptu32_t heart_trans(void)
{
    s64 heart_timeout=DJY_GetSysTime()/1000;
    s32 pings=0;
    while(1)
    {
        //if(DJY_GetSysTime()/(s64)1000 - heart_timeout > (s64)7000)
        if(DJY_GetSysTime()/1000 - heart_timeout > 7000)
        {
            if(GetSocketState())
            {
//                UITD_HeartBeatThread();
                printf("����\r\n");
                UITD_HeartBeat();
            }
            else
            {
                FIRST_HB_FLAG = false;
            }

            pings=xq_ping(GuiIpConfig[0].gip_address);
            if(pings<0)
            {
                ping_flag=false;
            }
            else
            {
                ping_flag=true;
            }
            heart_timeout=DJY_GetSysTime()/1000;
        }

        DJY_EventDelay(100*1000);
    }
    return 0;
}
u16 evtt_heart = CN_EVTT_ID_INVALID;
void trans_heart_init()
{
    PingLockInit();

    evtt_heart = DJY_EvttRegist(EN_CORRELATIVE, CN_PRIO_RRS, 0, 0,
            heart_trans, NULL, 0xD00, "socket heart trans");

    if (CN_EVTT_ID_INVALID != evtt_heart)
    {
        DJY_EventPop(evtt_heart, NULL, 0, 0, 0, 0);
    }
    else
    {
        printf("[DEBUG] Djy_EvttRegist-evtt_packet error\r\n");
    }
}
