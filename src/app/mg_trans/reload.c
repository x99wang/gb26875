#include "djyos.h"
#include <stdlib.h>
#include <stdio.h>
#include "host_send.h"
#include "../trans_dev/usr_info_trans_dev.h"
//�ط�
ptu32_t reload_trans(void)
{
    while(1)
    {
        if(GetSocketState())
        {
            UITD_ReloadThread();
        }
        DJY_EventDelay(1000*1000);
    }
    return 0;
}
u16 evtt_reload = CN_EVTT_ID_INVALID;
void trans_reload_init()
{
    evtt_reload = DJY_EvttRegist(EN_CORRELATIVE, CN_PRIO_RRS, 0, 0,
            reload_trans, NULL, 0x800, "socket reload trans");

    if (CN_EVTT_ID_INVALID != evtt_reload)
    {
        DJY_EventPop(evtt_reload, NULL, 0, 0, 0, 0);
    }
    else
    {
        printf("[DEBUG] Djy_EvttRegist-evtt_packet error\r\n");
    }
}
