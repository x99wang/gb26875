#include "djyos.h"
#include <stdlib.h>
#include <stdio.h>
#include "../trans_dev/usr_info_trans_dev.h"
ptu32_t socket_down_packet_trans(void)
{
    while(1)
    {
        //数据出队列并上传报文
        UITD_DownloadThread();

        DJY_EventDelay(10*1000);
    }
    return 0;
}
u16 evtt_down_packet = CN_EVTT_ID_INVALID;
void trans_socket_down_init()
{
    evtt_down_packet = DJY_EvttRegist(EN_CORRELATIVE, CN_PRIO_RRS-1, 0, 0,
            socket_down_packet_trans, NULL, 0x1200, "socket down packet trans");

    if (CN_EVTT_ID_INVALID != evtt_down_packet)
    {
        DJY_EventPop(evtt_down_packet, NULL, 0, 0, 0, 0);
    }
    else
    {
        printf("[DEBUG] Djy_EvttRegist-evtt_down_packet error\r\n");
    }
}
