#include <time.h>
#include "stdint.h"
#include "stddef.h"
#include "djyos.h"
#include <stdio.h>
#include <dbug.h>
#include "../mg_http/mg_http_client.h"
#include "../mg_trans/host_send.h"
#include "../GuiWin/Info/WinSwitch.h"
/*************MONGOOSE HTTP********************/

static char get_time[64]={0};
static bool_t http_flag=false;

#define HTTP_TIME_URL      "http://120.55.103.0/"

//mongoose http 操作句柄
static void cb_time_ev_handler(struct mg_connection *nc, int ev, void *ev_data)
{
    struct http_message *hm = (struct http_message *) ev_data;

    switch (ev)
    {
        case MG_EV_CONNECT:
            if (*(int *)ev_data != 0)
            {
                printf("warning: %s MG_EV_CONNECT failed: %d!\r\n", __FUNCTION__, *(int *)ev_data);
                nc->flags |= MG_F_CLOSE_IMMEDIATELY;
            }
            break;
        case MG_EV_HTTP_REPLY:
            nc->flags |= MG_F_CLOSE_IMMEDIATELY;

            if (hm->resp_code > 400)
            {
                printf("error: cb_asr_register_ev_handler ASR_STA_WS_ERROR, resp_code=%d!\r\n", hm->resp_code);
                break;
            }

            struct mg_str *date = mg_get_http_header(hm, "Date");
            if(date != NULL)
            {
                memcpy(get_time, date->p, date->len);
            }
            printf("get_time is %s\r\n",get_time);
            break;
        case MG_EV_CLOSE:

            http_flag=true;

            break;
        default:
            break;
    }
}

extern struct GuiIp GuiIpConfig[];
bool_t http_time_init(void)
{
    http_flag=false;
    //初始mongoose
    struct mg_connection *nc;
    struct mg_mgr mgr;
    mg_mgr_init(&mgr, NULL);
    // HTTP_TIME_URL,
    nc = mg_connect_http(&mgr, cb_time_ev_handler, \
            GuiIpConfig[0].gip_address, \
            "Content-Type: application/json\r\nUser-Agent: Mongoose/6.15\r\n", NULL);

    if (!nc)
    {
        mg_mgr_free(&mgr);
        return http_flag;
    }
    s64 timemark=DJY_GetSysTime() / 1000;
    while (1)
    {
        if (DJY_GetSysTime() / 1000 - timemark > 5000)
        {
            printf("warning: %s:%d timeout!\r\n", __FUNCTION__, __LINE__);
            http_flag=false;
            break;
        }

        if(http_flag)
        {
            break;
        }

        mg_mgr_poll(&mgr, 500);
    }
    mg_mgr_free(&mgr);
    return http_flag;

}

char *web_gmt_time()
{
    if(!http_time_init())
    {
        return NULL;
    }
    else
    {
        return get_time;
    }

}
/*************MONGOOSE HTTP************************/


/**
 * 当前时间戳 单位ms
 */
s64 get_timestamp()
{
    s64 temp_time;
    Time_Time(&temp_time);
    temp_time -= ((s64)8*3600);
//    temp_time -= (9 * 60); // FIXME 时间快9分钟
    return temp_time * 1000;
}

/**
 * 当前日历
 * 不需做任何加减处理
 */
struct tm *get_datetime(struct tm *datetime)
{
    s64 temp_time;
    Time_Time(&temp_time);
//    temp_time -= (9 * 60); // FIXME 时间快9分钟
    Time_LocalTime_r(&temp_time, datetime);
    datetime->tm_year += 1900;
    return datetime;
}

const char MONTHS[12][3] = {
        "Jan",
        "Feb",
        "Mar",
        "Apr",
        "May",
        "Jun",
        "Jul",
        "Aug",
        "Sep",
        "Oct",
        "Nov",
        "Dec"
};

/**
 * 根据月份首三位字母返回月数
 */
u16 mon2i(const char mon[])
{
    for(int i=0; i<12; i++)
    {
        char m[4];
        memcpy(m, MONTHS[i], 3);
        m[3] = '\0';
        s32 ss = strcmp(mon, m);
        if(ss == 0)
            return i+1;
    }
    return 0xFFFF;
}
/**
 * 配置时间
 */
void set_datetime(s32 year,s32 month,s32 day,s32 hour,s32 min,s32 sec)
{
    struct tm datetime;
    datetime.tm_year=year;
    datetime.tm_mon=month-1;
    datetime.tm_mday=day;
    datetime.tm_hour=hour;
    datetime.tm_min=min;
    datetime.tm_sec=sec;

    datetime.tm_year -= 1900;

    Time_SetDateTime(&datetime);
}

void GetLogTime(char *time,int len)
{
    struct tm date;
    get_datetime(&date);
    snprintf(time,len,"%d.%d.%d",date.tm_year,date.tm_mon+1,date.tm_mday);
    printf("get time is %s\r\n",time);
}
/*
char gmt_time[0x20];
// gmt time
char *web_gmt_time()
{
//    char *uri = "http://api.m.taobao.com/rest/api3.do?api=mtop.common.getTimestamp";
   // char *uri = "http://118.24.149.145:8083/api/";
   // http_request(uri, NULL, NULL, NULL, web_gmt_time_http_handler);

    char *uri = "http://120.55.103.0/";
    struct http_message *response = MG_HttpGet(uri);
    if(response == NULL)
    {
        printf("__WebGmtTime error!\r\n");
        return NULL;
    }
    struct mg_str *date = mg_get_http_header(response, "Date");
    if(date != NULL)
    {
        memcpy(gmt_time, date->p, date->len);
    }
    M_Free((void *)response);

    return gmt_time;
}
*/

/**
 * http date gmt时间格式化为tm
 */
void gmt2ymd(char *gmt_str, struct tm *time)
{
    if(gmt_str == NULL)
        return;

    char year_str[5];
    strncpy(year_str, gmt_str+12, 4);
    year_str[4] = '\0';
    time->tm_year = atoi(year_str);

    char month_str[4];
    strncpy(month_str, gmt_str+8, 3);
    month_str[3] = '\0';
    time->tm_mon = mon2i(month_str);

    char day_str[3];
    strncpy(day_str, gmt_str+5, 2);
    day_str[2] = '\0';
    time->tm_mday = atoi(day_str);

    char hour_str[3];
    strncpy(hour_str, gmt_str+17, 2);
    hour_str[2] = '\0';
    time->tm_hour = atoi(hour_str);

    char min_str[3];
    strncpy(min_str, gmt_str+20, 2);
    min_str[2] = '\0';
    time->tm_min = atoi(min_str);

    char sec_str[3];
    strncpy(sec_str, gmt_str+23, 2);
    sec_str[2] = '\0';
    time->tm_sec = atoi(sec_str);

    time->tm_us = 0;
}
static bool_t AddLogFlag=false;
void web_time_init()
{
    char *gmt_time = web_gmt_time();
    struct tm gmt_datetime;
    if(gmt_time == NULL)
        return;
//    printf("格林威治时间:[%s]", gmt_time);
    gmt2ymd(gmt_time, &gmt_datetime);
    if(gmt_datetime.tm_hour > 30 ||gmt_datetime.tm_year>3000 || gmt_datetime.tm_min > 60)
        return;
    gmt_datetime.tm_hour += 8;
    gmt_datetime.tm_year -= 1900;
    gmt_datetime.tm_mon -= 1;

    Time_SetDateTime(&gmt_datetime);

    if(gmt_datetime.tm_hour==25)
    {
        AddLogFlag=true;
    }

    if(gmt_datetime.tm_mday==1 && AddLogFlag)
    {
        remove_all(1,15);
        AddLogFlag=false;
    }
    else if(gmt_datetime.tm_mday==16 && AddLogFlag)
    {
        remove_all(16,31);
        AddLogFlag=false;
    }
}

void print_datetime(struct tm rtc_datetime)
{
    printf("\r\n当前系统时间:%04d-%02d-%02d %02d:%02d:%02d 星期%d\r\n",
            rtc_datetime.tm_year, rtc_datetime.tm_mon + 1, rtc_datetime.tm_mday,
            rtc_datetime.tm_hour, rtc_datetime.tm_min, rtc_datetime.tm_sec, rtc_datetime.tm_wday);
}

void print_nowDatetime()
{
    struct tm date;
    get_datetime(&date);
    print_datetime(date);
}

#include <shell.h>

void set_time(char *param)
{
    s32 time[6] = {0};
    sscanf(param, "%d %d %d %d %d %d", &time[0], &time[1], &time[2], &time[3], &time[4], &time[5]);
    set_datetime(time[0],time[1],time[2],time[3],time[4],time[5]);
}

void set_guilog(char *param)
{
    char s1[16],s2[64];
    sscanf(param,"%s %s",s1,s2);
    SetLogPageInf(s1,s2);
}
ADD_TO_ROUTINE_SHELL(settime, set_time, "设置时间");
ADD_TO_ROUTINE_SHELL(setguilog, set_guilog, "设置日志显示");
