/*
 * upgrade.c
 *
 *  Created on: 2020年11月12日
 *      Author: WangXi
 */


#include "upgrade.h"
#include <stdio.h>

#include <iboot_Info.h>

#if APP_UPGRADE

enum CheckVerify
{
    VerifyNo,           //还未得出校验结果
    VerifyFail,         //校验失败
    VerifySuccess,      //校验成功
};
static enum CheckVerify check_verify = VerifyNo;

void Set_check_verify (enum CheckVerify flag)
{
    check_verify = flag;
}

enum CheckVerify Get_check_verify (void)
{
    return check_verify;
}

u8 *update_addr = 0;
u8 *update_start_addr = 0;

#define NETWORK_UPDATEAPP_FILE_NAME      "djyapp.bin"

void __app_update_exe(void)
{
    int ret;
    u32 file_size;
    u8 time_buf[4],production[5];
    char app_info[MutualPathLen];
    u8 app_info_size = 0;

    if(Get_check_verify() == VerifySuccess)
    {
        file_size = update_addr - update_start_addr;
        memset(app_info, 0, MutualPathLen);
        memcpy(app_info + app_info_size, &update_start_addr, sizeof(update_start_addr));
//            printf("sizeof(update_start_addr)       =        %d.\r\n",sizeof(update_start_addr));
        app_info_size += sizeof(update_start_addr);
        memcpy(app_info + app_info_size, &file_size, sizeof(file_size));
//          printf("sizeof(file_size)       =        %d.\r\n",sizeof(file_size));
        app_info_size += sizeof(file_size);

        if((app_info_size + sizeof(NETWORK_UPDATEAPP_FILE_NAME)) > MutualPathLen)
        {
            printf("ERROR: iboot_info length exceeded!\r\n");
            return  ;
        }
        memcpy(app_info + app_info_size, NETWORK_UPDATEAPP_FILE_NAME, sizeof(NETWORK_UPDATEAPP_FILE_NAME));
//        set_update_result_flag(UpdateResult_success);
        printf("======Set_RunIbootUpdateApp=====\r\n");
        Iboot_SetRunIbootUpdateApp();
        Iboot_FillMutualUpdatePath(app_info, MutualPathLen);
        printf("update_start_addr   = %x,\r\n",(u32)update_start_addr);
        printf("update_addr   = %x,\r\n",(u32)update_addr);
        printf("read memory successful。\r\n");
        Iboot_SetUpdateSource("1");  //设置升级方式为，从ram获取app数据
        DJY_EventDelay(2000*1000);
//        sddev_control(WDT_DEV_NAME, WCMD_POWER_DOWN, 0);
        Set_RunIbootFlag();

/*********待测试，优先使用下面那个接口*************/
        CPU_Reboot();
        //CPU_RestartSystem();

    }
    else if(Get_check_verify() == VerifyFail)
    {
        DJY_EventDelay(10000*1000);
//            Set_update_flag(0);
        runapp(0);
    }
}

static int mark_precent = 0;

int mem_update(u8 *buf,s32 len, int fsize, int timeout)
{
    (void)timeout;
    static int mark_pos = 0;
    u32 file_size;
    struct AppHead *apphead;
    if(update_addr == 0)
    {
        update_addr = (u8*)malloc(2*1024*1024);
        if(update_addr == NULL)
        {
            printf("update buff malloc error!\r\n");
            return 0;
        }
        update_start_addr = update_addr;
        mark_pos = 0;
        printf("update_addr   = %x\r\n", (u32)update_addr);
        printf("downloading ...    ");
    }
    if(update_addr)
    {
        memcpy(update_addr,buf,len);
        update_addr += len;
//        printf("buf %02x-%02x-%02x\r\n", buf[0],buf[1],buf[2]);
        int pos = update_addr-update_start_addr;

        if (mark_precent != pos*100/fsize) {
            mark_precent = pos*100/fsize;
            printf("\b\b\b%2d%%", mark_precent);
        }
    }

    if (len > 0 ) mark_pos += len;

    if(fsize > 0 && mark_pos >= fsize)
    {
        apphead = (struct AppHead *)update_start_addr;
        file_size = update_addr - update_start_addr;
        if((apphead->app_bin_size != apphead->file_size) || (apphead->file_size != file_size))
        {
            printf("ERROR: file size diff.apphead_appbinsize = %d, apphead_filesize = %d, file_size = %d!\r\n",apphead->app_bin_size, apphead->file_size, file_size);
            Set_check_verify(VerifyFail);
        }
        else
        {
            if(XIP_AppFileCheck(update_start_addr))
            {
                printf("File check success\r\n");
                Set_check_verify(VerifySuccess);
            }
            else
            {
                printf("File check fail\r\n");
                Set_check_verify(VerifyFail);
            }
            __app_update_exe();
        }
    }
    return len;
}

static file_update_offset = 0;
static char upd_file_name[] = "/yaf2/app.bin";
int file_update(u8 *buf,s32 len, int fsize, int timeout)
{
    FILE *app_bin;
    long file_size = 0;
    s32 ret = 0;

    if(file_update_offset == 0)
        remove(upd_file_name);

    app_bin = fopen(upd_file_name,"a+");
    if(app_bin == NULL) return 0;

    do{
        ret += fwrite(buf + ret, len - ret, 1, app_bin);
    }while(ret < len);

    file_update_offset += len;
    if(file_update_offset == fsize)
    {
        file_update_offset = 0;
        printf("下载升级文件完成[%d]\r\n", fsize);
        updateapp(0);
    }
    else
    {
        printf("升级文件已经下载%d%\r\n", file_update_offset * 100 / fsize);
    }
    return len;
}
#endif
