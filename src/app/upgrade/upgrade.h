/*
 * upgrade.h
 *
 *  Created on: 2020��11��12��
 *      Author: WangXi
 */

#ifndef APP_UPGRADE_UPGRADE_H_
#define APP_UPGRADE_UPGRADE_H_
#include <stdint.h>

#define APP_UPGRADE         1

#if APP_UPGRADE
int mem_update(u8 *buf,s32 len, int fsize, int timeout);
int file_update(u8 *buf,s32 len, int fsize, int timeout);
#endif


#endif /* APP_UPGRADE_UPGRADE_H_ */
