/*
 * uitd_reporter.h
 *
 *  Created on: 2020��10��29��
 *      Author: WangXi
 */

#ifndef APP_UITD_REPORTER_H_
#define APP_UITD_REPORTER_H_

#include <djyos.h>
#include <systime.h>
#include "trans_dev/queue.h"
#include "mg_http/mg_http_client.h"

#define UITD_DEBUG_REPORT 0

void UITD_ReportStatusInit();

void UITD_ReportPush(char *str, u32 r, u32 m);

#endif /* APP_UITD_REPORTER_H_ */
