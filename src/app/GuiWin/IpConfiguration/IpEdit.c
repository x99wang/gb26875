//----------------------------------------------------
// Copyright (c) 2014, SHENZHEN PENGRUI SOFT CO LTD. All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------
// Copyright (c) 2014 著作权由都江堰操作系统开源开发团队所有。著作权人保留一切权利。
//
// 这份授权条款，在使用者符合以下二条件的情形下，授予使用者使用及再散播本
// 软件包装原始码及二进位可执行形式的权利，无论此包装是否经改作皆然：
//
// 1. 对于本软件源代码的再散播，必须保留上述的版权宣告、此三条件表列，以
//    及下述的免责声明。
// 2. 对于本套件二进位可执行形式的再散播，必须连带以文件以及／或者其他附
//    于散播包装中的媒介方式，重制上述之版权宣告、此三条件表列，以及下述
//    的免责声明。

// 免责声明：本软件是本软件版权持有人以及贡献者以现状（"as is"）提供，
// 本软件包装不负任何明示或默示之担保责任，包括但不限于就适售性以及特定目
// 的的适用性为默示性担保。版权持有人及本软件之贡献者，无论任何条件、
// 无论成因或任何责任主义、无论此责任为因合约关系、无过失责任主义或因非违
// 约之侵权（包括过失或其他原因等）而起，对于任何因使用本软件包装所产生的
// 任何直接性、间接性、偶发性、特殊性、惩罚性或任何结果的损害（包括但不限
// 于替代商品或劳务之购用、使用损失、资料损失、利益损失、业务中断等等），
// 不负任何责任，即在该种使用已获事前告知可能会造成此类损害的情形下亦然。
//-----------------------------------------------------------------------------

#include <stdint.h>
#include "stdlib.h"
#include "stdio.h"
#include "djyos.h"
#include "gdd.h"
#include <gdd_widget.h>
#include "board.h"
#include "../GuiWin/Info/GuiInfo.h"
#include "../GuiWin/Info/WinSwitch.h"

enum IpEditWidgeId{
    IP_BACKGROUND,

    IpTopBox,
    IpPageText,
    IpRunningState0,
    IpPortConfiguration0,
    IpServerConfiguration1,
    IpSystemSettings0,

    IpBox,
    IPConfiText,
    IpText1,
    IpText2,
    IpText3,

    IpTextBox1,
    IpTextBox2,

    IpConnectTest,
//    IpSave,
//    IpBack,
    IP_MAXNUM,//总数量
};
//==================================config======================================
static const  struct GUIINFO IpEditCfgTab[IP_MAXNUM] =
{
    [IP_BACKGROUND] = {
        .position = {0/LCDW_32,0/LCDH_32,1024/LCDW_32,600/LCDH_32},
        .name = "background",
        .type = widget_type_background,
        .userParam = RGB(255,255,255),
    },
    [IpTopBox] = {
        .position = {-9/LCDW_32,-6/LCDH_32,(-9+1042)/LCDW_32,(-6+88)/LCDH_32},
        .name = "box0",
        .type = widget_type_picture,
        .userParam = BMP_TopBox,
    },
    [IpPageText] = {
        .position = {24/LCDW_32,17/LCDH_32,(24+450)/LCDW_32,(17+37)/LCDH_32},
        .name = "text",
        .type = widget_type_picture,
        .userParam = BMP_HomePageText,
    },
    [IpRunningState0] = {
        .position = {651/LCDW_32,5/LCDH_32,(651+65)/LCDW_32,(5+60)/LCDH_32},
        .name = "top1",
        .type = widget_type_button,
        .userParam = BMP_RunningState0,
    },
    [IpPortConfiguration0] = {
        .position = {749/LCDW_32,8/LCDH_32,(749+65)/LCDW_32,(8+57)/LCDH_32},
        .name = "top2",
        .type = widget_type_button,
        .userParam = BMP_PortConfiguration0,
    },
    [IpServerConfiguration1] = {
        .position = {847/LCDW_32,5/LCDH_32,(847+47)/LCDW_32,(5+59)/LCDH_32},
        .name = "top3",
        .type = widget_type_button,
        .userParam = BMP_ServerConfiguration1,
    },
    [IpSystemSettings0] = {
        .position = {926/LCDW_32,5/LCDH_32,(926+65)/LCDW_32,(5+60)/LCDH_32},
        .name = "top4",
        .type = widget_type_button,
        .userParam = BMP_SystemSettings0,
    },

    [IpBox] = {
        .position = {0/LCDW_32,80/LCDH_32,(0+1024)/LCDW_32,(80+316)/LCDH_32},
        .name = "小框",
        .type = widget_type_picture,
        .userParam = BMP_SmallFrame,
    },
    [IPConfiText] = {
        .position = {35/LCDW_32,81/LCDH_32,(35+86)/LCDW_32,(81+27)/LCDH_32},
        .name = "IP配置",
        .type = widget_type_picture,
        .userParam = BMP_IPConfiText,
    },

    [IpText1] = {
        .position = {32/LCDW_32,136/LCDH_32,(32+78)/LCDW_32,(136+21)/LCDH_32},
        .name = "IP地址:",
        .type = widget_type_grouptext,
        .userParam = 0,
    },
    [IpText2] = {
        .position = {32/LCDW_32,191/LCDH_32,(32+78)/LCDW_32,(191+21)/LCDH_32},
        .name = "端口号:",
        .type = widget_type_grouptext,
        .userParam = 1,
    },
    [IpText3] = {
        .position = {32/LCDW_32,246/LCDH_32,(32+78)/LCDW_32,(246+21)/LCDH_32},
        .name = "连接测试:",
        .type = widget_type_text,
        .userParam = Bmp_NULL,
    },

    [IpTextBox1] = {
        .position = {114/LCDW_32,126/LCDH_32,(114+240)/LCDW_32,(126+40)/LCDH_32},
        .name = "文本框1",
        .type = widget_type_textbox,
        .userParam = Bmp_NULL,
    },
    [IpTextBox2] = {
        .position = {114/LCDW_32,181/LCDH_32,(114+240)/LCDW_32,(181+40)/LCDH_32},
        .name = "文本框2",
        .type = widget_type_textbox,
        .userParam = Bmp_NULL,
    },

    [IpConnectTest] = {
        .position = {114/LCDW_32,236/LCDH_32,(114+240)/LCDW_32,(236+40)/LCDH_32},
        .name = "连接测试",
        .type = widget_type_button,
        .userParam = Bmp_NULL,
    },
//    [IpSave] = {
//        .position = {410/LCDW_32,525/LCDH_32,(410+62)/LCDW_32,(525+36)/LCDH_32},
//        .name = "保存",
//        .type = widget_type_button,
//        .userParam = BMP_Save,
//    },
//    [IpBack] = {
//        .position = {492/LCDW_32,525/LCDH_32,(492+122)/LCDW_32,(525+36)/LCDH_32},
//        .name = "不保存并返回",
//        .type = widget_type_button,
//        .userParam = BMP_Back,
//    },


};

HWND IpBox_hwnd[2]={NULL,NULL};
static char IpBoxInput1[48]={0};
static char IpBoxInput2[48]={0};
char *GetIpBoxInput(void)
{
    return IpBoxInput1;
}
//按钮控件创建函数
static bool_t  IpEditButtonPaint(struct WindowMsg *pMsg)
{
    HWND hwnd;
    HDC  hdc;
    RECT rc;
    const char *bmp;
    hwnd =pMsg->hwnd;
    hdc =GDD_BeginPaint(hwnd);
    if(hdc)
    {
        GDD_GetClientRect(hwnd,&rc);
        const struct GUIINFO *buttoninfo = (struct GUIINFO *)GDD_GetWindowPrivateData(hwnd);

        if(buttoninfo->type == widget_type_button)
        {
            if(buttoninfo->userParam == Bmp_NULL)
            {
                GDD_SetFillColor(hdc,RGB(255,255,255));
                GDD_FillRect(hdc,&rc);
                GDD_SetDrawColor(hdc,RGB(0,198,255));
                GDD_DrawRect(hdc,&rc);
                GDD_SetTextColor(hdc,RGB(60,60,60));
                GDD_DrawText(hdc,buttoninfo->name,-1,&rc,DT_VCENTER|DT_CENTER);
            }
            else
            {
                bmp = Get_BmpBuf(buttoninfo->userParam);
                GDD_DrawBMP(hdc,0,0,bmp);
            }
        }

        GDD_EndPaint(hwnd,hdc);
        return true;
    }
    return false;
}
extern struct GuiIp GuiIpConfig[5];

//char Inf[2][32]={0};
//void IpInInit(u32 addr)
//{
//    if(GuiIpConfig[addr].gip_address !=NULL)
//    {
//        snprintf(Inf[0],sizeof(Inf[0]),"%s",GuiIpConfig[addr].gip_address);
//    }
//    if(GuiIpConfig[addr].gport !=NULL)
//    {
//        snprintf(Inf[1],sizeof(Inf[1]),"%s",GuiIpConfig[addr].gport);
//    }
//}

static bool_t IpEditTextBox_TouchDown(struct WindowMsg *pMsg)
{
    HWND hwnd;
    u32 recreat;
    u32 addr;
    if(pMsg==NULL)
        return false;
    hwnd =pMsg->hwnd;
    if(hwnd==NULL)
        return false;

    GDD_SetFocusWindow(hwnd);

    if(Get_TextBox_hwnd() != hwnd)
    {
        Set_TextBox_hwnd(hwnd);

        if(IpBox_hwnd[0]==hwnd)
        {
            SetGetTextBoxClick(0);
        }
        else if(IpBox_hwnd[1]==hwnd)
        {
            SetGetTextBoxClick(1);
        }
        Refresh_GuiWin(1);
    }


    addr=GetIpNumb();
    recreat = pMsg->Param1;

//    printf("recreat is %d\r\n",recreat);

    if(GetStatueMachine()== CtrlNull)
    {
        if(1 == recreat)
        {
            Widget_TextBoxTextCtrl(IpBox_hwnd[0],EN_GET_TEXT,IpBoxInput1);
            Widget_TextBoxTextCtrl(IpBox_hwnd[1],EN_GET_TEXT,IpBoxInput2);
            if(strlen(IpBoxInput1) < 1 || strlen(IpBoxInput2) < 1)
            {
                SetPromptBox(Box_Ip_Set);
                IpConnectTimer();
            }
            else
            {
                SetGuiIp(addr,Gui_IpAddress,IpBoxInput1);
                SetGuiIp(addr,Gui_Port,IpBoxInput2);

                SetClickResponse(Gui_Save_Ip);

                TextBox_Close(IpBox_hwnd[0]);
                TextBox_Close(IpBox_hwnd[1]);
                Jump_GuiWin(WIN_Ip_Win);
            }
        }
        else if(2 == recreat)
        {
            TextBox_Close(IpBox_hwnd[0]);
            TextBox_Close(IpBox_hwnd[1]);
            Jump_GuiWin(WIN_Ip_Win);
        }
    }


    return true;
}

/*---------------------------------------------------------------------------
功能：  窗口创建函数
    输入   :tagWindowMsg *pMsg
    输出 :false或true
---------------------------------------------------------------------------*/
static bool_t HmiCreate_IpEdit(struct WindowMsg *pMsg)
{
    HWND hwnd =pMsg->hwnd;

    //消息处理函数表
    static struct MsgProcTable s_gBmpMsgTablebutton[] =
    {
            {MSG_PAINT,IpEditButtonPaint},
    };

    static struct MsgTableLink  s_gBmpDemoMsgLinkBUtton;

    s_gBmpDemoMsgLinkBUtton.MsgNum = sizeof(s_gBmpMsgTablebutton) / sizeof(struct MsgProcTable);
    s_gBmpDemoMsgLinkBUtton.myTable = (struct MsgProcTable *)&s_gBmpMsgTablebutton;

    static const struct MsgProcTable s_gMsgTableTextBox[] =
    {
            {MSG_TOUCH_DOWN, IpEditTextBox_TouchDown},
    };
    static struct MsgTableLink  s_gDemoMsgLinkTextBox;
    s_gDemoMsgLinkTextBox.MsgNum = sizeof(s_gMsgTableTextBox) / sizeof(struct MsgProcTable);
    s_gDemoMsgLinkTextBox.myTable = (struct MsgProcTable *)&s_gMsgTableTextBox;

    for(int i=0;i<IP_MAXNUM;i++)
    {
        switch (IpEditCfgTab[i].type)
        {
            case  widget_type_button :
            {

                Widget_CreateButton(IpEditCfgTab[i].name, WS_CHILD|BS_NORMAL|WS_UNFILL,    //按钮风格
                                 IpEditCfgTab[i].position.left, IpEditCfgTab[i].position.top,\
                                 GDD_RectW(&IpEditCfgTab[i].position),GDD_RectH(&IpEditCfgTab[i].position), //按钮位置和大小
                                 hwnd,i,(ptu32_t)&IpEditCfgTab[i], &s_gBmpDemoMsgLinkBUtton);   //按钮所属的父窗口，ID,附加数据
            }
                break;

            case  widget_type_textbox :
            {
                IpBox_hwnd[i-IpTextBox1] = Widget_CreateTextBox(IpEditCfgTab[i].name, WS_CHILD|BS_NORMAL|WS_UNFILL,    //按钮风格
                                IpEditCfgTab[i].position.left, IpEditCfgTab[i].position.top,\
                                GDD_RectW(&IpEditCfgTab[i].position),GDD_RectH(&IpEditCfgTab[i].position), //按钮位置和大小
                                hwnd,i,NULL,&s_gDemoMsgLinkTextBox);   //按钮所属的父窗口，ID,附加数据
                if(i==IpTextBox1 && IpBox_hwnd[0])
                {
                    GDD_SetFocusWindow(IpBox_hwnd[0]);
                    Set_TextBox_hwnd(IpBox_hwnd[0]);
                }
            }
                break;
            default:    break;
        }
    }

    Widget_CreateVirKeyBoard("虚拟键盘", WS_CHILD|BS_NORMAL|WS_UNFILL,    //按钮风格
                    0,396,1024,204,Get_WindowsHwnd(),0,NULL, NULL);

    return true;
}

//绘制消息处函数
static bool_t HmiPaint_IpEdit(struct WindowMsg *pMsg)
{
    HWND hwnd;
    HDC  hdc;
    const char *bmp;
    hwnd =pMsg->hwnd;

    hdc =GDD_BeginPaint(hwnd);
    if(hdc)
    {
        for(int i=0;i<IP_MAXNUM;i++)
        {
            switch (IpEditCfgTab[i].type)
            {
                case  widget_type_background :
                    GDD_SetFillColor(hdc,IpEditCfgTab[i].userParam);
                    GDD_FillRect(hdc,&IpEditCfgTab[i].position);
                    break;
                case  widget_type_picture :
                    bmp = Get_BmpBuf(IpEditCfgTab[i].userParam);
                    if(bmp != NULL)
                    {
                        GDD_DrawBMP(hdc,IpEditCfgTab[i].position.left,\
                                IpEditCfgTab[i].position.top,bmp);
                    }
                    break;
                case  widget_type_text :
                    GDD_SetTextColor(hdc,RGB(60,60,60));
                    GDD_DrawText(hdc,IpEditCfgTab[i].name,-1,&IpEditCfgTab[i].position,DT_VCENTER|DT_LEFT);
                    break;
                case  widget_type_grouptext :
                    if(IpEditCfgTab[i].userParam == GetTextBoxClick())
                    {
                        GDD_SetTextColor(hdc,RGB(0,198,255));
                    }
                    else
                    {
                        GDD_SetTextColor(hdc,RGB(60,60,60));
                    }
                    GDD_DrawText(hdc,IpEditCfgTab[i].name,-1,&IpEditCfgTab[i].position,DT_VCENTER|DT_LEFT);
                    break;
                default:    break;
            }
        }

        GDD_EndPaint(hwnd,hdc);
        return true;
    }
    return false;
}

static enum WinType HmiNotify_IpEdit(struct WindowMsg *pMsg)
{
    enum WinType NextUi = WIN_NotChange;
    u16 event =HI16(pMsg->Param1);
    u16 id =LO16(pMsg->Param1);
//    u32 addr=GetIpNumb();

    if(event == MSG_BTN_UP && GetStatueMachine()== CtrlNull)
    {
        switch(id)
        {
            case IpRunningState0:
                TextBox_Close(IpBox_hwnd[0]);
                TextBox_Close(IpBox_hwnd[1]);
                NextUi= WIN_Home_Page;
                break;
            case IpPortConfiguration0:
                TextBox_Close(IpBox_hwnd[0]);
                TextBox_Close(IpBox_hwnd[1]);
                NextUi= WIN_Serial_Win  ;
                break;
//            case IpServerConfiguration1:
//                NextUi= WIN_Ip_Win  ;
//                break;
            case IpSystemSettings0:
                TextBox_Close(IpBox_hwnd[0]);
                TextBox_Close(IpBox_hwnd[1]);
                NextUi= WIN_System_Win  ;
                break;
            case  IpConnectTest:
                Widget_TextBoxTextCtrl(IpBox_hwnd[0],EN_GET_TEXT,IpBoxInput1);
                SetClickResponse(Gui_Ip_Test1);
                break;
//            case IpSave:
//                Widget_TextBoxTextCtrl(IpBox_hwnd[0],EN_GET_TEXT,IpBoxInput1);
//                Widget_TextBoxTextCtrl(IpBox_hwnd[1],EN_GET_TEXT,IpBoxInput2);
//                SetGuiIp(addr,Gui_IpAddress,IpBoxInput1);
//                SetGuiIp(addr,Gui_Port,IpBoxInput2);
//                TextBox_Close(IpBox_hwnd[0]);
//                TextBox_Close(IpBox_hwnd[1]);
//                NextUi= WIN_Ip_Win  ;
//                break;
//            case IpBack:
//                memset(IpBoxInput1,0,48);
//                memset(IpBoxInput2,0,48);
//                TextBox_Close(IpBox_hwnd[0]);
//                TextBox_Close(IpBox_hwnd[1]);
//                NextUi= WIN_Ip_Win  ;
//                break;
            default:
                break;
        }
    }
    return NextUi;
}


int Register_Ip_Edit()
{
    return Register_NewWin(WIN_Ip_Edit,HmiCreate_IpEdit,HmiPaint_IpEdit,HmiNotify_IpEdit);
}
