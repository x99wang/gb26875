//----------------------------------------------------
// Copyright (c) 2014, SHENZHEN PENGRUI SOFT CO LTD. All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------
// Copyright (c) 2014 著作权由都江堰操作系统开源开发团队所有。著作权人保留一切权利。
//
// 这份授权条款，在使用者符合以下二条件的情形下，授予使用者使用及再散播本
// 软件包装原始码及二进位可执行形式的权利，无论此包装是否经改作皆然：
//
// 1. 对于本软件源代码的再散播，必须保留上述的版权宣告、此三条件表列，以
//    及下述的免责声明。
// 2. 对于本套件二进位可执行形式的再散播，必须连带以文件以及／或者其他附
//    于散播包装中的媒介方式，重制上述之版权宣告、此三条件表列，以及下述
//    的免责声明。

// 免责声明：本软件是本软件版权持有人以及贡献者以现状（"as is"）提供，
// 本软件包装不负任何明示或默示之担保责任，包括但不限于就适售性以及特定目
// 的的适用性为默示性担保。版权持有人及本软件之贡献者，无论任何条件、
// 无论成因或任何责任主义、无论此责任为因合约关系、无过失责任主义或因非违
// 约之侵权（包括过失或其他原因等）而起，对于任何因使用本软件包装所产生的
// 任何直接性、间接性、偶发性、特殊性、惩罚性或任何结果的损害（包括但不限
// 于替代商品或劳务之购用、使用损失、资料损失、利益损失、业务中断等等），
// 不负任何责任，即在该种使用已获事前告知可能会造成此类损害的情形下亦然。
//-----------------------------------------------------------------------------

#include <stdint.h>
#include "stdlib.h"
#include "stdio.h"
#include "djyos.h"
#include "gdd.h"
#include <gdd_widget.h>
#include "board.h"
#include "string.h"
#include "../GuiWin/Info/GuiInfo.h"
#include "../GuiWin/Info/WinSwitch.h"

enum SerialEditWidgeId{
    SERIAL_BACKGROUND,

    SerialTopBox,
    SerialPageText,
    SerialRunningState0,
    SerialPortConfiguration1,
    SerialServerConfiguration0,
    SerialSystemSettings0,

    SerialText1,
    SerialText2,
    SerialText3,
    SerialText4,
    SerialText5,
    SerialText6,
    SerialText7,

    SerialRate1,
    SerialRate2,
    SerialRate3,
    SerialRate4,
    SerialRate5,

    SerialBit1,
    SerialBit2,
    SerialBit3,

    SerialCheck1,
    SerialCheck2,
    SerialCheck3,

    SerialStop1,
    SerialStop2,
    SerialStop3,

    SerialTextBox,

    SerialMatching1,
    SerialMatching2,
    SerialMatching3,
    SerialMatching4,

    SerialConnectTest,

    SerialSave,
    SerialBack,

    SERIAL_MAXNUM,//总数量
};

//==================================config======================================
static const  struct GUIINFO SerialEditCfgTab[SERIAL_MAXNUM] =
{
    [SERIAL_BACKGROUND] = {
        .position = {0/LCDW_32,0/LCDH_32,1024/LCDW_32,600/LCDH_32},
        .name = "background",
        .type = widget_type_background,
        .userParam = RGB(255,255,255),
    },
    [SerialTopBox] = {
        .position = {-9/LCDW_32,-6/LCDH_32,(-9+1042)/LCDW_32,(-6+88)/LCDH_32},
        .name = "box0",
        .type = widget_type_picture,
        .userParam = BMP_TopBox,
    },
    [SerialPageText] = {
        .position = {24/LCDW_32,17/LCDH_32,(24+450)/LCDW_32,(17+37)/LCDH_32},
        .name = "text",
        .type = widget_type_picture,
        .userParam = BMP_HomePageText,
    },
    [SerialRunningState0] = {
        .position = {651/LCDW_32,5/LCDH_32,(651+65)/LCDW_32,(5+60)/LCDH_32},
        .name = "top1",
        .type = widget_type_button,
        .userParam = BMP_RunningState0,
    },
    [SerialPortConfiguration1] = {
        .position = {749/LCDW_32,8/LCDH_32,(749+65)/LCDW_32,(8+57)/LCDH_32},
        .name = "top2",
        .type = widget_type_button,
        .userParam = BMP_PortConfiguration1,
    },
    [SerialServerConfiguration0] = {
        .position = {847/LCDW_32,5/LCDH_32,(847+47)/LCDW_32,(5+59)/LCDH_32},
        .name = "top3",
        .type = widget_type_button,
        .userParam = BMP_ServerConfiguration0,
    },
    [SerialSystemSettings0] = {
        .position = {926/LCDW_32,5/LCDH_32,(926+65)/LCDW_32,(5+60)/LCDH_32},
        .name = "top4",
        .type = widget_type_button,
        .userParam = BMP_SystemSettings0,
    },

    [SerialText1] = {
        .position = {26/LCDW_32,135/LCDH_32,(26+60)/LCDW_32,(135+21)/LCDH_32},
        .name = "波特率:",
        .type = widget_type_text,
        .userParam = Bmp_NULL,
    },
    [SerialText2] = {
        .position = {26/LCDW_32,191/LCDH_32,(26+60)/LCDW_32,(191+21)/LCDH_32},
        .name = "数据位:",
        .type = widget_type_text,
        .userParam = Bmp_NULL,
    },
    [SerialText3] = {
        .position = {26/LCDW_32,247/LCDH_32,(26+60)/LCDW_32,(247+21)/LCDH_32},
        .name = "校验位:",
        .type = widget_type_text,
        .userParam = Bmp_NULL,
    },
    [SerialText4] = {
        .position = {26/LCDW_32,303/LCDH_32,(26+60)/LCDW_32,(303+21)/LCDH_32},
        .name = "停止位:",
        .type = widget_type_text,
        .userParam = Bmp_NULL,
    },
    [SerialText5] = {
        .position = {26/LCDW_32,359/LCDH_32,(26+78)/LCDW_32,(359+21)/LCDH_32},
        .name = "设备地址:",
        .type = widget_type_text,
        .userParam = Bmp_NULL,
    },
    [SerialText6] = {
        .position = {26/LCDW_32,415/LCDH_32,(26+78)/LCDW_32,(415+21)/LCDH_32},
        .name = "配接设备:",
        .type = widget_type_text,
        .userParam = Bmp_NULL,
    },
    [SerialText7] = {
        .position = {26/LCDW_32,471/LCDH_32,(26+78)/LCDW_32,(471+21)/LCDH_32},
        .name = "连接测试:",
        .type = widget_type_text,
        .userParam = Bmp_NULL,
    },

    [SerialRate1] = {
        .position = {108/LCDW_32,125/LCDH_32,(108+120)/LCDW_32,(125+40)/LCDH_32},
        .name = "4800",
        .type = widget_type_groupbutton1,
        .userParam = 1,
    },
    [SerialRate2] = {
        .position = {253/LCDW_32,125/LCDH_32,(253+120)/LCDW_32,(125+40)/LCDH_32},
        .name = "9600",
        .type = widget_type_groupbutton1,
        .userParam = 2,
    },
    [SerialRate3] = {
        .position = {395/LCDW_32,125/LCDH_32,(395+120)/LCDW_32,(125+40)/LCDH_32},
        .name = "14400",
        .type = widget_type_groupbutton1,
        .userParam = 3,
    },
    [SerialRate4] = {
        .position = {538/LCDW_32,125/LCDH_32,(538+120)/LCDW_32,(125+40)/LCDH_32},
        .name = "19200",
        .type = widget_type_groupbutton1,
        .userParam = 4,
    },
    [SerialRate5] = {
        .position = {681/LCDW_32,125/LCDH_32,(681+120)/LCDW_32,(125+40)/LCDH_32},
        .name = "38400",
        .type = widget_type_groupbutton1,
        .userParam = 5,
    },

    [SerialBit1] = {
        .position = {108/LCDW_32,181/LCDH_32,(108+120)/LCDW_32,(181+40)/LCDH_32},
        .name = "6",
        .type = widget_type_groupbutton2,
        .userParam = 1,
    },
    [SerialBit2] = {
        .position = {253/LCDW_32,181/LCDH_32,(253+120)/LCDW_32,(181+40)/LCDH_32},
        .name = "7",
        .type = widget_type_groupbutton2,
        .userParam = 2,
    },
    [SerialBit3] = {
        .position = {395/LCDW_32,181/LCDH_32,(395+120)/LCDW_32,(181+40)/LCDH_32},
        .name = "8",
        .type = widget_type_groupbutton2,
        .userParam = 3,
    },

    [SerialCheck1] = {
        .position = {108/LCDW_32,237/LCDH_32,(108+120)/LCDW_32,(237+40)/LCDH_32},
        .name = "无校验",
        .type = widget_type_groupbutton3,
        .userParam = 1,
    },
    [SerialCheck2] = {
        .position = {253/LCDW_32,237/LCDH_32,(253+120)/LCDW_32,(237+40)/LCDH_32},
        .name = "奇校验",
        .type = widget_type_groupbutton3,
        .userParam = 2,
    },
    [SerialCheck3] = {
        .position = {395/LCDW_32,237/LCDH_32,(395+120)/LCDW_32,(237+40)/LCDH_32},
        .name = "偶校验",
        .type = widget_type_groupbutton3,
        .userParam = 3,
    },

    [SerialStop1] = {
        .position = {108/LCDW_32,293/LCDH_32,(108+120)/LCDW_32,(293+40)/LCDH_32},
        .name = "1",
        .type = widget_type_groupbutton4,
        .userParam = 1,
    },
    [SerialStop2] = {
        .position = {253/LCDW_32,293/LCDH_32,(253+120)/LCDW_32,(293+40)/LCDH_32},
        .name = "1.5",
        .type = widget_type_groupbutton4,
        .userParam = 2,
    },
    [SerialStop3] = {
        .position = {395/LCDW_32,293/LCDH_32,(395+120)/LCDW_32,(293+40)/LCDH_32},
        .name = "2",
        .type = widget_type_groupbutton4,
        .userParam = 3,
    },

    [SerialTextBox] = {
        .position = {108/LCDW_32,349/LCDH_32,(108+240)/LCDW_32,(349+40)/LCDH_32},
        .name = "设备的地址",
        .type = widget_type_textbox,
        .userParam = Bmp_NULL,
    },

    [SerialMatching1] = {
        .position = {108/LCDW_32,405/LCDH_32,(108+73)/LCDW_32,(405+40)/LCDH_32},
        .name = "无",
        .type = widget_type_groupbutton5,
        .userParam = Bmp_NULL,
    },
    [SerialMatching2] = {
        .position = {205/LCDW_32,405/LCDH_32,(205+187)/LCDW_32,(405+40)/LCDH_32},
        .name = "请选择设备类型",
        .type = widget_type_groupbutton,
        .userParam = 1,
    },
//    [SerialMatching3] = {
//        .position = {416/LCDW_32,405/LCDH_32,(416+187)/LCDW_32,(405+40)/LCDH_32},
//        .name = "请选择设备品牌",
//        .type = widget_type_groupbutton,
//        .userParam = 2,
//    },
//    [SerialMatching4] = {
//        .position = {628/LCDW_32,405/LCDH_32,(628+187)/LCDW_32,(405+40)/LCDH_32},
//        .name = "请选择设备型号",
//        .type = widget_type_groupbutton,
//        .userParam = 3,
//    },

    [SerialConnectTest] = {
        .position = {108/LCDW_32,461/LCDH_32,(108+136)/LCDW_32,(461+40)/LCDH_32},
        .name = "连接测试",
        .type = widget_type_button,
        .userParam = Bmp_NULL,
    },

    [SerialSave] = {
        .position = {410/LCDW_32,525/LCDH_32,(410+62)/LCDW_32,(525+36)/LCDH_32},
        .name = "保存",
        .type = widget_type_button,
        .userParam = BMP_Save,
    },
    [SerialBack] = {
        .position = {492/LCDW_32,525/LCDH_32,(492+122)/LCDW_32,(525+36)/LCDH_32},
        .name = "不保存并返回",
        .type = widget_type_button,
        .userParam = BMP_Back,
    },
};

extern struct GuiSerial GuiSerialConfig[5];
char SerialBoxInput[24]={0};

static u32 serial_addr=0;

static u8 RateFlag=0;  //设置比特率光标
static u8 BitFlag=0;   //设置数据位光标
static u8 CheckFlag=0; //设置校验位光标
static u8 StopFlag=0;  //设置停止位光标
u8 DevFlag=0;  //设置设备光标  //0初始值，1表示没选择设备，2表示选择了设备

void SerialFlagInit(u32 addr)
{
    RateFlag=GuiSerialConfig[addr].gbitrate_nub;
    BitFlag=GuiSerialConfig[addr].gdata_nub;
    CheckFlag=GuiSerialConfig[addr].gparity_nub;
    StopFlag=GuiSerialConfig[addr].gstop_nub;
    sprintf(SerialBoxInput,"%d",GuiSerialConfig[addr].gequipment_address);
}

//按钮控件创建函数
static bool_t  SerialEditButtonPaint(struct WindowMsg *pMsg)
{
    HWND hwnd;
    HDC  hdc;
    RECT rc;
    const char *bmp;
    hwnd =pMsg->hwnd;
    hdc =GDD_BeginPaint(hwnd);
    serial_addr=GetComNumb();
    if(hdc)
    {
        GDD_GetClientRect(hwnd,&rc);
        const struct GUIINFO *buttoninfo = (struct GUIINFO *)GDD_GetWindowPrivateData(hwnd);

        if(buttoninfo->type == widget_type_button)
        {
            if(buttoninfo->userParam == Bmp_NULL)
            {
                GDD_SetFillColor(hdc,RGB(255,255,255));
                GDD_FillRect(hdc,&rc);
                GDD_SetDrawColor(hdc,RGB(0,198,255));
                GDD_DrawRect(hdc,&rc);
                GDD_SetTextColor(hdc,RGB(60,60,60));
                GDD_DrawText(hdc,buttoninfo->name,-1,&rc,DT_VCENTER|DT_CENTER);
            }
            else
            {
                bmp = Get_BmpBuf(buttoninfo->userParam);
                GDD_DrawBMP(hdc,0,0,bmp);
            }
        }
        else
        {
            GDD_SetFillColor(hdc,RGB(255,255,255));
            GDD_FillRect(hdc,&rc);
            GDD_SetDrawColor(hdc,RGB(0,198,255));
            GDD_DrawRect(hdc,&rc);
            GDD_SetTextColor(hdc,RGB(60,60,60));

            rc.left+=10;
            if(buttoninfo->type == widget_type_groupbutton)
            {
                if(DevFlag == 2)
                {
                    GDD_DrawText(hdc,GetSyscType(),-1,&rc,DT_VCENTER|DT_LEFT);
                }
                else if(GuiSerialConfig[serial_addr].gsys_type_flag == 1)
                {
                    GDD_DrawText(hdc,GuiSerialConfig[serial_addr].gsys_type,-1,&rc,DT_VCENTER|DT_LEFT);
                }
                else
                {
                    GDD_DrawText(hdc,buttoninfo->name,-1,&rc,DT_VCENTER|DT_LEFT);
                }

            }
            else
            {
                GDD_DrawText(hdc,buttoninfo->name,-1,&rc,DT_VCENTER|DT_LEFT);
            }
            rc.left-=10;


            if(buttoninfo->type == widget_type_groupbutton)
            {
                rc.left+=156;
                rc.top+=7;
                bmp = Get_BmpBuf(BMP_Directory);
                GDD_DrawBMP(hdc,rc.left,rc.top,bmp);
            }
            else if(buttoninfo->type == widget_type_groupbutton1)
            {
                if(buttoninfo->userParam == RateFlag)
                {
                    rc.right-=29;
                    bmp = Get_BmpBuf(BMP_Check);
                    GDD_DrawBMP(hdc,rc.right,0,bmp);
                }

            }
            else if(buttoninfo->type == widget_type_groupbutton2)
            {
                if(buttoninfo->userParam == BitFlag)
                {
                    rc.right-=29;
                    bmp = Get_BmpBuf(BMP_Check);
                    GDD_DrawBMP(hdc,rc.right,0,bmp);
                }
            }
            else if(buttoninfo->type == widget_type_groupbutton3)
            {
                if(buttoninfo->userParam == CheckFlag)
                {
                    rc.right-=29;
                    bmp = Get_BmpBuf(BMP_Check);
                    GDD_DrawBMP(hdc,rc.right,0,bmp);
                }
            }
            else if(buttoninfo->type == widget_type_groupbutton4)
            {
                if(buttoninfo->userParam == StopFlag)
                {
                    rc.right-=29;
                    bmp = Get_BmpBuf(BMP_Check);
                    GDD_DrawBMP(hdc,rc.right,0,bmp);
                }
            }
            else if(buttoninfo->type == widget_type_groupbutton5)
            {
                if(DevFlag == 1 || GuiSerialConfig[serial_addr].gsys_type_flag == 0)
                {
                    rc.right-=29;
                    bmp = Get_BmpBuf(BMP_Check);
                    GDD_DrawBMP(hdc,rc.right,0,bmp);
                }
            }
        }

        GDD_EndPaint(hwnd,hdc);
        return true;
    }
    return false;
}

static HWND SerialBox_hwnd=NULL;
HWND GetSerialBoxHwnd(void)
{
    return SerialBox_hwnd;
}

static HWND RateHwnd[5]={NULL,NULL,NULL,NULL,NULL};
static void Update_RateStatus(void)
{
    for(int i=0;i<5;i++)
    {
        if(RateHwnd[i] == NULL)
            return;

        GDD_PostMessage(RateHwnd[i],MSG_PAINT,0,0);
    }

}

static HWND BitHwnd[3]={NULL,NULL,NULL};
static void Update_BitStatus(void)
{
    for(int i=0;i<3;i++)
    {
        if(BitHwnd[i] == NULL)
            return;

        GDD_PostMessage(BitHwnd[i],MSG_PAINT,0,0);
    }

}

static HWND CheckHwnd[3]={NULL,NULL,NULL};
static void Update_CheckStatus(void)
{
    for(int i=0;i<3;i++)
    {
        if(CheckHwnd[i] == NULL)
            return;

        GDD_PostMessage(CheckHwnd[i],MSG_PAINT,0,0);
    }

}

static HWND StopHwnd[3]={NULL,NULL,NULL};
static void Update_StopStatus(void)
{
    for(int i=0;i<3;i++)
    {
        if(StopHwnd[i] == NULL)
            return;

        GDD_PostMessage(StopHwnd[i],MSG_PAINT,0,0);
    }

}

static HWND MatchingHwnd=NULL;
static void Update_MatchingStatus(void)
{
        if(MatchingHwnd == NULL)
            return;

        GDD_PostMessage(MatchingHwnd,MSG_PAINT,0,0);
}

/*---------------------------------------------------------------------------
功能：  窗口创建函数
    输入   :tagWindowMsg *pMsg
    输出 :false或true
---------------------------------------------------------------------------*/
static bool_t HmiCreate_SerialEdit(struct WindowMsg *pMsg)
{
    HWND hwnd =pMsg->hwnd;
    serial_addr=GetComNumb();
    //消息处理函数表
    static struct MsgProcTable s_gBmpMsgTablebutton[] =
    {
            {MSG_PAINT,SerialEditButtonPaint},
    };

    static struct MsgTableLink  s_gBmpDemoMsgLinkBUtton;

    s_gBmpDemoMsgLinkBUtton.MsgNum = sizeof(s_gBmpMsgTablebutton) / sizeof(struct MsgProcTable);
    s_gBmpDemoMsgLinkBUtton.myTable = (struct MsgProcTable *)&s_gBmpMsgTablebutton;

    static const struct MsgProcTable s_gMsgTableTextBox[] =
    {
          //  {MSG_TOUCH_DOWN, WifiPassInputTextBox_TouchDown},
    };
    static struct MsgTableLink  s_gDemoMsgLinkTextBox;
    s_gDemoMsgLinkTextBox.MsgNum = sizeof(s_gMsgTableTextBox) / sizeof(struct MsgProcTable);
    s_gDemoMsgLinkTextBox.myTable = (struct MsgProcTable *)&s_gMsgTableTextBox;

    for(int i=0;i<SERIAL_MAXNUM;i++)
    {
        switch (SerialEditCfgTab[i].type)
        {
            case  widget_type_button :
            case  widget_type_groupbutton  :
            {

                Widget_CreateButton(SerialEditCfgTab[i].name, WS_CHILD|BS_NORMAL|WS_UNFILL,    //按钮风格
                                 SerialEditCfgTab[i].position.left, SerialEditCfgTab[i].position.top,\
                                 GDD_RectW(&SerialEditCfgTab[i].position),GDD_RectH(&SerialEditCfgTab[i].position), //按钮位置和大小
                                 hwnd,i,(ptu32_t)&SerialEditCfgTab[i], &s_gBmpDemoMsgLinkBUtton);   //按钮所属的父窗口，ID,附加数据
            }
                break;
            case  widget_type_groupbutton1 :
            {

                RateHwnd[i-SerialRate1]=Widget_CreateButton(SerialEditCfgTab[i].name, WS_CHILD|BS_NORMAL|WS_UNFILL,    //按钮风格
                                 SerialEditCfgTab[i].position.left, SerialEditCfgTab[i].position.top,\
                                 GDD_RectW(&SerialEditCfgTab[i].position),GDD_RectH(&SerialEditCfgTab[i].position), //按钮位置和大小
                                 hwnd,i,(ptu32_t)&SerialEditCfgTab[i], &s_gBmpDemoMsgLinkBUtton);   //按钮所属的父窗口，ID,附加数据
            }
                break;
            case  widget_type_groupbutton2 :
            {

                BitHwnd[i-SerialBit1]=Widget_CreateButton(SerialEditCfgTab[i].name, WS_CHILD|BS_NORMAL|WS_UNFILL,    //按钮风格
                                 SerialEditCfgTab[i].position.left, SerialEditCfgTab[i].position.top,\
                                 GDD_RectW(&SerialEditCfgTab[i].position),GDD_RectH(&SerialEditCfgTab[i].position), //按钮位置和大小
                                 hwnd,i,(ptu32_t)&SerialEditCfgTab[i], &s_gBmpDemoMsgLinkBUtton);   //按钮所属的父窗口，ID,附加数据
            }
                break;
            case  widget_type_groupbutton3 :
            {

                CheckHwnd[i-SerialCheck1]=Widget_CreateButton(SerialEditCfgTab[i].name, WS_CHILD|BS_NORMAL|WS_UNFILL,    //按钮风格
                                 SerialEditCfgTab[i].position.left, SerialEditCfgTab[i].position.top,\
                                 GDD_RectW(&SerialEditCfgTab[i].position),GDD_RectH(&SerialEditCfgTab[i].position), //按钮位置和大小
                                 hwnd,i,(ptu32_t)&SerialEditCfgTab[i], &s_gBmpDemoMsgLinkBUtton);   //按钮所属的父窗口，ID,附加数据
            }
                break;
            case  widget_type_groupbutton4 :
            {

                StopHwnd[i-SerialStop1]=Widget_CreateButton(SerialEditCfgTab[i].name, WS_CHILD|BS_NORMAL|WS_UNFILL,    //按钮风格
                                 SerialEditCfgTab[i].position.left, SerialEditCfgTab[i].position.top,\
                                 GDD_RectW(&SerialEditCfgTab[i].position),GDD_RectH(&SerialEditCfgTab[i].position), //按钮位置和大小
                                 hwnd,i,(ptu32_t)&SerialEditCfgTab[i], &s_gBmpDemoMsgLinkBUtton);   //按钮所属的父窗口，ID,附加数据
            }
                break;
            case  widget_type_groupbutton5 :
            {

                MatchingHwnd=Widget_CreateButton(SerialEditCfgTab[i].name, WS_CHILD|BS_NORMAL|WS_UNFILL,    //按钮风格
                                 SerialEditCfgTab[i].position.left, SerialEditCfgTab[i].position.top,\
                                 GDD_RectW(&SerialEditCfgTab[i].position),GDD_RectH(&SerialEditCfgTab[i].position), //按钮位置和大小
                                 hwnd,i,(ptu32_t)&SerialEditCfgTab[i], &s_gBmpDemoMsgLinkBUtton);   //按钮所属的父窗口，ID,附加数据
            }
                break;
            case  widget_type_textbox :
            {
                SerialBox_hwnd = Widget_CreateTextBox(SerialEditCfgTab[i].name, WS_CHILD|BS_NORMAL|WS_UNFILL,    //按钮风格
                                SerialEditCfgTab[i].position.left, SerialEditCfgTab[i].position.top,\
                                GDD_RectW(&SerialEditCfgTab[i].position),GDD_RectH(&SerialEditCfgTab[i].position), //按钮位置和大小
                                hwnd,i,0,&s_gDemoMsgLinkTextBox);   //按钮所属的父窗口，ID,附加数据
                if(SerialBox_hwnd)
                {
                    GDD_SetFocusWindow(SerialBox_hwnd);
                }
            }
                break;
            default:    break;
        }
    }
    return true;
}

//绘制消息处函数
static bool_t HmiPaint_SerialEdit(struct WindowMsg *pMsg)
{
    HWND hwnd;
    HDC  hdc;
    const char *bmp;
    hwnd =pMsg->hwnd;

    hdc =GDD_BeginPaint(hwnd);
    if(hdc)
    {
        GDD_SetTextColor(hdc,RGB(60,60,60));

        for(int i=0;i<SERIAL_MAXNUM;i++)
        {
            switch (SerialEditCfgTab[i].type)
            {
                case  widget_type_background :
                    GDD_SetFillColor(hdc,SerialEditCfgTab[i].userParam);
                    GDD_FillRect(hdc,&SerialEditCfgTab[i].position);
                    break;
                case  widget_type_picture :
                    bmp = Get_BmpBuf(SerialEditCfgTab[i].userParam);
                    if(bmp != NULL)
                    {
                        GDD_DrawBMP(hdc,SerialEditCfgTab[i].position.left,\
                                SerialEditCfgTab[i].position.top,bmp);
                    }
                    break;
                case  widget_type_text :
                    GDD_DrawText(hdc,SerialEditCfgTab[i].name,-1,&SerialEditCfgTab[i].position,DT_VCENTER|DT_LEFT);
                    break;
                default:    break;
            }
        }

        GDD_EndPaint(hwnd,hdc);
        return true;
    }
    return false;
}

static enum WinType HmiNotify_SerialEdit(struct WindowMsg *pMsg)
{
    enum WinType NextUi = WIN_NotChange;
    u16 event =HI16(pMsg->Param1);
    u16 id =LO16(pMsg->Param1);
    serial_addr=GetComNumb();
    if(event == MSG_BTN_UP && GetStatueMachine()== CtrlNull)
    {
        switch(id)
        {
            case SerialRunningState0:
                TextBox_Close(SerialBox_hwnd);
                NextUi= WIN_Home_Page;
                break;
//            case SerialPortConfiguration1:
//                TextBox_Close(SerialBox_hwnd);
//                NextUi= WIN_Serial_Win  ;
//                break;
            case SerialServerConfiguration0:
                TextBox_Close(SerialBox_hwnd);
                NextUi= WIN_Ip_Win  ;
                break;
            case SerialSystemSettings0:
                TextBox_Close(SerialBox_hwnd);
                NextUi= WIN_System_Win  ;
                break;

            case SerialRate1:
            case SerialRate2:
            case SerialRate3:
            case SerialRate4:
            case SerialRate5:
                RateFlag=id-SerialRate1+1;
                Update_RateStatus();
                break;

            case SerialBit1:
            case SerialBit2:
            case SerialBit3:
                BitFlag=id-SerialBit1+1;
                Update_BitStatus();
                break;

            case SerialCheck1:
            case SerialCheck2:
            case SerialCheck3:
                CheckFlag=id-SerialCheck1+1;
                Update_CheckStatus();
                break;

            case SerialStop1:
            case SerialStop2:
            case SerialStop3:
                StopFlag=id-SerialStop1+1;
                Update_StopStatus();
                break;

            case SerialMatching1:
                DevFlag=1;
                Update_MatchingStatus();
                break;

            case SerialMatching2:
                TextBox_Close(SerialBox_hwnd);
                NextUi= WIN_Equipment_Type  ;
                break;
//            case SerialMatching3:
//                TextBox_Close(SerialBox_hwnd);
//                NextUi= WIN_Equipment_Brand  ;
//                break;
//            case SerialMatching4:
//                TextBox_Close(SerialBox_hwnd);
//                NextUi= WIN_Equipment_Model  ;
//                break;
            case SerialSave:

                if((RateFlag != 0) && (BitFlag != 0) && (CheckFlag != 0) && (StopFlag != 0))
                {
                    GuiSerialConfig[serial_addr].gbitrate_nub=RateFlag;
                    GuiSerialConfig[serial_addr].gdata_nub=BitFlag;
                    GuiSerialConfig[serial_addr].gparity_nub=CheckFlag;
                    GuiSerialConfig[serial_addr].gstop_nub=StopFlag;

                    SetGuiSerial(serial_addr,Gui_Bitrate,SerialEditCfgTab[RateFlag+SerialRate1-1].name);
                    SetGuiSerial(serial_addr,Gui_DataBit,SerialEditCfgTab[BitFlag+SerialBit1-1].name);
                    SetGuiSerial(serial_addr,Gui_ParityBit,SerialEditCfgTab[CheckFlag+SerialCheck1-1].name);
                    SetGuiSerial(serial_addr,Gui_StopBit,SerialEditCfgTab[StopFlag+SerialStop1-1].name);
                    SetGuiSerial(serial_addr,Gui_EquipmentAddress,SerialBoxInput);
                    if(2 == DevFlag)
                    {
                        SetGuiSerial(serial_addr,Gui_SysType,GetSyscType());
                        GuiSerialConfig[serial_addr].gsys_type_flag=1;
                    }
                    else if(1 == DevFlag)
                    {
                        SetGuiSerial(serial_addr,Gui_SysType,"无");
                        GuiSerialConfig[serial_addr].gsys_type_flag=0;
                    }

                    Write_SerialInf(GuiSerialConfig[serial_addr],serial_addr);

                    SetClickResponse(Gui_Save_Serial);
                }
                else
                {
                    SetPromptBox(Box_Save_Error);
                    SerialSaveTimer();
                }

                DevFlag=0;
                memset(SerialBoxInput,0,24);
                TextBox_Close(SerialBox_hwnd);

                break;
            case SerialBack:
                DevFlag=0;
                if(GuiSerialConfig[serial_addr].gsys_type_flag == 2)
                {
                    GuiSerialConfig[serial_addr].gsys_type_flag=0;
                }
                memset(SerialBoxInput,0,24);
                TextBox_Close(SerialBox_hwnd);
                NextUi= WIN_Serial_Win  ;
                break;
            default:
                break;
        }
    }
    return NextUi;
}


int Register_Serial_Edit()
{
    return Register_NewWin(WIN_Serial_Edit,HmiCreate_SerialEdit,HmiPaint_SerialEdit,HmiNotify_SerialEdit);
}
