#ifndef _COMPONENT_STATUS_H_
#define _COMPONENT_STATUS_H_

#include "stdint.h"

void SetscStatues(u32 addr,u32 statues);
u32 GetscStatues(u32 addr);

void SeticStatues(u32 addr,u32 statues);
u32 GeticStatues(u32 addr);
void SetConnectNumb(u32 addr);
u32 GetConnectNumb(void);
#endif /* _COMPONENT_STATUS_H_ */
