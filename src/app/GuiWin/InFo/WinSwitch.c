/*
 * EngLishPlaza.c
 *
 *  Created on: 2019年9月20日
 *      Author: czz
 */

#include <stdint.h>
#include "stdlib.h"
#include "stdio.h"
#include "djyos.h"
#include "gdd.h"
#include <gdd_widget.h>
#include "GuiInfo.h"
#include "WinSwitch.h"
#include "align.h"
#include "project_config.h"
#include "../trans_dev/usr_info_trans_dev.h"
static struct GuiWinCb winCtrl[WIN_Max];
/*static*/ enum WinType eng_CurrentWindow = WIN_Home_Page;

static struct GuiSysLog GuiSysLogConfig[100]={0};
void SetGuiSysLog(GuiSysLogType type,u32 info,char *date,u32 len)
{
    switch(type)
    {
        case Sys_Log_Time:
            if(len<=sizeof(GuiSysLogConfig[info].SysLogTime))
            {
                memcpy(GuiSysLogConfig[info].SysLogTime,date,len);
            }
            break;
        case Sys_Log_Type:
            if(len<=sizeof(GuiSysLogConfig[info].SysLogType))
            {
                memcpy(GuiSysLogConfig[info].SysLogType,date,len);
            }
            break;
        case Sys_Log_Inf:
            if(len<=sizeof(GuiSysLogConfig[info].SysLogInf))
            {
                memcpy(GuiSysLogConfig[info].SysLogInf,date,len);
            }
            break;
        default:
            break;
    }
}
char *GetGuiSysLog(u32 s,u32 info)
{
    switch(s)
    {
        case 1:
            return GuiSysLogConfig[info].SysLogTime;
        case 2:
            return GuiSysLogConfig[info].SysLogType;
        case 3:
            return GuiSysLogConfig[info].SysLogInf;
        default:
            return NULL;
    }
}
enum WinType Get_SelectionWinType()
{
    return eng_CurrentWindow;
}

//主窗口消息分发
bool_t HmiNotify_Trans(struct WindowMsg *pMsg)
{
    enum WinType NextWin;
    static u8 left_flag=0;
    static s64 timebak;

    if((eng_CurrentWindow >= WIN_Max)||(winCtrl[eng_CurrentWindow].DoMsg==NULL))
    {
        eng_CurrentWindow = WIN_Main_WIN;
        return false;
    }

    if(eng_CurrentWindow == WIN_Ip_Win)
    {
        IpWinInit();
    }

//=============================屏蔽误触发=======================================
    u16 event = HI16(pMsg->Param1);
    if(event==MSG_BTN_PEN_MOVE)
    {
        if((left_flag == 1)&&(DJY_GetSysTime()-timebak<50*mS))
            return true;
        left_flag = 1;
    }

    if(left_flag && event==MSG_BTN_UP)
    {
        timebak =  DJY_GetSysTime();
        left_flag = 0;
        return true;
    }
    //move 消息之后短时间不响应按下弹起消息
    if(((DJY_GetSysTime() - timebak) < 50*mS)&&\
        ((event==MSG_BTN_PEN_MOVE)||(event==MSG_BTN_UP)||(event==MSG_BTN_DOWN)))
    {
        return true;
    }
//==============================================================================
    NextWin = winCtrl[eng_CurrentWindow].DoMsg(pMsg);
    if(NextWin != WIN_NotChange)
    {
        GDD_DestroyAllChild(pMsg->hwnd);      //删除当前控件。
        GDD_PostMessage(pMsg->hwnd, MSG_REFRESH_UI, NextWin, 0);
    }
    if(NextWin >= WIN_Error)
    {
        GDD_DestroyAllChild(pMsg->hwnd);      //删除当前控件。
        GDD_PostMessage(pMsg->hwnd, MSG_REFRESH_UI, WIN_Home_Page, 0);
        printf("this is page is error\r\n");
    }
    return true;
}

//主窗口界面更新
bool_t HmiRefresh(struct WindowMsg *pMsg)
{
    HWND hwnd;
    u32 recreat;
    RECT rc;

    hwnd =pMsg->hwnd;
    HDC hdc =GDD_BeginPaint(hwnd);
    if(hdc)
    {
        GDD_GetClientRect(hwnd,&rc);
        recreat = pMsg->Param2;
        eng_CurrentWindow = (enum WinType)pMsg->Param1;

        if((recreat < 1)&&(winCtrl[eng_CurrentWindow].HmiCreate != NULL))
            winCtrl[eng_CurrentWindow].HmiCreate(pMsg);//创建本界面各控件
        if(winCtrl[eng_CurrentWindow].HmiPaint != NULL)
            winCtrl[eng_CurrentWindow].HmiPaint(pMsg); //绘制各控件

        GDD_EndPaint(hwnd,hdc);

        if(recreat == 2)  //定时器页面响应结束后方可继续跳转
        {
            SetStatueMachine(CtrlNull);
        }

        return true;
    }
    return false;
}


//界面刷新
bool_t Refresh_SwitchWIn(HWND hwnd, u32 param2)
{
    if(param2 < 1)
    {
        GDD_DestroyAllChild(hwnd);      //删除当前控件。
    }
    GDD_PostMessage(hwnd, MSG_REFRESH_UI, eng_CurrentWindow, param2);

   return true;

}
//界面跳转
bool_t  Jump_Interface(HWND hwnd,enum WinType nextInterface)
{
    GDD_DestroyAllChild(hwnd);      //删除当前控件。
    GDD_PostMessage(hwnd, MSG_REFRESH_UI, nextInterface, 0);
    return true;
}

//注册所有的界面
int Init_WinSwitch()
{
    Register_Main_WIN();
    Register_Home_Page();
    Register_Admin_Login();

    Register_Serial_Win();
    Register_Serial_Edit();
    Register_Equipment_Type();
    Register_Equipment_Brand();
    Register_Equipment_Model();

    Register_Ip_Win();
    Register_Ip_Edit();

    Register_System_Win();
    Register_System_Password();
    Register_System_Log();
    Register_System_BR();
    Register_System_CU();

    Register_Prompt_Box();
    return 0;
}


//注册新的界面
int Register_NewWin(enum WinType id,T_HmiCreate HmiCreate,T_HmiPaint HmiPaint, T_DoMsg DoMsg)
{
    winCtrl[id].HmiCreate = HmiCreate;
    winCtrl[id].HmiPaint = HmiPaint;
    winCtrl[id].DoMsg = DoMsg;
    return 0;
}

u16 guiresponseEvtt = CN_EVTT_ID_INVALID;
static ClickResponse clickResponse= Gui_Null;  //点击响应类型
void SetClickResponse(ClickResponse type)
{
    clickResponse=type;
}

extern struct GuiSerial GuiSerialConfig[5];
extern struct GuiIp GuiIpConfig[5];
ptu32_t gui_response_event_main(void)
{
    struct SerialConfig trans_config={0};
    u32 serial_addr=0;
    u32 ip_addr=0;
    s32 ping=0;

    while (1)
    {
        switch(clickResponse)
        {
            case Gui_Save_Password:

                SetPromptBox(Box_Save_Password);
                PasswordSaveTimer();

                SetClickResponse(Gui_Null);
                break;

            case Gui_Save_Serial:
                serial_addr=GetComNumb();

                trans_config.addr=0;
                trans_config.data_bit=GuiSerialConfig[serial_addr].gdata_bit;
                trans_config.stop_bit=GuiSerialConfig[serial_addr].gstop_bit;
                trans_config.parity_bit=GuiSerialConfig[serial_addr].gparity_bit;
                trans_config.bitrate=GuiSerialConfig[serial_addr].gbitrate;
                trans_config.dev_addr =GuiSerialConfig[serial_addr].gequipment_address;

                SetProtocol(serial_addr,GuiSerialConfig[serial_addr].gsys_type_nub,trans_config);

                Jump_GuiWin(WIN_Serial_Win);
                SetClickResponse(Gui_Null);
                break;

            case Gui_Save_Ip:
                ip_addr=GetIpNumb();
                Write_IpInf(GuiIpConfig[ip_addr],ip_addr);

                SetTcpUrl(ip_addr);
                tra_breakout();
                SetConnectNumb(ip_addr);
                SetClickResponse(Gui_Null);
                break;

            case Gui_Ip_Test1:
                SetPromptBox(Box_Ip_Connecting);
                GDD_PostMessage(Get_WindowsHwnd(), MSG_REFRESH_UI,WIN_Prompt_Box, 1);
                SetClickResponse(Gui_Ip_Test2);
                break;
            case Gui_Ip_Test2:
                ping = xq_ping(GetIpBoxInput());
                if(ping>0)
                {
                    SetPromptBox(Box_Ip_Sucess);
                    printf("ping test: %dms", ping);
                }
                else
                {
                    SetPromptBox(Box_Ip_Error);
                    printf("ping test: %dms", ping);
                }
                IpConnectTimer();
                SetClickResponse(Gui_Null);
                break;
            case Gui_Usb_Copy:
                if(!MSC_DeviceReady(0))
                {
                    printf("U盘识别到了\r\n");
                    usb_log_copy();
                }
                else
                {
                    printf("U盘未识别\r\n");
                }
                SetClickResponse(Gui_Null);
                break;
            default:
                break;
        }
        DJY_EventDelay(100*1000);
    }
    return 0;
}

void GuiResponseInit(void)
{
    guiresponseEvtt = DJY_EvttRegist(EN_CORRELATIVE, CN_PRIO_RRS, 0, 0,
            gui_response_event_main, NULL, 0x2000, "guiresponse function");

    if (CN_EVTT_ID_INVALID != guiresponseEvtt)
    {
        DJY_EventPop(guiresponseEvtt, NULL, 0, 0, 0, 0);
    }
    else
    {
        printf("[DEBUG] Djy_EvttRegist-guiResponse_event_main error\r\n");
    }
}
