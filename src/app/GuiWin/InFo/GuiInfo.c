//----------------------------------------------------
// Copyright (c) 2014, SHENZHEN PENGRUI SOFT CO LTD. All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------
// Copyright (c) 2014 著作权由都江堰操作系统开源开发团队所有。著作权人保留一切权利。
//
// 这份授权条款，在使用者符合以下二条件的情形下，授予使用者使用及再散播本
// 软件包装原始码及二进位可执行形式的权利，无论此包装是否经改作皆然：
//
// 1. 对于本软件源代码的再散播，必须保留上述的版权宣告、此三条件表列，以
//    及下述的免责声明。
// 2. 对于本套件二进位可执行形式的再散播，必须连带以文件以及／或者其他附
//    于散播包装中的媒介方式，重制上述之版权宣告、此三条件表列，以及下述
//    的免责声明。

// 免责声明：本软件是本软件版权持有人以及贡献者以现状（"as is"）提供，
// 本软件包装不负任何明示或默示之担保责任，包括但不限于就适售性以及特定目
// 的的适用性为默示性担保。版权持有人及本软件之贡献者，无论任何条件、
// 无论成因或任何责任主义、无论此责任为因合约关系、无过失责任主义或因非违
// 约之侵权（包括过失或其他原因等）而起，对于任何因使用本软件包装所产生的
// 任何直接性、间接性、偶发性、特殊性、惩罚性或任何结果的损害（包括但不限
// 于替代商品或劳务之购用、使用损失、资料损失、利益损失、业务中断等等），
// 不负任何责任，即在该种使用已获事前告知可能会造成此类损害的情形下亦然。
//-----------------------------------------------------------------------------
/*
 * BmpInfo.c
 *
 *  Created on: 2019年9月20日
 *      Author: czz
 *
 *  功能:对要显示的Bmp文件集中管理，提供获取获取相应Bmp文件的接口
 *
 */
#define  GUI_INFO_FILE_C_
#include "GuiInfo.h"
#include "WinSwitch.h"
#include <gdd_widget.h>
#include <stddef.h>
#include "stdio.h"
#include "string.h"
#include "stdlib.h"
#include "board.h"
#include <dbug.h>
#include <time.h>

const char * fat_GetBmpPath(enum Bmptype type)
{
    switch (type)
    {
        case BMP_MainInterface1   :
            return "/yaf2/MainInterface1.bmp";
        case BMP_MainInterface2   :
            return "/yaf2/MainInterface2.bmp";
        case BMP_MainInterface3   :
            return "/yaf2/MainInterface3.bmp";
        case BMP_MainInterface4   :
            return "/yaf2/MainInterface4.bmp";
        case BMP_Connectioning   :
            return "/yaf2/Connectioning.bmp";
        case BMP_Disconnect   :
            return "/yaf2/Disconnect.bmp";
        case BMP_HomePageText   :
            return "/yaf2/HomePageText.bmp";
        case BMP_RunningState1   :
            return "/yaf2/RunningState1.bmp";
        case BMP_RunningState0   :
            return "/yaf2/RunningState0.bmp";
        case BMP_PortConfiguration1   :
            return "/yaf2/PortConfiguration1.bmp";
        case BMP_PortConfiguration0   :
            return "/yaf2/PortConfiguration0.bmp";
        case BMP_ServerConfiguration1   :
            return "/yaf2/ServerConfiguration1.bmp";
        case BMP_ServerConfiguration0   :
            return "/yaf2/ServerConfiguration0.bmp";
        case BMP_SystemSettings1   :
            return "/yaf2/SystemSettings1.bmp";
        case BMP_SystemSettings0   :
            return "/yaf2/SystemSettings0.bmp";
        case BMP_TopBox   :
            return "/yaf2/TopBox.bmp";
        case BMP_SmallFrame   :
            return "/yaf2/SmallFrame.bmp";
        case BMP_BigFrame   :
            return "/yaf2/BigFrame.bmp";
        case BMP_BRRemind   :
            return "/yaf2/BRRemind.bmp";

        case BMP_AboutMachText   :
            return "/yaf2/AboutMachText.bmp";
        case BMP_BRText   :
            return "/yaf2/BRText.bmp";
        case BMP_CheckUpdateText   :
            return "/yaf2/CheckUpdateText.bmp";
        case BMP_IPConfiText   :
            return "/yaf2/IPConfiText.bmp";
        case BMP_LogText   :
            return "/yaf2/LogText.bmp";
        case BMP_ModifPassText   :
            return "/yaf2/ModifPassText.bmp";
        case BMP_SysSetText   :
            return "/yaf2/SysSetText.bmp";
        case BMP_LoginText   :
            return "/yaf2/LoginText.bmp";

        case BMP_NetworkStatusBox   :
            return "/yaf2/NetworkStatusBox.bmp";
        case BMP_PowerStatusBox   :
            return "/yaf2/PowerStatusBox.bmp";
        case BMP_SerialStatusBox   :
            return "/yaf2/SerialStatusBox.bmp";
        case BMP_ServerStatusBox   :
            return "/yaf2/ServerStatusBox.bmp";
        case BMP_SystemInformationBox   :
            return "/yaf2/SystemInformationBox.bmp";

        case BMP_PortCom1   :
            return "/yaf2/PortCom1.bmp";
        case BMP_PortCom2   :
            return "/yaf2/PortCom2.bmp";
        case BMP_PortCom3   :
            return "/yaf2/PortCom3.bmp";
        case BMP_PortCom4   :
            return "/yaf2/PortCom4.bmp";
        case BMP_PortCom5   :
            return "/yaf2/PortCom5.bmp";
        case BMP_PortEdit   :
            return "/yaf2/PortEdit.bmp";
        case BMP_Directory   :
            return "/yaf2/Directory.bmp";
        case BMP_ChooseBackground   :
            return "/yaf2/ChooseBackground.bmp";
        case BMP_ChooseClose   :
            return "/yaf2/ChooseClose.bmp";
        case BMP_ChooseSave   :
            return "/yaf2/ChooseSave.bmp";
        case BMP_Check   :
            return "/yaf2/Check.bmp";
        case BMP_Back   :
            return "/yaf2/Back.bmp";
        case BMP_Save   :
            return "/yaf2/Save.bmp";

        case BMP_Gateway   :
            return "/yaf2/Gateway.bmp";
        case BMP_IpBox1   :
            return "/yaf2/IpBox1.bmp";
        case BMP_IpBox2   :
            return "/yaf2/IpBox2.bmp";
        case BMP_IpBox3   :
            return "/yaf2/IpBox3.bmp";
        case BMP_IpBox4   :
            return "/yaf2/IpBox4.bmp";
        case BMP_IpBox5   :
            return "/yaf2/IpBox5.bmp";

        case BMP_ExportButton   :
            return "/yaf2/ExportButton.bmp";
        case BMP_LogBackButton   :
            return "/yaf2/LogBackButton.bmp";
        case BMP_LogLeftButton   :
            return "/yaf2/LogLeftButton.bmp";
        case BMP_LogRightButton   :
            return "/yaf2/LogRightButton.bmp";
        case BMP_LogBox   :
            return "/yaf2/LogBox.bmp";
        case BMP_LogRemind   :
            return "/yaf2/LogRemind.bmp";
        case BMP_LogRemind2   :
            return "/yaf2/LogRemind2.bmp";

        case BMP_KeyBoard   :
            return "/yaf2/KeyBoard.bmp";

        case BMP_IpConnecting   :
            return "/yaf2/IpConnecting.bmp";
        case BMP_IpConnectError   :
            return "/yaf2/IpConnectError.bmp";
        case BMP_SaveSerialError   :
            return "/yaf2/SaveSerialError.bmp";
        case BMP_IpConnectSucess   :
            return "/yaf2/IpConnectSucess.bmp";

        default:      break;
    }
    return NULL;
}

static struct BmpInfo BmpPicture[BMP_MAXNNUM]={0};
struct HeapCB *my_heap=0;
void *psram_malloc (unsigned int size)
{
    if (my_heap==0){
        my_heap =Heap_FindHeap("extram");
        if(my_heap==NULL){
            printf("M_FindHeapd  ERROR!\r\n");
            return NULL;
        }
    }
    return M_MallocHeap(size,my_heap,0);
}
void DisplayPicture_Init(void)
{
    FILE *fp = NULL;
    s32 len;
    enum Bmptype count;

    for(count=BMP_MainInterface1;count < BMP_MAXNNUM;count++)
    {
        fp = fopen(fat_GetBmpPath(count),"r");

//        fseek(fp, 0L, SEEK_END);
//        BmpPicture.bmpsize = ftell(fp);
//        fseek(fp, 0L, SEEK_END);
        fseek(fp, 2, SEEK_SET);
        fread(&BmpPicture[count].bmpsize, 1, 4,fp);
        printf("BmpPicture.bmpsize is %d\r\n",BmpPicture[count].bmpsize);
        fseek(fp, 0, SEEK_SET);
        if(fp)
        {
            BmpPicture[count].Buf=psram_malloc(BmpPicture[count].bmpsize);
            if(BmpPicture[count].Buf != NULL)
            {
                len = fread(BmpPicture[count].Buf,1,BmpPicture[count].bmpsize,fp);
            }
            fclose(fp);
            printf("len is %d\r\n",len);
        }
    }

}

const char * Get_BmpBuf(enum Bmptype type)
{
    if((BMP_MainInterface1 <= type) && (type < BMP_MAXNNUM))
    {
        return BmpPicture[type].Buf;
    }
    else
    {
        return NULL;
    }
}
