#ifndef _DATA_STATUS_H_
#define _DATA_STATUS_H_


typedef enum
{
    Gui_Bitrate,             //  比特率
    Gui_DataBit,            //  数据位
    Gui_ParityBit,         //  校验位
    Gui_StopBit,            //  停止位
    Gui_EquipmentAddress,   //  设备地址
    Gui_SysType,           //  系统类型
//    Gui_SysBrand,          //  系统品牌
//    Gui_SysModel,          //  系统型号

}GuiSerialType;

struct GuiSerial
{
    u8  gdata_nub;            //  数据位界面位置
    u8  gparity_nub;          //  校验位界面位置
    u8  gstop_nub;            //  停止位界面位置
    u8  gbitrate_nub;         //  比特率界面位置
    u8  gdata_bit;            //  数据位
    u8  gparity_bit;          //  校验位
    u8  gstop_bit;            //  停止位
    u32 gbitrate;             //  比特率
    u8  gequipment_address;   //  设备地址
    u8  gsys_type_nub;        //  系统类型编码
    u32  gsys_type_flag;      //  是否选择了系统类型
    char  gsys_type[48];      //  系统类型
//    char  gsys_brand[48];          //  系统品牌
//    char  gsys_model[48];          //  系统型号
};


typedef enum
{
    Gui_IpAddress,       //  ip地址
    Gui_Port,            //  端口号

}GuiIpType;

struct GuiIp
{
    char gip_address[32];     //  ip地址
    char gport[16] ;          //  端口号
};

void SetComNumb(u32 numb);
void SetIpNumb(u32 numb);
u32 GetIpNumb(void);
u32 GetComNumb(void);
void SetGuiIp(u32 ip_numb,GuiIpType type,char *data);
void SetGuiSerial(u32 com_numb,GuiSerialType type,const char *data);
void SerialInfInit(void);
void IpInfInit(void);
#endif /* _DATA_STATUS_H_ */
