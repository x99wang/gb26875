/*
 * DataStatus.c
 *
 *  Created on: 2020年9月17日
 *      Author: cwj
 */

#include "stdint.h"
#include "string.h"
#include "stddef.h"
#include "djyos.h"
#include <stdio.h>
#include <dbug.h>
#include "WinSwitch.h"
#include "../trans_log/log.h"
#include "../trans_dev/trans_dev.h"
/*****************************COM结构体********************/
static u32 ComNumb=0;
struct GuiSerial GuiSerialConfig[5];

void SetComNumb(u32 numb)
{
    ComNumb=numb;
}
u32 GetComNumb(void)
{
    return ComNumb;
}
void SetGuiSerial(u32 com_numb,GuiSerialType type,const char *data)
{
    if(com_numb > 4)
        return;

    u32 result=0;
    switch(type)
    {
        case Gui_Bitrate :
            sscanf(data, "%d", &result);
            GuiSerialConfig[com_numb].gbitrate=result;
            break;
        case Gui_DataBit :
            sscanf(data, "%d", &result);
            GuiSerialConfig[com_numb].gdata_bit=(u8)result;
            break;
        case Gui_StopBit :
            sscanf(data, "%d", &result);
            GuiSerialConfig[com_numb].gstop_bit=(u8)result;
            break;
        case Gui_ParityBit :
            sscanf(data, "%d", &result);
            GuiSerialConfig[com_numb].gparity_bit=(u8)result;
            break;
        case Gui_EquipmentAddress :
            sscanf(data, "%d", &result);
            GuiSerialConfig[com_numb].gequipment_address=(u8)result;
            break;
        case Gui_SysType :
            strcpy(GuiSerialConfig[com_numb].gsys_type,data);
            break;
//        case Gui_SysBrand :
//            strcpy(GuiSerialConfig[com_numb].gsys_brand,data);
//            break;
//        case Gui_SysModel :
//            strcpy(GuiSerialConfig[com_numb].gsys_model,data);
//            break;
        default :
            break;
    }
}

void SerialInfInit(void)
{
    u32 i=0;
    for(i=0;i<UITD_SERIAL_NUM;i++)
    {
        Read_SerialInf(&GuiSerialConfig[i],i);
    }

    dev_protocol_init(GuiSerialConfig[0].gsys_type_flag,
                      GuiSerialConfig[1].gsys_type_flag,
                      GuiSerialConfig[2].gsys_type_flag,
                      GuiSerialConfig[3].gsys_type_flag,
                      GuiSerialConfig[4].gsys_type_flag);
}

/*******************************************************/


/*****************************IP结构体********************/
static u32 IpNumb=0;
struct GuiIp GuiIpConfig[5];

void SetIpNumb(u32 numb)
{
    IpNumb=numb;
}
u32 GetIpNumb(void)
{
    return IpNumb;
}
void SetGuiIp(u32 ip_numb,GuiIpType type,char *data)
{
    if(ip_numb > 4)
        return;

    switch(type)
    {
        case Gui_IpAddress :
            strcpy(GuiIpConfig[ip_numb].gip_address,data);
            break;
        case Gui_Port :
            strcpy(GuiIpConfig[ip_numb].gport,data);
            break;
        default :
            break;
    }
}
//服务器两个
void IpInfInit(void)
{
    u32 i=0;
    for(i=0;i<5;i++)
    {
        Read_IpInf(&GuiIpConfig[i],i);
    }
    SetTcpUrl(0);
}

/*******************************************************/
