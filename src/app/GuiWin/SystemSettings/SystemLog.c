//----------------------------------------------------
// Copyright (c) 2014, SHENZHEN PENGRUI SOFT CO LTD. All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------
// Copyright (c) 2014 著作权由都江堰操作系统开源开发团队所有。著作权人保留一切权利。
//
// 这份授权条款，在使用者符合以下二条件的情形下，授予使用者使用及再散播本
// 软件包装原始码及二进位可执行形式的权利，无论此包装是否经改作皆然：
//
// 1. 对于本软件源代码的再散播，必须保留上述的版权宣告、此三条件表列，以
//    及下述的免责声明。
// 2. 对于本套件二进位可执行形式的再散播，必须连带以文件以及／或者其他附
//    于散播包装中的媒介方式，重制上述之版权宣告、此三条件表列，以及下述
//    的免责声明。

// 免责声明：本软件是本软件版权持有人以及贡献者以现状（"as is"）提供，
// 本软件包装不负任何明示或默示之担保责任，包括但不限于就适售性以及特定目
// 的的适用性为默示性担保。版权持有人及本软件之贡献者，无论任何条件、
// 无论成因或任何责任主义、无论此责任为因合约关系、无过失责任主义或因非违
// 约之侵权（包括过失或其他原因等）而起，对于任何因使用本软件包装所产生的
// 任何直接性、间接性、偶发性、特殊性、惩罚性或任何结果的损害（包括但不限
// 于替代商品或劳务之购用、使用损失、资料损失、利益损失、业务中断等等），
// 不负任何责任，即在该种使用已获事前告知可能会造成此类损害的情形下亦然。
//-----------------------------------------------------------------------------

#include <stdint.h>
#include "stdlib.h"
#include "stdio.h"
#include "djyos.h"
#include "gdd.h"
#include <gdd_widget.h>
#include "board.h"
#include "../GuiWin/Info/GuiInfo.h"
#include "../GuiWin/Info/WinSwitch.h"

enum SystemLogWidgeId{
    SYS_BACKGROUND,

    SysTopBox,
    SysPageText,
    SysRunningState0,
    SysPortConfiguration0,
    SysServerConfiguration0,
    SysSystemSettings1,

    SysBigFrame,
    LogText,
    LogBox,
    LogRemind,
    LogPageInfo,
    LogPageList1,
    LogPageList2,
    LogPageList3,
    LogTableTime,
    LogTableType,
    LogTableDetail,
    LogLeftButton,
    LogRightButton,
    LogExportButton,
    LogBackButton,



    SYS_MAXNUM,//总数量
};

//==========================s========config======================================
static const  struct GUIINFO SystemLogCfgTab[SYS_MAXNUM] =
{
    [SYS_BACKGROUND] = {
        .position = {0/LCDW_32,0/LCDH_32,1024/LCDW_32,600/LCDH_32},
        .name = "background",
        .type = widget_type_background,
        .userParam = RGB(255,255,255),
    },
    [SysTopBox] = {
        .position = {-9/LCDW_32,-6/LCDH_32,(-9+1042)/LCDW_32,(-6+88)/LCDH_32},
        .name = "box0",
        .type = widget_type_picture,
        .userParam = BMP_TopBox,
    },
    [SysPageText] = {
        .position = {24/LCDW_32,17/LCDH_32,(24+450)/LCDW_32,(17+37)/LCDH_32},
        .name = "text1",
        .type = widget_type_picture,
        .userParam = BMP_HomePageText,
    },
    [SysRunningState0] = {
        .position = {651/LCDW_32,5/LCDH_32,(651+65)/LCDW_32,(5+60)/LCDH_32},
        .name = "top1",
        .type = widget_type_button,
        .userParam = BMP_RunningState0,
    },
    [SysPortConfiguration0] = {
        .position = {749/LCDW_32,8/LCDH_32,(749+65)/LCDW_32,(8+57)/LCDH_32},
        .name = "top2",
        .type = widget_type_button,
        .userParam = BMP_PortConfiguration0,
    },
    [SysServerConfiguration0] = {
        .position = {847/LCDW_32,5/LCDH_32,(847+47)/LCDW_32,(5+59)/LCDH_32},
        .name = "top3",
        .type = widget_type_button,
        .userParam = BMP_ServerConfiguration0,
    },
    [SysSystemSettings1] = {
        .position = {926/LCDW_32,5/LCDH_32,(926+65)/LCDW_32,(5+60)/LCDH_32},
        .name = "top4",
        .type = widget_type_button,
        .userParam = BMP_SystemSettings1,
    },

    [SysBigFrame] = {
        .position = {0/LCDW_32,80/LCDH_32,(0+1024)/LCDW_32,(80+520)/LCDH_32},
        .name = "大框",
        .type = widget_type_picture,
        .userParam = BMP_BigFrame,
    },
    [LogText] = {
        .position = {55/LCDW_32,81/LCDH_32,(55+67)/LCDW_32,(81+27)/LCDH_32},
        .name = "日志",
        .type = widget_type_picture,
        .userParam = BMP_LogText,
    },

    [LogBox] = {
        .position = {29/LCDW_32,117/LCDH_32,(29+967)/LCDW_32,(117+400)/LCDH_32},
        .name = "box1",
        .type = widget_type_picture,
        .userParam = BMP_LogBox,
    },
    [LogRemind] = {
        .position = {34/LCDW_32,536/LCDH_32,(34+321)/LCDW_32,(536+30)/LCDH_32},
        .name = "box2",
        .type = widget_type_pictureground,
        .userParam = BMP_LogRemind,
    },
    [LogPageInfo] = {
        .position = {463/LCDW_32,541/LCDH_32,(463+99)/LCDW_32,(541+18)/LCDH_32},
        .name = "页数",
        .type = widget_type_job,
        .userParam = Bmp_NULL,
    },
    [LogPageList1] = {
        .position = {29/LCDW_32,157/LCDH_32,(29+164)/LCDW_32,(157+40)/LCDH_32},
        .name = "第一列",
        .type = widget_type_job,
        .userParam = Bmp_NULL,
    },
    [LogPageList2] = {
        .position = {192/LCDW_32,157/LCDH_32,(192+104)/LCDW_32,(157+40)/LCDH_32},
        .name = "第二列",
        .type = widget_type_job,
        .userParam = Bmp_NULL,
    },
    [LogPageList3] = {
        .position = {308/LCDW_32,157/LCDH_32,(308+701)/LCDW_32,(157+40)/LCDH_32},
        .name = "第三列",
        .type = widget_type_job,
        .userParam = Bmp_NULL,
    },
    [LogTableTime] = {
        .position = {29/LCDW_32,117/LCDH_32,(29+164)/LCDW_32,(117+40)/LCDH_32},
        .name = "时间",
        .type = widget_type_text,
        .userParam = Bmp_NULL,
    },
    [LogTableType] = {
        .position = {192/LCDW_32,117/LCDH_32,(192+104)/LCDW_32,(117+40)/LCDH_32},
        .name = "类型",
        .type = widget_type_text,
        .userParam = Bmp_NULL,
    },
    [LogTableDetail] = {
        .position = {295/LCDW_32,117/LCDH_32,(295+701)/LCDW_32,(117+40)/LCDH_32},
        .name = "明细",
        .type = widget_type_text,
        .userParam = Bmp_NULL,
    },
    [LogLeftButton] = {
        .position = {422/LCDW_32,532/LCDH_32,(422+36)/LCDW_32,(532+36)/LCDH_32},
        .name = "button1",
        .type = widget_type_button,
        .userParam = BMP_LogLeftButton,
    },
    [LogRightButton] = {
        .position = {566/LCDW_32,532/LCDH_32,(566+36)/LCDW_32,(532+36)/LCDH_32},
        .name = "button2",
        .type = widget_type_button,
        .userParam = BMP_LogRightButton,
    },
    [LogExportButton] = {
        .position = {816/LCDW_32,534/LCDH_32,(816+80)/LCDW_32,(534+36)/LCDH_32},
        .name = "button3",
        .type = widget_type_button,
        .userParam = BMP_ExportButton,
    },
    [LogBackButton] = {
         .position = {916/LCDW_32,534/LCDH_32,(916+80)/LCDW_32,(534+36)/LCDH_32},
         .name = "button4",
         .type = widget_type_button,
         .userParam = BMP_LogBackButton,
     },


};

#define PAGE_MAX  10
static int page_info=1;
static int page_sum=PAGE_MAX;

void AddPageInfo(void)
{
    page_info++;
    if(page_info>page_sum)
    {
        page_info=page_sum;
    }
}
void ReducePageInfo(void)
{
    page_info--;
    if(page_info<1)
    {
        page_info=1;
    }
}
void GetLogTime(char *time,int len);
static int log_list=0;  //di
static char log_time[16]={0};
void SetLogPageInf(char *type,char *info)
{
    if(log_list>=page_sum*9)
    {
        log_list=0;
    }
    GetLogTime(log_time,sizeof(log_time));
    SetGuiSysLog(Sys_Log_Time,log_list,log_time,strlen(log_time));
    SetGuiSysLog(Sys_Log_Type,log_list,type,strlen(type));
    SetGuiSysLog(Sys_Log_Inf,log_list,info,strlen(info));
    log_list++;
}
//按钮控件创建函数
static bool_t  SystemLogButtonPaint(struct WindowMsg *pMsg)
{
    HWND hwnd;
    HDC  hdc;
    const char *bmp;
    hwnd =pMsg->hwnd;
    hdc =GDD_BeginPaint(hwnd);
    if(hdc)
    {
        const struct GUIINFO *buttoninfo = (struct GUIINFO *)GDD_GetWindowPrivateData(hwnd);

        if(buttoninfo->type == widget_type_button)
        {
            bmp = Get_BmpBuf(buttoninfo->userParam);
            GDD_DrawBMP(hdc,0,0,bmp);
        }

        GDD_EndPaint(hwnd,hdc);
        return true;
    }
    return false;
}


/*---------------------------------------------------------------------------
功能：  窗口创建函数
    输入   :tagWindowMsg *pMsg
    输出 :false或true
---------------------------------------------------------------------------*/
static bool_t HmiCreate_SystemLog(struct WindowMsg *pMsg)
{
    HWND hwnd =pMsg->hwnd;

    //消息处理函数表
    static struct MsgProcTable s_gBmpMsgTablebutton[] =
    {
            {MSG_PAINT,SystemLogButtonPaint},
    };

    static struct MsgTableLink  s_gBmpDemoMsgLinkBUtton;

    s_gBmpDemoMsgLinkBUtton.MsgNum = sizeof(s_gBmpMsgTablebutton) / sizeof(struct MsgProcTable);
    s_gBmpDemoMsgLinkBUtton.myTable = (struct MsgProcTable *)&s_gBmpMsgTablebutton;

    for(int i=0;i<SYS_MAXNUM;i++)
    {
        switch (SystemLogCfgTab[i].type)
        {
            case  widget_type_button :
            {

                Widget_CreateButton(SystemLogCfgTab[i].name, WS_CHILD|BS_NORMAL|WS_UNFILL,    //按钮风格
                                 SystemLogCfgTab[i].position.left, SystemLogCfgTab[i].position.top,\
                                 GDD_RectW(&SystemLogCfgTab[i].position),GDD_RectH(&SystemLogCfgTab[i].position), //按钮位置和大小
                                 hwnd,i,(ptu32_t)&SystemLogCfgTab[i], &s_gBmpDemoMsgLinkBUtton);   //按钮所属的父窗口，ID,附加数据
            }
                break;
            default:    break;
        }
    }
    return true;
}

//绘制消息处函数
static bool_t HmiPaint_SystemLog(struct WindowMsg *pMsg)
{
    HWND hwnd;
    HDC  hdc;
    RECT rc1,rc2,rc3;
    const char *bmp;
    hwnd =pMsg->hwnd;

    hdc =GDD_BeginPaint(hwnd);
    if(hdc)
    {
        GDD_SetTextColor(hdc,RGB(60,60,60));
        GDD_GetClientRect(hwnd,&rc1);
        GDD_GetClientRect(hwnd,&rc2);
        GDD_GetClientRect(hwnd,&rc3);

        for(int i=0;i<SYS_MAXNUM;i++)
        {
            switch (SystemLogCfgTab[i].type)
            {
                case  widget_type_background :
                    GDD_SetFillColor(hdc,SystemLogCfgTab[i].userParam);
                    GDD_FillRect(hdc,&SystemLogCfgTab[i].position);
                    break;
                case  widget_type_picture :
                    bmp = Get_BmpBuf(SystemLogCfgTab[i].userParam);
                    if(bmp != NULL)
                    {
                        GDD_DrawBMP(hdc,SystemLogCfgTab[i].position.left,\
                                SystemLogCfgTab[i].position.top,bmp);
                    }
                    break;
                case  widget_type_pictureground:
                    if(!MSC_DeviceReady(0))
                    {
                        bmp = Get_BmpBuf(BMP_LogRemind2);
                    }
                    else
                    {
                        bmp = Get_BmpBuf(BMP_LogRemind);
                    }

                    if(bmp != NULL)
                    {
                        GDD_DrawBMP(hdc,SystemLogCfgTab[i].position.left,\
                        SystemLogCfgTab[i].position.top,bmp);
                    }
                    break;
                case  widget_type_text :
                    GDD_DrawText(hdc,SystemLogCfgTab[i].name,-1,&SystemLogCfgTab[i].position,DT_VCENTER|DT_CENTER);
                    break;
                default:    break;
            }
        }

        char page[56]={0};
        snprintf(page,sizeof(page),"第%d页,共%d页",page_info,page_sum);
        GDD_DrawText(hdc,page,-1,&SystemLogCfgTab[LogPageInfo].position,DT_VCENTER|DT_CENTER);

        rc1=SystemLogCfgTab[LogPageList1].position;
        rc2=SystemLogCfgTab[LogPageList2].position;
        rc3=SystemLogCfgTab[LogPageList3].position;
        int begin=(page_info-1)*9;
        for(int i=begin;i<begin+9;i++)
        {
            GDD_DrawText(hdc,GetGuiSysLog(1,i),-1,&rc1,DT_VCENTER|DT_CENTER);
            GDD_DrawText(hdc,GetGuiSysLog(2,i),-1,&rc2,DT_VCENTER|DT_CENTER);
            GDD_DrawText(hdc,GetGuiSysLog(3,i),-1,&rc3,DT_LEFT|DT_CENTER);
            rc1.top+=40;
            rc2.top+=40;
            rc3.top+=40;
            rc1.bottom+=40;
            rc2.bottom+=40;
            rc3.bottom+=40;
        }
        GDD_EndPaint(hwnd,hdc);
        return true;
    }
    return false;
}

static enum WinType HmiNotify_SystemLog(struct WindowMsg *pMsg)
{
    enum WinType NextUi = WIN_NotChange;
    u16 event =HI16(pMsg->Param1);
    u16 id =LO16(pMsg->Param1);

    if(event == MSG_BTN_UP)
    {
        switch(id)
        {
            case SysRunningState0:
                NextUi= WIN_Home_Page;
                break;
            case SysPortConfiguration0:
                NextUi= WIN_Serial_Win  ;
                break;
            case SysServerConfiguration0:
                NextUi= WIN_Ip_Win  ;
                break;
//            case SysSystemSettings1:
//                NextUi= WIN_System_Win  ;
//                break;
            case LogLeftButton:
                ReducePageInfo();
                NextUi= WIN_System_Log  ;
                break;
            case LogRightButton:
                AddPageInfo();
                NextUi= WIN_System_Log  ;
                break;
            case LogBackButton:
                NextUi= WIN_System_Win  ;
                break;
            case LogExportButton:
                SetClickResponse(Gui_Usb_Copy);
                break;
            default:
                break;
        }
    }
    return NextUi;
}


int Register_System_Log()
{
    return Register_NewWin(WIN_System_Log,HmiCreate_SystemLog,HmiPaint_SystemLog,HmiNotify_SystemLog);
}
