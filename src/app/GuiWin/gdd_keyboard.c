//----------------------------------------------------
// Copyright (c) 2018, Djyos Open source Development team. All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:

// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. As a constituent part of djyos,do not transplant it to other software
//    without specific prior written permission.

// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------
// Copyright (c) 2018，著作权由都江堰操作系统开源开发团队所有。著作权人保留一切权利。
//
// 这份授权条款，在使用者符合以下三条件的情形下，授予使用者使用及再散播本
// 软件包装原始码及二进位可执行形式的权利，无论此包装是否经改作皆然：
//
// 1. 对于本软件源代码的再散播，必须保留上述的版权宣告、本条件列表，以
//    及下述的免责声明。
// 2. 对于本套件二进位可执行形式的再散播，必须连带以文件以及／或者其他附
//    于散播包装中的媒介方式，重制上述之版权宣告、本条件列表，以及下述
//    的免责声明。
// 3. 本软件作为都江堰操作系统的组成部分，未获事前取得的书面许可，不允许移植到非
//    都江堰操作系统环境下运行。

// 免责声明：本软件是本软件版权持有人以及贡献者以现状（"as is"）提供，
// 本软件包装不负任何明示或默示之担保责任，包括但不限于就适售性以及特定目
// 的的适用性为默示性担保。版权持有人及本软件之贡献者，无论任何条件、
// 无论成因或任何责任主义、无论此责任为因合约关系、无过失责任主义或因非违
// 约之侵权（包括过失或其他原因等）而起，对于任何因使用本软件包装所产生的
// 任何直接性、间接性、偶发性、特殊性、惩罚性或任何结果的损害（包括但不限
// 于替代商品或劳务之购用、使用损失、资料损失、利益损失、业务中断等等），
// 不负任何责任，即在该种使用已获事前告知可能会造成此类损害的情形下亦然。
//-----------------------------------------------------------------------------
//所属模块: GDD
//作者:  zhb.
//版本：V1.0.0
//文件描述: 虚拟键盘控件实现
//其他说明:
//修订历史:
//2. ...
//1. 日期: 2016-10-08
//   作者:  zhb.
//   新版本号：V1.0.0
//   修改说明: 原始版本
//---------------------------------

#include  "gdd.h"
#include  <gui/gdd/gdd_private.h>
#include "font.h"
#include    <gdd_widget.h>
#include "dbug.h"
#include "Info/GuiInfo.h"
#include "../GuiWin/Info/WinSwitch.h"
//界面元素信息
struct KEYINFO
{
    RECT position;//坐标，大小信息
    const char* name;
    ptu32_t userParam;
} ;

//数字、字符元素定义
enum KeyBoard{
    ID_KEY_1,
    ID_KEY_2,
    ID_KEY_3,
    ID_KEY_4,
    ID_KEY_5,
    ID_KEY_6,
    ID_KEY_7,
    ID_KEY_8,
    ID_KEY_9,
    ID_KEY_0,
    ID_KEY_POINT,
    ID_KEY_CANCEL,
    ID_KEY_Determine,
    ID_KEY_BACK,
    KEY_BACKGROUND,
    ENUM_MAXNUM,//总数量
};





//==================================数字、符号======================================
static struct KEYINFO KeyBoardCfgTab[ENUM_MAXNUM] =
{
    [ID_KEY_1] = {
        .position = {11/LCDW_32,13/LCDH_32,(11+82)/LCDW_32,(13+104)/LCDH_32},
        .name="1",
        .userParam = Bmp_NULL,
    },
    [ID_KEY_2] = {
        .position = {95/LCDW_32,13/LCDH_32,(95+82)/LCDW_32,(13+104)/LCDH_32},
        .name="2",
        .userParam = Bmp_NULL,
    },
    [ID_KEY_3] = {
        .position = {178/LCDW_32,13/LCDH_32,(178+82)/LCDW_32,(13+104)/LCDH_32},
        .name="3",
        .userParam = Bmp_NULL,
    },
    [ID_KEY_4] = {
        .position = {262/LCDW_32,13/LCDH_32,(262+82)/LCDW_32,(13+104)/LCDH_32},
        .name="4",
        .userParam = Bmp_NULL,
    },
    [ID_KEY_5] = {
        .position = {346/LCDW_32,13/LCDH_32,(346+82)/LCDW_32,(13+104)/LCDH_32},
        .name="5",
        .userParam = Bmp_NULL,
    },
    [ID_KEY_6] = {
        .position = {429/LCDW_32,13/LCDH_32,(429+82)/LCDW_32,(13+104)/LCDH_32},
        .name="6",
        .userParam = Bmp_NULL,
    },
    [ID_KEY_7] = {
        .position = {513/LCDW_32,13/LCDH_32,(513+82)/LCDW_32,(13+104)/LCDH_32},
        .name="7",
        .userParam = Bmp_NULL,
    },
    [ID_KEY_8] = {
        .position = {596/LCDW_32,13/LCDH_32,(596+82)/LCDW_32,(13+104)/LCDH_32},
        .name="8",
        .userParam = Bmp_NULL,
    },
    [ID_KEY_9] = {
        .position = {680/LCDW_32,13/LCDH_32,(680+82)/LCDW_32,(13+104)/LCDH_32},
        .name="9",
        .userParam = Bmp_NULL,
    },
    [ID_KEY_0] = {
        .position = {764/LCDW_32,13/LCDH_32,(764+82)/LCDW_32,(13+104)/LCDH_32},
        .name="0",
        .userParam = Bmp_NULL,
    },
    [ID_KEY_POINT] = {
        .position = {847/LCDW_32,13/LCDH_32,(847+82)/LCDW_32,(13+104)/LCDH_32},
        .name=".",
        .userParam = Bmp_NULL,
    },
    [ID_KEY_CANCEL] = {
        .position = {931/LCDW_32,13/LCDH_32,(931+82)/LCDW_32,(13+104)/LCDH_32},
        .name="取消",
        .userParam = Bmp_NULL,
    },
    [ID_KEY_Determine] = {
        .position = {430/LCDW_32,130/LCDH_32,(430+72)/LCDW_32,(130+46)/LCDH_32},
        .name="确定",
        .userParam = Bmp_NULL,
    },
    [ID_KEY_BACK] = {
        .position = {522/LCDW_32,130/LCDH_32,(522+72)/LCDW_32,(130+46)/LCDH_32},
        .name="返回",
        .userParam = Bmp_NULL,
    },
    [KEY_BACKGROUND] = {
        .position = {0/LCDW_32,0/LCDH_32,1024/LCDW_32,204/LCDH_32},
        .name="背景",
        .userParam = Bmp_NULL,
    },
};

#define BMPKEY 1
static u16 keydown=ENUM_MAXNUM;  //按键点击的数字
static HWND KeyDowmHwnd;
void Update_KeyDowmHwnd(void)
{
    if(KeyDowmHwnd!=NULL)
    {
        GDD_PostMessage(KeyDowmHwnd,MSG_PAINT,0,0);
    }

}

static bool_t ViKeyBoard_Tonch(struct WindowMsg *pMsg)
{
    s16 x = LO16(pMsg->Param2);
    s16 y = HI16(pMsg->Param2);

    y=y-396;

    //==================================大小写字母======================================

    if(13<=y && y<=(13+104))
    {
        if(11<=x && x<=(11+82))
        {
            keydown=ID_KEY_1;
        }
        else if(95<=x && x<=(95+82))
        {
            keydown=ID_KEY_2;
        }
        else if(178<=x && x<=(178+82))
        {
            keydown=ID_KEY_3;
        }
        else if(262<=x && x<=(262+82))
        {
            keydown=ID_KEY_4;
        }
        else if(346<=x && x<=(346+82))
        {
            keydown=ID_KEY_5;
        }
        else if(429<=x && x<=(429+82))
        {
            keydown=ID_KEY_6;
        }
        else if(513<=x && x<=(513+82))
        {
            keydown=ID_KEY_7;
        }
        else if(596<=x && x<=(596+82))
        {
            keydown=ID_KEY_8;
        }
        else if(680<=x && x<=(680+82))
        {
            keydown=ID_KEY_9;
        }
        else if(764<=x && x<=(764+82))
        {
            keydown=ID_KEY_0;
        }
        else if(847<=x && x<=(847+82))
        {
            keydown=ID_KEY_POINT;
        }
        else if(931<=x && x<=(931+82))
        {
            keydown=ID_KEY_CANCEL;
        }
    }
    else if(130<=y && y<=(130+46))
    {
        if(430<=x && x<=(430+72))
        {
            keydown=ID_KEY_Determine;
        }
        else if(522<=x && x<=(522+72))
        {
            keydown=ID_KEY_BACK;
        }
    }
    else
    {
        keydown=ENUM_MAXNUM;
    }

#if BMPKEY
#else
    Update_KeyDowmHwnd();
#endif
//    printf("=========keydown is %d\r\n",keydown);
    return true;
}

//----------------------------------------------------------------------------
//功能：虚拟键盘控件的MSG_PAINT消息响应函数
//参数：pMsg，窗口消息指针
//返回：成功返回true,失败则返回false。
//-----------------------------------------------------------------------------
static  bool_t ButtonKeyBoard_Paint(struct WindowMsg *pMsg)
{
    HWND hwnd;
    HDC hdc;

#if BMPKEY
    const char *bmp;
#else
    RECT rc;
#endif

    if(pMsg==NULL)
        return false;
    hwnd=pMsg->hwnd;
    if(hwnd==NULL)
        return false;
    hdc =GDD_BeginPaint(hwnd);
    if(hdc==NULL)
        return false;

#if BMPKEY
    bmp = Get_BmpBuf(BMP_KeyBoard);
    GDD_DrawBMP(hdc,0,0,bmp);
#else
    GDD_GetClientRect(hwnd,&rc);

    GDD_SetFillColor(hdc,RGB(255,255,255));
    GDD_FillRect(hdc,&rc);


    for(int i=ID_KEY_1;i<KEY_BACKGROUND;i++)
    {
        if(keydown == i)
        {
            GDD_SetFillColor(hdc,RGB(208,212,218));
            GDD_FillRect(hdc,&KeyBoardCfgTab[i].position);
        }
        else
        {
            GDD_SetFillColor(hdc,RGB(255,255,255));
            GDD_FillRect(hdc,&KeyBoardCfgTab[i].position);
        }
        GDD_SetDrawColor(hdc,RGB(115,115,115));
        GDD_DrawRect(hdc,&KeyBoardCfgTab[i].position);
        GDD_SetTextColor(hdc,RGB(28,32,42));
        GDD_DrawText(hdc,KeyBoardCfgTab[i].name,-1,&KeyBoardCfgTab[i].position,DT_VCENTER|DT_CENTER);
    }
#endif

    GDD_EndPaint(hwnd,hdc);

    return true;
}


//----------------------------------------------------------------------------
//功能：虚拟键盘控件的MSG_PAINT消息响应函数
//参数：pMsg，窗口消息指针
//返回：成功返回true,失败则返回false。
//-----------------------------------------------------------------------------
static  bool_t ViKeyBoard_Paint(struct WindowMsg *pMsg)
{
    HWND hwnd;
    HDC hdc;
    RECT rc;

    if(pMsg==NULL)
        return false;
    hwnd=pMsg->hwnd;
    if(hwnd==NULL)
        return false;
    hdc =GDD_BeginPaint(hwnd);
    if(hdc==NULL)
        return false;
    GDD_GetClientRect(hwnd,&rc);
    GDD_SetFillColor(hdc,RGB(208,212,218));
    GDD_FillRect(hdc,&rc);

    GDD_EndPaint(hwnd,hdc);

    return true;
}

//---------------------------------------------------------------------------
//功能：虚拟按键创建消息响应函数
//参数：pMsg，窗口消息指针
//返回：成功返回true,失败则返回false。
//-----------------------------------------------------------------------------
static bool_t __Widget_VirKeyBoardCreate(struct WindowMsg *pMsg)
{
    HWND hwnd =pMsg->hwnd;

//    Mainlistcb.Firstmenu = true;
    //消息处理函数表
    static struct MsgProcTable s_gBmpMsgTablebutton[] =
    {
            {MSG_PAINT,ButtonKeyBoard_Paint},
            {MSG_TOUCH_DOWN,ViKeyBoard_Tonch},
    };

    static struct MsgTableLink  s_gBmpDemoMsgLinkBUtton;

    s_gBmpDemoMsgLinkBUtton.MsgNum = sizeof(s_gBmpMsgTablebutton) / sizeof(struct MsgProcTable);
    s_gBmpDemoMsgLinkBUtton.myTable = (struct MsgProcTable *)&s_gBmpMsgTablebutton;

    KeyDowmHwnd=Widget_CreateButton("虚拟键盘", WS_CHILD|BS_NORMAL | WS_UNFILL,    //按钮风格
            KeyBoardCfgTab[KEY_BACKGROUND].position.left, KeyBoardCfgTab[KEY_BACKGROUND].position.top,\
                GDD_RectW(&KeyBoardCfgTab[KEY_BACKGROUND].position),GDD_RectH(&KeyBoardCfgTab[KEY_BACKGROUND].position), //按钮位置和大小
                hwnd,KEY_BACKGROUND,(ptu32_t)&KeyBoardCfgTab[KEY_BACKGROUND],&s_gBmpDemoMsgLinkBUtton);   //按钮所属的父窗口，ID,附加数据

    return true;
}


//-----------------------------------------------------------------
//功能：虚拟键盘控件MSG_NOTIFY消息响应函数
 //参数：pMsg，窗口消息指针。
 //返回：成功返回true,失败则返回false。
//-----------------------------------------------------------------------------
static bool_t __Widget_VirKeyBoardNotifyHandle(struct WindowMsg *pMsg)
{
    u16 event =HI16(pMsg->Param1);
    u16 id =LO16(pMsg->Param1);
    HWND focus_hwnd=GDD_GetFocusWindow();

    if(event==MSG_BTN_UP && keydown != ENUM_MAXNUM && GetStatueMachine()== CtrlNull)
    {
        switch(id)
        {
            case KEY_BACKGROUND:
                if(ID_KEY_Determine==keydown)
                {
                    GDD_PostMessage(Get_TextBox_hwnd(), MSG_TOUCH_DOWN, 1, 0);
                }
                else if(ID_KEY_BACK==keydown)
                {
                    GDD_PostMessage(Get_TextBox_hwnd(), MSG_TOUCH_DOWN, 2, 0);
                }
                else if(ID_KEY_CANCEL==keydown)
                {
                    GDD_PostMessage( focus_hwnd, MSG_KEY_DOWN, VK_DEL_CHAR, 0);
                }
                else
                {
                    GDD_PostMessage( focus_hwnd, MSG_KEY_DOWN, (u32)*KeyBoardCfgTab[keydown].name, 0);
                }
                keydown=ENUM_MAXNUM;
                break;
            default:
                  break;
        }

    }
    return true;
}



//默认虚拟键盘消息处理函数表，处理用户函数表中没有处理的消息。
static struct MsgProcTable s_gVirKeyBoardMsgProcTable[] =
{
    {MSG_CREATE,__Widget_VirKeyBoardCreate},
    {MSG_PAINT,ViKeyBoard_Paint},
    {MSG_NOTIFY,__Widget_VirKeyBoardNotifyHandle},
};

static struct MsgTableLink  s_gVirKeyBoardMsgLink;
// =============================================================================
// 函数功能: 虚拟键盘控件创建函数。
// 输入参数: Text:虚拟键盘窗口Text;
//           Style:虚拟键盘风格，参见gdd.h;
//           x:虚拟键盘起始位置x方向坐标(单位：像素);
//           y:虚拟键盘起始位置y方向坐标(单位：像素);
//           w:虚拟键盘宽度(单位：像素);
//           h:虚拟键盘高度(单位：像素);
//           hParent:虚拟键盘父窗口句柄;
//           WinId:虚拟键盘控件Id;
//           pdata:虚拟键盘控件私有数据结构;
//           UserMsgTableLink:虚拟键盘控件用户消息列表结构指针。
// 输出参数: 无。
// 返回值  :成功则返回文本框句柄，失败则返回NULL。
// =============================================================================
HWND Widget_CreateVirKeyBoard(const char *Text,u32 Style,
                    s32 x,s32 y,s32 w,s32 h,
                    HWND hParent,u32 WinId,ptu32_t pdata,
                    struct MsgTableLink *UserMsgTableLink)
{
    HWND pGddWin;
    s_gVirKeyBoardMsgLink.MsgNum = sizeof(s_gVirKeyBoardMsgProcTable) / sizeof(struct MsgProcTable);
    s_gVirKeyBoardMsgLink.myTable = (struct MsgProcTable *)&s_gVirKeyBoardMsgProcTable;
    pGddWin=GDD_CreateWindow(Text,WS_CHILD | WS_CAN_FOCUS|Style,x,y,w,h,hParent,WinId,
                            CN_WINBUF_PARENT,pdata,&s_gVirKeyBoardMsgLink);
    if(UserMsgTableLink != NULL)
        GDD_AddProcFuncTable(pGddWin,UserMsgTableLink);
    return pGddWin;
}



