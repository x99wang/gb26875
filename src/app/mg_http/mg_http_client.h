/*
 * mg_http_client.h
 *
 *  Mongoose HTTP Module
 *
 *  Created on: May 18, 2020
 *      Author: WangXi
 */

#ifndef MG_HTTP_CLIENT_H_
#define MG_HTTP_CLIENT_H_

#define MG_HTTP_CLIENT          1

#include "mongoose.h"
#define CFG_MAX_HTTP_HEADERS    MG_MAX_HTTP_HEADERS

struct MgHttpRequest
{
    const char *method;
    const char *url;
    const char *headers;
    char *data;
    char *params;
    bool_t allow_redirects;
    void * response;
    // todo 其他属性
};

#define MG_HTTP_METHOD_GET      "get"
#define MG_HTTP_METHOD_POST     "post"
#define MG_HTTP_METHOD_PUT      "put"
#define MG_HTTP_METHOD_PATCH    "patch"
#define MG_HTTP_METHOD_DELETE   "delete"
#define MG_HTTP_METHOD_OPTIONS  "options"
#define MG_HTTP_METHOD_HEAD     "head"

#define MG_HTTP_STATUS_OK                   200
#define MG_HTTP_STATUS_MOVE_TEMPORARILY     302
#define MG_HTTP_STATUS_BAD_REQUEST          400
#define MG_HTTP_STATUS_FORBIDDEN            401
#define MG_HTTP_STATUS_NOT_FOUND            404
#define MG_HTTP_STATUS_ERROR                0xFFFF


struct http_message *MG_HttpRequestBase(struct MgHttpRequest *request, u64 timeout);

struct http_message *MG_HttpRequestOpt(const char *method, const char *url, char *param, char *data, u64 timeout);

struct http_message *MG_HttpRequest(const char *method, const char *url, char *param, char *data);

/*
 * 功能:
 *    HTTP GET  请求
 * 参数:
 *    url,      请求地址
 *    param,    查询参数
 *    headers,  请求头
 */
#define MG_HttpGet(url)     MG_HttpRequest(MG_HTTP_METHOD_GET, url, NULL, NULL)

#define MG_HttpOptions(url)        MG_HttpRequest(MG_HTTP_METHOD_OPTIONS, url, NULL, true)

#define MG_HttpHead(url)           MG_HttpRequest(MG_HTTP_METHOD_HEAD, url, NULL, false)

/*
 * 功能:
 *    HTTP POST 请求
 * 参数:
 *    url,      请求地址
 *    data,     请求body
 *    param,    查询参数
 *    headers,  请求头
 */
#define MG_HttpPost(url, data)     MG_HttpRequest(MG_HTTP_METHOD_POST, url, NULL, data)

#define MG_HttpPut(url, data)      MG_HttpRequest(MG_HTTP_METHOD_PUT, url, NULL, data)

#define MG_HttpPatch(url, data)    MG_HttpRequest(MG_HTTP_METHOD_PATCH, url, NULL, data)

#define MG_HttpDelete(url)         MG_HttpRequest(MG_HTTP_METHOD_DELETE, url, NULL, NULL)

#endif /* MG_HTTP_CLIENT_H_ */
