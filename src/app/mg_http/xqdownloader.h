/*
 * xqdownloader.h
 *
 *  Created on: May 29, 2020
 *      Author: Magic
 */

#ifndef APP_XQ_APP_XQDOWNLOADER_H_
#define APP_XQ_APP_XQDOWNLOADER_H_


#include "mg_http_client.h"

#define XQ_DOWNLOADER           "xq_downloader"

struct XqDownloaderSession;

typedef void (*XqDownloader_Handler) (struct XqDownloaderSession *session);

struct XqDownloaderSession
{
    const char *file_url;
    const char *file_name;
    XqDownloader_Handler handler;
    s32 process;
};

void XqDownloader_DownloadFileBase(struct XqDownloaderSession *session);

void XqDownloader_DownloadFile(const char *file_url, const char *file_name, XqDownloader_Handler handler);

#endif /* APP_XQ_APP_XQDOWNLOADER_H_ */
