#include "../trans_dev/usr_info_trans_dev.h"
#include "../trans_dev/protocol_GB26875.h"

#define UITD_XUBOAFD_E90 1  // 智慧型故障电弧探测器

#if UITD_XUBOAFD_E90

#define UITD_XUBOAFD_E90_UNIT_NUM   1
#define UITD_XUBOAFD_E90_READ_NUM   8

#define UITD_E90_DIANYA_D          5
#define UITD_E90_PINLV_D            5
#define UITD_E90_DIANLIU_D          5
#define UITD_E90_LOUDIAN_D          5
#define UITD_E90_WENDUW_D           5
#define UITD_E90_WENDUN_D           5

struct XUBOAFD_E90
{
    u16 dianya;
    u16 pinlv;
    u16 dianliu;
    u16 loudian;
    u16 wenduwai;
    u16 wendunei;
};

struct XUBOAFD_E90 UITD_E90[UITD_SERIAL_NUM];

static u16 __E90_Min(u16 a, u16 b)
{
    return a<b?a:b;
}

static u16 __E90_Max(u16 a, u16 b)
{
    return a>b?a:b;
}

int XUBOAFD_E90_SetSendData(u8 COM_ID, u8 dev_addr, u8 *send_data)
{
    int checkcrc = 0;       /*crc校验*/

    char read_addr_L = 0;
    char read_addr_H = 0;

    send_data[0] = dev_addr;                /*从机地址              */
    send_data[1] = 03;                      /*功能码               */
    send_data[2] = read_addr_H;             /*起始地址高字节           */
    send_data[3] = read_addr_L;             /*起始地址低字节           */
    send_data[4] = 0;                       /*寄存器数量高字节          */
    send_data[5] = UITD_XUBOAFD_E90_READ_NUM;           /*寄存器数量低字节          */
    checkcrc = UITD_CRC_U16_MODBUS(send_data, 6);       /*计算CRC校验值          */
    send_data[6] = checkcrc&0xff;           /*CRC校验低字节          */
    send_data[7] = (checkcrc>>8)&0xff;      /*CRC校验高字节          */

    return 8;
}

u16 XUBOAFD_E90_GetSts(u16 val)
{
    return GB26875_SYS_STS_NORMAL | (val == 1 ? GB26875_SYS_STS_FIRE_ALARM : 0);
}

bool_t XUBOADF_IfUpd(u8 com_id, u16 addr, u16 value)
{
    bool_t ret = false;

    if(com_id >= UITD_SERIAL_NUM)
    {
        return false;
    }
    if(addr == 2)
    {
        ret = ((__E90_Max(UITD_E90[com_id].dianya,value) - __E90_Min(UITD_E90[com_id].dianya,value))>UITD_E90_DIANYA_D);
        if(ret) UITD_E90[com_id].dianya = value;
    } // 电压
    else if(addr == 4)
    {
        ret = ((__E90_Max(UITD_E90[com_id].pinlv,value) - __E90_Min(UITD_E90[com_id].pinlv,value))>UITD_E90_PINLV_D);
        if(ret) UITD_E90[com_id].pinlv = value;
    } // 频率
    else if(addr == 6)
    {
        ret = ((__E90_Max(UITD_E90[com_id].dianliu,value) - __E90_Min(UITD_E90[com_id].dianliu,value))>UITD_E90_DIANLIU_D);
        if(ret) UITD_E90[com_id].dianliu = value;
    } // 电流
    else if(addr == 8)
    {
        ret = ((__E90_Max(UITD_E90[com_id].loudian,value) - __E90_Min(UITD_E90[com_id].loudian,value))>UITD_E90_LOUDIAN_D);
        if(ret) UITD_E90[com_id].loudian = value;
    }
    else if(addr == 10)
    {
        ret = ((__E90_Max(UITD_E90[com_id].wenduwai,value) - __E90_Min(UITD_E90[com_id].wenduwai,value))>UITD_E90_WENDUW_D);
        if(ret) UITD_E90[com_id].wenduwai = value;
    }
    else if(addr == 12)
    {
        ret = ((__E90_Max(UITD_E90[com_id].wendunei,value) - __E90_Min(UITD_E90[com_id].wendunei,value))>UITD_E90_WENDUN_D);
        if(ret) UITD_E90[com_id].wendunei = value;
    }

    return ret;
}


u8 XUBOAFD_E90_GetAdType(u16 addr)
{
    u8 ret = GB26875_UNIT_AD_TYPE;
    if(addr == 2) { ret = GB26875_UNIT_AD_TYPE_VOLTAGE; } // 电压
    else if(addr == 4) { ret = 13; } // 频率
    else if(addr == 6) { ret = GB26875_UNIT_AD_TYPE_ELE; } // 电流
    else if(addr == 8) { ret = 14; } // 漏电
    else if(addr == 10) { ret = GB26875_UNIT_AD_TYPE_TEMP; }
    else if(addr == 12) { ret = 15; } // 外部温度

    return ret;
}

int XUBOAFD_E90_SetRecvData(u8 COM_ID, u8 dev_addr, u8 *read_buf, int len, struct FireCtrlInfo *info)
{
    int data_addr = 0;
    int read_addr = 0;
    int checkcrc = 0;
    char ret = -1;
    u16 data_b = 0;
    if(len < 5)
    {
        return 0;
    }

    if(UITD_U16CheckCRC(read_buf, len)) //  CRC校验
    {
        len = len > read_buf[2] ? read_buf[2] : len;
        for(data_addr = 3; data_addr < len; data_addr+=2)
        {
            if(data_addr == 3)
            {
                info->sys->status = XUBOAFD_E90_GetSts(((u16)read_buf[data_addr]<<8) + read_buf[data_addr+1]);
            }
            else if(read_addr < 6)
            {
                if(XUBOADF_IfUpd(COM_ID, data_addr - 3, ((u16)read_buf[data_addr]<<8) + read_buf[data_addr+1]))
                {
                    info->unit[read_addr]->sys_addr = COM_ID;
                    info->unit[read_addr]->addr = 0;
                    info->unit[read_addr]->type = 0;
                    info->unit[read_addr]->ad_val = ((u16)read_buf[data_addr]<<8) + read_buf[data_addr+1];
                    info->unit[read_addr]->ad_type = XUBOAFD_E90_GetAdType(data_addr - 3);
                    info->unit_size ++;
                    read_addr++;
                }
            }
        }
        return UITD_DATA_TYPE_SYS_STS | UITD_DATA_TYPE_UNIT_AD;
    }
    else
    {
        return UITD_DATA_TYPE_NULL;
    }
}

struct ProtocolVTable PROTOCOL_XUBOAFD_E90 = {\
    UITD_PROTOCOL_TYPE_2,\
    0,\
    3000,\
    210,\
    0,\
    XUBOAFD_E90_SetSendData,\
    0,\
    XUBOAFD_E90_SetRecvData,\
    1000\
};

#endif
