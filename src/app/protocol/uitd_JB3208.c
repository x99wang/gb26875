/*
 * uitd_8100.c
 *
 *  Created on: 2020年8月15日
 *      Author: cwj
 */

#include "../trans_dev/usr_info_trans_dev.h"
#include "../trans_dev/protocol_GB26875.h"

#define JB3208_TYPE_FIR       110  //火警
#define JB3208_TYPE_ERR       111  //故障
#define JB3208_TYPE_START     112  //启动
#define JB3208_TYPE_CB        113  //反馈
#define JB3208_TYPE_WTC       114  //监管
#define JB3208_TYPE_SHIELD    115  //屏蔽

u32 count[5]={0};

int JB3208SetSendData(u8 serial_addr, u8 COM_ID, u8 *send_data)
{
    u32 checkcrc = 0;             /*crc校验*/
    u8 type=0;

    if(count[serial_addr] == 0)
    {
        type=JB3208_TYPE_FIR;
    }
    else if(count[serial_addr] == 1)
    {
        type=JB3208_TYPE_ERR;
    }
    else if(count[serial_addr] == 2)
    {
        type=JB3208_TYPE_START;
    }
    else if(count[serial_addr] == 3)
    {
        type=JB3208_TYPE_CB;
    }
    else if(count[serial_addr] == 4)
    {
        type=JB3208_TYPE_WTC;
    }
    else if(count[serial_addr] == 5)
    {
        type=JB3208_TYPE_SHIELD;
    }
    send_data[0] = 254;                                       /*通讯起始位                */
    send_data[1] = COM_ID;                                    /*控制器号                    */
    send_data[2] = type;                           /*信息代码                    */
    send_data[3] = 1;                                         /*信息体长度                 */
    send_data[4] = 0;                                         /*信息体长度                 */
    send_data[5] = type;                           /*信息体                         */
    checkcrc = UITD_CRC_U16_MODBUS(&send_data[1], 5);          /*计算CRC校验值          */
    send_data[6] = checkcrc&0xff;                             /*CRC校验低字节          */
    send_data[7] = (checkcrc>>8)&0xff;                        /*CRC校验高字节          */
    send_data[8] = 255;                                       /*结束符                          */

    count[serial_addr]++;
    if(count[serial_addr] > 5)
    {
        count[serial_addr]=0;
    }
    return 9;
}

u16 JB3208GetStatus(u8 sts)
{
    u16 ret = GB26875_UNIT_STS_NORMAL;
    if(sts == JB3208_TYPE_FIR){ret |= GB26875_UNIT_STS_FIRE_ALARM;}    //火警
    else if(sts == JB3208_TYPE_ERR){ret |= GB26875_UNIT_STS_ERROR;} //故障           故障
    else if(sts == JB3208_TYPE_CB){ret |= GB26875_UNIT_STS_CB;} //动作          反馈
    else if(sts == JB3208_TYPE_START){ret |= GB26875_UNIT_STS_START;} //启动           启动
    else if(sts == JB3208_TYPE_SHIELD){ret |= GB26875_UNIT_STS_SHIELD;} //隔离          屏蔽
    else if(sts == JB3208_TYPE_WTC){ret |= GB26875_UNIT_STS_WTC;} //监管
    else ret = GB26875_UNIT_STS_NORMAL;
    return ret;
}

int JB3208SetRecvData(u8 serial_addr,u8 COM_ID, u8 *read_buf,int len)
{
    int i;
    u32 checkcrc = 0;             /*crc校验*/
    u8 controller_sends[9]={0};

    controller_sends[0] = 254;                                       /*通讯起始位                */
    controller_sends[1] = COM_ID;                                    /*控制器号                    */
    controller_sends[2] = 100;                           /*信息代码                    */
    controller_sends[3] = 1;                                         /*信息体长度                 */
    controller_sends[4] = 0;                                         /*信息体长度                 */
    controller_sends[5] = 100;                           /*信息体                         */
    checkcrc = UITD_CRC_U16_MODBUS(&controller_sends[1], 5);          /*计算CRC校验值          */
    controller_sends[6] = checkcrc&0xff;                             /*CRC校验低字节          */
    controller_sends[7] = (checkcrc>>8)&0xff;                        /*CRC校验高字节          */
    controller_sends[8] = 255;                                       /*结束符                          */


    for(i=0; i<9; i++)
    {
        if(controller_sends[i]!=read_buf[i]) return false;
    }
    return true;
}

int JB3208SetRespData(u8 serial_addr,u8 COM_ID, u8 *read_buf, int len, struct FireCtrlInfo *info)
{
    int data_addr = 0;
    int read_numb=0;
    char ret = -1;
    u8 huilu,dianhao,fenqudi,fenqugao;
    if(len < 10)
    {
        return 0;
    }
    if(info==NULL)
    {
        return 0;
    }
    if(serial_addr >= UITD_SERIAL_NUM)
    {
        serial_addr = UITD_SERIAL_NUM - 1;
    }

    if(read_buf[0] == 254 && read_buf[1] == COM_ID && read_buf[len-1] == 255)
    {
            ret = 0;
    }
    if(ret == 0)
    {
        read_numb = read_buf[5]+(((u16)read_buf[6])<<8);
        info->unit_size = 0;
        for(data_addr = 0; data_addr < read_numb; data_addr++)
        {
            huilu = 0;
            dianhao = 0;
            fenqudi = 0;
            fenqugao = 0;
            info->unit[data_addr]->sys_addr = serial_addr;
            huilu = read_buf[8+data_addr*32];
            dianhao = read_buf[9+data_addr*32];
            fenqudi = read_buf[10+data_addr*32];
            fenqugao = read_buf[11+data_addr*32];

            info->unit[data_addr]->addr = (((u32)huilu)<<8) + (((u32)dianhao)) +
                    (((u32)fenqudi)<<16) + (((u32)fenqugao)<<24);
            info->unit[data_addr]->type = 0;
            info->unit[data_addr]->status = JB3208GetStatus(read_buf[2]);
            info->unit_size ++;
        }

        return UITD_DATA_TYPE_UNIT_STS;
    }
    else
    {
        return UITD_DATA_TYPE_NULL;
    }
}

struct ProtocolVTable PROTOCOL_JB3208 = {
    UITD_PROTOCOL_TYPE_5,
    1,
    5000,
    1024,
    0,
    JB3208SetSendData,
    JB3208SetRecvData,
    JB3208SetRespData,
    1000
};

