
//----------------------------------------------------
// Copyright (c) 2014, SHENZHEN PENGRUI SOFT CO LTD. All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:

// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.

// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------
// Copyright (c) 2014 著作权由都江堰操作系统开源开发团队所有。著作权人保留一切权利。
//
// 这份授权条款，在使用者符合以下二条件的情形下，授予使用者使用及再散播本
// 软件包装原始码及二进位可执行形式的权利，无论此包装是否经改作皆然：
//
// 1. 对于本软件源代码的再散播，必须保留上述的版权宣告、此三条件表列，以
//    及下述的免责声明。
// 2. 对于本套件二进位可执行形式的再散播，必须连带以文件以及／或者其他附
//    于散播包装中的媒介方式，重制上述之版权宣告、此三条件表列，以及下述
//    的免责声明。

// 免责声明：本软件是本软件版权持有人以及贡献者以现状（"as is"）提供，
// 本软件包装不负任何明示或默示之担保责任，包括但不限于就适售性以及特定目
// 的的适用性为默示性担保。版权持有人及本软件之贡献者，无论任何条件、
// 无论成因或任何责任主义、无论此责任为因合约关系、无过失责任主义或因非违
// 约之侵权（包括过失或其他原因等）而起，对于任何因使用本软件包装所产生的
// 任何直接性、间接性、偶发性、特殊性、惩罚性或任何结果的损害（包括但不限
// 于替代商品或劳务之购用、使用损失、资料损失、利益损失、业务中断等等），
// 不负任何责任，即在该种使用已获事前告知可能会造成此类损害的情形下亦然。
//-----------------------------------------------------------------------------
/*
 * app_main.c
 *
 *  Created on: 2014-5-28
 *      Author: Administrator
 */

#include <stdlib.h>
#include <time.h>
#include <timer.h>

#include "stdint.h"
#include "stddef.h"
#include "djyos.h"
#include <stdio.h>
#include <dbug.h>
#include "project_config.h"     //本文件由IDE中配置界面生成，存放在APP的工程目录中。
                                //允许是个空文件，所有配置将按默认值配置。
#include "common/app_flash.h"
#include <Iboot_info.h>

#include "../trans_log/log.h"
#include "../mg_trans/host_send.h"
#include "../GuiWin/Info/WinSwitch.h"
#include "../trans_dev/usr_info_trans_dev.h"

#include "uitd_reporter.h"

#include "upgrade/downloader.h"
#include "upgrade/upgrade.h"

#define NET_TIME_NTP            0   // ntp网络时间

extern bool_t Set_StaticIp(char *param);

void trans_uitd_protocol_init();
void trans_socket_tcp_init();
void trans_socket_upload_init();
void trans_socket_down_init();
void trans_log_init();
void uitd_test_init();
void trans_heart_init();
void trans_reload_init();
void web_time_init();
void trans_reast_init();
void set_datetime(s32 year,s32 month,s32 day,s32 hour,s32 min,s32 sec);
void dev_protocol_init(u32 protocol0,u32 protocol1,u32 protocol2,u32 protocol3,u32 protocol4);
bool_t GetSocketState(void);// from host_send.c

int Get_DeviceSn(char *buf,int buflen)
{
//    const char *default_sn = "DJYOS0015";
    char default_sn[16];
    memset(default_sn, 0 ,sizeof(default_sn));
    if(Iboot_GetAPP_ProductInfo(APP_HEAD_SN, default_sn, sizeof(default_sn)))
    {
        char sn_tmp[30];
        memset(sn_tmp, 0, sizeof(sn_tmp));
        if (Sn_InfoLoad(sn_tmp , strlen(default_sn))) {
            if (sn_tmp[0]==0) {
                if (Sn_InfoSave(default_sn , strlen(default_sn))) {
                    printf("info: write sn info ok!\r\n");
                }
                else {
                    strcpy(sn_tmp, default_sn);
                }
            }
        }
        if (sn_tmp[0]!= 0 && (int)(strlen(sn_tmp)+1) <= buflen) {
            strcpy(buf, sn_tmp);
            //printf("info: sn: \"%s\"!\r\n", sn_tmp);
            return 0;
        }
    }
    return -1;
}


static bool_t first_net = false;

void after_online()
{
    Time_InfoInit();
#if APP_UPGRADE
    web_upgrade_firmware(0);
#endif
}

static s32 minute_cnt = 0;

void once_minute(struct Timer * timer)
{
    minute_cnt ++;

    Time_InfoInit();
    if(minute_cnt % 60 == 0)
    {
#if APP_UPGRADE
        web_upgrade_firmware(0);
#endif
    }
}

ptu32_t djy_main(void)
{
    struct Timer *min_timer;
    char static_ip_info[128] = {0};

    char *str_sn = Sn_Get();

    if(str_sn[0] == 0)
    {
        Sn_Init();
    }

    SetHomeInfo(Sn_Get(),PRODUCT_VERSION_LARGE,PRODUCT_VERSION_MEDIUM,PRODUCT_VERSION_SMALL);
    printf("\r\n当前App版本号为[%d.%d.%d]\r\n",PRODUCT_VERSION_LARGE, PRODUCT_VERSION_MEDIUM, PRODUCT_VERSION_SMALL);
    printf("\r\n当前App Sn号为[%s]\r\n", Sn_Get());

    // 随机数播种
    srand((unsigned)time(NULL));


    min_timer = Timer_Create("OneMinuteTimer", 60 * 1000 * 1000, once_minute);
    Timer_Ctrl(min_timer, EN_TIMER_SOFT_SETRELOAD, true);
    Timer_Ctrl(min_timer,  EN_TIMER_SOFT_START, NULL);

    // 初始化4G模块并拨号
    debug_printf("APP_MAIN", "==========初始化4G模块并拨号");
    Tcp_Initial();
    TcpStartNet();
    Comm_Port_Set(2);
    PPP_Start();
//    NetDev_SetDefault(NetDevGet("STM32F7_ETH"));

    //日志锁初始化
    LogLockInit();

    SerialInfInit();
    IpInfInit();

#if 0
    // 重新设置静态ip
    RouterRemoveByNetDev(CFG_SELECT_NETCARD);
    File_GetNameValueFs(CFG_STATIC_IP_INFO_FILE_NAME, static_ip_info, 128);
    if(strlen(static_ip_info) > 0)
        Set_StaticIp(static_ip_info);
#endif
    //设备状态变化
    trans_uitd_protocol_init();

    //socket数据上传和下载
    trans_socket_tcp_init();

    //socket数据包的处理
    trans_socket_upload_init();
    trans_socket_down_init();

    //socket重连
    trans_reast_init();

#if UITD_AUTO_HEARTBEAT
    //socket心跳
    trans_heart_init();
#endif

#if UITD_AUTO_RELOAD
    //重发
    trans_reload_init();
#endif

#if UITD_DEBUG_REPORT
    UITD_ReportStatusInit();
#endif

    //记录日志
    trans_log_init();

//    //模拟测试
//    uitd_test_init();

#if GUI_WIN_OPEN
    //界面初始化
    Main_GuiWin();

    //打开背光
    Lcd_BackLight_OnOff(1);

    DJY_EventDelay(3000*1000);

    Jump_GuiWin(WIN_Home_Page);
#endif

    s64 time_check=DJY_GetSysTime()/1000;

#if UITD_DEBUG_REPORT
    extern bool_t UITD_DEBUG_PRINT_FLAG;    // FIXME    调试用
#endif
    int i = 0;
    while(1)
    {

#if GUI_WIN_OPEN
        if(Get_SelectionWinType() == WIN_Home_Page)
        {
            Refresh_GuiWin(1);
        }
#endif

//        if(!MSC_DeviceReady(0))
//        {
//            printf("U盘识别到了\r\n");
//        }
//        else
//        {
//            printf("U盘未识别\r\n");
//        }

#if UITD_DEBUG_REPORT
        if(i >= 10)
        {
            i = 0;
            UITD_DEBUG_PRINT_FLAG = true;
        }
#endif

        if(GetPingFlag() && !first_net)
        {
            after_online();
            first_net = true;
        }

        DJY_EventDelay(1000*1000);
        i++;
    }

    return 0;
}

bool_t __app_ver()
{
    printf("当前APP版本号： %d.%d.%d\r\n", PRODUCT_VERSION_LARGE, PRODUCT_VERSION_MEDIUM, PRODUCT_VERSION_SMALL);
    return true;
}

#include "shell.h"
//ADD_TO_ROUTINE_SHELL(shengji, app_update, "新的升级接口");
ADD_TO_ROUTINE_SHELL(appver, __app_ver, "查看app版本号");
